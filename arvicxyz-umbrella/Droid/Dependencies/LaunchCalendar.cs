﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Umbrella.Interfaces;
using Umbrella.Droid.Dependencies;
using Xamarin.Forms;
using Android.Provider;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android;
using Umbrella.Utilities;
using Plugin.Permissions;
using System.Threading.Tasks;
using ZXing.Mobile;
using Android.Views.InputMethods;

[assembly: Dependency(typeof(LaunchCalendar))]
namespace Umbrella.Droid.Dependencies
{
    public class LaunchCalendar : ILaunchCalendar
    {
        
        void ILaunchCalendar.LaunchCalendar()
        {
            //CalendarContract.Events.ContentUri
            Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("content://com.android.calendar/time/"));
            Forms.Context.StartActivity(intent);
        }
        public async Task<string> LaunchScanner()
        {
            var optionsDefault = new MobileBarcodeScanningOptions();
            var optionsCustom = new MobileBarcodeScanningOptions();

            var scanner = new MobileBarcodeScanner()
            {
                TopText = "Scan the Printers QR Code",
                BottomText = "Not Scanning? Move closer or turn on the Light",
            };

            var scanResult = await scanner.Scan(optionsCustom);
            if (scanResult?.Text == null)
            {
                scanner.Cancel();
                return "Cancelled";
            }
            return scanResult.Text;
        }
        public void OpenDropbox()
        {
            Intent intent = Android.App.Application.Context.PackageManager.GetLaunchIntentForPackage("com.dropbox.android");
            if (intent != null)
            {
                intent.AddFlags(ActivityFlags.NewTask);
                Xamarin.Forms.Forms.Context.StartActivity(intent);
            }
            else
            {
                intent = new Intent(Intent.ActionView);
                intent.AddFlags(ActivityFlags.NewTask);
                intent.SetData(Android.Net.Uri.Parse("market://details?id=com.google.android.apps.docs"));
                Xamarin.Forms.Forms.Context.StartActivity(intent);
            }
        }
        public void OpenGmail()
        {
            Intent intent = Android.App.Application.Context.PackageManager.GetLaunchIntentForPackage("com.google.android.apps.docs");
            if (intent != null)
            {
                intent.AddFlags(ActivityFlags.NewTask);
                Xamarin.Forms.Forms.Context.StartActivity(intent);
            }
            else
            {
                intent = new Intent(Intent.ActionView);
                intent.AddFlags(ActivityFlags.NewTask);
                intent.SetData(Android.Net.Uri.Parse("market://details?id=com.google.android.apps.docs"));
                Xamarin.Forms.Forms.Context.StartActivity(intent);
            }
        }
        public string GetVersionNumber()
        {
            return Forms.Context.ApplicationContext.PackageManager.GetPackageInfo(Forms.Context
                 .ApplicationContext.PackageName, 0).VersionCode.ToString();

        }
        void ILaunchCalendar.LaunchGmail()
        {
            try
            {
                Intent newintent = Android.App.Application.Context.PackageManager.GetLaunchIntentForPackage("com.google.android.gm");
                //PackageManager manager = Forms.Context.PackageManager;
                //Intent i = Forms.Context.PackageManager.GetLaunchIntentForPackage("com.google.android.gmail");
                newintent.AddCategory(Intent.CategoryLauncher);
                Forms.Context.StartActivity(newintent);
            }
            catch(Exception ex)
            {

            }
            //Intent intent = new Intent(Intent.ActionMain);
            //intent.SetComponent(new ComponentName("com.package.address", "com.package.address.MainActivity"));
            //Forms.Context.StartActivity(intent);
           
        }
       
        void ILaunchCalendar.LaunchFileManager()
        {
            Intent newintent = new Intent();
            //PackageManager manager = Forms.Context.PackageManager;
            //Intent i = Forms.Context.PackageManager.GetLaunchIntentForPackage("com.google.android.gmail");
            newintent.SetAction(Intent.ActionView);
            newintent.SetType("*/*");
            Forms.Context.StartActivity(newintent);
            //Intent intent = new Intent(Intent.ActionMain);
            //intent.SetComponent(new ComponentName("com.package.address", "com.package.address.MainActivity"));
            //Forms.Context.StartActivity(intent);

        }

        public void LaunchImagePicker()
        {
            //var thisActivity = Forms.Context as Activity;
            //if (ContextCompat.CheckSelfPermission(thisActivity, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
            //{
            //    ActivityCompat.RequestPermissions(thisActivity, new string[] { Manifest.Permission.ReadExternalStorage }, 0);

            //}
            //else
            //{
            //    ActivityCompat.RequestPermissions(thisActivity, new string[] { Manifest.Permission.ReadExternalStorage }, 0);

            //}

            var imageIntent = new Intent(
              Intent.ActionPick);
            imageIntent.SetType("image/*");
            imageIntent.PutExtra(Intent.ExtraAllowMultiple, true);
            imageIntent.SetAction(Intent.ActionGetContent);
            ((Activity)Forms.Context).StartActivityForResult(
                Intent.CreateChooser(imageIntent, "Select photo"), 0);
        }

        void ILaunchCalendar.AskPermission()
        {
            var thisActivity = Forms.Context as Activity;
            if (ContextCompat.CheckSelfPermission(thisActivity, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
            {
                ActivityCompat.RequestPermissions(thisActivity, new string[] { Manifest.Permission.ReadExternalStorage }, 0);
            }
           
        }
        public void LaunchSingleImagePicker()
        {
            var imageIntent = new Intent(
             Intent.ActionPick);
            imageIntent.SetType("image/*");
            imageIntent.PutExtra(Intent.ExtraAllowMultiple, false);
            imageIntent.SetAction(Intent.ActionGetContent);
            ((Activity)Forms.Context).StartActivityForResult(
                Intent.CreateChooser(imageIntent, "Select photo"), 0);
        }
        public void LaunchChooser()
        {
            Global.SetIsFromLocalFiles(true);
            string[] mimeTypes = { "image/*", "application/pdf" };
            var imageIntent = new Intent(
             Intent.ActionGetContent);
            if (Build.VERSION.SdkInt >= Build.VERSION_CODES.Kitkat)
            {
                imageIntent.SetType(mimeTypes.Length == 1 ? mimeTypes[0] : "*/*");
                if (mimeTypes.Length > 0)
                {
                    imageIntent.PutExtra(Intent.ExtraMimeTypes, mimeTypes);
                }
            }
            else
            {
                string mimeTypesStr = "";

                foreach (var item in mimeTypes)
                {
                    mimeTypesStr += item + "|";
                }

                imageIntent.SetType(mimeTypesStr.Substring(0, mimeTypesStr.Length - 1));
            }

            //imageIntent.SetType("image/*");
            // imageIntent.SetType("image/*|application/pdf");
            imageIntent.PutExtra(Intent.ExtraAllowMultiple, false);
            imageIntent.SetAction(Intent.ActionGetContent);
            ((Activity)Forms.Context).StartActivityForResult(
                Intent.CreateChooser(imageIntent, "Select file"), 0);
        }
        bool ILaunchCalendar.CheckIfPermissionAllowed()
        {
            var thisActivity = Forms.Context as Activity;
            if (ContextCompat.CheckSelfPermission(thisActivity, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
            {
                return false;
            }
            return true;

        }

        void ILaunchCalendar.HideKeyboard()
        {
            var context = Forms.Context;
            var inputMethodManager = context.GetSystemService(Context.InputMethodService) as InputMethodManager;
            if (inputMethodManager != null && context is Activity)
            {
                var activity = context as Activity;
                var token = activity.CurrentFocus?.WindowToken;
                inputMethodManager.HideSoftInputFromWindow(token, HideSoftInputFlags.None);

                activity.Window.DecorView.ClearFocus();
            }
        }
    }
}