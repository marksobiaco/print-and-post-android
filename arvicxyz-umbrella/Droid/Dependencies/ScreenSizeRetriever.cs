﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Umbrella.Interfaces;
using Umbrella.Droid.Dependencies;
using Xamarin.Forms;
using Android.Content.Res;

[assembly: Dependency(typeof(ScreenSizeRetriever))]
namespace Umbrella.Droid.Dependencies
{
    public class ScreenSizeRetriever : IScreenSizeRetriever
    {
        public int GetHeight()
        {
            var firstResult = (double)Resources.System.DisplayMetrics.HeightPixels / (double)Resources.System.DisplayMetrics.Density;
            return (int)firstResult;
        }
    }
}