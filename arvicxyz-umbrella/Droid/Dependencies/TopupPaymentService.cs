﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Umbrella.Interfaces;
using Stripe;
using System.Threading.Tasks;
using Umbrella.Droid.Dependencies;
using System.Net.Http;
using Xamarin.Auth;
using Xamarin.Forms;
using Umbrella.Constants;
using Newtonsoft.Json;
using Umbrella.Models;
using System.Net;
using ModernHttpClient;
using RestSharp;

[assembly: Xamarin.Forms.Dependency(typeof(TopupPaymentService))]
namespace Umbrella.Droid.Dependencies
{
    public class TopupPaymentService : ITopupPaymentService
    {
        private readonly string _stripeCustomerApiUri = "https://api.stripe.com/v1/customers";
        private readonly string _stripeSearchQueryUri = "https://api.stripe.com/v1/search?query=";
        private OntraportContactIdRetriever _contactRetriever = new OntraportContactIdRetriever();
        //recommended to be stored in server
        private readonly string _stripeSecretTestkey = "sk_live_nWEZlQRdf2SXPIQxnRtsf7NE";
        // pk_live_vm8U9Hv7t403k2gvsTI9KkWm live api
        // sk_live_nWEZlQRdf2SXPIQxnRtsf7NE portal api
        // pk_test_YGEv1HMfF1ZXsT2ty8CVo7fe00WFTzibu6 key test new
        // sk test recent sk_test_qoFVdVRY9DVy5Tj04g34UyUq00UVRx0CT1
        // sk live sk_live_nWEZlQRdf2SXPIQxnRtsf7NE
        public async Task<Dictionary<bool,string>> ProcessCustomerCard(Models.Card card, int amount, string email, string currency)
        {
            Dictionary<bool, string> result;
            var customerId = await CheckCustomerHasStripeAccnt(email);
            if (!string.IsNullOrEmpty(customerId))
            {
                var stripeToken = new Stripe.Token();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                StripeConfiguration.ApiKey = "sk_live_nWEZlQRdf2SXPIQxnRtsf7NE";

                var tokenOptions = new TokenCreateOptions()
                {
                    Card = new CreditCardOptions()
                    {
                        Number = card.Number,
                        ExpYear = card.ExpiryYear,
                        ExpMonth = card.ExpiryMonth,
                        Cvc = card.CVC,
                        Name = card.Name
                    }
                };
                var tokenService = new TokenService();
                try
                {
                    stripeToken = tokenService.Create(tokenOptions);
                    var cardOptions = new CardCreateOptions()
                    {
                        Source = stripeToken.Id,
                    };

                    var cardService = new CardService();
                    var newCard = cardService.Create(customerId, cardOptions);
                    return result = await CreatePaymentIntent(customerId, amount, newCard.Id,currency);
                }
                catch (StripeException e)
                {
                    return result = new Dictionary<bool, string>()
                    {
                         { false, e.Message }
                    };
                }
              

            }
            else
            {
                var stripeToken = new Stripe.Token();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                StripeConfiguration.ApiKey = "sk_live_nWEZlQRdf2SXPIQxnRtsf7NE";

                var tokenOptions = new TokenCreateOptions()
                {
                    Card = new CreditCardOptions()
                    {
                        Number = card.Number,
                        ExpYear = card.ExpiryYear,
                        ExpMonth = card.ExpiryMonth,
                        Cvc = card.CVC,
                        Name = card.Name
                    }
                };
                var tokenService = new TokenService();
                try
                {
                    stripeToken = tokenService.Create(tokenOptions);
                }
                catch (StripeException e)
                {
                    return result = new Dictionary<bool, string>()
                    {
                         { false, e.Message }
                    };
                }
                try
                {
                    var customerOptions = new CustomerCreateOptions()
                    {
                        Email = email,
                        Source = stripeToken.Id,
                    };

                    var customerService = new CustomerService();
                    Customer customer = customerService.Create(customerOptions);
                    return result = await CreatePaymentIntent(customer.Id, amount, stripeToken.Card.Id, currency);

                }
                catch (StripeException e)
                {
                    return result = new Dictionary<bool, string>()
                        {
                            { false, e.Message }
                        };
                }

            }
            return result = new Dictionary<bool, string>();
        }
        public async Task<List<RetrievedCard>> GetAllCurrentCards(string email)
        {
            var list = new List<RetrievedCard>();
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer sk_live_nWEZlQRdf2SXPIQxnRtsf7NE");
                    var response = await client.GetStringAsync(_stripeCustomerApiUri);
                    var data = JsonConvert.DeserializeObject<RootStripeApiJsonData>(response);
                    string customerId = string.Empty;
                    System.Diagnostics.Debug.WriteLine("testing cards " + response);
                    foreach (var item in data.data)
                    {
                        System.Diagnostics.Debug.WriteLine("customer emails " + item.email);
                        if (item.email == email)
                        {
                            customerId = item.id;
                        }
                    }
                    //customerId = data.data.Count == 0 ? string.Empty : data.data[0].id;
                    StripeConfiguration.ApiKey = "sk_live_nWEZlQRdf2SXPIQxnRtsf7NE";

                    if (string.IsNullOrWhiteSpace(customerId))
                    {
                        return list = new List<RetrievedCard>();
                    }
                    else
                    {
                        var cardService = new CardService();

                        var cardlist = cardService.List(customerId,
                          new CardListOptions()
                          {
                              Limit = 5
                          }
                        );

                        foreach (var item in cardlist)
                        {

                            list.Add(new RetrievedCard()
                            {
                                Brand = item.Brand,
                                Name = item.Name,
                                Last4 = item.Last4,
                                Id = item.Id
                            });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("get all cards error " + ex.Message);
            }
            return list;
        }
        public async Task<Dictionary<bool,string>> ProcessExistingCard(int amount,string email, RetrievedCard card, string currency)
        {
            Dictionary<bool, string> result;
            var customerId = await CheckCustomerHasStripeAccnt(email);
            return result = await ChargeCustomerCard(customerId, amount, card.Id,currency);
        }
        public async Task<Dictionary<bool, string>> ProcessIntentPayments(int amount, string email, RetrievedCard card, string currency)
        {
            Dictionary<bool, string> result;
            var customerId = await CheckCustomerHasStripeAccnt(email);
            return result = await CreatePaymentIntent(customerId, amount, card.Id,currency);
        }
        private async Task<Dictionary<bool, string>> CreatePaymentIntent(string customer_Id, int amount, string tokenId, string currency)
        {
            Dictionary<bool, string> result;
            StripeConfiguration.ApiKey = "sk_live_nWEZlQRdf2SXPIQxnRtsf7NE";
            try
            {
                var service = new PaymentIntentService();
                var options = new PaymentIntentCreateOptions
                {
                    Amount = amount,
                    Currency = string.IsNullOrEmpty(currency) ? "gbp" : currency,
                    Description = "Top up charge",
                    CustomerId = customer_Id,
                    SourceId = tokenId,
                    PaymentMethodTypes = new List<string> { "card" },
                    Confirm = true,
                };

                var intent = service.Create(options);
                System.Diagnostics.Debug.WriteLine("client sec " + intent.PaymentMethodId + " " + intent.Status + " " + intent.ClientSecret);

                if (intent.Status == "requires_action")
                {
                    return result = new Dictionary<bool, string>()
                    {
                       { true, intent.ClientSecret }
                    };
                }
                else if (intent.Status == "succeeded")
                {
                    await SendPurchaseOntraport(amount, customer_Id,currency);
                    return result = new Dictionary<bool, string>()
                    {
                       { true, "Topup Success" }
                    };
                }
                else
                {
                    await SendTagFailedPaymentOntraport(currency,amount);
                    return result = new Dictionary<bool, string>()
                    {
                       { false, "Charge Failed" }
                    };
                }

            }
            catch (StripeException e)
            {
                await SendTagFailedPaymentOntraport(currency, amount);
                return result = new Dictionary<bool, string>()
                {
                    { false, e.Message }
                };
            }
        }
        private async Task<Dictionary<bool, string>> ChargeCustomerCard(string customer_Id, int amount, string tokenId, string currency)
        {
            Dictionary<bool, string> result;
            try
            {
                var service = new PaymentIntentService();
                var options = new PaymentIntentCreateOptions
                {
                    Amount = amount,
                    Currency = string.IsNullOrEmpty(currency) ? "gbp" : currency,
                    Description = "Top up charge",
                    CustomerId = customer_Id,
                    SourceId = tokenId,
                    PaymentMethodTypes = new List<string> { "card" },
                    Confirm = true,
                };
                var intent = service.Create(options);
                System.Diagnostics.Debug.WriteLine("client sec " + tokenId + " " + intent.Status + " " + intent.ClientSecret);
                if (intent != null)
                {
                    if (intent.Status == "requires_action" && intent.NextAction.Type == "use_stripe_sdk")
                    {
                        StripeConfiguration.ApiKey = "sk_live_nWEZlQRdf2SXPIQxnRtsf7NE";
                        var confirmOptions = new PaymentIntentConfirmOptions
                        {

                        };
                        var newIntent = service.Confirm(intent.Id, confirmOptions);
                        System.Diagnostics.Debug.WriteLine("client sec2 " + intent.NextAction.Type + " " + newIntent.NextAction.Type + " " + newIntent.Status + " " + newIntent.ClientSecret);
                        await SendTagFailedPaymentOntraport(currency, amount);
                        return result = new Dictionary<bool, string>()
                    {
                        { false, "Charge Failed" }
                    };
                    }
                    else
                    {
                        await SendPurchaseOntraport(amount, customer_Id,currency);
                        return result = new Dictionary<bool, string>()
                        {
                             { true, "Topup Success" }
                        };
                    }
                }
                else
                {
                    await SendTagFailedPaymentOntraport(currency, amount);
                    return result = new Dictionary<bool, string>()
                    {
                        { false, "Charge Failed" }
                    };
                }
            }
            catch (StripeException e)
            {
                await SendTagFailedPaymentOntraport(currency, amount);
                return result = new Dictionary<bool, string>()
                {
                    { false, e.Message }
                };
            }
        }

        private async Task SendPurchaseOntraport(int amount, string customerId, string currency)
        {
            var service = new CredentialRetriever();
            var email = service.GetCredential().User.Email;
            var contactId = await _contactRetriever.GetOntraportContactId(email);
            using (HttpClient client = new HttpClient())
            {
                var newAmount = amount / 100;
                string xmlData = "<purchases contact_id='" + contactId + "' product_id ='" + GetProductId(amount,currency) + "'><field name='Price'>" + newAmount + "</field></purchases>";

                var requestContent = string.Format("appid={0}&key={1}&return_id={2}&reqType={3}&data={4}",
                        Uri.EscapeDataString(OntraportApi.API_APP_ID),
                        Uri.EscapeDataString(OntraportApi.API_KEY),
                        Uri.EscapeDataString(OntraportApi.RETURN_ID.ToString()),
                        Uri.EscapeDataString(OntraportApi.REQUEST_SALE),
                        Uri.EscapeDataString(xmlData));

                var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                var _op = new TopupUploadOntraport()
                {
                    PartnerID = credential.UserID,
                    CurrencyOP = currency,
                    TopupAmount = newAmount,
                    //PhoneUsed = Device.RuntimePlatform == Device.Android ? 519 : 518,
                };
                var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var responseTopup = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                if (responseTopup.IsSuccessStatusCode)
                {
                    System.Diagnostics.Debug.WriteLine("contact id " + contactId);
                    var userprofile = new AddTagOntraport()
                    {
                        objectID = "0",
                        Ids = new string[]
                      {
                        contactId
                      },
                        AddLists = new string[]
                      {
                        "Print & Post Topup SUCCESS"
                      }


                    };
                    var responseContentRequest = JsonConvert.SerializeObject(userprofile, new JsonSerializerSettings());
                    client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                    client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                    System.Diagnostics.Debug.WriteLine("responsess " + content);
                    // var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                    var responseTag = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_TAGS_BYNAME, new StringContent(responseContentRequest, Encoding.UTF8, "application/x-www-form-urlencoded"));

                }

            }
        }
        private async Task SendTagFailedPaymentOntraport(string currency, int newamount)
        {
            var service = new CredentialRetriever();
            var email = service.GetCredential().User.Email;
            newamount = newamount / 100;
            var contactId = await _contactRetriever.GetOntraportContactId(email);
            using (HttpClient client = new HttpClient())
            {
                var userprofile = new AddTagOntraport()
                {
                    objectID = "0",
                    Ids = new string[]
                       {
                        contactId
                       },
                    AddLists = new string[]
                       {
                        "Print & Post Topup FAILURE"
                       }

                };
                var responseContentRequest = JsonConvert.SerializeObject(userprofile, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                System.Diagnostics.Debug.WriteLine("responsess " + responseContentRequest);
                // var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                var responseTag = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_TAGS_BYNAME, new StringContent(responseContentRequest, Encoding.UTF8, "application/x-www-form-urlencoded")); var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                var _op = new TopupUploadOntraport()
                {
                    PartnerID = credential.UserID,
                    CurrencyOP = currency,
                    TopupAmount = newamount,
                };
                var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var responseTopup = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
            }
        }
        private async Task<string> CheckCustomerHasStripeAccnt(string email)
        {
            var customerId = "";
            using(HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer sk_live_nWEZlQRdf2SXPIQxnRtsf7NE");
                var response = await client.GetStringAsync(_stripeCustomerApiUri);
                var data = JsonConvert.DeserializeObject<RootStripeApiJsonData>(response);
                foreach (var item in data.data)
                {
                    System.Diagnostics.Debug.WriteLine("customer emails " + item.email);
                    if (item.email == email)
                    {
                        customerId = item.id;
                    }
                }
                return customerId;
            }
        }
        private string GetProductId(int amount, string currency)
        {
            if (currency == "gbp")
            {
                switch (amount)
                {
                    case 1000:
                        return "36";
                    case 2500:
                        return "26";
                    case 5000:
                        return "27";
                    case 10000:
                        return "28";
                    case 15000:
                        return "37";
                    case 25000:
                        return "34";
                    default:
                        return "";
                }
            }
            else if (currency == "usd")
            {
                switch (amount)
                {
                    case 1000:
                        return "47";
                    case 2500:
                        return "48";
                    case 5000:
                        return "49";
                    case 10000:
                        return "50";
                    default:
                        return "";
                }
            }
            else if (currency == "eur")
            {
                switch (amount)
                {
                    case 1000:
                        return "51";
                    case 2500:
                        return "52";
                    case 5000:
                        return "53";
                    case 10000:
                        return "54";
                    default:
                        return "";
                }
            }
            else
            {
                switch (amount)
                {
                    case 1000:
                        return "36";
                    case 2500:
                        return "26";
                    case 5000:
                        return "27";
                    case 10000:
                        return "28";
                    case 15000:
                        return "37";
                    case 25000:
                        return "34";
                    default:
                        return "";
                }
            }

        }

    }
}