﻿using System;
using System.Linq;
using Umbrella.Droid.Dependencies;
using Umbrella.Interfaces;
using Umbrella.Models;
using Xamarin.Auth;
using Xamarin.Forms;

[assembly: Dependency(typeof(CredentialRetriever))]
namespace Umbrella.Droid.Dependencies
{
    public class CredentialRetriever : ICredentialRetriever
    {
        public string UserName
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("Umbrella").FirstOrDefault();
                return (account != null) ? account.Username : null;
            }
        }

        public string Password
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("Umbrella").FirstOrDefault();
                return (account != null) ? account.Properties["Password"] : null;
            }
        }
        public string UserID
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("Umbrella").FirstOrDefault();
                return (account != null) ? account.Properties["UserID"] : null;
            }
        }
        public string Email
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("Umbrella").FirstOrDefault();
                return (account != null) ? account.Properties["Email"] : null;
            }
        }
        public string MessagesCount
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("UnOpenNotification").FirstOrDefault();
                return (account != null) ? account.Properties["MessagesCount"] : null;
            }
        }
        public string StoreStatus
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("NewStoreStatus").FirstOrDefault();
                return (account != null) ? account.Properties["StoreStatus"] : null;
            }
        }
        public string InkType
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsRed").FirstOrDefault();
                return (account != null) ? account.Properties["InkType"] : null;
            }
        }
        public string CompanyToken
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("CompanyTokenPPT").FirstOrDefault();
                return (account != null) ? account.Properties["CompanyToken"] : null;
            }
        }
        public string AllowLeaflet
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsRed").FirstOrDefault();
                return (account != null) ? account.Properties["AllowLeaflet"] : null;
            }
        }
        public string PaperType
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsRed").FirstOrDefault();
                return (account != null) ? account.Properties["PaperType"] : null;
            }
        }
        public string LabelType
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsRed").FirstOrDefault();
                return (account != null) ? account.Properties["LabelType"] : null;
            }
        }
        public string PostageClass
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsRed").FirstOrDefault();
                return (account != null) ? account.Properties["PostageClass"] : null;
            }
        }
        public string DefaultCurrency
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultCurrencyPPT").FirstOrDefault();
                return (account != null) ? account.Properties["DefaultCurrency"] : null;
            }
        }
        public string DefaultAmount
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultCurrencyPPT").FirstOrDefault();
                return (account != null) ? account.Properties["DefaultAmount"] : null;
            }
        }
        public string DispatchSpeed
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsRed").FirstOrDefault();
                return (account != null) ? account.Properties["DispatchSpeed"] : null;
            }
        }
        public string PartnerId
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsRed").FirstOrDefault();
                return (account != null) ? account.Properties["PartnerId"] : null;
            }
        }

        public string GetCompanyToken()
        {
            string token = "";
            try
            {
                token = CompanyToken;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("token error " + ex.Message + " " + ex.InnerException);
            }
            return CompanyToken;
        }
        public DefaultTopupDetails GetCurrencyDefault()
        {
            var def = new DefaultTopupDetails();
            try
            {
                def = new DefaultTopupDetails()
                {
                    DefaultAmount = DefaultAmount,
                    DefaultCurrency = DefaultCurrency
                };
            }
            catch (Exception ex)
            {

            }
            return def;
        }
        public string GeneralNotif
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsNotificationPpt").FirstOrDefault();
                return (account != null) ? account.Properties["GeneralNotif"] : null;
            }
        }
        public string NewLeadNotif
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsNotificationPpt").FirstOrDefault();
                return (account != null) ? account.Properties["NewLeadNotif"] : null;
            }
        }
        public string LeadConvertedNotif
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsNotificationPpt").FirstOrDefault();
                return (account != null) ? account.Properties["LeadConvertedNotif"] : null;
            }
        }
      
        public string NewMessageNotif
        {
            get
            {
                var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsNotificationPpt").FirstOrDefault();
                return (account != null) ? account.Properties["NewMessageNotif"] : null;
            }
        }
        public Credential GetCredential()
        {
            if (Password != null)
            {
                var credential = new Credential();
                credential.User = new User()
                {
                    Username = UserName,
                    Email = Email
                };
                credential.Password = Password;
                credential.UserID = UserID;
                return credential;
            }
            else
            {
                return null;
            }
        }
        public DefaultNotification GetDefaultNotification()
        {
            DefaultNotification def = new DefaultNotification();
            try
            {
                System.Diagnostics.Debug.WriteLine("defufu " + GeneralNotif);
                def = new DefaultNotification()
                {
                    GeneralNotif = GeneralNotif,
                    NewLeadNotif = NewLeadNotif,
                    NewMessageNotif = NewMessageNotif,
                    LeadConvertedNotif = LeadConvertedNotif,
                };
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("ex defautss" + ex.InnerException);
            }
            return def;
        }
        public Default GetDefault()
        {
            var def = new Default()
            {
                InkType = InkType,
                PaperType = PaperType,
                LabelType = LabelType,
                PartnerId = PartnerId,
                DispatchSpeed = DispatchSpeed,
                AllowLeaflet = AllowLeaflet,
                PostageClass = PostageClass
            };
            return def;
        }
        public string GetMessageCount()
        {
            return MessagesCount;
        }
        public string GetStatus()
        {
            return string.IsNullOrEmpty(StoreStatus) ? "Open" : StoreStatus;
        }
    }
}
