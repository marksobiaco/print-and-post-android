using System;
using System.IO;
using Xamarin.Forms;
using System.Collections.Generic;
using Umbrella.Interfaces;
using Umbrella.Droid.Dependencies;
using Android.Graphics;
using static Android.Graphics.Bitmap;
using Android.Util;

[assembly: Dependency(typeof(FileHelper))]
namespace Umbrella.Droid.Dependencies
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return System.IO.Path.Combine(path, filename);
        }
        public void ForPrintOnly(string message)
        {
            Log.Info("umbrellapp", message);
        }
        public bool CheckTime()
        {
            var isInRange = false;
            var britishZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/London");
            var newDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
            var timespan1 = TimeSpan.Parse("17:00");
            var timespan2 = TimeSpan.Parse("19:30");
            if(newDate.DayOfWeek != DayOfWeek.Sunday)
            {
                if (newDate.TimeOfDay >= timespan1 && newDate.TimeOfDay <= timespan2)
                {
                    System.Diagnostics.Debug.WriteLine("in range " + newDate.TimeOfDay);
                    isInRange = true;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("not in range " + newDate.TimeOfDay);
                    isInRange = false;
                }
            }
            System.Diagnostics.Debug.WriteLine("evvver " + newDate);
            return isInRange;
        }
        public List<string> GetSpecialFolders()
        {
            List<string> folders = new List<string>();
            foreach (var folder in Enum.GetValues(typeof(Environment.SpecialFolder)))
            {
                if (!string.IsNullOrEmpty(System.Environment.GetFolderPath((Environment.SpecialFolder)folder)))
                {
                    folders.Add($"{folder}={System.Environment.GetFolderPath((Environment.SpecialFolder)folder)}");
                }
            }

            return folders;
        }
        public byte[] HighQualityUpload(byte[] imageData)
        {
            var arr = new byte[] { };
            try
            {
                BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
                options.InPurgeable = true; // inPurgeable is used to free up memory while required
                options.InScaled = false;
                Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);

                float newHeight = 0;
                float newWidth = 0;

                float aspect_x = 2;
                float aspect_y = 3;
                float width = 0;
                float height = 0;

                if (originalImage.Width > originalImage.Height)
                {
                    height = (aspect_x / aspect_y) * originalImage.Width;
                    width = originalImage.Width;
                }
                else if (originalImage.Height > originalImage.Width)
                {
                    height = originalImage.Height;
                    width = (aspect_x / aspect_y) * originalImage.Height;
                }
                else
                {
                    height = originalImage.Height;
                    width = (aspect_x / aspect_y) * originalImage.Height;
                }

                float xScale = (float)width / originalImage.Width;
                float yScale = (float)height / originalImage.Height;
                float scale = Math.Max(xScale, yScale);

                float scaledWidth = scale * originalImage.Width;
                float scaledHeight = scale * originalImage.Height;

                float left = (width - scaledWidth) / 2;
                float top = (height - scaledHeight) / 2;

                RectF targetRect = new RectF(left, top, left + scaledWidth, top
               + scaledHeight);

                Bitmap resizedImage = Bitmap.CreateBitmap((int)width, (int)height, originalImage.GetConfig());
                Canvas canvas = new Canvas(resizedImage);
                canvas.DrawBitmap(originalImage, null, targetRect, null);

                if (resizedImage != originalImage)
                {
                    originalImage.Recycle();
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 70, ms);

                    resizedImage.Recycle();

                    arr = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception " + ex.InnerException + " " + ex.Message);

            }
            return arr;
        }
        public byte[] HighQualityNormalUpload(byte[] imageData)
        {
            var arr = new byte[] { };
            try
            {
                BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
                options.InPurgeable = true; // inPurgeable is used to free up memory while required
                options.InScaled = false;
                Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);

                using (MemoryStream ms = new MemoryStream())
                {
                    originalImage.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);

                    originalImage.Recycle();

                    arr = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception " + ex.InnerException + " " + ex.Message);

            }
            return arr;
        }
        public byte[] LowQualityNormalUpload(byte[] imageData)
        {
            var arr = new byte[] { };
            try
            {
                BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
                options.InPurgeable = true; // inPurgeable is used to free up memory while required
                options.InScaled = false;
                Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);

                using (MemoryStream ms = new MemoryStream())
                {
                    originalImage.Compress(Bitmap.CompressFormat.Jpeg, 70, ms);

                    originalImage.Recycle();

                    arr = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception " + ex.InnerException + " " + ex.Message);

            }
            return arr;
        }
        public byte[] LowQualityUpload(byte[] imageData)
        {
            var arr = new byte[] { };

            try
            {
                BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
                options.InPurgeable = true; // inPurgeable is used to free up memory while required
                //options.InScaled = false;
                Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);
                float aspect_x = 2;
                float aspect_y = 3;
                float width = 0;
                float height = 0;

                System.Diagnostics.Debug.WriteLine("widt " + originalImage.Width + " " + originalImage.Height);
                if (originalImage.Width > originalImage.Height)
                {
                    height = (aspect_x / aspect_y) * originalImage.Width;
                    width = originalImage.Width;
                }
                else if (originalImage.Height > originalImage.Width)
                {
                    height = originalImage.Height;
                    width = (aspect_x / aspect_y) * originalImage.Height;
                }
                else
                {
                    height = originalImage.Height;
                    width = (aspect_x / aspect_y) * originalImage.Height;
                }
                /*
                float xScale = (float)width / originalImage.Width;
                float yScale = (float)height / originalImage.Height;
                float scale = Math.Max(xScale, yScale);

                float scaledWidth = scale * originalImage.Width;
                float scaledHeight = scale * originalImage.Height;

                float left = (width - scaledWidth) / 2;
                float top = (height - scaledHeight) / 2;

                RectF targetRect = new RectF(left, top, left + scaledWidth, top
               + scaledHeight);
                System.Diagnostics.Debug.WriteLine("rectf " + " " + left + " " + top + " " + left + " " + scaledWidth + " " + scaledHeight);
               */
                //var test = (newHeight / 2) - (newWidth / 2);
                //var tes2 = (originalImage.Height / 2) - (originalImage.Width / 2);

                //System.Diagnostics.Debug.WriteLine("widt " + width + " " + height + " " + newHeight + " " + newWidth + " " + test + " " + tes2 + " " + shiftX + " " + shiftY);

                //Matrix matrix = new Matrix();
                // RESIZE THE BIT MAP
                //matrix.PostScale(scalingX, scalingY);
                Bitmap resizedImage = Bitmap.CreateBitmap((int)width,(int)height,originalImage.GetConfig());
                Canvas canvas = new Canvas(resizedImage);
                canvas.DrawBitmap(originalImage, null, new RectF(0,0,width,height), null);

                if (resizedImage != originalImage)
                {
                    originalImage.Recycle();
                    System.Diagnostics.Debug.WriteLine("in recycle");
                }

                System.Diagnostics.Debug.WriteLine("in " + resizedImage.Width + " " + resizedImage.Height);
                using (MemoryStream ms = new MemoryStream())
                {
                    resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 70, ms);

                    resizedImage.Recycle();

                    arr = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception " + ex.InnerException + " " + ex.Message);
            }
            return arr;
        }
        public byte[] ResizeImage(byte[] imageData)
        {
            // Load the bitmap 
            BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
            options.InPurgeable = true; // inPurgeable is used to free up memory while required
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);

            float newHeight = 0;
            float newWidth = 0;
            float width = 1080;
            float height = 720;

            var originalHeight = originalImage.Height;
            var originalWidth = originalImage.Width;

            if (originalHeight > originalWidth)
            {
                newHeight = height;
                float ratio = originalHeight / height;
                newWidth = originalWidth / ratio;
            }
            else
            {
                newWidth = width;
                float ratio = originalWidth / width;
                newHeight = originalHeight / ratio;
            }
            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)newWidth, (int)newHeight, true);

            if (resizedImage != originalImage)
            {
                originalImage.Recycle();
                System.Diagnostics.Debug.WriteLine("in recycle");
            }
            System.Diagnostics.Debug.WriteLine("in " + originalHeight + " " + originalWidth + " " +  resizedImage.Width + " " + resizedImage.Height);

            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Compress(Bitmap.CompressFormat.Png, 100, ms);

                resizedImage.Recycle();

                return ms.ToArray();
            }

        }
    }
}