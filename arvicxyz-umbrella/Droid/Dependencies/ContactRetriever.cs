﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Umbrella.Droid.Dependencies;
using Umbrella.Interfaces;
using Umbrella.Models;
using Xamarin.Forms;

[assembly: Dependency(typeof(ContactRetriever))]
namespace Umbrella.Droid.Dependencies
{
    public class ContactRetriever : IContactRetriever
    {
        private string ReturnType(string type)
        {
            switch (type)
            {
                case "1":
                    return "Home";
                case "2":
                    return "Mobile";
                case "3":
                    return "Work";
                default:
                    return "";
            }
        }

        public IEnumerable<PhoneContact> GetAllContacts()
        {
            var phoneContacts = new List<PhoneContact>();
            var contact = new PhoneContact();
            var uri = ContactsContract.Contacts.ContentUri;
            var orgUri = ContactsContract.Data.ContentUri;
            var uriEmail = ContactsContract.CommonDataKinds.Email.ContentUri;
            var uriPhone = ContactsContract.CommonDataKinds.Phone.ContentUri;
            var postalAddUri = ContactsContract.CommonDataKinds.StructuredPostal.ContentUri;
            string[] cname;
            var cnt = 0;
            bool[] checkall;
            var number1 = "";
            var num = "";
            var lastname = "";
            var nameofuser = ContactsContract.CommonDataKinds.Phone.InterfaceConsts.DisplayName + " ASC";
            var loader = Android.App.Application.Context.ContentResolver.Query(uriPhone, null, null, null, nameofuser);
            cname = new string[loader.Count];
            checkall = new bool[loader.Count];
            string orgWhere = ContactsContract.Data.InterfaceConsts.Mimetype + " = ?";
            string[] paramss = new string[]
            {
                ContactsContract.CommonDataKinds.Organization.ContentItemType
            };
            string address = "";

            while (loader.MoveToNext())
            {
                var name = loader.GetString(loader.GetColumnIndex(ContactsContract.CommonDataKinds.Phone.InterfaceConsts.DisplayName));
                var number = loader.GetString(loader.GetColumnIndex(ContactsContract.CommonDataKinds.Phone.Number));
                var emailid = loader.GetString(loader.GetColumnIndex(ContactsContract.CommonDataKinds.Phone.InterfaceConsts.ContactId));
                System.Diagnostics.Debug.WriteLine("adddrrqasdasd  ");
                var contentLoader = Android.App.Application.Context.ContentResolver.Query(uri, null, null, null, null);

                if (contentLoader.MoveToFirst())
                {
                    do
                    {
                        System.Diagnostics.Debug.WriteLine("gggg  " + number1);
                        number1 = loader.GetString(loader.GetColumnIndex(ContactsContract.CommonDataKinds.Phone.Number));
                        int type = loader.GetInt(loader.GetColumnIndex(ContactsContract.CommonDataKinds.Phone.InterfaceConsts.Type));
                        if (number1.Contains(number))
                        {
                            switch (type)
                            {
                                case (int)PhoneDataKind.Home:

                                    num = "Home";
                                    break;

                                case (int)PhoneDataKind.Mobile:

                                    num = "Mobile";
                                    break;


                                case (int)PhoneDataKind.Work:

                                    num = "Work";
                                    break;

                                default:
                                    break;
                            }

                        }

                    } while (contentLoader.MoveToNext());
                }

                var orgLoader = Android.App.Application.Context.ContentResolver.Query(orgUri, null, orgWhere, paramss, null);
                var ifnull = orgLoader.MoveToFirst();
                var company = "";
                if (ifnull)
                {
                    company = orgLoader.GetString(orgLoader.GetColumnIndex(ContactsContract.CommonDataKinds.Organization.Company));
                }
                try
                {
                    var addLoader = Android.App.Application.Context.ContentResolver.Query(postalAddUri, null, ContactsContract.Data.InterfaceConsts.ContactId + " = " + emailid, null, null);
                    if (addLoader.MoveToFirst())
                    {
                        System.Diagnostics.Debug.WriteLine("ggghfdfsdf  ");
                        do
                        {

                            var add = addLoader.GetString(addLoader.GetColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.InterfaceConsts.Type));
                            if (add == "1")
                            {
                                var adrString = addLoader.GetString(addLoader.GetColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.FormattedAddress));
                                address = adrString;
                                System.Diagnostics.Debug.WriteLine("adddrr  " + address);
                            }

                        } while (addLoader.MoveToNext());
                    }
                }
                catch(Exception eff)
                { }
                try
                {
                    System.Diagnostics.Debug.WriteLine("hjdfg  ");
                    var emailLoader = Android.App.Application.Context.ContentResolver.Query(uriEmail, null,
                    ContactsContract.CommonDataKinds.Email.InterfaceConsts.ContactId + " = " + emailid, null, null);
                    var bools = emailLoader.MoveToFirst();
                    var email = "";
                    System.Diagnostics.Debug.WriteLine("email" + bools);
                    if (bools)
                    {
                          email = emailLoader.GetString(
                          emailLoader.GetColumnIndex(ContactsContract.CommonDataKinds.Email.InterfaceConsts.Data));

                    }
                    var trans = new PhoneContact();
                    if (lastname != name)
                    {
                      
                        lastname = name;
                        string[] words = name.Split(' ');
                        trans.FirstName = words[0];
                        if (words.Length > 1)
                            trans.LastName = words[1];
                        else
                            trans.LastName = "";
                        if (num == "Work")
                        {
                            trans.OfficeNumber = number1;
                        }
                        else if (num == "Home")
                        {
                            trans.HomeNumber = number1;
                        }
                        else
                        {
                            trans.MobileNumber = number1;
                        }
                        trans.EmailAdd = email;
                        trans.CompanyName = company;
                        trans.Address = address;
                        contact = trans;
                        phoneContacts.Add(contact);
                    }
                    else
                    {
                        foreach (var item in phoneContacts)
                        {
                            if (item.Name == lastname)
                            {
                                if (num == "Work")
                                {
                                    item.OfficeNumber = number1;
                                }
                                else if (num == "Home")
                                {
                                    item.HomeNumber = number1;
                                }
                                else
                                {
                                    item.MobileNumber = number1;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("error " + ex.InnerException + " " + ex.Message);
                }
                //cname[cnt] = name + "\n" + number1 + "\n" + email + num;
                //System.Diagnostics.Debug.WriteLine("cname " + cname[cnt]);
                //contact = trans;

                checkall[cnt] = true;
                cnt++;
                num = "";

            }
            loader.Close();
            return phoneContacts;
        }
    }
}