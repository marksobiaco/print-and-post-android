﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Widget;
using Plugin.Geolocator.Abstractions;
using Umbrella.Custom;
using Umbrella.Droid.Renderers;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace Umbrella.Droid.Renderers
{
    public class CustomMapRenderer : MapRenderer, GoogleMap.IInfoWindowAdapter
    {
        List<CustomPin> customPins;

        public CustomMapRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                NativeMap.InfoWindowClick -= OnInfoWindowClick;
            }

            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;
                customPins = formsMap.CustomPins;
            }
        }

        protected override void OnMapReady(GoogleMap map)
        {
            base.OnMapReady(map);

            NativeMap.InfoWindowClick += OnInfoWindowClick;
            NativeMap.SetInfoWindowAdapter(this);
        }
        int GetDayInt(string day)
        {
            var num = 0;
            switch (day)
            {
                case "Monday":
                    num = 1;
                    break;
                case "Tuesday":
                    num = 2;
                    break;
                case "Wednesday":
                    num = 3;
                    break;
                case "Thursday":
                    num = 4;
                    break;
                case "Friday":
                    num = 5;
                    break;
                case "Saturday":
                    num = 6;
                    break;
                case "Sunday":
                    num = 0;
                    break;
            }
            return num;
        }
        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            var items = Global.GetListPins();
            //marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.map_green_small));
            System.Diagnostics.Debug.WriteLine("itemsggggg " + pin.Address + " " + pin.Label);

            foreach (var cusPin in items)
            {
                System.Diagnostics.Debug.WriteLine("items " + cusPin.Address);
                if(cusPin.Address == pin.Address)
                {
                   
                    System.Diagnostics.Debug.WriteLine("compare  " + cusPin.Address + " " + pin.Address);
                    if (cusPin.Periods != null)
                    {
                        var currentDay = GetDayInt(DateTime.Now.DayOfWeek.ToString());
                        var currentTime = DateTime.Now.ToString("HH:mm");
                        System.Diagnostics.Debug.WriteLine("current day and time " + currentDay + " " + currentTime);

                        foreach (var item in cusPin.Periods)
                        {
                            var openingHours = item.open;
                            var closingHours = item.close;

                            if (currentDay == openingHours.day && currentDay == closingHours.day)
                            {
                                //TimeSpan timeOpen = new TimeSpan(0, 0, 0);
                                //TimeSpan timeClose = new TimeSpan(2, 0, 0);
                                TimeSpan timeOpen = TimeSpan.Parse(openingHours.time.Insert(2, ":"));
                                TimeSpan timeClose = TimeSpan.Parse(closingHours.time.Insert(2, ":"));
                                TimeSpan timeCurrent = TimeSpan.Parse(currentTime);
                                var timeMinusHour = timeClose.Subtract(new TimeSpan(1, 0, 0));
                                System.Diagnostics.Debug.WriteLine("time open " + timeOpen.ToString());
                                System.Diagnostics.Debug.WriteLine("time current " + timeCurrent.ToString());
                                System.Diagnostics.Debug.WriteLine("time close " + timeClose.ToString());
                                System.Diagnostics.Debug.WriteLine("timeminushour " + timeMinusHour.ToString());

                                System.Diagnostics.Debug.WriteLine("checking  " + TimeSpan.Compare(timeCurrent, timeOpen) + " " + TimeSpan.Compare(timeCurrent, timeClose)
                                + " " + TimeSpan.Compare(timeCurrent, timeOpen));

                                if ((TimeSpan.Compare(timeCurrent, timeMinusHour) == 1 && TimeSpan.Compare(timeCurrent, timeClose) == -1))
                                {
                                    System.Diagnostics.Debug.WriteLine("orange");
                                    marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.print_orange_p));
                                }
                                else
                                {
                                    if (TimeSpan.Compare(timeCurrent, timeOpen) == 1 && TimeSpan.Compare(timeCurrent, timeClose) == -1)
                                    {
                                        System.Diagnostics.Debug.WriteLine("green");
                                        marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.print_green_p));
                                    }
                                    else if (TimeSpan.Compare(timeCurrent, timeOpen) == 0)
                                    {
                                        System.Diagnostics.Debug.WriteLine("green");
                                        marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.print_green_p));
                                    }
                                    else if (TimeSpan.Compare(timeCurrent, timeOpen) == -1 || TimeSpan.Compare(timeCurrent, timeClose) == 1 || TimeSpan.Compare(timeCurrent, timeClose) == 0)
                                    {
                                        System.Diagnostics.Debug.WriteLine("red");
                                        marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.print_red_p));
                                    }
                                }

                            }
                        }

                    }
                    else
                    {
                        marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.pin_locator_small));
                    }
                }
            }

            return marker;
        }

        void OnInfoWindowClick(object sender, GoogleMap.InfoWindowClickEventArgs e)
        {
            var customPin = GetCustomPin(e.Marker);
            System.Diagnostics.Debug.WriteLine("clicking " + e.Marker);
           
            if (customPin == null)
            {
                throw new Exception("Custom pin not found");
            }
           
            if (!string.IsNullOrWhiteSpace(customPin.Name))
            {
                if (customPin.Url != "pin_locator")
                {
                    MessagingCenter.Send<string>(customPin.Url, "PinSelected");

                }
                /*
                var url = Android.Net.Uri.Parse(customPin.Url);
                var intent = new Intent(Intent.ActionView, url);
                intent.AddFlags(ActivityFlags.NewTask);
                Android.App.Application.Context.StartActivity(intent);
                */
            }
        }

        public Android.Views.View GetInfoContents(Marker marker)
        {
            var inflater = Android.App.Application.Context.GetSystemService(Context.LayoutInflaterService) as Android.Views.LayoutInflater;
            if (inflater != null)
            {
                Android.Views.View view;

                var customPin = GetCustomPin(marker);
                if (customPin == null)
                {
                    throw new Exception("Custom pin not found");
                }
                LatLng coordinate = new LatLng(customPin.Position.Longitude, customPin.Position.Latitude);

                //CameraPosition camera = new CameraPosition.Builder().Target(coordinate).Zoom(1).Bearing(0).Tilt(45).Build();
                //NativeMap.AnimateCamera(CameraUpdateFactory.NewCameraPosition(camera));

               
                //Projection projection = NativeMap.Projection;
                //Android.Graphics.Point markerScreenPosition = projection.ToScreenLocation(coordinate);
                // Android.Graphics.Point halfscreen = new Android.Graphics.Point(markerScreenPosition.X, markerScreenPosition.Y - this.Height / 2);
                //LatLng targetPos = projection.FromScreenLocation(halfscreen);
                //markerScreenPosition.Set(markerScreenPosition.X, markerScreenPosition.Y - 30);
               // NativeMap.MoveCamera(CameraUpdateFactory.NewLatLngZoom(coordinate,2));

                view = inflater.Inflate(Resource.Layout.MapInfoWindow, null);

                System.Diagnostics.Debug.WriteLine("xxx idd  " + this.Height);
                var infoTitle = view.FindViewById<TextView>(Resource.Id.InfoWindowTitle);
                var infoSubtitle = view.FindViewById<TextView>(Resource.Id.InfoWindowSubtitle);
                var infoImage = view.FindViewById<ImageView>(Resource.Id.InfoWindowImage);
                var infoButton = view.FindViewById<ImageView>(Resource.Id.InfoWindowButton);
                if (infoTitle != null)
                {
                    infoTitle.Text = marker.Title;
                    System.Diagnostics.Debug.WriteLine("vvc idd  " + marker.Title);
                }
                if (infoSubtitle != null)
                {
                    infoSubtitle.Text = marker.Snippet;
                }
                System.Diagnostics.Debug.WriteLine("ggg " + marker.Snippet);

                if (infoImage != null)
                {
                    if(customPin.Url != "pin_locator")
                    {
                        if (marker.Title.Contains("Costa Coffee"))
                        {
                            infoImage.SetImageResource(Resource.Drawable.costa_icon);
                        }
                        else if (marker.Title.Contains("Caffè Nero"))
                        {
                            infoImage.SetImageResource(Resource.Drawable.cafenero_icon);
                        }
                    }
                    else
                    {
                        infoImage.SetImageResource(Resource.Drawable.pin_locator_small);
                        infoButton.SetImageResource(0);
                        infoSubtitle.Text = "";
                    }
                }
                return view;
            }
            return null;
        }

        public Android.Views.View GetInfoWindow(Marker marker)
        {
            return null;
        }

        CustomPin GetCustomPin(Marker annotation)
        {
            customPins = Global.GetListPins();
            var position = new Xamarin.Forms.Maps.Position(annotation.Position.Latitude, annotation.Position.Longitude);
            foreach (var pin in customPins)
            {
                if (pin.Position == position)
                {
                    return pin;
                }
            }
            return null;
        }
    }
}
