﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Theartofdev.Edmodo.Cropper;
using Umbrella.Custom;
using Umbrella.Droid.Renderers;
using Umbrella.Utilities;
using Umbrella.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Umbrella.Custom.CropView), typeof(CropViewRenderer))]
namespace Umbrella.Droid.Renderers
{
    public class CropViewRenderer : PageRenderer
    {
        public CropViewRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);
            var page = Element as CropView;
            if (page != null)
            {
                var cropImageView = new CropImageView(Context);
                cropImageView.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
                Bitmap bitmp = BitmapFactory.DecodeByteArray(page.Image, 0, page.Image.Length);
                cropImageView.SetImageBitmap(bitmp);
                var crop = new CropImageOptions
                {
                    MinCropWindowHeight = 500,
                    MinCropWindowWidth = 500
                };
                cropImageView.SetAspectRatio(1, 1);
                cropImageView.SetFixedAspectRatio(true);
                cropImageView.AutoZoomEnabled = false;
                //cropImageView.SetCropShape(CropImageView.CropShape.Oval);
                //cropImageView.Enabled = false;
                //cropImageView.SetMaxCropResultSize(150, 50);
                //cropImageView.SetMinCropResultSize(150, 50);
                //cropImageView.SetAspectRatio(500, 500);
                System.Diagnostics.Debug.WriteLine("width " + bitmp.Width);
                System.Diagnostics.Debug.WriteLine("height " + bitmp.Height);
                //cropImageView.AutoZoomEnabled = true;
                var scrollView = new Xamarin.Forms.ScrollView
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Content = cropImageView.ToView() }
                ;
                var stackLayout = new StackLayout
                {
                    Children =
                    {
                        scrollView
                    }
                };

                var rotateButton = new Xamarin.Forms.Button
                {
                    Text = "Rotate",
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                };

                rotateButton.Clicked += (sender, ex) =>
                {
                    cropImageView.RotateImage(90);
                };
                stackLayout.Children.Add(rotateButton);

                var finishButton = new Xamarin.Forms.Button
                {
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    Text = "Finished"
                };
                finishButton.Clicked += (sender, ex) =>
                {
                    System.Diagnostics.Debug.WriteLine("cropviewrenderer ");
                    try
                    {
                        Bitmap cropped = cropImageView.CroppedImage;
                        using (MemoryStream memory = new MemoryStream())
                        {
                            System.Diagnostics.Debug.WriteLine("cropviewrender in ");
                            cropped.Compress(Bitmap.CompressFormat.Jpeg, 100, memory);
                            App.CroppedImage = memory.ToArray();
                        }
                        BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
                        options.InPurgeable = true; // inPurgeable is used to free up memory while required
                        Bitmap originalImage = BitmapFactory.DecodeByteArray(App.CroppedImage, 0, App.CroppedImage.Length, options);
                        System.Diagnostics.Debug.WriteLine("height and width " + originalImage.Width + " " + originalImage.Height);

                    }
                    catch(Exception exs)
                    {
                        System.Diagnostics.Debug.WriteLine("exxx " + exs.InnerException + " " + exs.Message);
                    }

                   // Global.SetIsDonePick(false);
                   // Global.ClearImagePaths();
                    page.Navigation.PopModalAsync();
                 
                };

                stackLayout.Children.Add(finishButton);
                page.Content = stackLayout;
            }
        }
    }
}