﻿using System;
using System.Reflection;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Umbrella.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Xamarin.Forms.TimePicker), typeof(CustomTimePickerRenderer))]
namespace Umbrella.Droid
{
    public class CustomTimePickerRenderer : TimePickerRenderer, Android.Views.View.IOnClickListener, TimePickerDialog.IOnTimeSetListener
    {
        public CustomTimePickerRenderer(Context context) : base(context)
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TimePicker> e)
        {
            base.OnElementChanged(e);

            Control.SetOnClickListener(this);
        }
        public void OnTimeSet(Android.Widget.TimePicker view, int hourOfDay, int minute)
        {
            var time = new TimeSpan(hourOfDay, minute, 0);
            this.Element.SetValue(Xamarin.Forms.TimePicker.TimeProperty, time);
        }
        public void OnClick(global::Android.Views.View v)
        {
            Xamarin.Forms.TimePicker view = Element;
            IElementController ElementController = Element as IElementController;

            ElementController.SetValueFromRenderer(VisualElement.IsFocusedProperty, true);

            BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.NonPublic
                                     | BindingFlags.GetField;


            //Get private member _dialog  
            FieldInfo dialogField = typeof(TimePickerRenderer).GetField("_dialog", bindFlags);

            //Get private member dialog if private  _dialog not found  
            if (dialogField == null)
            {
                dialogField = typeof(TimePickerRenderer).GetField("dialog", bindFlags);
            }

            if (dialogField != null)
            {
                var _dialog = (AlertDialog)dialogField.GetValue(this);

                _dialog = new TimePickerDialog(Context, this, view.Time.Hours, view.Time.Minutes, true);
                _dialog.Show();
            }
        }
    }
}
