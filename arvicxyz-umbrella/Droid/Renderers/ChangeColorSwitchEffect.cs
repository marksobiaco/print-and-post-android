﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Color = Xamarin.Forms.Color;
using Switch = Android.Widget.Switch;

using ChangeColorSwitchEffect = Umbrella.Droid.Renderers.ChangeColorSwitchEffect;
using XECCSwitchEffect = Umbrella.Custom.ChangeColorSwitchEffect;
using Xamarin.Forms;
using Android.Support.V7.Widget;
using static Java.Util.ResourceBundle;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;

[assembly: ExportEffect(typeof(ChangeColorSwitchEffect), "ChangeColorSwitchEffect")]
namespace Umbrella.Droid.Renderers
{
    [Android.Runtime.Preserve(AllMembers = true)]
    public class ChangeColorSwitchEffect : PlatformEffect
    {
        private Color _trueColor;
        private Color _falseColor;
        private Color _thumbColor;

        // slightly darker for the tracks, otherwise there's no virtual
        // 'depth' below the 'thumb' part of the slider.
        private Color _falseColorDarker;
        private Color _trueColorDarker;

        protected override void OnAttached()
        {
            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.JellyBean)
            {
                _thumbColor = (Color)Element.GetValue(XECCSwitchEffect.ThumbColorProperty);
                _trueColor = (Color)Element.GetValue(XECCSwitchEffect.TrueColorProperty);
                _falseColor = (Color)Element.GetValue(XECCSwitchEffect.FalseColorProperty);

                _falseColorDarker = _falseColor.AddLuminosity(-0.25);
                _trueColorDarker = _trueColor.AddLuminosity(-0.25);


                ((Xamarin.Forms.Switch)Element).Toggled += ChangeColorSwitchEffect_Toggled;

                ((SwitchCompat)Control).TrackDrawable.SetColorFilter(_trueColorDarker.ToAndroid(), PorterDuff.Mode.Multiply);

                ((SwitchCompat)Control).ThumbDrawable.SetColorFilter(_thumbColor.ToAndroid(), PorterDuff.Mode.Multiply);
                // to change the colour of the thumb-drawable to the 'true' (or 'false') colour, enable the line below (and disable the one above)
                // ((SwitchCompat)Control).ThumbDrawable.SetColorFilter(_trueColor.ToAndroid(), PorterDuff.Mode.Multiply);
            }
        }

        private void ChangeColorSwitchEffect_Toggled(object sender, ToggledEventArgs e)
        {
            var elem = (Xamarin.Forms.Switch)sender;
            if (elem.IsToggled)
            {
                ((SwitchCompat)Control).TrackDrawable.SetColorFilter(_trueColorDarker.ToAndroid(), PorterDuff.Mode.Multiply);
                // to change the colour of the thumb-drawable to the 'true' (or false) colour, enable the line below
                // ((SwitchCompat)Control).ThumbDrawable.SetColorFilter(_trueColor.ToAndroid(), PorterDuff.Mode.Multiply);
            }
            else
            {
                ((SwitchCompat)Control).TrackDrawable.SetColorFilter(_falseColorDarker.ToAndroid(), PorterDuff.Mode.Multiply);
                // to change the colour of the thumb-drawable to the 'true' (or false) colour, enable the line below
                // ((SwitchCompat)Control).ThumbDrawable.SetColorFilter(_thumbColor.ToAndroid(), PorterDuff.Mode.Multiply);
            }
        }

        private void OnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs checkedChangeEventArgs)
        {
            if (checkedChangeEventArgs.IsChecked)
            {
                ((SwitchCompat)Control).TrackDrawable.SetColorFilter(_trueColorDarker.ToAndroid(), PorterDuff.Mode.Multiply);
                // to change the colour of the thumb-drawable to the 'true' (or false) colour, enable the line below
                // ((SwitchCompat)Control).ThumbDrawable.SetColorFilter(_trueColor.ToAndroid(), PorterDuff.Mode.Multiply);
            }
            else
            {
                ((SwitchCompat)Control).TrackDrawable.SetColorFilter(_falseColorDarker.ToAndroid(), PorterDuff.Mode.Multiply);
                // to change the colour of the thumb-drawable to the 'true' (or false) colour, enable the line below
                // ((SwitchCompat)Control).ThumbDrawable.SetColorFilter(_thumbColor.ToAndroid(), PorterDuff.Mode.Multiply);
            }
        }

        protected override void OnDetached()
        {
            try
            {
                if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.JellyBean)
                {
                    ((Switch)Control).CheckedChange -= OnCheckedChange;
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}