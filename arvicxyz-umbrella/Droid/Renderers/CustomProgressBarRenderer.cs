﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Umbrella.Custom;
using Umbrella.Droid.Renderers;
using Umbrella.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(CustomProgressBarRenderer))]
namespace Umbrella.Droid.Renderers
{
    public class CustomProgressBarRenderer : ProgressBarRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
        {
            base.OnElementChanged(e);

            Control.ProgressDrawable.SetColorFilter(Xamarin.Forms.Color.FromRgb(217, 150, 148).ToAndroid(), Android.Graphics.PorterDuff.Mode.SrcIn);
            Control.ProgressTintList = Android.Content.Res.ColorStateList.ValueOf(Xamarin.Forms.Color.FromRgb(217, 150, 148).ToAndroid());
            //Control.ScaleY = 6; // This changes the height
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Control.ScaleY = 6;
            }
            else if (sizeChecker > 700)
            {
                Control.ScaleY = 10;
            }
            else
            {
                Control.ScaleY = 6;
            }
        }
    }
}