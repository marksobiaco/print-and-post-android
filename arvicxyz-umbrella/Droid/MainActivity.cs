﻿using System;
using System.Linq;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Umbrella.Models;
using Xamarin.Auth;
using Xamarin.Forms;
using Stripe;
using ModernHttpClient;
using Umbrella.Utilities;
using CarouselView.FormsPlugin.Android;
using Com.OneSignal;
using Com.OneSignal.Abstractions;
using System.Collections.Generic;
using Com.Onesignal.Shortcutbadger;
using Android.Content;
using Android.Provider;
using Android.Database;
using System.IO;
using Android.Support.V4.Content;
using Android.Support.V4.App;
using Android;
using Android.Support.Design.Widget;
using Umbrella.Interfaces;
using Plugin.Permissions;
using System.Net.Http;
using Java.IO;
using Plugin.Badge;
using Android.Graphics;
using Android.Util;
using Android.Widget;
using Android.Text;
using Account = Xamarin.Auth.Account;
using BranchXamarinSDK;
using ZXing.Mobile;

namespace Umbrella.Droid
{
    [Activity(
        Label = "@string/appName",
        Icon = "@drawable/Icon_1024",
        Theme = "@style/Theme.Main",
        ScreenOrientation = ScreenOrientation.Portrait,
        LaunchMode =LaunchMode.SingleTask,
        ConfigurationChanges = ConfigChanges.ScreenSize
        | ConfigChanges.Orientation)]


    [IntentFilter(new[] { "android.intent.action.VIEW" },
        Categories = new[] { "android.intent.category.DEFAULT", "android.intent.category.BROWSABLE" },
        DataScheme = "testxamarinformsapp",
        DataHost = "open")]

    [IntentFilter(new[] { "android.intent.action.VIEW" },
        Categories = new[] { "android.intent.category.DEFAULT", "android.intent.category.BROWSABLE" },
        DataScheme = "https",
        DataHost = "printpost.app.link")]

    //[IntentFilter(new string[] { Intent.ActionSend }, Categories = new string[] { Intent.CategoryDefault, Intent.CategoryBrowsable }, DataMimeType = "image/*")]
    [IntentFilter(new[] { Intent.ActionSend },
        Categories = new[] { Intent.CategoryDefault },
        DataMimeType = @"application/pdf", Label = "@string/appName")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private string fileUrl = "";
        public static string AUTHORITY = "uk.co.umbrellasupport.fileprovider.provider";
        const int RequestLocationId = 0;

        readonly string[] LocationPermissions =
        {
            Manifest.Permission.AccessCoarseLocation,
            Manifest.Permission.AccessFineLocation
        };
        protected override void OnStart()
        {
            base.OnStart();
            if ((int)Build.VERSION.SdkInt >= 23)
            {
                if (CheckSelfPermission(Manifest.Permission.AccessFineLocation) != Permission.Granted)
                {
                    RequestPermissions(LocationPermissions, RequestLocationId);
                }
                else
                {
                    // Permissions already granted - display a message.
                }
            }
        }
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(bundle);
            //key test new pk_test_YGEv1HMfF1ZXsT2ty8CVo7fe00WFTzibu6
            //pk_live_vm8U9Hv7t403k2gvsTI9KkWm
            //pk_test_u289X2Do4OavHR2STjbI2TsL portal api
            // sk test sk_test_qoFVdVRY9DVy5Tj04g34UyUq00UVRx0CT1
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);
           
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            global::Xamarin.Forms.Forms.Init(this, bundle);
            Xamarin.FormsMaps.Init(this, bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);
            MobileBarcodeScanner.Initialize(Application);
            BranchAndroid.Init(this, GetString(Resource.String.branch_key), new App());
            MessagingCenter.Subscribe<string>(this, "RemoveDefaultNotification", RemoveDefaultNotification, null);
            MessagingCenter.Subscribe<DefaultNotification>(this, "SaveDefaultNotification", SaveDefaultNotification, null);
            LoadApplication(new App());
          
            //CrossBadge.Current.SetBadge(5);
            //if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
            //{
            //    ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.ReadExternalStorage }, 0);
            //}
            CarouselViewRenderer.Init();
            // var id = GetString(Resource.String.gcm_defaultSenderId);
            OneSignal.Current.SetSubscription(true);

            OneSignal.Current.StartInit("7968f180-c07b-4dd8-bdca-6c4525a4ef6d").HandleNotificationReceived(HandleNotificationReceived)
                     .HandleNotificationOpened(HandleNotificationOpened)
                     .Settings(new Dictionary<string, bool>() {
                    { IOSSettings.kOSSettingsKeyAutoPrompt, false },
                    { IOSSettings.kOSSettingsKeyInAppLaunchURL, false } }).EndInit();
            OneSignal.Current.RegisterForPushNotifications();

            if (Intent.Action == Intent.ActionSend && Intent.Extras.ContainsKey(Intent.ExtraStream))
            {
                System.Diagnostics.Debug.WriteLine("Intent in");
                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
                {
                    ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.ReadExternalStorage }, 0);
                }
                fileUrl = GetActualPathFromFile((Android.Net.Uri)Intent.Extras.GetParcelable(Intent.ExtraStream));

                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == (int)Permission.Granted && credential != null)
                {
                    if (!string.IsNullOrEmpty(fileUrl))
                    {
                        try
                        {
                            var filepath = fileUrl.Split('/');
                            var pdfname = filepath[filepath.Length - 1];
                            System.Diagnostics.Debug.WriteLine("pdfname " + pdfname);
                            Global.SetPdfName(pdfname);
                            Global.SetCountPush(1);
                            System.Diagnostics.Debug.WriteLine("momo");
                            Global.SetNotifTopic("pdf");
                            byte[] b = System.IO.File.ReadAllBytes(fileUrl);
                            Global.SetByte(b);
                            MessagingCenter.Send<string>("pdf", "MoveRewardPage");
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine("ex pdf send " + ex.Message);
                        }
                    }

                }
                else if (credential == null)
                {
                    var filepath = fileUrl.Split('/');
                    var pdfname = filepath[filepath.Length - 1];
                    System.Diagnostics.Debug.WriteLine("pdfname " + pdfname);
                    Global.SetPdfName(pdfname);
                    Global.SetCountPush(1);
                    System.Diagnostics.Debug.WriteLine("momo");
                    Global.SetNotifTopic("pdf");
                    byte[] b = System.IO.File.ReadAllBytes(fileUrl);
                    Global.SetByte(b);
                }
            }

          
            OneSignal.Current.IdsAvailable((playerID, pushToken) =>
            {
                System.Diagnostics.Debug.WriteLine("player ID " + playerID);
                System.Diagnostics.Debug.WriteLine("push tokens " + pushToken);
                Global.SetPlayerID(playerID);
            });
            MessagingCenter.Subscribe<Credential>(this, "SaveUserData", SaveUserData, null);
            MessagingCenter.Subscribe<string>(this, "RemoveUserData", RemoveUserData, null);
            MessagingCenter.Subscribe<string>(this, "RemoveStoreStatus", RemoveStoreStatus, null);
            MessagingCenter.Subscribe<AppointmentStoreNew>(this, "NewAppiontment", SaveNewAppiontment, null);
            MessagingCenter.Subscribe<string>(this, "NewAppiontment", RemoveNewAppiontmentData, null);
            //MessagingCenter.Subscribe<Models.Card>(this, "SaveCard", SaveCard, null);
            MessagingCenter.Subscribe<string>(this, "SaveStoreStatus", SaveStoreStatus, null);
            MessagingCenter.Subscribe<string>(this, "RemoveDefaultSelection", RemoveDefaultSelection, null);
            MessagingCenter.Subscribe<Default>(this, "SaveDefaultSelection", SaveDefaultSelection, null);
            MessagingCenter.Subscribe<string>(this, "RemoveDefaultCurrency", RemoveDefaultCurrency, null);
            MessagingCenter.Subscribe<DefaultTopupDetails>(this, "SaveDefaultCurrency", SaveDefaultCurrency, null);
            MessagingCenter.Subscribe<Dictionary<DateTime, string>>(this, "SaveUnOpenedNotification", SaveUnOpenedNotification, null);
            MessagingCenter.Subscribe<string>(this, "RemoveUnOpenedNotification", RemoveUnOpenedNotification, null);
            MessagingCenter.Subscribe<string>(this, "RemoveCompanyToken", RemoveCompanyToken, null);
            MessagingCenter.Subscribe<string>(this, "SaveCompanyToken", SaveCompanyToken, null);


            System.Diagnostics.Debug.WriteLine("google initialized " + Xamarin.FormsMaps.IsInitialized);
        }
       
        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            this.Intent = intent;
            System.Diagnostics.Debug.WriteLine("Intent in" + intent.Action);
            //System.Diagnostics.Debug.WriteLine("Intent in" + intent.Extras.ContainsKey(Intent.ExtraStream));
            if (intent.Action == Intent.ActionSend && intent.Extras.ContainsKey(Intent.ExtraStream))
            {
                System.Diagnostics.Debug.WriteLine("Intent in");
                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
                {
                    ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.ReadExternalStorage }, 0);
                }
                fileUrl = GetActualPathFromFile((Android.Net.Uri)intent.Extras.GetParcelable(Intent.ExtraStream));

                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == (int)Permission.Granted && credential != null)
                {
                    if (!string.IsNullOrEmpty(fileUrl))
                    {
                        try
                        {

                            var filepath = fileUrl.Split('/');
                            var pdfname = filepath[filepath.Length - 1];
                            System.Diagnostics.Debug.WriteLine("pdfname " + pdfname);
                            Global.SetPdfName(pdfname);
                            Global.SetCountPush(1);
                            System.Diagnostics.Debug.WriteLine("momo");
                            Global.SetNotifTopic("pdf");
                            byte[] b = System.IO.File.ReadAllBytes(fileUrl);
                            Global.SetByte(b);
                            MessagingCenter.Send<string>("pdf", "MoveRewardPage");
                        }
                        catch(Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine("pdf in new intent " + ex.Message);
                        }
                    }
                   
                }
            }
        }
        protected override void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            System.Diagnostics.Debug.WriteLine("ON ACTIVITY");
            Global.ClearImagePaths();
            bool braked = false;
            string path = "";
            List<string> paths = new List<string>();
            try
            {
                if (resultCode == Android.App.Result.Ok)
                {
                    paths = new List<string>();
                    if (data != null)
                    {
                        //data.AddFlags(ActivityFlags.GrantReadUriPermission);
                        ClipData clipData = data.ClipData;
                        if (clipData != null)
                        {
                            System.Diagnostics.Debug.WriteLine("item clip " + clipData.ItemCount);

                            for (int i = 0; i < clipData.ItemCount; i++)
                            {
                                if (clipData.ItemCount > 5)
                                {
                                    braked = true;
                                    break;
                                }
                                ClipData.Item item = clipData.GetItemAt(i);
                                global::Android.Net.Uri uri = item.Uri;
                                //this.GrantUriPermission("filemanager.fileexplorer.manager", uri, ActivityFlags.GrantReadUriPermission);
                                //In case you need image's absolute path
                                if (Global.GetIsFromLocalFiles())
                                {
                                    System.Diagnostics.Debug.WriteLine("yows");
                                    Global.SetTestingURI(uri.Path + " " + uri);
                                    Global.SetTestingURI(uri.Path);
                                    var uriString = uri.Path;
                                    var wholePath = string.Empty;
                                    var pdfname = string.Empty;
                                    if (uri.PathSegments[i].Contains("storage"))
                                    {
                                        int ix = uri.PathSegments[i].IndexOf("storage", StringComparison.CurrentCulture);
                                        if (ix != -1)
                                        {
                                            System.Diagnostics.Debug.WriteLine("numx " + ix + " " + "storage".Length);
                                            wholePath = uri.PathSegments[i].Substring(ix);
                                            System.Diagnostics.Debug.WriteLine("code " + wholePath);
                                            int last = wholePath.LastIndexOf("/", StringComparison.CurrentCulture);
                                            pdfname = wholePath.Substring(last + 1);
                                            System.Diagnostics.Debug.WriteLine("lastname " + last + " " + pdfname);
                                        }
                                    }
                                    else
                                    {
                                        System.Diagnostics.Debug.WriteLine("1");
                                        path = GetActualPathFromFile(uri);
                                        wholePath = path;
                                        int last = wholePath.LastIndexOf("/", StringComparison.CurrentCulture);
                                        pdfname = wholePath.Substring(last + 1);
                                        //paths.Add(path);
                                    }

                                    System.Diagnostics.Debug.WriteLine("last path " + wholePath.Replace(':', ' '));
                                    if (wholePath.Contains("jpeg") || wholePath.Contains("JPG") || wholePath.Contains("jpg")
                                        || wholePath.Contains("JPEG") || wholePath.Contains("PNG") || wholePath.Contains("png"))
                                    {
                                        Global.SetUploadPageType("image");
                                        paths.Add(wholePath);
                                    }
                                    else
                                    {
                                        Global.SetPdfName(pdfname);
                                        byte[] b = System.IO.File.ReadAllBytes(wholePath);
                                        Global.SetByte(b);
                                        Global.SetUploadPageType("document");
                                    }
                                    Global.SetIsDonePick(true);
                                    System.Diagnostics.Debug.WriteLine("urist local " + uriString + " " + uri.EncodedPath + " " + wholePath);
                                }
                                else
                                {
                                    path = GetActualPathFromFile(uri);
                                    paths.Add(path);
                                }

                            }
                        }
                        else
                        {
                            global::Android.Net.Uri uri = data.Data;
                            if (Global.GetIsFromLocalFiles())
                            {
                                System.Diagnostics.Debug.WriteLine("yowsxcxc");
                                Global.SetTestingURI(uri.Path + " " + uri);
                                System.Diagnostics.Debug.WriteLine("uri local " + uri);
                                var uriString = uri.Path;
                                //var uriString = "/document/1879";
                                System.Diagnostics.Debug.WriteLine("xxxx " + uriString);
                                var wholePath = string.Empty;
                                var pdfname = string.Empty;
                                var list = uri.PathSegments;
                                if (uriString.Contains("storage"))
                                {
                                    for (int i = 0; i < uri.PathSegments.Count; i++)
                                    {
                                        if (uri.PathSegments[i].Contains("storage"))
                                        {
                                            int ix = uri.PathSegments[i].IndexOf("storage", StringComparison.CurrentCulture);
                                            if (ix != -1)
                                            {
                                                System.Diagnostics.Debug.WriteLine("numx " + ix + " " + "storage".Length);
                                                wholePath = uri.PathSegments[i].Substring(ix);
                                                System.Diagnostics.Debug.WriteLine("code " + wholePath);
                                                int last = wholePath.LastIndexOf("/", StringComparison.CurrentCulture);
                                                pdfname = wholePath.Substring(last + 1);
                                                System.Diagnostics.Debug.WriteLine("lastname " + last + " " + pdfname);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.Diagnostics.Debug.WriteLine("2");
                                    path = GetActualPathFromFile(uri);
                                    //path = GetActualPathFromFile(Android.Net.Uri.Parse("content://com.android.providers.downloads.documents/document/1879"));
                                    wholePath = path;
                                    int last = wholePath.LastIndexOf("/", StringComparison.CurrentCulture);
                                    pdfname = wholePath.Substring(last + 1);
                                    //paths.Add(path);
                                }

                                if (wholePath.Contains("jpeg") || wholePath.Contains("JPG") || wholePath.Contains("jpg")
                                    || wholePath.Contains("JPEG") || wholePath.Contains("PNG") || wholePath.Contains("png"))
                                {
                                    Global.SetUploadPageType("image");
                                    paths.Add(wholePath);
                                }
                                else
                                {
                                    Global.SetPdfName(pdfname);
                                    System.Diagnostics.Debug.WriteLine("last path " + wholePath);
                                    byte[] b = System.IO.File.ReadAllBytes(wholePath);
                                    Global.SetByte(b);
                                    Global.SetUploadPageType("document");
                                }
                                Global.SetIsDonePick(true);
                                System.Diagnostics.Debug.WriteLine("urist local " + uriString + " " + uri.EncodedPath + " " + wholePath);
                            }
                            else
                            {
                                path = GetActualPathFromFile(uri);
                                paths.Add(path);
                            }
                        }
                        System.Diagnostics.Debug.WriteLine("brake " + braked);
                        if (braked == true)
                        {
                            Toast.MakeText(Xamarin.Forms.Forms.Context, "Only the 5 images will be uploaded", ToastLength.Long).Show();
                        }
                    }
                }
                //Send the paths to forms
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.InnerException + " " + ex.Message);
                //Toast.MakeText (Xamarin.Forms.Forms.Context, "Unable to open, error:" + ex.ToString(), ToastLength.Long).Show ();
            }
            try
            {
                if (paths.Any())
                {
                    foreach (var item in paths)
                    {
                        System.Diagnostics.Debug.WriteLine("paths " + item);
                        byte[] b = System.IO.File.ReadAllBytes(item);
                       // var newbyte = ResizeImage(b);
                        Global.AddImagePaths(b);
                        Global.SetIsDonePick(true);
                    }
                }
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Path error" + ex.Message);
            }

        }
        public byte[] ResizeImage(byte[] imageData)
        {
            // Load the bitmap 
            BitmapFactory.Options options = new BitmapFactory.Options();// Create object of bitmapfactory's option method for further option use
            options.InPurgeable = true; // inPurgeable is used to free up memory while required
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);

            float aspect_x = 2;
            float aspect_y = 2;
            float width = (aspect_x / aspect_y) * originalImage.Height;
            float height = originalImage.Height;
            float newHeight = 0;
            float newWidth = 0;

            var originalHeight = originalImage.Height;
            var originalWidth = originalImage.Width;

            if (originalHeight > originalWidth)
            {
                newHeight = height;
                float ratio = originalHeight / height;
                newWidth = originalWidth / ratio;
            }
            else
            {
                newWidth = width;
                float ratio = originalWidth / width;
                newHeight = originalHeight / ratio;
            }

            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)newWidth, (int)newHeight, true);

            //originalImage.Recycle();
            if (resizedImage != originalImage)
            {
                originalImage.Recycle();
            }

            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Compress(Bitmap.CompressFormat.Png, 100, ms);

                resizedImage.Recycle();

                return ms.ToArray();
            }

        }
       
        private string GetPathToImage(global::Android.Net.Uri uri)
        {
            ICursor cursor = ContentResolver.Query(uri, null, null, null, null);
            cursor.MoveToFirst();
            string documentId = cursor.GetString(0);
            documentId = documentId.Split(':')[1];
            cursor.Close();

            cursor = ContentResolver.Query(
            Android.Provider.MediaStore.Images.Media.ExternalContentUri,
            null, MediaStore.Images.Media.InterfaceConsts.Id + " = ? ", new[] { documentId }, null);
            cursor.MoveToFirst();
            string path = cursor.GetString(cursor.GetColumnIndex(MediaStore.Images.Media.InterfaceConsts.Data));
            cursor.Close();

            return path;
        }
        private static string GetFileName(Context context, global::Android.Net.Uri uri)
        {
            string result = string.Empty;

            if (uri.Scheme.Equals("content"))
            {
                var cursor = context.ContentResolver.Query(uri, null, null, null, null);
                try
                {
                    if (cursor != null && cursor.MoveToFirst())
                        result = cursor.GetString(cursor.GetColumnIndex(OpenableColumns.DisplayName));
                }
                finally
                {
                    cursor.Close();
                }
            }

            if (string.IsNullOrEmpty(result))
            {
                result = uri.Path;
                int cut = result.LastIndexOf('/');

                if (cut != -1)
                    result = result.Substring(cut + 1);
            }

            return result;
        }
        private static Java.IO.File GetDocumentCacheDir(Context context)
        {
            Java.IO.File dir = new Java.IO.File(context.CacheDir, "documents");

            if (!dir.Exists())
                dir.Mkdirs();

            return dir;
        }
        public static Java.IO.File GenerateFileName(string name, Java.IO.File directory)
        {
            if (name == null) return null;

            Java.IO.File file = new Java.IO.File(directory, name);

            if (file.Exists())
            {
                string fileName = name;
                string extension = string.Empty;
                int dotIndex = name.LastIndexOf('.');
                if (dotIndex > 0)
                {
                    fileName = name.Substring(0, dotIndex);
                    extension = name.Substring(dotIndex);

                    int index = 0;

                    while (file.Exists())
                    {
                        index++;
                        name = $"{fileName}({index}){extension}";
                        file = new Java.IO.File(directory, name);
                    }
                }
            }

            try
            {
                if (!file.CreateNewFile())
                    return null;
            }
            catch (System.Exception ex)
            {
                return null;
            }

            return file;
        }
        private static void SaveFile(Context context, global::Android.Net.Uri uri, string destinationPath)
        {
            Stream i = null;
            BufferedOutputStream bos = null;
            try
            {
                i = context.ContentResolver.OpenInputStream(uri);
                bos = new BufferedOutputStream(System.IO.File.OpenWrite(destinationPath));
                byte[] buf = new byte[1024];
                int bufferSize = 1024 * 4;
                i.Read(buf, 0, bufferSize);
                do
                {
                    bos.Write(buf);
                } while (i.Read(buf, 0, bufferSize) != -1);
            }
            catch (System.IO.IOException e)
            {

            }
            finally
            {
                try
                {
                    if (i != null) i.Close();
                    if (bos != null) bos.Close();
                }
                catch (System.IO.IOException e)
                {

                }
            }
        }
        private string GetActualPathFromFile(Android.Net.Uri uri)
        {
            bool isKitKat = Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat;

            if (isKitKat && DocumentsContract.IsDocumentUri(this, uri))
            {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri))
                {
                    string docId = DocumentsContract.GetDocumentId(uri);

                    char[] chars = { ':' };
                    string[] split = docId.Split(chars);
                    string type = split[0];

                    if ("primary".Equals(type, StringComparison.OrdinalIgnoreCase))
                    {
                        return Android.OS.Environment.ExternalStorageDirectory + "/" + split[1];
                    }
                    else if ("home".Equals(type, StringComparison.OrdinalIgnoreCase))
                    {
                        return Android.OS.Environment.ExternalStorageDirectory + "/documents/" + split[1];
                    }
                }
                else if (isLocalStorageDocument(uri))
                {
                    // The path is the id
                    return DocumentsContract.GetDocumentId(uri);
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri))
                {
                    try
                    {
                        string id = DocumentsContract.GetDocumentId(uri);
                        string[] contentUriPrefixesToTry = new string[]{
                        "content://downloads/public_downloads",
                        "content://downloads/my_downloads",
                        "content://downloads/all_downloads"
                     };
                        if (!TextUtils.IsEmpty(id))
                        {
                            if (id.StartsWith("raw:", StringComparison.CurrentCulture))
                            {
                                System.Diagnostics.Debug.WriteLine("rawwww");
                                return id.Replace("raw:", "");
                            }
                            string testpath = "";

                            foreach (var item in contentUriPrefixesToTry)
                            {
                                Android.Net.Uri contentUri = ContentUris.WithAppendedId(Android.Net.Uri.Parse(item), long.Parse(id));
                                try
                                {
                                    testpath = getDataColumnForContent(this, contentUri, null, null);
                                    if (testpath != null)
                                    {
                                        //fortesting
                                        return testpath;
                                    }
                                }
                                catch (System.Exception e)
                                {

                                }
                            }
                            string fileName = GetFileName(this, uri);
                            Java.IO.File cacheDir = GetDocumentCacheDir(this);
                            Java.IO.File file = GenerateFileName(fileName, cacheDir);
                            string destinationPath = null;
                            if (file != null)
                            {
                                destinationPath = file.AbsolutePath;
                                SaveFile(this, uri, destinationPath);
                            }

                            if (string.IsNullOrEmpty(destinationPath))
                            {
                                return global::Android.OS.Environment.ExternalStorageDirectory.ToString() + "/Download/" + GetFileName(this, uri);
                            }

                            return destinationPath;

                        }
                    }
                    catch (System.Exception ex)
                    {
                        return global::Android.OS.Environment.ExternalStorageDirectory.ToString() + "/Download/" + GetFileName(this, uri);
                    }
                }
                // MediaProvider
                else if (isMediaDocument(uri))
                {
                    String docId = DocumentsContract.GetDocumentId(uri);

                    char[] chars = { ':' };
                    String[] split = docId.Split(chars);

                    String type = split[0];

                    Android.Net.Uri contentUri = null;
                    if ("image".Equals(type))
                    {
                        contentUri = MediaStore.Images.Media.ExternalContentUri;
                    }
                    else if ("video".Equals(type))
                    {
                        contentUri = MediaStore.Video.Media.ExternalContentUri;
                    }
                    else if ("audio".Equals(type))
                    {
                        contentUri = MediaStore.Audio.Media.ExternalContentUri;
                    }

                    String selection = "_id=?";
                    String[] selectionArgs = new String[]
                    {
                         split[1]
                    };
                    var data = string.Empty;
                    data = getDataColumnForContent(this, contentUri, selection, selectionArgs);
                    if (string.IsNullOrEmpty(data))
                    {
                        return getDataColumn(this, contentUri, selection, selectionArgs);
                    }
                    return data;
                }
                else
                {
                    return getDataColumn(this, uri, null, null);
                }
            }
            // MediaStore (and general)
            else if ("content".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                if (isGooglePhotosUri(uri))
                {
                    return uri.LastPathSegment;
                }
                // Return the remote address
                var contents = "";

                contents = getDataColumnForContent(this, uri, null, null);
                if (string.IsNullOrEmpty(contents))
                {
                    contents = getDataColumn(this, uri, null, null);
                }
                return contents;
            }
            // File
            else if ("file".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                return uri.Path;
            }

            return null;
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            if (requestCode == RequestLocationId)
            {
                if ((grantResults.Length == 1) && (grantResults[0] == (int)Permission.Granted))
                {

                }
                // Permissions granted - display a message.
                else
                {

                }
                // Permissions denied - display a message.
            }
            else
            {
                base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            }
            try
            {
                //System.Diagnostics.Debug.WriteLine("perms " + permissions.First());
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                System.Diagnostics.Debug.WriteLine("req" + permissions[0]);
                //System.Diagnostics.Debug.WriteLine("reqss" + grantResults[0]);
                var permissionMap = Manifest.Permission.AccessCoarseLocation;
                var permissionPdf = Manifest.Permission.ReadExternalStorage;
                if (permissionPdf == permissions[0])
                {
                    if (grantResults[0] == Permission.Granted && credential != null)
                    {
                        if (!string.IsNullOrEmpty(fileUrl))
                        {
                            var filepath = fileUrl.Split('/');
                            var pdfname = filepath[filepath.Length - 1];
                            System.Diagnostics.Debug.WriteLine("pdfname " + pdfname);
                            Global.SetPdfName(pdfname);
                            Global.SetCountPush(1);
                            Global.SetNotifTopic("pdf");
                            byte[] b = System.IO.File.ReadAllBytes(fileUrl);
                            var imgByte = new ByteArrayContent(System.IO.File.ReadAllBytes(fileUrl));
                            Global.SetByte(b);
                            MessagingCenter.Send<string>("pdf", "MoveRewardPage");
                        }
                        if (Global.GetUploadPageType() == "PDFTutorialPage")
                        {

                            var imageIntent = new Intent(
                             Intent.ActionPick);
                            imageIntent.SetType("image/*");
                            imageIntent.PutExtra(Intent.ExtraAllowMultiple, false);
                            imageIntent.SetAction(Intent.ActionGetContent);
                            ((Activity)Forms.Context).StartActivityForResult(
                                Intent.CreateChooser(imageIntent, "Select photo"), 0);
                        }
                        if (Global.GetUploadPageType() == "PDFDetailsPage")
                        {

                            var imageIntent = new Intent(
                             Intent.ActionPick);
                            imageIntent.SetType("image/*");
                            imageIntent.PutExtra(Intent.ExtraAllowMultiple, true);
                            imageIntent.SetAction(Intent.ActionGetContent);
                            ((Activity)Forms.Context).StartActivityForResult(
                                Intent.CreateChooser(imageIntent, "Select photo"), 0);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("map error " + ex.InnerException + " " + ex.Message);
            }
          

        }
        public static String getDataColumnForContent(Context context, Android.Net.Uri uri, String selection, String[] selectionArgs)
        {
            ICursor cursor = null;
            string column = "_data";
            string[] projection =
            {
                column
            };
            try
            {

                cursor = context.ContentResolver.Query(uri, projection, selection, selectionArgs, null);
                if (cursor != null && cursor.MoveToFirst())
                {
                    int index = cursor.GetColumnIndexOrThrow(column);
                    return cursor.GetString(index);
                }
            }
            catch (System.Exception ss)
            {
                System.Diagnostics.Debug.WriteLine("why though " + ss.Message + " " + ss.InnerException);
            }
            finally
            {
                if (cursor != null)
                    cursor.Close();
            }
            return null;
        }


        public static String getDataColumn(Context context, Android.Net.Uri uri, String selection, String[] selectionArgs)
        {
            Intent intent = null;
            InputStream inputs = null;
            FileOutputStream os = null;
            String fullPath = null;

            ICursor cursor = null;
            String column = "_data";
            String[] projection =
            {
                column
            };
            String name = null;
            cursor = context.ContentResolver.Query(uri, new String[] { MediaStore.MediaColumns.DisplayName }, null, null, null);
            cursor.MoveToFirst();
            int nameIndex = cursor.GetColumnIndex(MediaStore.MediaColumns.DisplayName);
            if (nameIndex >= 0)
            {
                name = cursor.GetString(nameIndex);
            }

            int n = name.LastIndexOf(".", StringComparison.CurrentCulture);
            String fileName, fileExt;

            var streams = context.ContentResolver.OpenInputStream(uri);
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filename = System.IO.Path.Combine(path, name);
            var outputstream = System.IO.File.Create(filename);

            byte[] buffer = new byte[4096];
            int count;
            while ((count = streams.Read(buffer, 0, buffer.Length)) > 0)
            {
                outputstream.Write(buffer, 0, count);
            }
            outputstream.Close();
            streams.Close();

            return filename;
        }
        private void SaveDefaultNotification(DefaultNotification dfault)
        {
            Account account = new Account();
            System.Diagnostics.Debug.WriteLine("yow defaults");
            account.Properties.Add("GeneralNotif", dfault.GeneralNotif);
            account.Properties.Add("LeadConvertedNotif", dfault.LeadConvertedNotif);
            account.Properties.Add("NewLeadNotif", dfault.NewLeadNotif);
            account.Properties.Add("NewMessageNotif", dfault.NewMessageNotif);

            //account.Properties.g
            AccountStore.Create(Forms.Context).Save(account, "DefaultSettingsNotificationPpt");
        }
        private void RemoveDefaultNotification(string message)
        {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsNotificationPpt").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create(Forms.Context).Delete(account, "DefaultSettingsNotificationPpt");
            }
            System.Diagnostics.Debug.WriteLine(message);
        }
        //Whether the Uri authority is ExternalStorageProvider.
        public static bool isExternalStorageDocument(Android.Net.Uri uri)
        {
            return "com.android.externalstorage.documents".Equals(uri.Authority);
        }
        public static bool isLocalStorageDocument(Android.Net.Uri uri)
        {
            return AUTHORITY.Equals(uri.Authority);
        }
        //Whether the Uri authority is DownloadsProvider.
        public static bool isDownloadsDocument(Android.Net.Uri uri)
        {
            return "com.android.providers.downloads.documents".Equals(uri.Authority);
        }

        //Whether the Uri authority is MediaProvider.
        public static bool isMediaDocument(Android.Net.Uri uri)
        {
            return "com.android.providers.media.documents".Equals(uri.Authority);
        }

        //Whether the Uri authority is Google Photos.
        public static bool isGooglePhotosUri(Android.Net.Uri uri)
        {
            return "com.google.android.apps.photos.content".Equals(uri.Authority);
        }
        private static void HandleNotificationReceived(OSNotification notification)
        {
            OSNotificationPayload payload = notification.payload;
            string message = payload.body;
            object test;
            System.Diagnostics.Debug.WriteLine("Notification Received");
            Dictionary<string, object> additionalData = payload.additionalData;
            if (!additionalData.TryGetValue("targetPage", out test))
            {
                System.Diagnostics.Debug.Write("object");
                Global.SetNotifTopic("");
            }
            else
            {
                var topic = string.IsNullOrEmpty(additionalData["targetPage"].ToString()) ? "" : additionalData["targetPage"].ToString();
                Global.SetListTopic(DateTime.Now, topic.ToLower());
                Global.SetNotifTopic(topic.ToLower());
                MessagingCenter.Send<string>(topic.ToLower(), "HandleNotification");
                MessagingCenter.Send<Dictionary<DateTime, string>>(Global.GetAllTopic(), "SaveUnOpenedNotification");
            }
            var countss = Global.GetAllTopic().Count;
            //CrossBadge.Current.SetBadge(countss);
            System.Diagnostics.Debug.Write("receive " + countss);

        }
        private static void HandleNotificationOpened(OSNotificationOpenedResult result)
        {
            OSNotificationPayload payload = result.notification.payload;
            Dictionary<string, object> additionalData = payload.additionalData;
            string message = payload.body;
            string actionID = result.action.actionID;
            object test;
            System.Diagnostics.Debug.WriteLine("Notification Open");
            Global.SetCountPush(1);
            if (!additionalData.TryGetValue("targetPage", out test))
            {
                System.Diagnostics.Debug.Write("object");
                Global.SetNotifTopic("");
               // if (!Global.GetIsSleep())
               // {
                    MessagingCenter.Send<string>("", "MoveRewardPage");
               // }
            }
            else
            {
                var topic = string.IsNullOrEmpty(additionalData["targetPage"].ToString()) ? "" : additionalData["targetPage"].ToString();
                Global.SetNotifTopic(topic.ToLower());
                Global.SetIsNotif(true);
               // if (!Global.GetIsSleep())
               // {
                    MessagingCenter.Send<string>(topic.ToLower(), "MoveRewardPage");
               // }
            }
            if (actionID != null)
            {
                // actionSelected equals the id on the button the user pressed.
                // actionSelected will equal "DEFAULT" when the notification itself was tapped when buttons were present.
                //  extraMessage = "Pressed ButtonId: " + actionID;
                //  System.Diagnostics.Debug.WriteLine("extra " + extraMessage);
            }
        }
        private void SaveUnOpenedNotification(Dictionary<DateTime, string> dfault)
        {
            Account account = new Account();
            var currentMessageCount = dfault.Where(e => e.Value == "messages").Count();
            account.Properties.Add("MessagesCount", currentMessageCount.ToString());
            //account.Properties.g
            AccountStore.Create(Forms.Context).Save(account, "UnOpenNotification");
        }
        private void SaveDefaultCurrency(DefaultTopupDetails dfault)
        {
            Account account = new Account();
            account.Properties.Add("DefaultCurrency", dfault.DefaultCurrency);
            account.Properties.Add("DefaultAmount", dfault.DefaultAmount);
            AccountStore.Create(Forms.Context).Save(account, "DefaultCurrencyPPT");
        }
        private void RemoveDefaultCurrency(string message)
        {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultCurrencyPPT").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create(Forms.Context).Delete(account, "DefaultCurrencyPPT");
            }
            System.Diagnostics.Debug.WriteLine(message);
        }
        private void RemoveUnOpenedNotification(string message)
        {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService("UnOpenNotification").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create(Forms.Context).Delete(account, "UnOpenNotification");
            }
            System.Diagnostics.Debug.WriteLine(message);
        }
        private void SaveCompanyToken(string companyToken)
        {
            Account account = new Account();
            account.Properties.Add("CompanyToken", companyToken);
            //account.Properties.g
            AccountStore.Create(Forms.Context).Save(account, "CompanyTokenPPT");
        }
        private void RemoveCompanyToken(string message)
        {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService("CompanyTokenPPT").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create(Forms.Context).Delete(account, "CompanyTokenPPT");
            }
            System.Diagnostics.Debug.WriteLine(message);
        }
        private void SaveDefaultSelection(Default dfault)
        {
            Account account = new Account();
            account.Properties.Add("InkType", dfault.InkType);
            account.Properties.Add("LabelType", dfault.LabelType);
            account.Properties.Add("PaperType", dfault.PaperType);
            account.Properties.Add("DispatchSpeed", dfault.DispatchSpeed);
            account.Properties.Add("PartnerId", dfault.PartnerId);
            account.Properties.Add("AllowLeaflet", dfault.AllowLeaflet);
            account.Properties.Add("PostageClass", dfault.PostageClass);
            //account.Properties.g
            AccountStore.Create(Forms.Context).Save(account, "DefaultSettingsRed");
        }
        private void RemoveDefaultSelection(string message)
        {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService("DefaultSettingsRed").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create(Forms.Context).Delete(account, "DefaultSettingsRed");
            }
            System.Diagnostics.Debug.WriteLine(message);
        }
        private void SaveNewAppiontment(AppointmentStoreNew app)
        {
            Account account = new Account();
            account.Properties.Add("AppoitmentTime", app.AppoitmentTime);
            account.Properties.Add("AppoitmentDate", app.AppoitmentDate);
            AccountStore.Create(Forms.Context).Save(account, "NewAppiontment");
        }
        private void RemoveNewAppiontmentData(string message)
        {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService("NewAppiontment").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create(Forms.Context).Delete(account, "NewAppiontment");
            }
            System.Diagnostics.Debug.WriteLine(message);
        }
        private void SaveUserData(Credential credential)
        {
            Account account = new Account
            {
                Username = credential.User.Username
            };
            account.Properties.Add("Password", credential.Password);
            account.Properties.Add("Email", credential.User.Email);
            account.Properties.Add("UserID", credential.User.Id.ToString());
            AccountStore.Create(Forms.Context).Save(account, App.AppName);
        }
        private void SaveStoreStatus(string storeStatus)
        {
            Account account = new Account();
            account.Properties.Add("StoreStatus", storeStatus);
            AccountStore.Create(Forms.Context).Save(account, "NewStoreStatus");
        }
        private void RemoveStoreStatus(string message)
        {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService("NewStoreStatus").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create(Forms.Context).Delete(account, "NewStoreStatus");
            }
        }
        private void RemoveUserData(string message)
        {
            var account = AccountStore.Create(Forms.Context).FindAccountsForService(App.AppName).FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create(Forms.Context).Delete(account, App.AppName);
            }
            System.Diagnostics.Debug.WriteLine(message);
        }
      

    }
}
