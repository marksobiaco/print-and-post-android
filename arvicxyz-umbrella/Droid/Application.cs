﻿using System;
using Android.App;
using Android.Runtime;
using BranchXamarinSDK;

namespace Umbrella.Droid
{

    [Application(AllowBackup = true, Label = "@string/appName",
        Icon = "@drawable/Icon_1024")]
    [MetaData("io.branch.sdk.auto_link_disable", Value = "false")]
    [MetaData("io.branch.sdk.TestMode", Value = "false")]
    [MetaData("io.branch.sdk.BranchKey", Value = "@string/branch_key")]
    public class TestAndroidApp : Application
    {
        public TestAndroidApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            BranchAndroid.GetAutoInstance(this.ApplicationContext);
        }
    }
}
