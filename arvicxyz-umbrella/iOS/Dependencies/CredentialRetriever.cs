﻿using System.Linq;
using Umbrella.Interfaces;
using Umbrella.iOS.Dependencies;
using Umbrella.Models;
using Xamarin.Auth;
using Xamarin.Forms;

[assembly: Dependency(typeof(CredentialRetriever))]
namespace Umbrella.iOS.Dependencies
{
    public class CredentialRetriever : ICredentialRetriever
    {
        public string UserName
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();
                return (account != null) ? account.Username : null;
            }
        }

        public string Password
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();
                return (account != null) ? account.Properties["Password"] : null;
            }
        }
        public string UserID
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();
                return (account != null) ? account.Properties["UserID"] : null;
            }
        }
        public string Email
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();
                return (account != null) ? account.Properties["Email"] : null;
            }
        }
        public string MessagesCount
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService("UnOpenNotification").FirstOrDefault();
                return (account != null) ? account.Properties["MessagesCount"] : null;
            }
        }
        public string StoreStatus
        {
            get
            {
                var account = AccountStore.Create().FindAccountsForService("NewStoreStatus").FirstOrDefault();
                return (account != null) ? account.Properties["StoreStatus"] : null;
            }
        }
        public string GetStatus()
        {
            return string.IsNullOrEmpty(StoreStatus) ? "Open" : StoreStatus;
        }
        public Credential GetCredential()
        {
            if (Password != null)
            {
                var credential = new Credential();
                credential.User = new User()
                {
                    Username = UserName,
                    Email = Email
                };
                credential.Password = Password;
                credential.UserID = UserID;
                return credential;
            }
            else
            {
                return null;
            }
        }

        public Default GetDefault()
        {
            throw new System.NotImplementedException();
        }

        public string GetMessageCount()
        {
            throw new System.NotImplementedException();
        }
    }
}
