﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Umbrella.Services;

namespace Umbrella.iOS.Services
{
    public class iOSCustomerBackgroundService
    {
        nint _taskId;
        CancellationTokenSource _cts;

        public void Start()
        {
            _cts = new CancellationTokenSource();

            _taskId = UIApplication.SharedApplication.BeginBackgroundTask("LongRunningTask", OnExpiration);

            Task.Run(() => {
                var counter = new TaskLoadCustomerService();
                counter.MethodRunPeriodically("customers").Wait();
            });

            UIApplication.SharedApplication.EndBackgroundTask(_taskId);
        }

        public void Stop()
        {
            _cts.Cancel();
        }

        void OnExpiration()
        {
            _cts.Cancel();
        }
    }
}
