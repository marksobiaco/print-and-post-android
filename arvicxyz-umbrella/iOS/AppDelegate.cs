﻿using System;
using System.Linq;
using Foundation;
using Plugin.FirebasePushNotification;
using Syncfusion.SfBusyIndicator.XForms.iOS;
using Syncfusion.SfCalendar.XForms.iOS;
using UIKit;
using Umbrella.Models;
using Xamarin.Auth;
using Xamarin.Forms;
using Stripe;
using ModernHttpClient;
using UserNotifications;

using Umbrella.iOS.Services;

namespace Umbrella.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        iOSLongRunningTask longRunningTask;
        iOSCustomerBackgroundService customerBackgroundService;
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            UIColor colorPrimary = UIColor.Clear.FromHex(0xD7090A);

            UINavigationBar.Appearance.TintColor = colorPrimary;
            UITabBar.Appearance.TintColor = colorPrimary;

            UINavigationBar.Appearance.BarTintColor = UIColor.White;
            UITabBar.Appearance.BarTintColor = UIColor.White;
            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
            {
                TextColor = colorPrimary
            });
            

            new SfBusyIndicatorRenderer();
            new SfCalendarRenderer();

            StripeConfiguration.HttpMessageHandler = new NativeMessageHandler();
            Stripe.StripeClient.DefaultPublishableKey = "pk_test_u289X2Do4OavHR2STjbI2TsL";

            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            WireUpLongRunningTask();

            FirebasePushNotificationManager.Initialize(options, true);
            MessagingCenter.Subscribe<Credential>(this, "SaveUserData", SaveUserData, null);
            MessagingCenter.Subscribe<string>(this, "RemoveUserData", RemoveUserData, null);
            MessagingCenter.Subscribe<string>(this, "RemoveStoreStatus", RemoveStoreStatus, null);
            MessagingCenter.Subscribe<string>(this, "SaveStoreStatus", SaveStoreStatus, null);
            MessagingCenter.Subscribe<AppointmentStoreNew>(this, "NewAppiontment", SaveNewAppiontment, null);
            MessagingCenter.Subscribe<string>(this, "NewAppiontment", RemoveNewAppiontmentData, null);
            MessagingCenter.Subscribe<Models.Card>(this, "SaveCard", SaveCard, null);
 

            return base.FinishedLaunching(app, options);
        }
        void WireUpLongRunningTask()
        {
            MessagingCenter.Subscribe<StartCustomerMessage>(this, "StartCustomerMessage",  message =>
            {
                customerBackgroundService = new iOSCustomerBackgroundService();
                customerBackgroundService.Start();
            });
        }
        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
#if DEBUG
            FirebasePushNotificationManager.DidRegisterRemoteNotifications(deviceToken, FirebaseTokenType.Sandbox);
#endif
#if RELEASE
                    FirebasePushNotificationManager.DidRegisterRemoteNotifications(deviceToken,FirebaseTokenType.Production);
#endif

        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            FirebasePushNotificationManager.RemoteNotificationRegistrationFailed(error);

        }
        // To receive notifications in foregroung on iOS 9 and below.
        // To receive notifications in background in any iOS version
        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired 'till the user taps on the notification launching the application.

            // If you disable method swizzling, you'll need to call this method. 
            // This lets FCM track message delivery and analytics, which is performed
            // automatically with method swizzling enabled.
           

            FirebasePushNotificationManager.DidReceiveMessage(userInfo);
            // Do your magic to handle the notification data

            System.Console.WriteLine(userInfo);
        }

        public override void OnActivated(UIApplication uiApplication)
        {
            FirebasePushNotificationManager.Connect();

        }   
        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
            FirebasePushNotificationManager.Disconnect();
        }
        private void SaveNewAppiontment(AppointmentStoreNew app)
        {
            Account account = new Account();            
            account.Properties.Add("AppoitmentTime", app.AppoitmentTime);
            account.Properties.Add("AppoitmentDate", app.AppoitmentDate);
            AccountStore.Create().Save(account, "NewAppiontment");
        }
        private void SaveStoreStatus(string storeStatus)
        {
            Account account = new Account();
            account.Properties.Add("StoreStatus", storeStatus);
            AccountStore.Create().Save(account, "NewStoreStatus");
        }
        private void RemoveStoreStatus(string message)
        {
            var account = AccountStore.Create().FindAccountsForService("NewStoreStatus").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create().Delete(account, "NewStoreStatus");
            }
        }
        private void SaveUserData(Credential credential)
        {
            Account account = new Account
            {
                Username = credential.User.Username
            };
            account.Properties.Add("Password", credential.Password);
            account.Properties.Add("Email", credential.User.Email);
            account.Properties.Add("UserID", credential.UserID);
            
            System.Diagnostics.Debug.WriteLine(" SaveUserData credential " +credential. UserID);
            //account.Properties.g
            AccountStore.Create().Save(account, App.AppName);
        }

        private void RemoveUserData(string message)
        {
            var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create().Delete(account, App.AppName);
            }
            System.Diagnostics.Debug.WriteLine(message);
        }
        private void RemoveNewAppiontmentData(string message)
        {
            var account = AccountStore.Create().FindAccountsForService("NewAppiontment").FirstOrDefault();
            if (account != null)
            {
                AccountStore.Create().Delete(account, App.AppName);
            }
            System.Diagnostics.Debug.WriteLine(message);
        }

        private async void SaveCard(Models.Card card)
        {
            var stripeCard = new Stripe.Card
            {
                Name = card.Name,
                Number = card.Number,
                ExpiryMonth = card.ExpiryMonth,
                ExpiryYear = card.ExpiryYear,
                CVC = card.CVC
            };

            try
            {
                var token = await Stripe.StripeClient.CreateToken(stripeCard);

                // Slightly different for non-Apple Pay use, see 
                // 'Sending the token to your server' for more info
                //await CreateBackendCharge(token);
            }
            catch (Exception ex)
            {
                // Handle a failure
                Console.WriteLine(ex);
            }
        }
    }
}
