﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;
using Umbrella.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CarouselPage), typeof(BaseCarouselPageRenderer))]
namespace Umbrella.iOS.Renderers
{
    public sealed class BaseCarouselPageRenderer : CarouselPageRenderer
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            //for iOS 11 carouselpage
            //View.Subviews.OfType<UIScrollView>().Single().ContentInsetAdjustmentBehavior =
            //    UIScrollViewContentInsetAdjustmentBehavior.Never;
        }
    }
}
