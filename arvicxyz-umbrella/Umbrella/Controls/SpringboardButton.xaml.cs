﻿using System;
using Xamarin.Forms;

namespace Umbrella.Controls
{
    public delegate void SpringboardTappedEventHandler(object sender, EventArgs args);

    public partial class SpringboardButton : ContentView
    {
        public FontAttributes FontAttrib
        {
            get { return ButtonLabel.FontAttributes; }
            set { ButtonLabel.FontAttributes = value; }
        }
        public ImageSource Icon
        {
            get { return ButtonIcon.Source; }
            set { ButtonIcon.Source = value; }
        }

        public string Label
        {
            get { return ButtonLabel.Text; }
            set { ButtonLabel.Text = value; }
        }
        public Color TextCol
        {
            get { return ButtonLabel.TextColor; }
            set { ButtonLabel.TextColor = value; }
        }

        public event SpringboardTappedEventHandler Tapped;

        public SpringboardButton()
        {
            InitializeComponent();
        }

        private void SpringboardButtonTapped(object sender, EventArgs args)
        {
            Tapped?.Invoke(sender, args);
        }
    }
}
