﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Utilities;
using Xamarin.Forms;

namespace Umbrella.Controls
{
    public partial class MainTimeView : ContentView
    {
        private List<string> timeStrings;
        private string _classIdLastTap = "";
        public MainTimeView()
        {
            InitializeComponent();
            timeStrings = new List<string>()
            {
                "9:00 AM",
                "9:20 AM",
                "9:40 AM",
                "10:00 AM",
                "10:20 AM",
                "10:40 AM",
            };

            var secondCount = 0;
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (s, e) => {
                var fr = (Frame)s;
                var stacklayout = (StackLayout)fr.Content;
                var label = (Label)stacklayout.Children[1];
                var image = (Image)stacklayout.Children[0];
                if (label.Text != _classIdLastTap)
                {
                    foreach (Frame item in timeGrid.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    image.IsVisible = true;
                    fr.BackgroundColor = Color.FromHex("#363636");
                    _classIdLastTap = label.Text;
                    Global.SetChosenTime(label.Text);
                    MessagingCenter.Send(this, "ChangeSummaryCommand", true);
                }
                else
                {
                    fr.BackgroundColor = Color.FromHex("#363636");
                    _classIdLastTap = "";
                    Global.SetChosenTime("");
                    image.IsVisible = false;
                    MessagingCenter.Send(this, "ChangeSummaryCommand", false);
                }

            };
            for (int i = 0; i < timeStrings.Count; i++)
            {
                string time = timeStrings[i];
                if (i <= 2)
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);

                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizer);
                    timeGrid.Children.Add(frame, 0, i);
                }
                else
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,

                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);
                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizer);
                    timeGrid.Children.Add(frame, 1, secondCount);
                    secondCount += 1;
                }

                foreach (Frame item in timeGrid.Children)
                {
                    var stack = (StackLayout)item.Content;
                    var newImage = (Image)stack.Children[0];
                    var label = (Label)stack.Children[1];
                    if (label.Text != Global.GetChosenTime())
                    {
                        newImage.IsVisible = false;
                    }
                    else
                    {
                        newImage.IsVisible = true;
                    }
                }
            }
        }


    }
}
