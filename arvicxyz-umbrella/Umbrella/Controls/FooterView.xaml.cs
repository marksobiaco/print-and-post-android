﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Umbrella.DAL;
using Umbrella.Enums;
using Umbrella.Interfaces;
using Umbrella.Resx;
using Umbrella.Services;
using Umbrella.Utilities;
using Umbrella.Views;
using Xamarin.Forms;

namespace Umbrella.Controls
{
    public partial class FooterView : ContentView
    {
        public FooterView()
        {
            InitializeComponent();
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                RewardLevelTab.Label = AppResources.FooterHome;
                CallBacksTab.Label = AppResources.FooterImport;
                LeadsTab.Label = AppResources.FooterTopup;
                MessagesTab.Label = AppResources.FooterTrack;
                ReferAndEarnTab.Label = AppResources.FooterReferEarn;
            }

        }
        public void ActiveReferAndEarn(bool active)
        {
            if (active)
            {
                ReferAndEarnTab.Icon = "refer_and_earn_tab_icon_red";
                ReferAndEarnTab.LabelColor = Color.Black;
            }
            else
            {
                ReferAndEarnTab.Icon = "refer_and_earn_tab_icon";
                ReferAndEarnTab.LabelColor = Color.Gray;
            }
        }
        public void ActiveCallback(bool active)
        {
            if (active)
            {
                CallBacksTab.Icon = "searchd";
                CallBacksTab.LabelColor = Color.Black;
            }
            else
            {
                CallBacksTab.Icon = "searchd_inactive";
                CallBacksTab.LabelColor = Color.Gray;
            }
        }
        public void ActiveMessages(bool active)
        {
            if (active)
            {
                MessagesTab.Icon = "importicon";
                MessagesTab.LabelColor = Color.Black;
            }
            else
            {
                MessagesTab.Icon = "importicon_inactive";
                MessagesTab.LabelColor = Color.Gray;
            }
        }
        public void ActiveLeads(bool active)
        {
            if (active)
            {
                LeadsTab.Icon = "topupsign";
                LeadsTab.LabelColor = Color.Black;
            }
            else
            {
                LeadsTab.Icon = "topupsign_inactive";
                LeadsTab.LabelColor = Color.Gray;
            }
        }
        public void ActiveRewardLevel(bool active)
        {
            var current = Global.GetrewardPoints();
            if (active)
            {
                RewardLevelTab.Icon = "newhome";
                RewardLevelTab.LabelColor = Color.Black;
            }
            else
            {
                RewardLevelTab.Icon = "newhome_inactive";
                RewardLevelTab.LabelColor = Color.Gray;
            }
        }
       
        private async void RewardLevelButtonTapped(object sender, EventArgs args)
        {
            if (Navigation.NavigationStack.Count != 1)
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new HomePage());
                Navigation.RemovePage(page);
            }
            else
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new HomePage());
            }
        }

        private async void MessagesButtonTapped(object sender, EventArgs args)
        {
            if (Navigation.NavigationStack.Count != 1)
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new ImportPage());
                Navigation.RemovePage(page);
            }
            else
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new ImportPage());
            }
        }

        private async void LeadsButtonTapped(object sender, EventArgs args)
        {
            if (Navigation.NavigationStack.Count != 1)
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new TopupPage());
                Navigation.RemovePage(page);
            }
            else
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new TopupPage());
            }
        }

        private async void CallBacksButtonTapped(object sender, EventArgs args)
        {
            if (Navigation.NavigationStack.Count != 1)
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new TrackPage());
                Navigation.RemovePage(page);
            }
            else
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new TrackPage());
            }
        }

        private async void ReferAndEarnButtonTapped(object sender, EventArgs args)
        {
            if (Navigation.NavigationStack.Count != 1)
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new ReferFriendPage());
                Navigation.RemovePage(page);
            }
            else
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new ReferFriendPage());
            }
        }
    }
}
