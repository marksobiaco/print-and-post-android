﻿using Xamarin.Forms;

namespace Umbrella.Controls
{
    public partial class DropdownView : ContentView
    {
        public string SelectedText
        {
            get { return DropdownBox.Text; }
            set { DropdownBox.Text = value; }
        }

        public Color SelectedTextColor
        {
            get { return DropdownBox.TextColor; }
            set { DropdownBox.TextColor = value; }
        }

        public DropdownView()
        {
            InitializeComponent();
        }
    }
}
