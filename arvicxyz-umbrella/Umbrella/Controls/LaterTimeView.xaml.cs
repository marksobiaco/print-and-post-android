﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Utilities;
using Xamarin.Forms;

namespace Umbrella.Controls
{
    public partial class LaterTimeView : ContentView
    {
        private List<string> laterTimeStrings;
        private string _classIdLastTap = "";
        public LaterTimeView()
        {
            InitializeComponent();
            System.Diagnostics.Debug.WriteLine("laterview");
            laterTimeStrings = new List<string>()
            {
                "11:00 AM",
                "11:20 AM",
                "11:40 AM",
                "12:00 PM",
                "12:20 PM",
                "12:40 PM",
            };
            var secondCountLater = 0;
            var tapGestureRecognizerLater = new TapGestureRecognizer();
            tapGestureRecognizerLater.Tapped += (s, e) => {
                var fr = (Frame)s;
                var stacklayout = (StackLayout)fr.Content;
                var label = (Label)stacklayout.Children[1];
                var image = (Image)stacklayout.Children[0];
                if (label.Text != _classIdLastTap)
                {
                    foreach (Frame item in timeGridLater.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    image.IsVisible = true;
                    fr.BackgroundColor = Color.FromHex("#363636");
                    _classIdLastTap = label.Text;
                    Global.SetChosenTime(label.Text);
                    MessagingCenter.Send(this, "ChangeSummaryCommand", true);
                }
                else
                {
                    fr.BackgroundColor = Color.FromHex("#363636");
                    _classIdLastTap = "";
                    Global.SetChosenTime("");
                    image.IsVisible = false;
                    MessagingCenter.Send(this, "ChangeSummaryCommand", false);
                }

            };
            for (int i = 0; i < laterTimeStrings.Count; i++)
            {
                string time = laterTimeStrings[i];
                if (i <= 2)
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);

                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizerLater);
                    timeGridLater.Children.Add(frame, 0, i);
                }
                else
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);
                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizerLater);
                    timeGridLater.Children.Add(frame, 1, secondCountLater);
                    secondCountLater += 1;
                }
                foreach (Frame item in timeGridLater.Children)
                {
                    var stack = (StackLayout)item.Content;
                    var newImage = (Image)stack.Children[0];
                    var label = (Label)stack.Children[1];
                    if (label.Text != Global.GetChosenTime())
                    {
                        newImage.IsVisible = false;
                    }
                    else
                    {
                        newImage.IsVisible = true;
                    }
                }
            }

        }

    }
}
