﻿using Plugin.Connectivity;
using System;
using System.Linq;
using Umbrella.Enums;
using Umbrella.Interfaces;
using Umbrella.Utilities;
using Umbrella.Views;
using Xamarin.Forms;

namespace Umbrella.Controls
{
    public partial class HeaderView : ContentView
    {
        public HeaderView()
        {
            InitializeComponent();
            var _defaultTopupDetails = DependencyService.Get<ICredentialRetriever>().GetCurrencyDefault();
            var currency = "";
            if (_defaultTopupDetails != null)
            {
                currency = string.IsNullOrEmpty(_defaultTopupDetails.DefaultCurrency) ? "gbp" : _defaultTopupDetails.DefaultCurrency;
            }
            ChangeHeader(currency);
        }
        private async void TopupButtonTapped(object sender, EventArgs args)
        {
            if (Navigation.NavigationStack.Count != 1)
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new TopupPage());
                Navigation.RemovePage(page);
            }
            else
            {
                var page = Navigation.NavigationStack.Last();
                await Navigation.PushAsync(new TopupPage());
            }
        }
        public void ChangeHeader(string currency)
        {
            if (currency == "gbp")
            {
                topupIcon.Source = "topup_icon";
            }
            else if (currency == "usd")
            {
                topupIcon.Source = "dollar_icon";
            }
            else if(currency == "eur")
            {
                topupIcon.Source = "euro_icon";
            }
            else
            {
                topupIcon.Source = "topup_icon";
            }
        }
        private async void SettingsButtonTapped(object sender, EventArgs args)
        {
            var page = new SettingsPage();
            await Navigation.PushAsync(page);
        }
        private async void UmbrellaLogoTapped(object sender, EventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("s" + Global.GetBoolIsHomePage());
            if (Navigation.NavigationStack.Count >= 2 && Global.GetCountPush() == 0)
            {
                System.Diagnostics.Debug.WriteLine("s4");

                //await Navigation.PopToRootAsync();
                await Navigation.PushModalAsync(new NavigationPage(new HomePage(true)));
            }
            else if (Navigation.NavigationStack.Count >= 2 && Global.GetCountPush() >= 1)
            {
                System.Diagnostics.Debug.WriteLine("s5");

                Global.SetPushToZero(0);
                await Navigation.PushModalAsync(new NavigationPage(new HomePage(true)));
            }
            else
            {
                if (!Global.GetBoolIsHomePage())
                {
                    System.Diagnostics.Debug.WriteLine("s2");

                    var page = new HomePage(true);
                    Navigation.InsertPageBefore(page, Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
                    await Navigation.PopAsync();
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("s3");

                    if (Global.GetCountPush() >= 1)
                    {
                        Global.SetPushToZero(0);
                        await Navigation.PushModalAsync(new NavigationPage(new HomePage(true)));
                    }
                }
            }
        }
    }
}
