﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Enums;
using Xamarin.Forms;

namespace Umbrella.Custom
{
    public class CustomReturnTypeEditor : Editor
    {
        public new event EventHandler Completed;

        public static readonly BindableProperty ReturnTypeProperty = BindableProperty.Create(
            nameof(ReturnType),
            typeof(Umbrella.Enums.ReturnType),
            typeof(CustomReturnTypeEditor),
            Umbrella.Enums.ReturnType.Done,
            BindingMode.OneWay
        );

        public Umbrella.Enums.ReturnType ReturnType
        {
            get { return (Umbrella.Enums.ReturnType)GetValue(ReturnTypeProperty); }
            set { SetValue(ReturnTypeProperty, value); }
        }

        public void InvokeCompleted()
        {
            if (this.Completed != null)
                this.Completed.Invoke(this, null);
        }

    }
}
