﻿using System;
using System.Collections.Generic;
using Umbrella.Models;
using Xamarin.Forms.Maps;

namespace Umbrella.Custom
{
    public class CustomPin : Pin
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public List<Period> Periods { get; set; }
    }
}
