﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Umbrella.Custom;
using Umbrella.Enums;
using Umbrella.Models;

namespace Umbrella.Utilities
{
    public static class Global
    {
      
        private static Loyalty loyalty;
        private static PhoneContact phoneContact;
     
        private static string loyaltyPoints = "0";
        private static int rewardPoints = 0;
        //notification
        private static int notifCount;
        private static int messageCount;
        private static int calendarEventMonth = 0;
        private static int countPush = 0;
        private static int calendarEventYear = 0;
        private static double balance;

        private static Dictionary<DateTime, string> listTopic = new Dictionary<DateTime, string>();
        private static List<Lead> listLeads = new List<Lead>();
        private static List<CalendarEvent> _calendarListWeek = new List<CalendarEvent>();
        private static List<CalendarEvent> _calendarListMonth = new List<CalendarEvent>();
        private static List<CalendarEvent> _calendarListYear = new List<CalendarEvent>();
        private static List<byte[]> _imagePaths = new List<byte[]>();
        private static List<SwipeModel> _favDishes = new List<SwipeModel>();
        private static List<CustomPin> _listPins = new List<CustomPin>();
        private static ReferralBranchModel branch = new ReferralBranchModel();

        private static bool _isHomePage = true;
        private static bool isNotificationClick = false;
        private static bool isSleep = false;
        private static bool _isDoneApiPull = false;
        private static bool isAbort = false;
        private static bool isNotif = false;
        private static bool _isDonePick = false;
        private static bool isFromLocalFiles = false;


        private static string chosenTime = "12:00 AM";
        private static string notifTopic;
        private static string uploadPageType = "";
        private static string testingname = "";
        private static string _pdfFilename = "";
        private static string _playerID = "";
        private static byte[] _bytePdf = new byte[] { };
        private static string _pdfPath = "";
        private static string _storeStatus = "";

        #region GET 
        public static string GetTestingURI()
        {
            return testingname;
        }
        public static List<SwipeModel> GetLikedDishes()
        {
            return _favDishes;
        }
        public static string GetPlayerID()
        {
            return _playerID;
        }
        public static int GetCalendarYear()
        {
            return calendarEventYear;
        }
        public static ReferralBranchModel GetBranch()
        {
            return branch;
        }
        public static PhoneContact GetPhoneContact()
        {
            return phoneContact;
        }
        public static bool GetBoolApiDone()
        {
            return _isDoneApiPull;
        }
       
        public static bool GetIsAbort()
        {
            return isAbort;
        }
        public static bool GetIsFromLocalFiles()
        {
            return isFromLocalFiles;
        }
        public static string GetPdfName()
        {
            return _pdfFilename;
        }
        public static List<CustomPin> GetListPins()
        {
            return _listPins;
        }
        public static string GetUploadPageType()
        {
            return uploadPageType;
        }
        public static byte[] GetByte()
        {
            return _bytePdf;
        }
        public static int GetCountPush()
        {
            return countPush;
        }
        public static bool GetBoolIsHomePage()
        {
            return _isHomePage;
        }
        public static string GetChosenTime()
        {
            return chosenTime;
        }
        public static int GetCalendarMonth()
        {
            return calendarEventMonth;
        }
        public static bool GetIsNotif()
        {
            return isNotif;
        }
        public static List<byte[]> GetImagePaths()
        {
            return _imagePaths;
        }
        public static bool GetIsSleep()
        {
            return isSleep;
        }
        public static List<CalendarEvent> GetCalendarListWeek()
        {
            return _calendarListWeek;
        }
        public static List<CalendarEvent> GetCalendarListMonth()
        {
            return _calendarListMonth;
        }
        public static List<CalendarEvent> GetCalendarListYear()
        {
            return _calendarListYear;
        }
      
        public static double GetBalance()
        {
            return balance;
        }
        public static string GetStoreStatus()
        {
            return _storeStatus;
        }
        public static int GetMessageCount()
        {
            return messageCount;
        }
        public static Dictionary<DateTime,string> GetAllTopic()
        {
            return listTopic;
        }
        public static bool GetIsDonePick()
        {
            return _isDonePick;
        }

        public static string GetNotifTopic()
        {
            return notifTopic;
        }
      
        public static int GetrewardPoints()
        {
            return rewardPoints;
        }
        public static Loyalty GetLoyalty()
        {
            return loyalty;
        }
        #endregion
        #region SET 
      
        public static void SetIsDonePull(bool abort)
        {
            _isDoneApiPull = abort;
        }
        public static void SetPlayerID(string id)
        {
            _playerID = id;
        }
        public static void SetBranchModel(ReferralBranchModel model)
        {
            branch = model;
        }
        public static void SetTestingURI(string id)
        {
            testingname = id;
        }
        public static void SetUploadPageType(string type)
        {
            uploadPageType = type;
        }
        public static void SetByte(byte[] pdf)
        {
            _bytePdf = pdf;
        }
        public static void SetPhoneContact(PhoneContact contact)
        {
            phoneContact = contact;
        }

        public static void SetPdfName(string pdf)
        {
            _pdfFilename = pdf;
        }
        public static void SetPdfPath(string path)
        {
            _pdfPath = path;
        }
        public static void SetBoolIsHomePage(bool isHome)
        {
            _isHomePage = isHome;
        }
        public static void SetIsSleep(bool sleep)
        {
            isSleep = sleep;
        }
        public static void SetIsNotificationClick(bool click)
        {
            isNotificationClick = click;
        }
        public static void SetIsNotif(bool click)
        {
            isNotif = click;
        }
        public static void SetIsFromLocalFiles(bool done)
        {
            isFromLocalFiles = done;
        }
        public static void SetChosenTime(string time)
        {
            chosenTime = time;
        }
        public static void SetStoreStatus(string storeStatus)
        {
            _storeStatus = storeStatus;
        }
        public static void SetrewardPoints(int point)
        {
            rewardPoints = point;
        }
        public static void SetCountPush(int point)
        {
            countPush = countPush + point;
        }
        public static void SetPushToZero(int point)
        {
            countPush = point;
        }
      
        public static void SetBalance(double balanceApi)
        {
            balance = balanceApi;
        }
        public static void SetLoyalty(Loyalty Loyalty)
        {
            loyalty = Loyalty;
        }
        public static void AddPinList(CustomPin pin)
        {
            _listPins.Add(pin);
        }
        public static void ClearPinList()
        {
            _listPins.Clear();
        }
        public static void SetNotifTopic(string topic)
        {
            notifTopic = topic;
        }
        public static void SetListTopic(DateTime id, string topic)
        {
            listTopic.Add(id, topic);
        }
        public static void SetAllTopic(Dictionary<DateTime,string> list)
        {
            listTopic = list;
        }
        public static void SetCalendarYearNumAdd(int numadd)
        {
            calendarEventYear = calendarEventYear + numadd;
        }
        public static void SetCalendarYearNum(int numadd)
        {
            calendarEventYear = numadd;
        }
        public static void SetCalendarMonthNumAdd(int numadd)
        {
            calendarEventMonth = calendarEventMonth + numadd;
        }
        public static void SetCalendarMonthNum(int numadd)
        {
            calendarEventMonth = numadd;
        }
        public static void SetIsDonePick(bool done)
        {
            _isDonePick = done;
        }
        public static void SetCalendarListWeek(List<CalendarEvent> list)
        {
            _calendarListWeek = list;
        }
        public static void AddImagePaths(byte[] path)
        {
            _imagePaths.Add(path);
        }
        public static void ClearImagePaths()
        {
            _imagePaths.Clear();
        }
        public static void AddFavDishes(SwipeModel chosen)
        {
            _favDishes.Add(chosen);
        }
        public static void SetCalendarListMonth(List<CalendarEvent> list)
        {
            _calendarListMonth = list;
        }
        public static void SetCalendarListYear(List<CalendarEvent> list)
        {
            _calendarListYear = list;
        }
      
        // password
        #endregion
    }
}