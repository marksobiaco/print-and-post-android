﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Umbrella.Constants;
using Umbrella.Enums;
using Umbrella.Models;
using RestSharp;
using System.Threading;
using Umbrella.Utilities;

namespace Umbrella.RestClient
{
    public class UserClientService : ApiClient
    {
        protected override string Controller { get { return "user"; } }

        public UserClientService()
        {
        }

        public async Task<User> Authenticate(string username, string password, AuthenticationType authType)
        {
            string action = "generate_auth_cookie";
            string type = (authType == AuthenticationType.Username) ? "username" : "email";
            string url = $"{UmbrellaApi.SCHEME}://{UmbrellaApi.WP_HOST}/{UmbrellaApi.ROOT}" +
                $"/{Controller}/{action}/?{type}={username}&password={password}";

            var httpClient = new HttpClient();             
            var json = await httpClient.GetStringAsync(url).ConfigureAwait(false);
            var result = JsonConvert.DeserializeObject<AuthenticationCookie>(json);

            if (result.Status.Equals("ok"))
            {
                if (result.CookieName.StartsWith("wordpress_logged_in_", StringComparison.CurrentCultureIgnoreCase))
                {
                    return result.User;
                }
            }
            else if (result.Status.Equals("error"))
            {
                Debug.WriteLine($"Error: {result.Error}");
            }

            return null;
        }
        public async Task<User> AuthenticateToAPI(string username, string password, AuthenticationType authType)
        {
            var ls = new LoginJson();
            ls.status = "empty";
            var user = new User();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/validateaccount/");
            var request = new RestRequest(Method.POST);

            request.AddParameter("email", username);
            request.AddParameter("password", password);
            request.AddParameter("player_id", Global.GetPlayerID());
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);

            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(10));

            EventWaitHandle Wait = new AutoResetEvent(false);
            var asyncHandle = client.ExecuteAsync<LoginJson>(request, response => {
                try
                {
                    System.Diagnostics.Debug.WriteLine("Login Data " + response.Content);
                    var json = response.Content.Replace("<", "");
                    ls = JsonConvert.DeserializeObject<LoginJson>(json);
                    if (ls.error == "true")
                    {
                        if (ls.result == "Invalid username or password. Please try again.")
                        {
                            user.AuthenticationStatus = AuthenticationStatus.AuthenticationFailed;
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("Server Error");
                            user.AuthenticationStatus = AuthenticationStatus.ServerError;
                        }
                    }
                    else if (ls.message == "Login successful")
                    {
                        System.Diagnostics.Debug.WriteLine("Success Login");
                        user.Username = username;
                        user.Email = username;
                        user.AuthenticationStatus = AuthenticationStatus.AuthenticationSuccess;

                        if (Int32.TryParse(ls.partner_id, out int j))
                        {
                            user.Id = j;
                        }

                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Server Error");
                        user.AuthenticationStatus = AuthenticationStatus.ServerError;
                    }

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Error cast : " + e.ToString());
                    user.AuthenticationStatus = AuthenticationStatus.AuthenticationFailed;
                }
                Wait.Set();

            });

            Wait.WaitOne();

            return user;
        }
        public async Task<LoginJson> GetLoyatyData(string partner_id)
        {

            var ls = new LoginJson();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/loyalt_ycard/");
          //  var client = new RestSharp.RestClient("http://mastereman.com/firebase/qr.php");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", partner_id);
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);


           
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<LoginJson>(request, response => {
                System.Diagnostics.Debug.WriteLine(response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    ls = JsonConvert.DeserializeObject<LoginJson>(json);
                }
                catch (InvalidCastException e)
                {
                    System.Diagnostics.Debug.WriteLine("ERRor : " + e.ToString());
                }
                 
                Wait.Set();
            });
            Wait.WaitOne();

            if (ls.points == "0")
            {
                System.Diagnostics.Debug.WriteLine("NULL if Stat" + ls.points);

                return null;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("else Stat" + ls.points);

               
                //u.Registered = "";
                System.Diagnostics.Debug.WriteLine("u.Id " + ls.barcode);
               
                return ls;

            }


        }
    }
}
