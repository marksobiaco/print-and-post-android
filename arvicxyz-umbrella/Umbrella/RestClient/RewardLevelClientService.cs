﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using UmbrellaRestClient.Services;

namespace Umbrella.RestClient
{
    public class RewardLevelClientService : ApiClient
    {
        protected override string Controller { get { return "reward_level"; } }

        public RewardLevelClientService()
        {
        }

        public async Task<int> GetRewardLevel(string email)
        {
            var rest = await ServiceRestClient.ExecuteGetTaskAsync("reward_level/?email="+ email).ConfigureAwait(false);
            System.Diagnostics.Debug.WriteLine("resultrest" + rest);
            var obj = JObject.Parse(rest.Content);
            System.Diagnostics.Debug.WriteLine("objreward" + obj);
            var result = (string)obj["reward_level"];
            System.Diagnostics.Debug.WriteLine("Resultreward" + result);
            if (result != null)
            {
                return int.Parse(result);
            }
            return 1;
        }
    }
}
