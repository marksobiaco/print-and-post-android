﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostCardDetailsPage : ContentPage
    {
        private string[] SelectionArray = { "Standard Dispatch", "Priority Dispatch" };
        private string[] SelectionArrayFR = { "Normal", "Prioritaire" };
        private PhoneContact _phoneContact;
        APIHelper2 apiHelper;
        private List<FileUpload> fileIdList;
        private Credential account;
        public PostCardDetailsPage(PhoneContact phoneContact)
        {
            InitializeComponent();
            account = DependencyService.Get<ICredentialRetriever>().GetCredential();
            nextButton.IsEnableButton = CrossConnectivity.Current.IsConnected;
            fileIdList = new List<FileUpload>();
            apiHelper = new APIHelper2();

            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                nextButton.IsEnableButton = args.IsConnected ? true : false;
            };
            var defaults = DependencyService.Get<ICredentialRetriever>().GetDefault();

            if (account.User.Username == "juju@raptorsms.com")
            {
                colourText.Text = AppResources.PostcardDetailsColor;
                blackWhiteText.Text = AppResources.PostcardDetailsBW;
                System.Diagnostics.Debug.WriteLine("asdasdfff " + AppResources.PostcardDetailsEditorText + " " + defaults.DispatchSpeed);
                editss.Text = AppResources.PostcardDetailsEditorText;
                wordCount.Text = AppResources.PostcardDetailsMaxText;
                compText.Text = AppResources.PostcardDetailsComputer;
                handText.Text = AppResources.PostcardDetailsHand;
                firstClassTxt.Text = AppResources.PostcardDetails1st;
                signedForTxt.Text = AppResources.PostcardDetailsSigned;
                postAsapText.Text = AppResources.PostcardDetailsPostAsap;
                postLaterText.Text = AppResources.PostcardDetailsPostLater;
                dispatchDesc.Text = AppResources.PostcardDetailsDispatchDesc;
                dispatchTitle.Text = AppResources.PostcardDetailsDispatch;
                prevButton.Text = AppResources.PostcardDetailsBackBtn;
                nextButton.Text = AppResources.PostcardDetailsPostSendBtn;
                secBtnText.Text = AppResources.PostcardDetailsCheapest;
                if (!string.IsNullOrEmpty(defaults.DispatchSpeed))
                {
                    if (defaults.DispatchSpeed == "Standard")
                    {
                        urgentDropdown.SelectedText = "Normal";
                    }
                    else
                    {
                        urgentDropdown.SelectedText = "Prioritaire";
                    }
                }
                else
                {
                    urgentDropdown.SelectedText = "Normal";
                }

            }
            else
            {
                urgentDropdown.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard Dispatch" : defaults.DispatchSpeed + " Dispatch";

            }
            customProgbar.Progress = 0.75;
            System.Diagnostics.Debug.WriteLine("df" + defaults.InkType);


            //urgentDropdown.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard Dispatch" : defaults.DispatchSpeed + " Dispatch";
            _phoneContact = new PhoneContact();
            _phoneContact = phoneContact;
            DatePick.MinDate = DateTime.Now.AddDays(3);
            standardBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    firstClassBtn.Checked = false;
                    signedForBtn.Checked = false;
                }
            };
            firstClassBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    standardBtn.Checked = false;
                    signedForBtn.Checked = false;
                }
            };
            signedForBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    firstClassBtn.Checked = false;
                    standardBtn.Checked = false;
                }
            };
            postLaterBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    postNowBtn.Checked = false;
                }
            };
            postNowBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    postLaterBtn.Checked = false;
                }

            };
            colorBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    blckBtn.Checked = false;
                }
            };
            blckBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    colorBtn.Checked = false;
                }

            };

            DatePick.DateSelected += (sender, args) =>
            {
                postNowBtn.Checked = false;
                postLaterBtn.Checked = true;
            };
            handBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    printBtn.Checked = false;
                }

            };
            printBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    handBtn.Checked = false;
                }

            };

            if (defaults.InkType == "Black & White" || defaults.InkType == "Noir et Blanc")
            {
                blckBtn.Checked = true;
            }
            else
            {
                colorBtn.Checked = true;

            }
            if (defaults.LabelType == "Hand Written" || defaults.InkType == "Ecrit a la main")
            {
                handBtn.Checked = true;
            }
            else
            {
                printBtn.Checked = true;
            }
            if (defaults.PostageClass == "Standard" || defaults.InkType == "Lettre Verte")
            {
                standardBtn.Checked = true;
            }
            else if (defaults.PostageClass == "1st Class" || defaults.InkType == "Lettre Prioritaire")
            {
                firstClassBtn.Checked = true;
            }
            else if (defaults.PostageClass == "Signed For" || defaults.InkType == "Avec accusé de réception")
            {
                signedForBtn.Checked = true;
            }
            else
            {
                standardBtn.Checked = true;
            }
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                blackWhiteText.FontSize = 11;
                colourText.FontSize = 11;
                handText.FontSize = 11;
                compText.FontSize = 11;
                postAsapText.FontSize = 11;
                postLaterText.FontSize = 11;
                dispatchDesc.FontSize = 10;
                firstClassTxt.FontSize = 11;
                signedForTxt.FontSize = 11;
                firstBtnText.FontSize = 11;
                secBtnText.FontSize = 9;
                colorBtn.Margin = new Thickness(0, 15, 0, 0);
                firstClassBtn.Margin = new Thickness(0, 25, 0, 0);
                signedForBtn.Margin = new Thickness(0, 25, 0, 0);
                standardBtn.Margin = new Thickness(0, 25, 0, 0);

                blckBtn.Margin = new Thickness(0, 15, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 15, 0, 0);
                postNowBtn.Margin = new Thickness(0, 15, 0, 0);
                handBtn.Margin = new Thickness(0, 15, 0, 0);
                printBtn.Margin = new Thickness(0, 15, 0, 0);
                DatePick.HeightRequest = 32;
                calendarWhite.HeightRequest = 20;
                calendarWhite.WidthRequest = 20;
                urgentDropdown.HeightRequest = 35;
            }
            else if (sizeChecker > 700)
            {
                postAsapText.FontSize = 15;
                postLaterText.FontSize = 15;
                blackWhiteText.FontSize = 15;
                colourText.FontSize = 15;
                handText.FontSize = 15;
                compText.FontSize = 15;
                firstClassTxt.FontSize = 15;
                signedForTxt.FontSize = 15;
                firstBtnText.FontSize = 15;
                secBtnText.FontSize = 12;
                dispatchDesc.FontSize = 14;
                firstClassBtn.Margin = new Thickness(0, 35, 0, 0);
                signedForBtn.Margin = new Thickness(0, 35, 0, 0);
                standardBtn.Margin = new Thickness(0, 35, 0, 0);
                colorBtn.Margin = new Thickness(0, 25, 0, 0);
                blckBtn.Margin = new Thickness(0, 25, 0, 0);
                handBtn.Margin = new Thickness(0, 25, 0, 0);
                printBtn.Margin = new Thickness(0, 25, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 25, 0, 0);
                postNowBtn.Margin = new Thickness(0, 25, 0, 0);
                DatePick.HeightRequest = 40;
                calendarWhite.HeightRequest = 30;
                calendarWhite.WidthRequest = 30;
                urgentDropdown.HeightRequest = 40;
            }
            else
            {
                handText.FontSize = 11;
                compText.FontSize = 11;
                postAsapText.FontSize = 11;
                postLaterText.FontSize = 11;
                blackWhiteText.FontSize = 11;
                colourText.FontSize = 11;
                dispatchDesc.FontSize = 10;
                firstClassTxt.FontSize = 11;
                signedForTxt.FontSize = 11;
                firstBtnText.FontSize = 11;
                secBtnText.FontSize = 9;
                firstClassBtn.Margin = new Thickness(0, 25, 0, 0);
                signedForBtn.Margin = new Thickness(0, 25, 0, 0);
                standardBtn.Margin = new Thickness(0, 25, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 15, 0, 0);
                postNowBtn.Margin = new Thickness(0, 15, 0, 0);
                blckBtn.Margin = new Thickness(0, 15, 0, 0);
                colorBtn.Margin = new Thickness(0, 15, 0, 0);
                handBtn.Margin = new Thickness(0, 15, 0, 0);
                printBtn.Margin = new Thickness(0, 15, 0, 0);
                DatePick.HeightRequest = 32;
                calendarWhite.HeightRequest = 20;
                calendarWhite.WidthRequest = 20;
                urgentDropdown.HeightRequest = 35;
            }
        }
        public PostCardDetailsPage(PhoneContact phoneContact, bool noOrg)
        {
            InitializeComponent();
            nextButton.IsEnableButton = CrossConnectivity.Current.IsConnected;
            fileIdList = new List<FileUpload>();
            apiHelper = new APIHelper2();
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                nextButton.IsEnableButton = args.IsConnected ? true : false;
            };
            customProgbar.Progress = 0.75;
            var defaults = DependencyService.Get<ICredentialRetriever>().GetDefault();
            account = DependencyService.Get<ICredentialRetriever>().GetCredential();
            System.Diagnostics.Debug.WriteLine("df" + defaults.InkType);
            if (account.User.Username == "juju@raptorsms.com")
            {
                try
                {
                    colourText.Text = AppResources.PostcardDetailsColor;
                    blackWhiteText.Text = AppResources.PostcardDetailsBW;
                    System.Diagnostics.Debug.WriteLine("asdasd " + AppResources.PostcardDetailsEditorText + " " + defaults.DispatchSpeed);
                    editss.Text = AppResources.PostcardDetailsEditorText;
                    wordCount.Text = AppResources.PostcardDetailsMaxText;
                    compText.Text = AppResources.PostcardDetailsComputer;
                    handText.Text = AppResources.PostcardDetailsHand;
                    firstClassTxt.Text = AppResources.PostcardDetails1st;
                    signedForTxt.Text = AppResources.PostcardDetailsSigned;
                    postAsapText.Text = AppResources.PostcardDetailsPostAsap;
                    postLaterText.Text = AppResources.PostcardDetailsPostLater;
                    dispatchDesc.Text = AppResources.PostcardDetailsDispatchDesc;
                    dispatchTitle.Text = AppResources.PostcardDetailsDispatch;
                    prevButton.Text = AppResources.PostcardDetailsBackBtn;
                    nextButton.Text = AppResources.PostcardDetailsPostSendBtn;
                    secBtnText.Text = AppResources.PostcardDetailsCheapest;
                    if (!string.IsNullOrEmpty(defaults.DispatchSpeed))
                    {
                        if (defaults.DispatchSpeed == "Standard")
                        {
                            urgentDropdown.SelectedText = "Normal";
                        }
                        else
                        {
                            urgentDropdown.SelectedText = "Prioritaire";
                        }
                    }
                    else
                    {
                        urgentDropdown.SelectedText = "Normal";
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("ex " + ex.InnerException);
                    colourText.Text = AppResources.PostcardDetailsColor;
                    blackWhiteText.Text = AppResources.PostcardDetailsBW;
                    System.Diagnostics.Debug.WriteLine("asdasd " + AppResources.PostcardDetailsEditorText);
                    editss.Text = AppResources.PostcardDetailsEditorText;
                    wordCount.Text = AppResources.PostcardDetailsMaxText;
                    compText.Text = AppResources.PostcardDetailsComputer;
                    handText.Text = AppResources.PostcardDetailsHand;
                    firstClassTxt.Text = AppResources.PostcardDetails1st;
                    signedForTxt.Text = AppResources.PostcardDetailsSigned;
                    postAsapText.Text = AppResources.PostcardDetailsPostAsap;
                    postLaterText.Text = AppResources.PostcardDetailsPostLater;
                    dispatchDesc.Text = AppResources.PostcardDetailsDispatchDesc;
                    dispatchTitle.Text = AppResources.PostcardDetailsDispatch;
                    prevButton.Text = AppResources.PostcardDetailsBackBtn;
                    nextButton.Text = AppResources.PostcardDetailsPostSendBtn;
                    secBtnText.Text = AppResources.PostcardDetailsCheapest;
                }

            }
            else
            {
                urgentDropdown.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard Dispatch" : defaults.DispatchSpeed + " Dispatch";
            }

            //urgentDropdown.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard Dispatch" : defaults.DispatchSpeed + " Dispatch";
            _phoneContact = new PhoneContact();
            _phoneContact = phoneContact;
            DatePick.MinDate = DateTime.Now.AddDays(3);
            standardBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    firstClassBtn.Checked = false;
                    signedForBtn.Checked = false;
                }
            };
            firstClassBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    standardBtn.Checked = false;
                    signedForBtn.Checked = false;
                }
            };
            signedForBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    firstClassBtn.Checked = false;
                    standardBtn.Checked = false;
                }
            };
            postLaterBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    postNowBtn.Checked = false;
                }
            };
            postNowBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    postLaterBtn.Checked = false;
                }

            };
            colorBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    blckBtn.Checked = false;
                }
            };
            blckBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    colorBtn.Checked = false;
                }

            };

            DatePick.DateSelected += (sender, args) =>
            {
                postNowBtn.Checked = false;
                postLaterBtn.Checked = true;
            };
            handBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    printBtn.Checked = false;
                }

            };
            printBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    handBtn.Checked = false;
                }

            };
            if (defaults.InkType == "Black & White" || defaults.InkType == "Noir et Blanc")
            {
                blckBtn.Checked = true;
            }
            else
            {
                colorBtn.Checked = true;

            }
            if (defaults.LabelType == "Hand Written" || defaults.InkType == "Ecrit a la main")
            {
                handBtn.Checked = true;
            }
            else
            {
                printBtn.Checked = true;
            }
            if (defaults.PostageClass == "Standard" || defaults.InkType == "Lettre Verte")
            {
                standardBtn.Checked = true;
            }
            else if (defaults.PostageClass == "1st Class" || defaults.InkType == "Lettre Prioritaire")
            {
                firstClassBtn.Checked = true;
            }
            else if (defaults.PostageClass == "Signed For" || defaults.InkType == "Avec accusé de réception")
            {
                signedForBtn.Checked = true;
            }
            else
            {
                standardBtn.Checked = true;
            }
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                blackWhiteText.FontSize = 11;
                colourText.FontSize = 11;
                handText.FontSize = 11;
                compText.FontSize = 11;
                postAsapText.FontSize = 11;
                postLaterText.FontSize = 11;
                dispatchDesc.FontSize = 10;
                firstClassTxt.FontSize = 11;
                signedForTxt.FontSize = 11;
                firstBtnText.FontSize = 11;
                secBtnText.FontSize = 9;
                firstClassBtn.Margin = new Thickness(0, 25, 0, 0);
                signedForBtn.Margin = new Thickness(0, 25, 0, 0);
                standardBtn.Margin = new Thickness(0, 25, 0, 0);
                colorBtn.Margin = new Thickness(0, 15, 0, 0);
                blckBtn.Margin = new Thickness(0, 15, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 15, 0, 0);
                postNowBtn.Margin = new Thickness(0, 15, 0, 0);
                handBtn.Margin = new Thickness(0, 15, 0, 0);
                printBtn.Margin = new Thickness(0, 15, 0, 0);
                DatePick.HeightRequest = 32;
                calendarWhite.HeightRequest = 20;
                calendarWhite.WidthRequest = 20;
                urgentDropdown.HeightRequest = 35;
            }
            else if (sizeChecker > 700)
            {
                postAsapText.FontSize = 15;
                postLaterText.FontSize = 15;
                blackWhiteText.FontSize = 15;
                colourText.FontSize = 15;
                handText.FontSize = 15;
                compText.FontSize = 15;
                dispatchDesc.FontSize = 14;
                firstClassTxt.FontSize = 15;
                signedForTxt.FontSize = 15;
                firstBtnText.FontSize = 15;
                secBtnText.FontSize = 12;
                firstClassBtn.Margin = new Thickness(0, 35, 0, 0);
                signedForBtn.Margin = new Thickness(0, 35, 0, 0);
                standardBtn.Margin = new Thickness(0, 35, 0, 0);
                colorBtn.Margin = new Thickness(0, 25, 0, 0);
                blckBtn.Margin = new Thickness(0, 25, 0, 0);
                handBtn.Margin = new Thickness(0, 25, 0, 0);
                printBtn.Margin = new Thickness(0, 25, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 25, 0, 0);
                postNowBtn.Margin = new Thickness(0, 25, 0, 0);
                DatePick.HeightRequest = 40;
                calendarWhite.HeightRequest = 30;
                calendarWhite.WidthRequest = 30;
                urgentDropdown.HeightRequest = 40;
            }
            else
            {
                handText.FontSize = 11;
                compText.FontSize = 11;
                postAsapText.FontSize = 11;
                postLaterText.FontSize = 11;
                blackWhiteText.FontSize = 11;
                colourText.FontSize = 11;
                dispatchDesc.FontSize = 10;
                firstClassTxt.FontSize = 11;
                signedForTxt.FontSize = 11;
                firstBtnText.FontSize = 11;
                secBtnText.FontSize = 9;
                firstClassBtn.Margin = new Thickness(0, 25, 0, 0);
                signedForBtn.Margin = new Thickness(0, 25, 0, 0);
                standardBtn.Margin = new Thickness(0, 25, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 15, 0, 0);
                postNowBtn.Margin = new Thickness(0, 15, 0, 0);
                blckBtn.Margin = new Thickness(0, 15, 0, 0);
                colorBtn.Margin = new Thickness(0, 15, 0, 0);
                handBtn.Margin = new Thickness(0, 15, 0, 0);
                printBtn.Margin = new Thickness(0, 15, 0, 0);
                DatePick.HeightRequest = 32;
                calendarWhite.HeightRequest = 20;
                calendarWhite.WidthRequest = 20;
                urgentDropdown.HeightRequest = 35;
            }
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;

        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private async void BackTapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
        private void EditorFocused(object sender, EventArgs e)
        {
            editss.Focus();
            if (editss.Text == "Write the content of your postcard here" || editss.Text == "Veuillez écrire le texte de votre carte postale.")
            {
                editss.Text = "";
                editss.TextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
            }
        }

        void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            char[] delimiters = new char[] { ' ', '\r', '\n' };
            var entry = (Entry)sender;
            if (entry.Text != "Write the content of your postcard here" || entry.Text != "Veuillez écrire le texte de votre carte postale.")
            {
                var num = entry.Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).Length;
                entry.Text = Regex.Replace(entry.Text, @"[^\u0000-\u007F]+", "");
                if (account.User.Username != "juju@raptorsms.com")
                {
                    entry.Text = Regex.Replace(entry.Text, @"[^\u0000-\u007F]+", "");
                }
                var total = 80 - num;
                if (account.User.Username == "juju@raptorsms.com")
                {
                    wordCount.Text = total + " mots maximum";
                }
                else
                {
                    wordCount.Text = "Max Word Count : " + total;
                }

            }
            else
            {
                if (account.User.Username == "juju@raptorsms.com")
                {
                    wordCount.Text = "80 mots maximum";
                }
                else
                {
                    wordCount.Text = "Max Word Count : 80";
                }

            }
        }

        private void EditorUnfocused(object sender, EventArgs e)
        {
            editss.Unfocus();
            if (editss.Text == "")
            {
                if (account.User.Username == "juju@raptorsms.com")
                {
                    editss.Text = AppResources.PostcardDetailsEditors;
                }
                else
                {
                    editss.Text = "Write the content of your postcard here";
                }
                editss.TextColor = (Color)Application.Current.Resources["SecondaryTextColor"];
            }
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();

        }
        private async void TaskDropdownTapped(object sender, EventArgs e)
        {
            try
            {
                var action = "";
                if (account.User.Username == "juju@raptorsms.com")
                {
                    action = await DisplayActionSheet(urgentDropdown.SelectedText, "Cancel", null, SelectionArrayFR);
                }
                else
                {
                    action = await DisplayActionSheet(urgentDropdown.SelectedText, "Cancel", null, SelectionArray);
                }
                if (!action.Equals("Cancel"))
                {
                    urgentDropdown.SelectedText = action;
                    urgentDropdown.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {

            }
           
        }
        private async void NextTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                if (colorBtn.Checked.Value)
                {
                    _phoneContact.Ink = "colour";
                }
                else
                {
                    _phoneContact.Ink = "black";
                }
                if (urgentDropdown.SelectedText == "Priority Dispatch")
                {
                    _phoneContact.Priority = "yes";
                }
                else
                {
                    _phoneContact.Priority = "no";
                }
                if (handBtn.Checked.Value)
                {
                    _phoneContact.Handwritten = "yes";
                }
                else
                {
                    _phoneContact.Handwritten = "no";
                }
                if (postNowBtn.Checked.Value)
                {
                    _phoneContact.PostDate = "";
                }
                else
                {
                    _phoneContact.PostDate = DatePick.Date.ToString("yyyy-MM-dd");
                }

                if (standardBtn.Checked.Value)
                {
                    _phoneContact.PostageClass = "economy";
                }
                else if (firstClassBtn.Checked.Value)
                {
                    _phoneContact.PostageClass = "1st_class";
                }
                else if (signedForBtn.Checked.Value)
                {
                    _phoneContact.PostageClass = "1st_class_signed_for";
                }

                if (postNowBtn.Checked.Value)
                {
                    _phoneContact.PostDate = "";
                }
                else
                {
                    _phoneContact.PostDate = DatePick.Date.ToString("yyyy-MM-dd hh:ss");
                    System.Diagnostics.Debug.WriteLine("date " + _phoneContact.PostDate);
                }

                _phoneContact.PostCardMessage = editss.Text;
                var isInRange = DependencyService.Get<IFileHelper>().CheckTime();
                if (isInRange)
                {
                    await Navigation.PushAsync(new LateDispatchPage(_phoneContact, "postcard", false));
                }
                else
                {
                    await Navigation.PushAsync(new PostCardDonePage(_phoneContact,true));
                }
            }
            else
            {
                await DisplayAlert("", "Internet Connection Required", "OK");
            }
        }
    }
}