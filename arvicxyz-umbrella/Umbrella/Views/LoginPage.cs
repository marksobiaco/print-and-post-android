﻿using System;
using System.Threading.Tasks;
using Umbrella.DAL;
using Umbrella.Enums;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Umbrella.Utilities;
using Umbrella.ViewModels;
using Plugin.Connectivity;
using Com.OneSignal;
using System.Linq;
using System.Collections.Generic;
using Umbrella.Resx;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private AuthenticationService _authenticationService;

        public LoginPage()
        {
            InitializeComponent();
            UsernameEntry.Placeholder = AppResources.LoginFirstEntryText;
            PasswordEntry.Placeholder = AppResources.LoginSecondEntryText;
            loginBtn.Text = AppResources.LoginButtonText;
            forgotPassLabel.Text = AppResources.LoginForgotPassText;
            newHereLabel.Text = AppResources.LoginNewAccText;
            Indicator.Title = AppResources.BusyIndicator;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                Indicator.TextSize = 35;
            }
            else
            {
                Indicator.TextSize = 25;
            }
            NavigationPage.SetBackButtonTitle(this, "");
            _authenticationService = new AuthenticationService();
            CheckCredential();
        }
        private async void NewHereTapped(object sender, EventArgs e)
        {
             await Navigation.PushAsync(new RegistrationPage());
           // await Navigation.PushModalAsync(new RegisteredTopupPage(""));
            //Device.OpenUri(new Uri("https://www.managedmailservice.co.uk/mobile-app-create-account/"));
        }
        private async void LoginClicked(object sender, EventArgs e)
        {
            StartBusyIndicator();
            
            string username = UsernameEntry.Text;
            string password = PasswordEntry.Text;

            var result= await Task.Run(() => _authenticationService.AuthenticateUser(username, password));
            if (result.Status == AuthenticationStatus.CredentialsRequired)
            {
                StopBusyIndicator();
                await DisplayAlert("Something went wrong.", "Please double check your email address and password and try again.", "OK");
            }
            else if (result.Status == AuthenticationStatus.ServerError)
            {
                StopBusyIndicator();
                await DisplayAlert("Oops!", "Problem with the server, please try again later.", "OK");
            }
            else if (result.Status == AuthenticationStatus.AuthenticationFailed)
            {
                StopBusyIndicator();
                await DisplayAlert("Something went wrong.", "Please double check your email address and password and try again.", "OK");
            }
            else
            {
                SaveUserData(result.User, password);
                StopBusyIndicator();
                if (!string.IsNullOrEmpty(Global.GetPdfName()))
                {
                    LoadLeadsAsync();
                    await Navigation.PushModalAsync(new NavigationPage(new PDFPage()));
                }
                else
                {
                    LoadLeadsAsync();
                    var page = new HomePage(true);
                    await Navigation.PushModalAsync(new NavigationPage(page));
                }
            }
        }
        private async void ForgotPasswordTapped(object sender, EventArgs e)
        {
            var page = new ForgotPasswordPage();
            await Navigation.PushAsync(page);
        }
        private void  StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
              
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }     
        private void CheckCredential()
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("check");
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (credential != null)
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        if (!Global.GetIsNotif())
                        {
                            System.Diagnostics.Debug.WriteLine("LoadAsync");
                            LoadLeadsAsync();
                        }

                    }

                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error" + e.Message);
            }
        }
        private void LoadLeadsAsync()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            OneSignal.Current.SetSubscription(true);
            OneSignal.Current.SendTag("printpostss", credential.UserID);
            System.Diagnostics.Debug.WriteLine("One Signal Initialized");
        }
        private void SaveUserData(User user, string password)
        {
            Credential credential = new Credential()
            {
                User = user,
                Password = password,
                UserID = user.Id.ToString(),
            };
            DefaultNotification def = new DefaultNotification()
            {
                NewLeadNotif = "Yes",
                NewMessageNotif = "Yes",
                GeneralNotif = "Yes",
                LeadConvertedNotif = "Yes"
            };
            MessagingCenter.Send<Credential>(credential, "SaveUserData");
            MessagingCenter.Send<DefaultNotification>(def, "SaveDefaultNotification");
        }
    }
}
