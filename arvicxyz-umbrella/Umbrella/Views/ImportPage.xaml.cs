﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Custom;
using Umbrella.Interfaces;
using Umbrella.Utilities;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]

	public partial class ImportPage : ContentPage
	{
        ImageSource _imageSource;
        private IMedia _mediaPicker;
        Image image;
        public ImportPage ()
		{
			InitializeComponent ();
            image = new Image
            {
                Aspect = Aspect.AspectFit,
            };
           
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                importDesc.FontSize = 21;
                importTitle.FontSize = 17;
            }
            else if (sizeChecker > 700)
            {
                importTitle.FontSize = 21;
                importDesc.FontSize = 17;
            }
            else
            {
                importDesc.FontSize = 17;
                importTitle.FontSize = 21;
            }
            pdfFooter.ActiveMessages(true);
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (credential.UserID == "24757" || credential.UserID == "80385")
            {
                printCollectLayout.IsVisible = true;
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            DependencyService.Get<ILaunchCalendar>().AskPermission();

        }
        private void NewContactTapped(object sender, EventArgs e)
        {
            DependencyService.Get<ILaunchCalendar>().OpenGmail();
            //Device.OpenUri(new Uri("https://play.google.com/store/apps/details?id=com.google.android.apps.docs&hl=en"));
            /*
            var canOpen = await Launcher.CanOpenAsync("googledrive://");
            System.Diagnostics.Debug.WriteLine("canopen " + canOpen);
            if (canOpen)
            {
                await Launcher.OpenAsync("googledrive://");
            }
            else
            {
                await Launcher.OpenAsync("https://play.google.com/store/apps/details?id=com.google.android.apps.docs&hl=en");
            }
            */
        }
        private async void PrintCollectTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PrintCollectPage());
        }
        private void ExistingContactTapped(object sender, EventArgs e)
        {
            DependencyService.Get<ILaunchCalendar>().OpenDropbox();
            /*
            //Device.OpenUri(new Uri("https://play.google.com/store/apps/details?id=com.dropbox.android"));
            var canOpen = await Launcher.CanOpenAsync("googledrive://");
            System.Diagnostics.Debug.WriteLine("canopen " + canOpen);
            if (canOpen)
            {
                await Launcher.OpenAsync("googledrive://");
            }
            else
            {
                await Launcher.OpenAsync("https://play.google.com/store/apps/details?id=com.dropbox.android");
            }
            */
        }
        private void SendToMeTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("mailto://"));
        }
        private void PostToCompanyTapped(object sender, EventArgs e)
        {
            Global.SetUploadPageType("ImageCropPage");
            //DependencyService.Get<ILaunchCalendar>().LaunchGmail();
            DependencyService.Get<ILaunchCalendar>().LaunchSingleImagePicker();
            //DependencyService.Get<ILaunchCalendar>().LaunchGmail();
        }
        private void iCloudDriveTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://play.google.com/store/apps/details?id=com.microsoft.skydrive"));

            /*
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if(credential.UserID == "24757")
            {
                await Navigation.PushAsync(new SwipeTestPage());
            }
            else
            {

            }
            */
        }
        private void Refresh()
        {
            try
            {
                if (App.CroppedImage != null)
                {
                    Stream stream = new MemoryStream(App.CroppedImage);
                    image.Source = ImageSource.FromStream(() => stream);
                     
                    //Content = image;
                    //Navigation.PushModalAsync(new NavigationPage(new ImageCropPage(ImageSource.FromStream(() => stream))));

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private async void Setup()
        {
            if (_mediaPicker != null)
            {
                return;
            }

            ////RM: hack for working on windows phone? 
            await CrossMedia.Current.Initialize();
            _mediaPicker = CrossMedia.Current;
        }

        private void LocalFilesTapped(object sender, EventArgs e)
        {
            DependencyService.Get<ILaunchCalendar>().AskPermission();
            DependencyService.Get<ILaunchCalendar>().LaunchChooser();
            /*
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (credential.UserID == "24757")
            {
                await Navigation.PushAsync(new FavoriteTestPage());
            }
            else
            {
              

            }
           */
        }
    }
}