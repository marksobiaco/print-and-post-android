﻿using System;
using System.Collections.Generic;
using Umbrella.Models;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class LateDispatchPage : ContentPage
    {
        private PhoneContact _phoneContact;
        private string _pageFrom;
        private bool _isLowQuality;
        public LateDispatchPage(PhoneContact phoneContact, string pageFrom, bool isLowQuality)

        {
            InitializeComponent();
            _phoneContact = new PhoneContact();
            _phoneContact = phoneContact;
            _isLowQuality = isLowQuality;
            _pageFrom = pageFrom;
        }
        public async void GreenTapped(object sender, EventArgs e)
        {
            _phoneContact.IsFromLateDispatchPage = true;
            if (_pageFrom == "postcard")
            {
                await Navigation.PushAsync(new PostCardDonePage(_phoneContact,true));
            }
            else if(_pageFrom == "pdf")
            {
                await Navigation.PushAsync(new DonePage(_phoneContact, _isLowQuality,true));
            }
            else
            {
                return;
            }

        }
        public async void RedTapped(object sender, EventArgs e)
        {
            _phoneContact.IsFromLateDispatchPage = false;
            if (_pageFrom == "postcard")
            {
                await Navigation.PushAsync(new PostCardDonePage(_phoneContact,true));
            }
            else if(_pageFrom == "pdf")
            {
                await Navigation.PushAsync(new DonePage(_phoneContact, _isLowQuality,true));
            }
            else
            {
                return;
            }
        }
    }
}
