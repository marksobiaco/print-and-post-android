﻿using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PhoneContactListPage : ContentPage
    {
        private IEnumerable<PhoneContact> _phoneList;
        private IEnumerable<PhoneContact> _recentList;
        private bool _isFromPostCard;
        public PhoneContactListPage(bool isFromPostCard)
        {
            InitializeComponent();
            _isFromPostCard = isFromPostCard;
            NavigationPage.SetBackButtonTitle(this, "");
            _phoneList = new List<PhoneContact>();
            _recentList = new List<PhoneContact>();
            searchBar.TextChanged += SearchBar_TextChanged;
        }
        void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var text = (Entry)sender;
            int n;
            bool isNumeric = int.TryParse(text.Text, out n);
            if (!isNumeric)
            {
                PhoneListView.ItemsSource = _phoneList.Where(stringToCheck => stringToCheck.Name.ToLower().Contains(text.Text.ToLower()));
            }
            else
            {
                PhoneListView.ItemsSource = _phoneList.Where(stringToCheck => stringToCheck.HomeNumber != null && stringToCheck.HomeNumber.Contains(text.Text)
                                                             || stringToCheck.MobileNumber != null && stringToCheck.MobileNumber.Contains(text.Text)
                                                             || stringToCheck.OfficeNumber != null && stringToCheck.OfficeNumber.Contains(text.Text));
            }

            System.Diagnostics.Debug.WriteLine("text " + text.Text + " " + isNumeric);
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Contacts);
            if (status != PermissionStatus.Granted)
            {
                System.Diagnostics.Debug.WriteLine("inggggggss");
                if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Contacts))
                {
                    await DisplayAlert("Contacts needed", "We need the Contacts before we can proceed", "OK");
                    System.Diagnostics.Debug.WriteLine("ghsdasd");
                }

                var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Contacts);
                //Best practice to always check that the key exists

                if (results.ContainsKey(Permission.Contacts))
                    status = results[Permission.Contacts];
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("granted");
            }

            if (status == PermissionStatus.Granted)
            {
                System.Diagnostics.Debug.WriteLine("in");
                var list = DependencyService.Get<IContactRetriever>().GetAllContacts();
                System.Diagnostics.Debug.WriteLine("inss");
             
                try
                {
                    _phoneList = list;
                    foreach (var item in list)
                    {
                        System.Diagnostics.Debug.WriteLine("list" + item.PartnerId + " " + item.Name);

                    }
                    var idlist = await App.RecentlyUsedContactDatabase.GetItemsAsync();
                    var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                    if (idlist.Count != 0)
                    {
                        _recentList = idlist.Where(e => e.PartnerId == credential.UserID).ToList();
                        PhoneListView.ItemsSource = _recentList;
                        _phoneList = list;
                        await ShowAll();
                    }
                    else
                    {
                        _phoneList = list;
                        System.Diagnostics.Debug.WriteLine("in" + list.FirstOrDefault().FirstName);
                        ShowMeeting();
                    }
                }
                catch(Exception eff)
                {
                    System.Diagnostics.Debug.WriteLine("tess " + eff.InnerException + " " + eff.Message);

                
                }
            }
            else if (status != PermissionStatus.Unknown)
            {
                await DisplayAlert("Contacts Denied", "Can not continue, try again.", "OK");
                _phoneList = new List<PhoneContact>();
                _recentList = new List<PhoneContact>();
            }
        }
        private async void BackTapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
        private async void PhoneItemTapped(object sender, ItemTappedEventArgs e)
        {
            var lead = e.Item as PhoneContact;
            if (_isFromPostCard)
            {
                var page = new PostCardRecipientPage(lead);
                await Navigation.PushAsync(page);
            }
            else
            {
                var page = new RecipientAddressPage(lead);
                await Navigation.PushAsync(page);
            }
        }
        async void AzTapped(object sender, EventArgs args)
        {
            await ShowAll();
        }
        void ZaTapped(object sender, EventArgs args)
        {
            ShowMeeting();
        }
        private async Task ShowAll()
        {
            searchBar.IsVisible = false;
            var list = await App.RecentlyUsedContactDatabase.GetItemsAsync();
            _recentList = list;
            PhoneListView.ItemsSource = _recentList;
            SegmentedTabButtonManager.SelectTab(AZTab);
            SegmentedTabButtonManager.DeselectTab(ZATab);

        }
        private void ShowMeeting()
        {
            searchBar.IsVisible = true;
            if (_phoneList.Any())
            {
                _phoneList = _phoneList.OrderBy(e => e.Name);
            }
            PhoneListView.ItemsSource = _phoneList;
            SegmentedTabButtonManager.SelectTab(ZATab);
            SegmentedTabButtonManager.DeselectTab(AZTab);

        }

    }
}
