﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class MapSelectLocationPage : ContentPage
    {
        private string _address;
        private int _numCopies = 1;
        public MapSelectLocationPage(string address)
        {
            InitializeComponent();
            _address = address;
            checkBoxTerms.CheckedChanged += (sender, e) =>
            {
                if (e.Value)
                {
                    termsLayout.BackgroundColor = Color.FromHex("#D99694");
                    printButtonQueue.Source = "printer_aftercheck";
                }
                else
                {
                    printButtonQueue.Source = "printer_queue";
                }

            };
            colorBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    blckBtn.Checked = false;
                }
            };
            blckBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    colorBtn.Checked = false;
                }

            };
        }
        private void TappedTerms(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.managedmailservice.co.uk/terms-of-service/"));
        }

        async void ColoredDirectedButton_Clicked(System.Object sender, System.EventArgs args)
        {
            if (checkBoxTerms.IsChecked)
            {
                await Navigation.PushAsync(new MapDonePage(_address));

            }
            else
            {
                termsLayout.BackgroundColor = Color.FromHex("#F9E99B");
            }

        }
        void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            if (_numCopies < 5)
            {
                _numCopies += 1;
                numLabel.Text = _numCopies.ToString();
                System.Diagnostics.Debug.WriteLine("asdadasd");
            }
        }

        void TapGestureRecognizer_Tapped_1(System.Object sender, System.EventArgs e)
        {
            if (_numCopies > 1)
            {
                _numCopies = _numCopies - 1;
                numLabel.Text = _numCopies.ToString();
            }
            System.Diagnostics.Debug.WriteLine("asdxxxxadasd");
        }
    }
}
