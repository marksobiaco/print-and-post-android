﻿using Com.OneSignal;
using Plugin.Connectivity;
using Umbrella.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotificationsPage : ContentPage
    {
        public NotificationsPage()
        {
            InitializeComponent();
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                var defNotification = DependencyService.Get<ICredentialRetriever>().GetDefaultNotification();
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (defNotification.GeneralNotif == "Yes" || defNotification.GeneralNotif == "Oui")
                {
                    OneSignal.Current.SendTag("general", credential.UserID);
                }
                else
                {
                    OneSignal.Current.DeleteTag("general");
                }
                if (defNotification.NewLeadNotif == "Yes" || defNotification.NewLeadNotif == "Oui")
                {
                    OneSignal.Current.SendTag("signed_alerts", credential.UserID);
                }
                else
                {
                    OneSignal.Current.DeleteTag("signed_alerts");
                }
                if (defNotification.NewMessageNotif == "Yes" || defNotification.NewMessageNotif == "Oui")
                {
                    OneSignal.Current.SendTag("special_delivery", credential.UserID);
                }
                else
                {
                    OneSignal.Current.DeleteTag("special_delivery");
                }
                if (defNotification.LeadConvertedNotif == "Yes" || defNotification.LeadConvertedNotif == "Oui")
                {
                    OneSignal.Current.SendTag("refer_earn_updates", credential.UserID);
                }
                else
                {
                    OneSignal.Current.DeleteTag("refer_earn_updates");
                }

            };
        }

        private void NotificationsItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e == null)
            {
                return;
            }
            NotificationsListView.SelectedItem = null;
        }
    }
}
