﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipientAddressPage : ContentPage
    {
        private PhoneContact _phoneContact;
        private APIHelper2 _apiHelper;
        private string _str = "";
        public RecipientAddressPage()
        {
            InitializeComponent();
            udpateBtn.IsVisible = false;
         
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                descTxt.Text = AppResources.RecipientPageDesc;
                labelTxt.Text = AppResources.RecipientPageTitle;
                FirstNameEntry.Placeholder = AppResources.RecipientPageFirstname;
                LastNameEntry.Placeholder = AppResources.RecipientPageLastname;
                adline1.Placeholder = AppResources.RecipientPageAdd1;
                adline2.Placeholder = AppResources.RecipientPageAdd2;
                cityText.Placeholder = AppResources.RecipientPageCity;
                postCodeText.Placeholder = AppResources.RecipientPagePostcode;
                townText.Placeholder = AppResources.RecipientPageTown;
                countryText.Placeholder = AppResources.RecipientPageCounty;
                accountDesc.Text = AppResources.RecipientPageSecDesc;
                nextButton.Text = AppResources.RecipientPageNext;
                prevButton.Text = AppResources.RecipientPageBack;
                udpateBtn.Text = AppResources.RecipientPageUpdate;
            }
            else
            {
                descTxt.Text = "Please confirm the UK address you woud like this PDF printed and posted too..";
                labelTxt.Text = "RECIPIENT'S ADDRESS";
            }
            _apiHelper = new APIHelper2();
            _phoneContact = new PhoneContact();
            customProgbar.Progress = 0.50;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                descTxt.FontSize = 15;
                Indicator.TextSize = 30;
                accountDesc.FontSize = 15;
                OrganisationEntry.HeightRequest = 35;
                OrganisationEntry.FontSize = 14;
                FirstNameEntry.HeightRequest = 35;
                FirstNameEntry.FontSize = 14;
                LastNameEntry.HeightRequest = 35;
                LastNameEntry.FontSize = 14;
                adline1.HeightRequest = 35;
                adline1.FontSize = 14;
                adline2.HeightRequest = 35;
                adline2.FontSize = 14;
                townText.HeightRequest = 35;
                townText.FontSize = 14;
                countryText.HeightRequest = 35;
                countryText.FontSize = 14;
                cityText.HeightRequest = 35;
                cityText.FontSize = 14;
                postCodeText.HeightRequest = 35;
                postCodeText.FontSize = 14;
            }
            else if (sizeChecker > 700)
            {
                descTxt.FontSize = 17;
                accountDesc.FontSize = 17;
                Indicator.TextSize = 35;
                OrganisationEntry.HeightRequest = 45;
                OrganisationEntry.FontSize = 16;
                FirstNameEntry.HeightRequest = 45;
                FirstNameEntry.FontSize = 16;
                LastNameEntry.HeightRequest = 45;
                LastNameEntry.FontSize = 16;
                adline1.HeightRequest = 45;
                adline1.FontSize = 16;
                adline2.HeightRequest = 45;
                adline2.FontSize = 16;
                townText.HeightRequest = 45;
                townText.FontSize = 16;
                countryText.HeightRequest = 45;
                countryText.FontSize = 16;
                cityText.HeightRequest = 45;
                cityText.FontSize = 16;
                postCodeText.HeightRequest = 45;
                postCodeText.FontSize = 16;
            }
            else
            {
                Indicator.TextSize = 25;
                OrganisationEntry.HeightRequest = 35;
                OrganisationEntry.FontSize = 14;
                FirstNameEntry.HeightRequest = 35;
                FirstNameEntry.FontSize = 14;
                LastNameEntry.HeightRequest = 35;
                LastNameEntry.FontSize = 14;
                descTxt.FontSize = 15;
                accountDesc.FontSize = 15;
                adline1.HeightRequest = 35;
                adline1.FontSize = 14;
                adline2.HeightRequest = 35;
                adline2.FontSize = 14;
                townText.HeightRequest = 35;
                townText.FontSize = 14;
                countryText.HeightRequest = 35;
                countryText.FontSize = 14;
                cityText.HeightRequest = 35;
                cityText.FontSize = 14;
                postCodeText.HeightRequest = 35;
                postCodeText.FontSize = 14;
            }
        }
        public RecipientAddressPage(string send)
        {
            InitializeComponent();
            _apiHelper = new APIHelper2();
            _str = send;
            udpateBtn.IsVisible = true;
            labelTxt.Text = "YOUR ADDRESS";
            udpateBtn.Opacity = 1;
            descTxt.Text = "Showing an out of date address?";
            customProgbar.Progress = 0.50;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                FirstNameEntry.Placeholder = AppResources.RecipientPageFirstname;
                LastNameEntry.Placeholder = AppResources.RecipientPageLastname;
                adline1.Placeholder = AppResources.RecipientPageAdd1;
                adline2.Placeholder = AppResources.RecipientPageAdd2;
                cityText.Placeholder = AppResources.RecipientPageCity;
                postCodeText.Placeholder = AppResources.RecipientPagePostcode;
                townText.Placeholder = AppResources.RecipientPageTown;
                countryText.Placeholder = AppResources.RecipientPageCounty;
                accountDesc.Text = AppResources.RecipientPageSecDesc;
                nextButton.Text = AppResources.RecipientPageNext;
                prevButton.Text = AppResources.RecipientPageBack;
                udpateBtn.Text = AppResources.RecipientPageUpdate;
            }
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                descTxt.FontSize = 11;
                Indicator.TextSize = 30;
                accountDesc.FontSize = 15;
                OrganisationEntry.HeightRequest = 35;
                OrganisationEntry.FontSize = 14;
                FirstNameEntry.HeightRequest = 35;
                FirstNameEntry.FontSize = 14;
                LastNameEntry.HeightRequest = 35;
                LastNameEntry.FontSize = 14;
                adline1.HeightRequest = 35;
                adline1.FontSize = 14;
                adline2.HeightRequest = 35;
                adline2.FontSize = 14;
                townText.HeightRequest = 35;
                townText.FontSize = 14;
                countryText.HeightRequest = 35;
                countryText.FontSize = 14;
                cityText.HeightRequest = 35;
                cityText.FontSize = 14;
                postCodeText.HeightRequest = 35;
                postCodeText.FontSize = 14;
            }
            else if (sizeChecker > 700)
            {
                descTxt.FontSize = 15;
                accountDesc.FontSize = 17;
                OrganisationEntry.HeightRequest = 45;
                OrganisationEntry.FontSize = 16;
                FirstNameEntry.HeightRequest = 45;
                FirstNameEntry.FontSize = 16;
                LastNameEntry.HeightRequest = 45;
                LastNameEntry.FontSize = 16;
                adline1.HeightRequest = 45;
                adline1.FontSize = 16;
                adline2.HeightRequest = 45;
                adline2.FontSize = 16;
                townText.HeightRequest = 45;
                townText.FontSize = 16;
                countryText.HeightRequest = 45;
                countryText.FontSize = 16;
                cityText.HeightRequest = 45;
                cityText.FontSize = 16;
                postCodeText.HeightRequest = 45;
                postCodeText.FontSize = 16;
                Indicator.TextSize = 35;
            }
            else
            {
                Indicator.TextSize = 25;
                OrganisationEntry.HeightRequest = 35;
                OrganisationEntry.FontSize = 14;
                FirstNameEntry.HeightRequest = 35;
                FirstNameEntry.FontSize = 14;
                LastNameEntry.HeightRequest = 35;
                LastNameEntry.FontSize = 14;
                descTxt.FontSize = 11;
                accountDesc.FontSize = 15;
                adline1.HeightRequest = 35;
                adline1.FontSize = 14;
                adline2.HeightRequest = 35;
                adline2.FontSize = 14;
                townText.HeightRequest = 35;
                townText.FontSize = 14;
                countryText.HeightRequest = 35;
                countryText.FontSize = 14;
                cityText.HeightRequest = 35;
                cityText.FontSize = 14;
                postCodeText.HeightRequest = 35;
                postCodeText.FontSize = 14;
            }
        }
        public RecipientAddressPage(string first, string last, string org, DetailedAddress address)
        {
            InitializeComponent();
            udpateBtn.IsVisible = false;
            var detailAddr = new DetailedAddress();
            detailAddr = address;
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                descTxt.Text = AppResources.RecipientPageDesc;
                labelTxt.Text = AppResources.RecipientPageTitle;
                FirstNameEntry.Placeholder = AppResources.RecipientPageFirstname;
                LastNameEntry.Placeholder = AppResources.RecipientPageLastname;
                adline1.Placeholder = AppResources.RecipientPageAdd1;
                adline2.Placeholder = AppResources.RecipientPageAdd2;
                cityText.Placeholder = AppResources.RecipientPageCity;
                postCodeText.Placeholder = AppResources.RecipientPagePostcode;
                townText.Placeholder = AppResources.RecipientPageTown;
                countryText.Placeholder = AppResources.RecipientPageCounty;
                accountDesc.Text = AppResources.RecipientPageSecDesc;
                nextButton.Text = AppResources.RecipientPageNext;
                prevButton.Text = AppResources.RecipientPageBack;
                udpateBtn.Text = AppResources.RecipientPageUpdate;
            }
            else
            {
                descTxt.Text = "Please confirm the UK address you woud like this PDF printed and posted too..";
                labelTxt.Text = "RECIPIENT'S ADDRESS";
            }
            _apiHelper = new APIHelper2();

            if (string.IsNullOrEmpty(first) && string.IsNullOrEmpty(org))
            {
                FirstNameEntry.Text = "Sir/Madam";
            }
            else
            {
                FirstNameEntry.Text = first;
                LastNameEntry.Text = last;
            }
            _phoneContact = new PhoneContact();
            adline1.Text = detailAddr.AddLine1;
            adline2.Text = detailAddr.AddLine2;
            postCodeText.Text = detailAddr.PostalCode;
            countryText.Text = detailAddr.Locality;
            cityText.Text = detailAddr.Region;

            OrganisationEntry.Text = org;
            customProgbar.Progress = 0.50;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                descTxt.FontSize = 15;
                accountDesc.FontSize = 15;
                OrganisationEntry.HeightRequest = 35;
                OrganisationEntry.FontSize = 14;
                FirstNameEntry.HeightRequest = 35;
                FirstNameEntry.FontSize = 14;
                LastNameEntry.HeightRequest = 35;
                LastNameEntry.FontSize = 14;
                adline1.HeightRequest = 35;
                adline1.FontSize = 14;
                adline2.HeightRequest = 35;
                adline2.FontSize = 14;
                townText.HeightRequest = 35;
                townText.FontSize = 14;
                countryText.HeightRequest = 35;
                countryText.FontSize = 14;
                cityText.HeightRequest = 35;
                cityText.FontSize = 14;
                postCodeText.HeightRequest = 35;
                postCodeText.FontSize = 14;
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                descTxt.FontSize = 17;
                accountDesc.FontSize = 17;
                Indicator.TextSize = 35;
                OrganisationEntry.HeightRequest = 45;
                OrganisationEntry.FontSize = 16;
                FirstNameEntry.HeightRequest = 45;
                FirstNameEntry.FontSize = 16;
                LastNameEntry.HeightRequest = 45;
                LastNameEntry.FontSize = 16;
                adline1.HeightRequest = 45;
                adline1.FontSize = 16;
                adline2.HeightRequest = 45;
                adline2.FontSize = 16;
                townText.HeightRequest = 45;
                townText.FontSize = 16;
                countryText.HeightRequest = 45;
                countryText.FontSize = 16;
                cityText.HeightRequest = 45;
                cityText.FontSize = 16;
                postCodeText.HeightRequest = 45;
                postCodeText.FontSize = 16;
            }
            else
            {
                Indicator.TextSize = 25;
                OrganisationEntry.HeightRequest = 35;
                OrganisationEntry.FontSize = 14;
                FirstNameEntry.HeightRequest = 35;
                FirstNameEntry.FontSize = 14;
                LastNameEntry.HeightRequest = 35;
                LastNameEntry.FontSize = 14;
                descTxt.FontSize = 15;
                accountDesc.FontSize = 15;
                adline1.HeightRequest = 35;
                adline1.FontSize = 14;
                adline2.HeightRequest = 35;
                adline2.FontSize = 14;
                townText.HeightRequest = 35;
                townText.FontSize = 14;
                countryText.HeightRequest = 35;
                countryText.FontSize = 14;
                cityText.HeightRequest = 35;
                cityText.FontSize = 14;
                postCodeText.HeightRequest = 35;
                postCodeText.FontSize = 14;
            }
        }
        public RecipientAddressPage(PhoneContact phone)
        {
            InitializeComponent();
            udpateBtn.IsVisible = false;
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                descTxt.Text = AppResources.RecipientPageDesc;
                labelTxt.Text = AppResources.RecipientPageTitle;
                FirstNameEntry.Placeholder = AppResources.RecipientPageFirstname;
                LastNameEntry.Placeholder = AppResources.RecipientPageLastname;
                adline1.Placeholder = AppResources.RecipientPageAdd1;
                adline2.Placeholder = AppResources.RecipientPageAdd2;
                cityText.Placeholder = AppResources.RecipientPageCity;
                postCodeText.Placeholder = AppResources.RecipientPagePostcode;
                townText.Placeholder = AppResources.RecipientPageTown;
                countryText.Placeholder = AppResources.RecipientPageCounty;
                accountDesc.Text = AppResources.RecipientPageSecDesc;
                nextButton.Text = AppResources.RecipientPageNext;
                prevButton.Text = AppResources.RecipientPageBack;
                udpateBtn.Text = AppResources.RecipientPageUpdate;
            }
            else
            {
                descTxt.Text = "Please confirm the UK address you woud like this PDF printed and posted too..";
                labelTxt.Text = "RECIPIENT'S ADDRESS";
            }
            _apiHelper = new APIHelper2();
            _phoneContact = new PhoneContact();
            _phoneContact = phone;
            adline1.Text = _phoneContact.Address;
            adline2.Text = _phoneContact.Address2;
            postCodeText.Text = _phoneContact.PostalCode;
            townText.Text = _phoneContact.State;
            countryText.Text = _phoneContact.Country;
            cityText.Text = _phoneContact.City;
            FirstNameEntry.Text = _phoneContact.FirstName;
            LastNameEntry.Text = _phoneContact.LastName;
            OrganisationEntry.Text = _phoneContact.CompanyName;
            customProgbar.Progress = 0.50;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                descTxt.FontSize = 15;
                accountDesc.FontSize = 15;
                OrganisationEntry.HeightRequest = 35;
                OrganisationEntry.FontSize = 14;
                FirstNameEntry.HeightRequest = 35;
                FirstNameEntry.FontSize = 14;
                Indicator.TextSize = 30;
                LastNameEntry.HeightRequest = 35;
                LastNameEntry.FontSize = 14;
                adline1.HeightRequest = 35;
                adline1.FontSize = 14;
                adline2.HeightRequest = 35;
                adline2.FontSize = 14;
                townText.HeightRequest = 35;
                townText.FontSize = 14;
                countryText.HeightRequest = 35;
                countryText.FontSize = 14;
                cityText.HeightRequest = 35;
                cityText.FontSize = 14;
                postCodeText.HeightRequest = 35;
                postCodeText.FontSize = 14;
            }
            else if (sizeChecker > 700)
            {
                descTxt.FontSize = 17;
                accountDesc.FontSize = 17;
                Indicator.TextSize = 35;
                OrganisationEntry.HeightRequest = 45;
                OrganisationEntry.FontSize = 16;
                FirstNameEntry.HeightRequest = 45;
                FirstNameEntry.FontSize = 16;
                LastNameEntry.HeightRequest = 45;
                LastNameEntry.FontSize = 16;
                adline1.HeightRequest = 45;
                adline1.FontSize = 16;
                adline2.HeightRequest = 45;
                adline2.FontSize = 16;
                townText.HeightRequest = 45;
                townText.FontSize = 16;
                countryText.HeightRequest = 45;
                countryText.FontSize = 16;
                cityText.HeightRequest = 45;
                cityText.FontSize = 16;
                postCodeText.HeightRequest = 45;
                postCodeText.FontSize = 16;
            }
            else
            {
                Indicator.TextSize = 25;
                OrganisationEntry.HeightRequest = 35;
                OrganisationEntry.FontSize = 14;
                FirstNameEntry.HeightRequest = 35;
                FirstNameEntry.FontSize = 14;
                LastNameEntry.HeightRequest = 35;
                LastNameEntry.FontSize = 14;
                descTxt.FontSize = 15;
                accountDesc.FontSize = 15;
                adline1.HeightRequest = 35;
                adline1.FontSize = 14;
                adline2.HeightRequest = 35;
                adline2.FontSize = 14;
                townText.HeightRequest = 35;
                townText.FontSize = 14;
                countryText.HeightRequest = 35;
                countryText.FontSize = 14;
                cityText.HeightRequest = 35;
                cityText.FontSize = 14;
                postCodeText.HeightRequest = 35;
                postCodeText.FontSize = 14;
            }
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            customProgbar.Progress = 0.50;
            if (_str == "send")
            {

                if (CrossConnectivity.Current.IsConnected)
                {
                    var local = await App.OntraportAddressDatabase.GetItemsAsync();
                    System.Diagnostics.Debug.WriteLine(local.Count);
                    if (!local.Any())
                    {
                        StartBusyIndicator();
                        var apiHelper = await Task.Run(() => _apiHelper.GetAddresDetails());
                        _phoneContact = new PhoneContact();
                        if (apiHelper.Any())
                        {
                            _phoneContact.Address = apiHelper.FirstOrDefault().address;
                            _phoneContact.Address2 = apiHelper.FirstOrDefault().address2;
                            _phoneContact.City = apiHelper.FirstOrDefault().city;
                            _phoneContact.State = apiHelper.FirstOrDefault().Town_340;
                            _phoneContact.Country = apiHelper.FirstOrDefault().County_456;
                            _phoneContact.LastName = apiHelper.FirstOrDefault().lastname;
                            _phoneContact.FirstName = apiHelper.FirstOrDefault().firstname;
                            _phoneContact.CompanyName = apiHelper.FirstOrDefault().company;
                            _phoneContact.PostalCode = apiHelper.FirstOrDefault().zip;
                            adline1.Text = _phoneContact.Address;
                            adline2.Text = _phoneContact.Address2;
                            postCodeText.Text = _phoneContact.PostalCode;
                            townText.Text = _phoneContact.State;
                            countryText.Text = _phoneContact.Country;
                            cityText.Text = _phoneContact.City;
                            FirstNameEntry.Text = _phoneContact.FirstName;
                            LastNameEntry.Text = _phoneContact.LastName;
                            OrganisationEntry.Text = _phoneContact.CompanyName;
                            await App.OntraportAddressDatabase.SaveSingleOntraportJsonAddressAsync(apiHelper.FirstOrDefault());
                        }
                    }
                    else
                    {
                        var localAdd = await App.OntraportAddressDatabase.GetItemsAsync();
                        _phoneContact = new PhoneContact();
                        _phoneContact.Address = localAdd.FirstOrDefault().address;
                        _phoneContact.Address2 = localAdd.FirstOrDefault().address2;
                        _phoneContact.City = localAdd.FirstOrDefault().city;
                        _phoneContact.State = localAdd.FirstOrDefault().Town_340;
                        _phoneContact.Country = localAdd.FirstOrDefault().County_456;
                        _phoneContact.LastName = localAdd.FirstOrDefault().lastname;
                        _phoneContact.FirstName = localAdd.FirstOrDefault().firstname;
                        _phoneContact.CompanyName = localAdd.FirstOrDefault().company;
                        _phoneContact.PostalCode = localAdd.FirstOrDefault().zip;
                        adline1.Text = _phoneContact.Address;
                        adline2.Text = _phoneContact.Address2;
                        postCodeText.Text = _phoneContact.PostalCode;
                        townText.Text = _phoneContact.State;
                        countryText.Text = _phoneContact.Country;
                        cityText.Text = _phoneContact.City;
                        FirstNameEntry.Text = _phoneContact.FirstName;
                        LastNameEntry.Text = _phoneContact.LastName;
                        OrganisationEntry.Text = _phoneContact.CompanyName;
                    }
                }
                else
                {
                    var apiHelper = await App.OntraportAddressDatabase.GetItemsAsync();
                    _phoneContact = new PhoneContact();
                    if (apiHelper.Any())
                    {
                        _phoneContact.Address = apiHelper.FirstOrDefault().address;
                        _phoneContact.Address2 = apiHelper.FirstOrDefault().address2;
                        _phoneContact.City = apiHelper.FirstOrDefault().city;
                        _phoneContact.State = apiHelper.FirstOrDefault().Town_340;
                        _phoneContact.Country = apiHelper.FirstOrDefault().County_456;
                        _phoneContact.LastName = apiHelper.FirstOrDefault().lastname;
                        _phoneContact.FirstName = apiHelper.FirstOrDefault().firstname;
                        _phoneContact.CompanyName = apiHelper.FirstOrDefault().company;
                        _phoneContact.PostalCode = apiHelper.FirstOrDefault().zip;
                        adline1.Text = _phoneContact.Address;
                        adline2.Text = _phoneContact.Address2;
                        postCodeText.Text = _phoneContact.PostalCode;
                        townText.Text = _phoneContact.State;
                        countryText.Text = _phoneContact.Country;
                        cityText.Text = _phoneContact.City;
                        FirstNameEntry.Text = _phoneContact.FirstName;
                        LastNameEntry.Text = _phoneContact.LastName;
                        OrganisationEntry.Text = _phoneContact.CompanyName;
                    }
                    
                }

            }
            StopBusyIndicator();
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private async void NextTapped(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(FirstNameEntry.Text) ||  string.IsNullOrEmpty(countryText.Text) || string.IsNullOrEmpty(adline1.Text))
            {
                await DisplayAlert("", "Please fill up the necessary fields.", "OK");
                if (string.IsNullOrEmpty(FirstNameEntry.Text))
                {
                    FirstNameEntry.IsBorderErrorVisible = true;
                    FirstNameEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (string.IsNullOrEmpty(countryText.Text))
                {
                    countryText.IsBorderErrorVisible = true;
                    countryText.BackgroundColor = Color.FromHex("#F9E99B");

                }
                if (string.IsNullOrEmpty(adline1.Text))
                {
                    adline1.IsBorderErrorVisible = true;
                    adline1.BackgroundColor = Color.FromHex("#F9E99B");

                }
            }
            else
            {
                _phoneContact.Address = adline1.Text;
                _phoneContact.Address2 = adline2.Text;
                _phoneContact.PostalCode = postCodeText.Text;
                _phoneContact.State = townText.Text;
                _phoneContact.Country = countryText.Text;
                _phoneContact.City = cityText.Text;
                _phoneContact.FirstName = FirstNameEntry.Text;
                _phoneContact.LastName = LastNameEntry.Text;
                _phoneContact.CompanyName = OrganisationEntry.Text;

                if (string.IsNullOrEmpty(OrganisationEntry.Text))
                {
                    System.Diagnostics.Debug.WriteLine("no leaflet");
                    await Navigation.PushAsync(new PDFDetailsPage(_phoneContact));

                }
                else if (!string.IsNullOrEmpty(_str))
                {
                    await Navigation.PushAsync(new PDFDetailsPage(_phoneContact));
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("must have leaflet");
                    await Navigation.PushAsync(new PDFDetailsPage(_phoneContact, true));

                }
            }
        }
        private async void UpdateTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                StartBusyIndicator();
                var apiHelper = await Task.Run(() => _apiHelper.GetAddresDetails());
                _phoneContact = new PhoneContact();
                if (apiHelper.Any())
                {
                    _phoneContact.Address = apiHelper.FirstOrDefault().address;
                    _phoneContact.Address2 = apiHelper.FirstOrDefault().address2;
                    _phoneContact.City = apiHelper.FirstOrDefault().city;
                    _phoneContact.State = apiHelper.FirstOrDefault().Town_340;
                    _phoneContact.Country = apiHelper.FirstOrDefault().County_456;
                    _phoneContact.LastName = apiHelper.FirstOrDefault().lastname;
                    _phoneContact.FirstName = apiHelper.FirstOrDefault().firstname;
                    _phoneContact.CompanyName = apiHelper.FirstOrDefault().company;
                    _phoneContact.PostalCode = apiHelper.FirstOrDefault().zip;
                    adline1.Text = _phoneContact.Address;
                    adline2.Text = _phoneContact.Address2;
                    postCodeText.Text = _phoneContact.PostalCode;
                    townText.Text = _phoneContact.State;
                    countryText.Text = _phoneContact.Country;
                    cityText.Text = _phoneContact.City;
                    FirstNameEntry.Text = _phoneContact.FirstName;
                    LastNameEntry.Text = _phoneContact.LastName;
                    OrganisationEntry.Text = _phoneContact.CompanyName;
                    await App.OntraportAddressDatabase.SaveSingleOntraportJsonAddressAsync(apiHelper.FirstOrDefault());
                }
            }
            else
            {
                await DisplayAlert("", "Internet Connection Required", "OK");
            }
            StopBusyIndicator();
        }
        private async void BackTapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}