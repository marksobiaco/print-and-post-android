﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PDFDetailsPage : ContentPage
    {
        private string[] SelectionArray = { "Standard Dispatch", "Priority Dispatch" };
        private PhoneContact _phoneContact;
        APIHelper2 apiHelper;
        private List<FileUpload> fileIdList;
        private bool _isLowQuality;
        private Credential account;
        public PDFDetailsPage(PhoneContact phoneContact)
        {
            InitializeComponent();
            account = DependencyService.Get<ICredentialRetriever>().GetCredential();
            nextButton.IsEnableButton = CrossConnectivity.Current.IsConnected;
            fileIdList = new List<FileUpload>();
            apiHelper = new APIHelper2();
            leafletLayout.IsVisible = false;
            Global.SetUploadPageType("");
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                nextButton.IsEnableButton = args.IsConnected ? true : false;
            };
            customProgbar.Progress = 0.75;
            var defaults = DependencyService.Get<ICredentialRetriever>().GetDefault();
            System.Diagnostics.Debug.WriteLine("df" + defaults.InkType);


            if (account.User.Username == "juju@raptorsms.com")
            {
                colourText.Text = AppResources.PostcardDetailsColor;
                blackWhiteText.Text = AppResources.PostcardDetailsBW;
                System.Diagnostics.Debug.WriteLine("asdasdfff " + AppResources.PostcardDetailsEditorText + " " + defaults.DispatchSpeed);
                compText.Text = AppResources.PostcardDetailsComputer;
                handText.Text = AppResources.PostcardDetailsHand;
                firstClassTxt.Text = AppResources.PostcardDetails1st;
                signedForTxt.Text = AppResources.PostcardDetailsSigned;
                postAsapText.Text = AppResources.PostcardDetailsPostAsap;
                postLaterText.Text = AppResources.PostcardDetailsPostLater;
                dispatchDesc.Text = AppResources.PostcardDetailsDispatchDesc;
                dispatchTitle.Text = AppResources.PostcardDetailsDispatch;
                prevButton.Text = AppResources.PostcardDetailsBackBtn;
                nextButton.Text = AppResources.PostcardDetailsPostSendBtn;
                secBtnText.Text = AppResources.PostcardDetailsCheapest;
                if (!string.IsNullOrEmpty(defaults.DispatchSpeed))
                {
                    if (defaults.DispatchSpeed == "Standard")
                    {
                        urgentDropdown.SelectedText = "Normal";
                    }
                    else
                    {
                        urgentDropdown.SelectedText = "Prioritaire";
                    }
                }
                else
                {
                    urgentDropdown.SelectedText = "Normal";
                }

            }
            else
            {
                urgentDropdown.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard Dispatch" : defaults.DispatchSpeed + " Dispatch";

            }
            _phoneContact = new PhoneContact();
            _phoneContact = phoneContact;
            DatePick.MinDate = DateTime.Now.AddDays(3);
            standardCheapestBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    firstClassBtn.Checked = false;
                    signedForBtn.Checked = false;
                }
            };
            firstClassBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    standardCheapestBtn.Checked = false;
                    signedForBtn.Checked = false;
                }
            };
            signedForBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    firstClassBtn.Checked = false;
                    standardCheapestBtn.Checked = false;
                }
            };
            postLaterBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    postNowBtn.Checked = false;
                }
            };
            postNowBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    postLaterBtn.Checked = false;
                }

            };
            colorBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    blckBtn.Checked = false;
                }
            };
            blckBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    colorBtn.Checked = false;
                }

            };

            DatePick.DateSelected += (sender, args) =>
            {
                postNowBtn.Checked = false;
                postLaterBtn.Checked = true;
            };
            handBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    printBtn.Checked = false;
                }

            };
            printBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    handBtn.Checked = false;
                }

            };
            if (defaults.InkType == "Black & White" || defaults.InkType == "Noir et Blanc")
            {
                blckBtn.Checked = true;
            }
            else
            {
                colorBtn.Checked = true;

            }
            if (defaults.LabelType == "Hand Written" || defaults.InkType == "Ecrit a la main")
            {
                handBtn.Checked = true;
            }
            else
            {
                printBtn.Checked = true;
            }
            if (defaults.PostageClass == "Standard" || defaults.InkType == "Lettre Verte")
            {
                standardBtn.Checked = true;
            }
            else if (defaults.PostageClass == "1st Class" || defaults.InkType == "Lettre Prioritaire")
            {
                firstClassBtn.Checked = true;
            }
            else if (defaults.PostageClass == "Signed For" || defaults.InkType == "Avec accusé de réception")
            {
                signedForBtn.Checked = true;
            }
            else
            {
                standardBtn.Checked = true;
            }
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                blackWhiteText.FontSize = 11;
                colourText.FontSize = 11;
                handText.FontSize = 11;
                standardText.FontSize = 11;
                premText.FontSize = 11;
                recycledText.FontSize = 11;
                compText.FontSize = 11;
                postAsapText.FontSize = 11;
                postLaterText.FontSize = 11;
                dispatchDesc.FontSize = 10;
                firstClassTxt.FontSize = 11;
                signedForTxt.FontSize = 11;
                firstBtnText.FontSize = 11;
                secBtnText.FontSize = 9;
                firstClassBtn.Margin = new Thickness(0, 25, 0, 0);
                signedForBtn.Margin = new Thickness(0, 25, 0, 0);
                standardCheapestBtn.Margin = new Thickness(0, 25, 0, 0);
                colorBtn.Margin = new Thickness(0, 15, 0, 0);
                blckBtn.Margin = new Thickness(0, 15, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 15, 0, 0);
                standardBtn.Margin = new Thickness(0, 15, 0, 0);
                premiumBtn.Margin = new Thickness(0, 15, 0, 0);
                recylcBtn.Margin = new Thickness(0, 15, 0, 0);
                postNowBtn.Margin = new Thickness(0, 15, 0, 0);
                handBtn.Margin = new Thickness(0, 15, 0, 0);
                printBtn.Margin = new Thickness(0, 15, 0, 0);
                DatePick.HeightRequest = 32;
                urgentDropdown.HeightRequest = 35;
                calendarWhite.HeightRequest = 20;
                calendarWhite.WidthRequest = 20;
            }
            else if (sizeChecker > 700)
            {
                postAsapText.FontSize = 15;
                postLaterText.FontSize = 15;
                blackWhiteText.FontSize = 15;
                standardText.FontSize = 15;
                premText.FontSize = 15;
                recycledText.FontSize = 15;
                colourText.FontSize = 15;
                handText.FontSize = 15;
                compText.FontSize = 15;
                dispatchDesc.FontSize = 14;
                firstClassTxt.FontSize = 15;
                signedForTxt.FontSize = 15;
                firstBtnText.FontSize = 15;
                secBtnText.FontSize = 12;
                firstClassBtn.Margin = new Thickness(0, 35, 0, 0);
                signedForBtn.Margin = new Thickness(0, 35, 0, 0);
                standardCheapestBtn.Margin = new Thickness(0, 35, 0, 0);
                colorBtn.Margin = new Thickness(0, 25, 0, 0);
                blckBtn.Margin = new Thickness(0, 25, 0, 0);
                handBtn.Margin = new Thickness(0, 25, 0, 0);
                printBtn.Margin = new Thickness(0, 25, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 25, 0, 0);
                postNowBtn.Margin = new Thickness(0, 25, 0, 0);
                standardBtn.Margin = new Thickness(0, 25, 0, 0);
                premiumBtn.Margin = new Thickness(0, 25, 0, 0);
                recylcBtn.Margin = new Thickness(0, 25, 0, 0);
                DatePick.HeightRequest = 40;
                urgentDropdown.HeightRequest = 40;
                calendarWhite.HeightRequest = 30;
                calendarWhite.WidthRequest = 30;
            }
            else
            {
                handText.FontSize = 11;
                compText.FontSize = 11;
                postAsapText.FontSize = 11;
                standardText.FontSize = 11;
                premText.FontSize = 11;
                recycledText.FontSize = 11;
                postLaterText.FontSize = 11;
                blackWhiteText.FontSize = 11;
                colourText.FontSize = 11;
                dispatchDesc.FontSize = 10;
                firstClassTxt.FontSize = 11;
                signedForTxt.FontSize = 11;
                firstBtnText.FontSize = 11;
                secBtnText.FontSize = 9;
                firstClassBtn.Margin = new Thickness(0, 25, 0, 0);
                signedForBtn.Margin = new Thickness(0, 25, 0, 0);
                standardCheapestBtn.Margin = new Thickness(0, 25, 0, 0);
                standardBtn.Margin = new Thickness(0, 15, 0, 0);
                premiumBtn.Margin = new Thickness(0, 15, 0, 0);
                recylcBtn.Margin = new Thickness(0, 15, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 15, 0, 0);
                postNowBtn.Margin = new Thickness(0, 15, 0, 0);
                blckBtn.Margin = new Thickness(0, 15, 0, 0);
                colorBtn.Margin = new Thickness(0, 15, 0, 0);
                handBtn.Margin = new Thickness(0, 15, 0, 0);
                printBtn.Margin = new Thickness(0, 15, 0, 0);
                DatePick.HeightRequest = 32;
                urgentDropdown.HeightRequest = 35;
                calendarWhite.HeightRequest = 20;
                calendarWhite.WidthRequest = 20;
            }
        }
        public PDFDetailsPage(PhoneContact phoneContact, bool noOrg)
        {
            InitializeComponent();
            nextButton.IsEnableButton = CrossConnectivity.Current.IsConnected;
            fileIdList = new List<FileUpload>();
            apiHelper = new APIHelper2();
            leafletLayout.IsVisible = true;
            Global.SetUploadPageType("");
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                nextButton.IsEnableButton = args.IsConnected ? true : false;
            };
            customProgbar.Progress = 0.75;
            var defaults = DependencyService.Get<ICredentialRetriever>().GetDefault();
            System.Diagnostics.Debug.WriteLine("df" + defaults.InkType);


            account = DependencyService.Get<ICredentialRetriever>().GetCredential();
            System.Diagnostics.Debug.WriteLine("df" + defaults.InkType);
            if (account.User.Username == "juju@raptorsms.com")
            {
                try
                {
                    colourText.Text = AppResources.PostcardDetailsColor;
                    blackWhiteText.Text = AppResources.PostcardDetailsBW;
                    System.Diagnostics.Debug.WriteLine("asdasd " + AppResources.PostcardDetailsEditorText + " " + defaults.DispatchSpeed);
                    compText.Text = AppResources.PostcardDetailsComputer;
                    handText.Text = AppResources.PostcardDetailsHand;
                    firstClassTxt.Text = AppResources.PostcardDetails1st;
                    signedForTxt.Text = AppResources.PostcardDetailsSigned;
                    postAsapText.Text = AppResources.PostcardDetailsPostAsap;
                    postLaterText.Text = AppResources.PostcardDetailsPostLater;
                    dispatchDesc.Text = AppResources.PostcardDetailsDispatchDesc;
                    dispatchTitle.Text = AppResources.PostcardDetailsDispatch;
                    prevButton.Text = AppResources.PostcardDetailsBackBtn;
                    nextButton.Text = AppResources.PostcardDetailsPostSendBtn;
                    secBtnText.Text = AppResources.PostcardDetailsCheapest;
                    if (!string.IsNullOrEmpty(defaults.DispatchSpeed))
                    {
                        if (defaults.DispatchSpeed == "Standard")
                        {
                            urgentDropdown.SelectedText = "Normal";
                        }
                        else
                        {
                            urgentDropdown.SelectedText = "Prioritaire";
                        }
                    }
                    else
                    {
                        urgentDropdown.SelectedText = "Normal";
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("ex " + ex.InnerException);
                    colourText.Text = AppResources.PostcardDetailsColor;
                    blackWhiteText.Text = AppResources.PostcardDetailsBW;
                    System.Diagnostics.Debug.WriteLine("asdasd " + AppResources.PostcardDetailsEditorText);
                    compText.Text = AppResources.PostcardDetailsComputer;
                    handText.Text = AppResources.PostcardDetailsHand;
                    firstClassTxt.Text = AppResources.PostcardDetails1st;
                    signedForTxt.Text = AppResources.PostcardDetailsSigned;
                    postAsapText.Text = AppResources.PostcardDetailsPostAsap;
                    postLaterText.Text = AppResources.PostcardDetailsPostLater;
                    dispatchDesc.Text = AppResources.PostcardDetailsDispatchDesc;
                    dispatchTitle.Text = AppResources.PostcardDetailsDispatch;
                    prevButton.Text = AppResources.PostcardDetailsBackBtn;
                    nextButton.Text = AppResources.PostcardDetailsPostSendBtn;
                    secBtnText.Text = AppResources.PostcardDetailsCheapest;
                }

            }
            else
            {
                urgentDropdown.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard Dispatch" : defaults.DispatchSpeed + " Dispatch";
            }

            _phoneContact = new PhoneContact();
            _phoneContact = phoneContact;
            DatePick.MinDate = DateTime.Now.AddDays(3);
            standardCheapestBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    firstClassBtn.Checked = false;
                    signedForBtn.Checked = false;
                }
            };
            firstClassBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    standardCheapestBtn.Checked = false;
                    signedForBtn.Checked = false;
                }
            };
            signedForBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    firstClassBtn.Checked = false;
                    standardCheapestBtn.Checked = false;
                }
            };
            allowBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    dontAllowBtn.Checked = false;
                }
            };
            dontAllowBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    allowBtn.Checked = false;
                }

            };
            postLaterBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    postNowBtn.Checked = false;
                }
            };
            postNowBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    postLaterBtn.Checked = false;
                }

            };
            colorBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    blckBtn.Checked = false;
                }
            };
            blckBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    colorBtn.Checked = false;
                }

            };

            DatePick.DateSelected += (sender, args) =>
            {
                postNowBtn.Checked = false;
                postLaterBtn.Checked = true;
            };
            handBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    printBtn.Checked = false;
                }

            };
            printBtn.PropertyChanged += (sender, e) => {

                var check = ((Custom.RadioButtonSmall)sender).Checked;
                if (check == true)
                {
                    handBtn.Checked = false;
                }

            };
            if (defaults.InkType == "Black & White" || defaults.InkType == "Noir et Blanc")
            {
                blckBtn.Checked = true;
            }
            else
            {
                colorBtn.Checked = true;

            }
            if (defaults.LabelType == "Hand Written" || defaults.InkType == "Ecrit a la main")
            {
                handBtn.Checked = true;
            }
            else
            {
                printBtn.Checked = true;
            }
            if (defaults.PostageClass == "Standard" || defaults.InkType == "Lettre Verte")
            {
                standardBtn.Checked = true;
            }
            else if (defaults.PostageClass == "1st Class" || defaults.InkType == "Lettre Prioritaire")
            {
                firstClassBtn.Checked = true;
            }
            else if (defaults.PostageClass == "Signed For" || defaults.InkType == "Avec accusé de réception")
            {
                signedForBtn.Checked = true;
            }
            else
            {
                standardBtn.Checked = true;
            }
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                blackWhiteText.FontSize = 11;
                colourText.FontSize = 11;
                handText.FontSize = 11;
                standardText.FontSize = 11;
                premText.FontSize = 11;
                recycledText.FontSize = 11;
                compText.FontSize = 11;
                postAsapText.FontSize = 11;
                postLaterText.FontSize = 11;
                dispatchDesc.FontSize = 10;
                allowText.FontSize = 11;
                dontAllowText.FontSize = 11;
                firstClassTxt.FontSize = 11;
                signedForTxt.FontSize = 11;
                firstBtnText.FontSize = 11;
                secBtnText.FontSize = 9;
                firstClassBtn.Margin = new Thickness(0, 25, 0, 0);
                signedForBtn.Margin = new Thickness(0, 25, 0, 0);
                standardCheapestBtn.Margin = new Thickness(0, 25, 0, 0);
                allowBtn.Margin = new Thickness(0, 40, 0, 0);
                dontAllowBtn.Margin = new Thickness(0, 40, 0, 0);
                colorBtn.Margin = new Thickness(0, 15, 0, 0);
                blckBtn.Margin = new Thickness(0, 15, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 15, 0, 0);
                standardBtn.Margin = new Thickness(0, 15, 0, 0);
                premiumBtn.Margin = new Thickness(0, 15, 0, 0);
                recylcBtn.Margin = new Thickness(0, 15, 0, 0);
                postNowBtn.Margin = new Thickness(0, 15, 0, 0);
                handBtn.Margin = new Thickness(0, 15, 0, 0);
                printBtn.Margin = new Thickness(0, 15, 0, 0);
                DatePick.HeightRequest = 32;
                urgentDropdown.HeightRequest = 35;
                calendarWhite.HeightRequest = 20;
                calendarWhite.WidthRequest = 20;
            }
            else if (sizeChecker > 700)
            {
                postAsapText.FontSize = 15;
                postLaterText.FontSize = 15;
                blackWhiteText.FontSize = 15;
                standardText.FontSize = 15;
                premText.FontSize = 15;
                recycledText.FontSize = 15;
                colourText.FontSize = 15;
                handText.FontSize = 15;
                compText.FontSize = 15;
                dispatchDesc.FontSize = 14;
                allowText.FontSize = 15;
                dontAllowText.FontSize = 15;
                firstClassTxt.FontSize = 15;
                signedForTxt.FontSize = 15;
                firstBtnText.FontSize = 15;
                secBtnText.FontSize = 12;
                firstClassBtn.Margin = new Thickness(0, 35, 0, 0);
                signedForBtn.Margin = new Thickness(0, 35, 0, 0);
                standardCheapestBtn.Margin = new Thickness(0, 35, 0, 0);
                allowBtn.Margin = new Thickness(0, 40, 0, 0);
                dontAllowBtn.Margin = new Thickness(0, 40, 0, 0);
                colorBtn.Margin = new Thickness(0, 25, 0, 0);
                blckBtn.Margin = new Thickness(0, 25, 0, 0);
                handBtn.Margin = new Thickness(0, 25, 0, 0);
                printBtn.Margin = new Thickness(0, 25, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 25, 0, 0);
                postNowBtn.Margin = new Thickness(0, 25, 0, 0);
                standardBtn.Margin = new Thickness(0, 25, 0, 0);
                premiumBtn.Margin = new Thickness(0, 25, 0, 0);
                recylcBtn.Margin = new Thickness(0, 25, 0, 0);
                DatePick.HeightRequest = 40;
                urgentDropdown.HeightRequest = 40;
                calendarWhite.HeightRequest = 30;
                calendarWhite.WidthRequest = 30;
            }
            else
            {
                handText.FontSize = 11;
                compText.FontSize = 11;
                postAsapText.FontSize = 11;
                standardText.FontSize = 11;
                premText.FontSize = 11;
                recycledText.FontSize = 11;
                postLaterText.FontSize = 11;
                blackWhiteText.FontSize = 11;
                colourText.FontSize = 11;
                dispatchDesc.FontSize = 10;
                allowText.FontSize = 11;
                dontAllowText.FontSize = 11;
                firstClassTxt.FontSize = 11;
                signedForTxt.FontSize = 11;
                firstBtnText.FontSize = 11;
                secBtnText.FontSize = 9;
                firstClassBtn.Margin = new Thickness(0, 25, 0, 0);
                signedForBtn.Margin = new Thickness(0, 25, 0, 0);
                standardCheapestBtn.Margin = new Thickness(0, 25, 0, 0);
                allowBtn.Margin = new Thickness(0, 40, 0, 0);
                dontAllowBtn.Margin = new Thickness(0, 40, 0, 0);
                standardBtn.Margin = new Thickness(0, 15, 0, 0);
                premiumBtn.Margin = new Thickness(0, 15, 0, 0);
                recylcBtn.Margin = new Thickness(0, 15, 0, 0);
                postLaterBtn.Margin = new Thickness(0, 15, 0, 0);
                postNowBtn.Margin = new Thickness(0, 15, 0, 0);
                blckBtn.Margin = new Thickness(0, 15, 0, 0);
                colorBtn.Margin = new Thickness(0, 15, 0, 0);
                handBtn.Margin = new Thickness(0, 15, 0, 0);
                printBtn.Margin = new Thickness(0, 15, 0, 0);
                DatePick.HeightRequest = 32;
                urgentDropdown.HeightRequest = 35;
                calendarWhite.HeightRequest = 20;
                calendarWhite.WidthRequest = 20;
            }
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;

        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private async void BackTapped(object sender, EventArgs e)
        {
            //await Navigation.PopAsync();
            if (Global.GetImagePaths().Count != 0)
            {
                await Navigation.PushAsync(new RecipientAddressPage("send"));
            }
            else
            {
                await Navigation.PopAsync();
            }
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var count = 1;
            System.Diagnostics.Debug.WriteLine("pick " + Global.GetIsDonePick());
            if (Global.GetIsDonePick())
            {
                bool answer = await App.Current.MainPage.DisplayAlert("", "Uploading these photos from your phone to our server will take time. How would you like us to receive them?", "Good Quality (Faster Upload)", "Best Quality (Slower Upload)");
                StopBusyIndicator();
                _isLowQuality = answer;
                imageUpload.Source = "minidone";
                labelUpload.Text = "Photos Ready to Upload";
                labelUpload.VerticalTextAlignment = TextAlignment.Center;
                imageUpload.HeightRequest = 35;
                arrowLabel.IsVisible = false;
                imageUpload.WidthRequest = 35;
                uploadLay.HorizontalOptions = LayoutOptions.CenterAndExpand;
                labelCountUpload.IsVisible = false;
                Global.SetIsDonePick(false);
            }
           
          

        }
        private async void ClickPhoto(object sender, EventArgs e)
        {
            var permission = DependencyService.Get<ILaunchCalendar>().CheckIfPermissionAllowed();
            if (permission)
            {
                Global.SetUploadPageType("PDFDetailsPage");
                Global.SetPhoneContact(_phoneContact);
                await DisplayAlert("", "You can choose up to 5 images. Depending on your phone model, you may need to hold down your finger to select multiple images.", "OK");
                DependencyService.Get<ILaunchCalendar>().LaunchImagePicker();
            }
            else
            {
                Global.SetUploadPageType("PDFDetailsPage");
                Global.SetPhoneContact(_phoneContact);
                DependencyService.Get<ILaunchCalendar>().AskPermission();
            }
        }
        private async void TaskDropdownTapped(object sender, EventArgs e)
        {
            try
            {
                var action = await DisplayActionSheet(urgentDropdown.SelectedText, "Cancel", null, SelectionArray);
                if (!action.Equals("Cancel"))
                {
                    urgentDropdown.SelectedText = action;
                    urgentDropdown.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch(Exception ex)
            {
                
            }
        }
        private async void NextTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                if (colorBtn.Checked.Value)
                {
                    _phoneContact.Ink = "colour";
                }
                else
                {
                    _phoneContact.Ink = "black";
                }
                if (urgentDropdown.SelectedText == "Priority Dispatch")
                {
                    _phoneContact.Priority = "yes";
                }
                else
                {
                    _phoneContact.Priority = "no";
                }
                if (handBtn.Checked.Value)
                {
                    _phoneContact.Handwritten = "yes";
                }
                else
                {
                    _phoneContact.Handwritten = "no";
                }
                if (allowBtn.Checked.Value)
                {
                    _phoneContact.AllowLeaflet = "yes";
                }
                else
                {
                    _phoneContact.AllowLeaflet = "no";
                }
                if (standardCheapestBtn.Checked.Value)
                {
                    _phoneContact.PostageClass = "economy";
                }
                else if(firstClassBtn.Checked.Value)
                {
                    _phoneContact.PostageClass = "1st_class";
                }
                else if (signedForBtn.Checked.Value)
                {
                    _phoneContact.PostageClass = "1st_class_signed_for";
                }
                if (postNowBtn.Checked.Value)
                {
                    _phoneContact.PostDate = "";
                }
                else
                {
                    _phoneContact.PostDate = DatePick.Date.ToString("yyyy-MM-dd hh:ss");
                    System.Diagnostics.Debug.WriteLine("date " + _phoneContact.PostDate);
                }


                var isInRange = DependencyService.Get<IFileHelper>().CheckTime();
                if (isInRange)
                {
                    await Navigation.PushAsync(new LateDispatchPage(_phoneContact,"pdf", _isLowQuality));
                }
                else
                {
                    await Navigation.PushAsync(new DonePage(_phoneContact, _isLowQuality,true));
                }
            }
            else
            {
                await DisplayAlert("", "Internet Connection Required", "OK");
            }
        }
    }
}