﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BranchXamarinSDK;
using Plugin.Clipboard;
using Plugin.Messaging;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class ReferFriendPage : ContentPage, IBranchBUOSessionInterface, IBranchUrlInterface, IBranchLinkShareInterface
    {
        private string _univLink = "";
        public ReferFriendPage()
        {
            InitializeComponent();
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (credential.User.Username == "juju@raptorsms.com")
            {
                titleRefer.Text = AppResources.ReferFriendTitle;
                descRefer.Text = AppResources.ReferFriendDesc;
                fbLabel.Text = AppResources.ReferFriendFB;
                whatsAppLabel.Text = AppResources.ReferFriendWhatsapp;
                smsLabel.Text = AppResources.ReferFriendSMS;
                copyLabel.Text = AppResources.ReferFriendCopy;
                shareViaLabel.Text = AppResources.ReferFriendShareVia;
                referralFormLabel.Text = AppResources.ReferFriendForm;
                emailLabel.Text = AppResources.ReferFriendEmail;
            }
            MessagingCenter.Subscribe<string>(this, "ChangeReferCodes", ChangeReferCodes, null);
            var branch = Global.GetBranch();
            if (!string.IsNullOrEmpty(branch.ReferCodes))
            {
                codeEntry.Text = branch.ReferCodes;
            }
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();

        }
        public void InitSessionComplete(BranchUniversalObject buo, BranchLinkProperties blp)
        {
            var paramss = Branch.GetInstance().GetFirstReferringParams();
        }

        public void SessionRequestError(BranchError error)
        {

        }
        #region IBranchSessionInterface implementation

        public void InitSessionComplete(Dictionary<string, object> data)
        {
            System.Diagnostics.Debug.WriteLine("Session Complete");
        }

        public void CloseSessionComplete()
        {

        }
        public void ChangeReferCodes(string code)
        {
            System.Diagnostics.Debug.WriteLine("code " + code);
            codeEntry.Text = code;
        }
        #endregion
        private async void SkipTapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
        void CreateDeepLink()
        {
            BranchUniversalObject universalObject = new BranchUniversalObject();
            universalObject.title = "Print & Post";
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[5];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var finalString = new String(stringChars);
            universalObject.contentDescription = "Am impressed with this cool Print & Post app. It lets you upload documents or photos stored on your phone, " +
                "email or other app and then the company behind it will " +
                "print and post them for you at the same price as doing it yourself, not bad! " +
                "Free to download and can used for personal or business. Check it out here if you’re interested";
            universalObject.imageUrl = "https://is4-ssl.mzstatic.com/image/thumb/Purple113/v4/91/b0/5f/91b05fa6-5430-c00c-130b-73559df4379a/source/60x60bb.jpg";
            universalObject.metadata.AddCustomMetadata("referlink", credential.UserID + "" + finalString);

            BranchLinkProperties linkProperties = new BranchLinkProperties();
            //linkProperties.tags.Add("tag1");
            //linkProperties.tags.Add("tag2");
            linkProperties.feature = "sharing";
            //linkProperties.channel = "facebook";
            //linkProperties.controlParams.Add("$desktop_url", "http://example.com");

            Branch.GetInstance().GetShortURL(this,
                                              universalObject,
                                              linkProperties);

        }
        async void ShareFbTapped(object sender, System.EventArgs e)
        {
            CreateDeepLink();
            await Task.Delay(800);
            System.Diagnostics.Debug.WriteLine("univ link " + _univLink);
            await Share.RequestAsync(new ShareTextRequest
            {

                Uri = _univLink,
                Text = "Hey! Check out this cool Print Post app. It lets you upload documents or photos stored on your phone, email or " +
                    "other app and then the company behind it will print and post them for you at the same price as doing it yourself! " +
                    "Its free to download and can used for personal or business ",
                Title = "Print Post"
            });
        }
        async void OtherTapped(object sender, System.EventArgs e)
        {
            //await Navigation.PushAsync(new ReferAndEarnPage());
            CreateDeepLink();
            await Task.Delay(800);
            System.Diagnostics.Debug.WriteLine("univ link " + _univLink);
            await Share.RequestAsync(new ShareTextRequest
            {

                Uri = _univLink,
                Text = "Hey! Check out this cool Print Post app. It lets you upload documents or photos stored on your phone, email or " +
                    "other app and then the company behind it will print and post them for you at the same price as doing it yourself! " +
                    "Its free to download and can used for personal or business ",
                Title = "Print Post"
            });
        }
        async void ReferralTapped(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ReferAndEarnPage());
            //await Navigation.PopAsync();
        }
        async void CopyTapped(object sender, System.EventArgs e)
        {
            CreateDeepLink();
            await Task.Delay(700);
            CrossClipboard.Current.SetText(_univLink);
            await DisplayAlert("", "Link copied! You can now paste it anywhere.", "OK");
        }
        async void WhatsAppTapped(object sender, System.EventArgs e)
        {
            try
            {
                CreateDeepLink();
                await Task.Delay(700);
                Device.OpenUri(new Uri("whatsapp://send?phone=&text=Hey%21+Check+out+this+cool+Print+%26+Post+app.+It+lets+you+upload+documents+or+photos+stored+on+your+phone%2C+email+or+other+app+and+then+the+company+behind+it+will+print+and+post+them+for+you+at+the+same+price" +
                    "+as+doing+it+yourself%21+Its+free+to+download+and+can+used+for+personal+or+business%3A+" + _univLink));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("whats app error " + ex.InnerException + " " + ex.Message);
            }
        }
        async void SmsTapped(object sender, System.EventArgs e)
        {
            try
            {
                CreateDeepLink();
                await Task.Delay(700);
                var smsMessenger = CrossMessaging.Current.SmsMessenger;
                if (smsMessenger.CanSendSms)
                    smsMessenger.SendSms("", "Hey! Check out this cool Print & Post app. It lets you upload documents or photos " +
                        "stored on your phone, email or other app and then the company behind it will print and post them for you at " +
                        "the same price as doing it yourself! " +
                        "Its free to download and can used for personal or business: " + _univLink);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("sms error " + ex.InnerException + " " + ex.Message);
            }
        }
        async void EmailTapped(object sender, System.EventArgs e)
        {
            try
            {
                CreateDeepLink();
                await Task.Delay(700);
                Device.OpenUri(new Uri("mailto:?subject=Thought This Might Interest You&body=Hey " + Environment.NewLine + Environment.NewLine + "I came across this cool Print %26 Post mobile app and thought of you :) "
                + Environment.NewLine + Environment.NewLine + "It lets you upload documents or photos stored on your phone, " +
                    "email or other app and then the company behind it will print and post them for you at " +
                    "the same price as doing it yourself!"
                    + Environment.NewLine + Environment.NewLine + "They also provide an Email 2 Print service too (details: https://www.managedmailservice.co.uk/email-to-print/) "
                    + Environment.NewLine + Environment.NewLine + "Its free to download and can used for personal or business: " + _univLink
                    + Environment.NewLine + Environment.NewLine + "Take Care."));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("email error " + ex.InnerException + " " + ex.Message);
            }
        }
        public void ReceivedUrl(string uri)
        {
            _univLink = uri;
            System.Diagnostics.Debug.WriteLine("Received " + uri);
        }

        public void UrlRequestError(BranchError error)
        {
            System.Diagnostics.Debug.WriteLine("Branch Error " + error.ErrorMessage);
        }

        public void ChannelSelected(string channel)
        {
            System.Diagnostics.Debug.WriteLine("Channel " + channel);
        }

        public void LinkShareResponse(string sharedLink, string sharedChannel)
        {
            System.Diagnostics.Debug.WriteLine("Link Share " + sharedLink + " " + sharedChannel);
        }

        public void LinkShareRequestError(BranchError error)
        {
            System.Diagnostics.Debug.WriteLine("Link Share Error " + error.ErrorMessage);
        }
    }
}
