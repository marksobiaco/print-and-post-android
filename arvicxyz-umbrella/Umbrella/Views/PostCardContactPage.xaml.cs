﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Interfaces;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostCardContactPage : ContentPage
    {
        public PostCardContactPage()
        {
            InitializeComponent();
            Global.SetIsDonePick(false);
            Global.SetIsFromLocalFiles(false);
            Global.SetBoolIsHomePage(false);
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                pdfText.Text = AppResources.PostcardTitle;
                postcardDescLabel.Text = AppResources.PostcardDesc;
                postCompLabel.Text = AppResources.PostcardCompany;
                postMeLabel.Text = AppResources.PostcardMe;
                //fromPhoneLabel.Text = AppResources.PostcardFromPhone;
            }
        }
        async void PreviousTapped(object sender, EventArgs args)
        {
            await Navigation.PopAsync();
        }
        private async void NewContactTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PostCardRecipientPage());
        }
        private async void ExistingContactTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PreviousAddressesPage(true));
        }
        private async void SendToMeTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PostCardRecipientPage("send"));
        }
        private async void SendCompanyTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SearchCompanyPage(true));
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}