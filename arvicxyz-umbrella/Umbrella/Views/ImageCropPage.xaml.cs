﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Custom;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageCropPage : ContentPage
    {
        APIHelper2 apiHelper;
        List<FileUpload> filesList;
        public ImageCropPage()
        {
            InitializeComponent();
            apiHelper = new APIHelper2();
            filesList = new List<FileUpload>();
            Global.SetBoolIsHomePage(false);
            list.BackgroundColor = Color.FromHex("#8F0607");
        }
        private void Refresh()
        {
            System.Diagnostics.Debug.WriteLine("xxxxxxxxxxxxxx ");
            try
            {
                System.Diagnostics.Debug.WriteLine("dubidubidabdabss ");
                if (App.CroppedImage != null)
                {
                    System.Diagnostics.Debug.WriteLine("dubidubidabdab ");
                    // byte[] imageAsBytes = System.Convert.FromBase64String(App.CroppedImage.ToString());
                   // Stream stream = new MemoryStream(App.CroppedImage);
                   // image.Source = ImageSource.FromStream(() => new MemoryStream(App.CroppedImage));
                   // image.Source = ImageSource.FromStream(() =>
                   // {
                   //     Stream stream = new MemoryStream(App.CroppedImage);
                   //     return stream;
                   // });

                  //Content = image;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("exception refreshh " + ex.InnerException + " " + ex.Message);
               // Debug.WriteLine(ex.Message);
            }
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;

        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private void LinkItemTapped(object sender, ItemTappedEventArgs e)
        {
            var fp = e.Item as FileUpload;
            Device.OpenUri(new Uri(fp.url));
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var count = 1;

            System.Diagnostics.Debug.WriteLine("on appearing imagecrop");
            if (Global.GetIsDonePick())
            {
                if(App.CroppedImage == null)
                {
                    bool answer = false;
                    string action = await App.Current.MainPage.DisplayActionSheet("", "Cancel", null, "Original", "Square", "2:3");
                    if(action != "Cancel")
                    {
                        answer = await App.Current.MainPage.DisplayAlert("", "Uploading these photos from your phone to our server will take time. How would you like us to receive them?", "Good Quality (Faster Upload)", "Best Quality (Slower Upload)");
                        System.Diagnostics.Debug.WriteLine("new " + answer);
                    }
                    else
                    {
                        Global.ClearImagePaths();
                        Global.SetIsDonePick(false);
                    }

                    if (action == "2:3")
                    {
                        foreach (var item in Global.GetImagePaths())
                        {
                            if (CrossConnectivity.Current.IsConnected)
                            {
                                System.Diagnostics.Debug.WriteLine("answer " + answer);
                                if (answer)
                                {
                                    var jpgB = DependencyService.Get<IFileHelper>().ResizeImage(item);
                                    if (jpgB != null && jpgB.Length > 0)
                                    {
                                        StartBusyIndicator();
                                        Indicator.Title = "Uploading " + count + " / " + Global.GetImagePaths().Count;
                                        var result = await Task.Run(() => apiHelper.FileImageUpload(jpgB));
                                        System.Diagnostics.Debug.WriteLine("url " + result.url);
                                        filesList.Add(result);
                                        count += 1;
                                    }

                                }
                                else
                                {
                                    var jpgB = DependencyService.Get<IFileHelper>().ResizeImage(item);
                                    if (jpgB != null && jpgB.Length > 0)
                                    {
                                        StartBusyIndicator();
                                        Indicator.Title = "Uploading " + count + " / " + Global.GetImagePaths().Count;
                                        var result = await Task.Run(() => apiHelper.FileImageUpload(jpgB));
                                        System.Diagnostics.Debug.WriteLine("url " + result.url);
                                        filesList.Add(result);
                                        count += 1;
                                    }
                                }

                            }
                            else
                            {
                                await DisplayAlert("", "No Internet Connection!", "OK");
                            }


                        }
                        var inRange = DependencyService.Get<IFileHelper>().CheckTime();
                        if (inRange)
                        {
                            this.BackgroundColor = Color.Blue;
                            list.BackgroundColor = Color.Blue;
                        }
                        else
                        {
                            this.BackgroundColor = Color.FromHex("#8F0607");
                            list.BackgroundColor = Color.FromHex("#8F0607");
                        }
                        Global.ClearImagePaths();
                        Global.SetIsDonePick(false);

                        list.ItemsSource = filesList;
                    }
                    else if (action == "Square")
                    {
                        await Navigation.PushModalAsync(new CropView(Global.GetImagePaths().FirstOrDefault(), Refresh));
                    }

                }
                else
                {
                    StartBusyIndicator();
                    Indicator.Title = "Uploading " + count + " / " + Global.GetImagePaths().Count;
                    var result = await Task.Run(() => apiHelper.FileImageUpload(App.CroppedImage));
                    System.Diagnostics.Debug.WriteLine("url " + result.url);
                    filesList.Add(result);
                    count += 1;
                    Global.ClearImagePaths();
                    Global.SetIsDonePick(false);
                    App.CroppedImage = null;
                    list.ItemsSource = filesList;
                }

                StopBusyIndicator();
             
            }
        }
    }
}