﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Umbrella.Constants;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class RegistrationAddressPage : ContentPage
    {
        private string[] SelectionInkType = { "United Kingdom", "United States" };
        private string[] SelectionPaperType = { "United Kingdom", "United States", "Worldwide" };
        private APIHelper2 apiHelper;
        private string _contactId;
        private OntraportContact _op;
        public RegistrationAddressPage(string contactId, OntraportContact op)
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
            apiHelper = new APIHelper2();
            _contactId = contactId;
            _op = new OntraportContact();
            _op = op;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                Indicator.TextSize = 35;
            }
            else
            {
                Indicator.TextSize = 25;
            }

        }
        private async void SaveClicked(object sender, EventArgs e)
        {
            if (Dropdown.SelectedText == "Select..." || string.IsNullOrEmpty(Dropdown.SelectedText) || DropdownPaper.SelectedText == "Select..." || string.IsNullOrEmpty(DropdownPaper.SelectedText) ||
                AppUsageEntry.Text == "i.e. business, personal reasons, printing tickets..." || string.IsNullOrEmpty(AppUsageEntry.Text))
            {
                await DisplayAlert("Submission Failed", "Please fill up all the entry", "OK");
            }
            else
            {
                StartBusyIndicator();

                using (HttpClient client = new HttpClient())
                {
                    _op.Country = Dropdown.SelectedText;
                    _op.MMSCountry = DropdownPaper.SelectedText;
                    _op.MMSComments = AppUsageEntry.Text;
                    _op.PartnerID = _contactId;
                    var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                    client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                    client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                    var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        StopBusyIndicator();
                        await DisplayAlert("Account Setup!", "Please check your email for your login details", "OK");

                        var page = new RegisteredTopupPage(_op.Email);
                        await Navigation.PushModalAsync(page);

                    }
                    else
                    {
                        await DisplayAlert("Account Setup!", "Register Failed!", "OK");
                    }
                }
            }

        }
        private async void InkDropDown(object sender, EventArgs e)
        {
            StartBusyIndicator();
            var action = await DisplayActionSheet(Dropdown.SelectedText, "Cancel", null, SelectionInkType);
            StopBusyIndicator();
            if (!action.Equals("Cancel"))
            {
                Dropdown.SelectedText = action;
                Dropdown.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
            }
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            using (HttpClient client = new HttpClient())
            {
                StartBusyIndicator();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer sk_live_nWEZlQRdf2SXPIQxnRtsf7NE");
                var response = await client.GetStringAsync("https://restcountries.eu/rest/v2/all");
                System.Diagnostics.Debug.WriteLine(response);
                var data = JsonConvert.DeserializeObject<List<Countries>>(response);
                foreach (var item in data)
                {
                    System.Diagnostics.Debug.WriteLine("country name " + item.CountryName);
                }
                data.Insert(0, new Countries()
                {
                    CountryName = "United Kingdom"
                });
                data.Insert(1, new Countries()
                {
                    CountryName = "United States"
                });
                SelectionInkType = data.Select(e => e.CountryName).ToArray();
                data.Insert(2, new Countries()
                {
                    CountryName = "Worldwide"
                });
                SelectionPaperType = data.Select(e => e.CountryName).ToArray();

            }
            StopBusyIndicator();
        }
        private void EditorFocused(object sender, EventArgs e)
        {
            AppUsageEntry.Focus();
            if (AppUsageEntry.Text == "i.e. business, personal reasons, printing tickets...")
            {
                AppUsageEntry.Text = "";
                AppUsageEntry.TextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
            }
        }
        private void EditorUnfocused(object sender, EventArgs e)
        {
            AppUsageEntry.Unfocus();
            if (AppUsageEntry.Text == "")
            {
                AppUsageEntry.Text = "i.e. business, personal reasons, printing tickets...";
                AppUsageEntry.TextColor = (Color)Application.Current.Resources["SecondaryTextColor"];
            }
        }
        private async void PaperDroppedDown(object sender, EventArgs e)
        {
            StartBusyIndicator();
            var action = await DisplayActionSheet(DropdownPaper.SelectedText, "Cancel", null, SelectionPaperType);
            StopBusyIndicator();
            if (!action.Equals("Cancel"))
            {
                DropdownPaper.SelectedText = action;
                DropdownPaper.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
            }
        }
    }
}
