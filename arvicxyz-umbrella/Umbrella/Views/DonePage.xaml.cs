﻿using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Constants;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DonePage : ContentPage
    {
        private APIHelper2 _apiHelper;
        private PhoneContact _phoneContact;
        private List<FileUpload> _fileLists;
        private bool isFail = false;
        private bool _isLowQual;
        private bool _isFromDetailsPage = false;
        public DonePage(PhoneContact phoneContact, bool isLowQual, bool isFromDetailsPage)
        {
            InitializeComponent();
            _phoneContact = new PhoneContact();
            _isFromDetailsPage = isFromDetailsPage;
            _phoneContact = phoneContact;
            _apiHelper = new APIHelper2();
            _fileLists = new List<FileUpload>();
            _isLowQual = isLowQual;
            pdfText.Text = "Give us a moment...";
            trainingVidBtn.IsEnableButton = CrossConnectivity.Current.IsConnected;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                imgCheck.HeightRequest = 200;
                imgCheck.WidthRequest = 200;
                Indicator.TextSize = 25;
            }
            else if (sizeChecker > 700)
            {
                imgCheck.HeightRequest = 300;
                imgCheck.WidthRequest = 300;
                Indicator.TextSize = 35;
            }
            else
            {
                imgCheck.HeightRequest = 200;
                Indicator.TextSize = 25;
                imgCheck.WidthRequest = 200;
            }
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                trainingVidBtn.IsEnableButton = args.IsConnected ? true : false;
            };
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private async void DoneTapped(object sender, EventArgs e)
        {
            if (isFail)
            {
                pdfText.Opacity = 1;
                if (CrossConnectivity.Current.IsConnected)
                {
                    StartBusyIndicator();
                    uploadTxt.Text = "Uploading..";
                    pdfText.Text = "Give us a moment...";
                    imgCheck.Opacity = 0;
                    var result = new FileUpload();
                    result = await Task.Run(() => _apiHelper.FileUpload());
                    System.Diagnostics.Debug.WriteLine("msg " + result.message);
                    if (result.message == "File is valid, and was successfully uploaded.")
                    {
                        StopBusyIndicator();
                        var count = 1;
                        foreach (var item in Global.GetImagePaths())
                        {
                            if (_isLowQual)
                            {
                                var jpgB = DependencyService.Get<IFileHelper>().LowQualityNormalUpload(item);
                                if (jpgB != null && jpgB.Length > 0)
                                {
                                    StartBusyIndicator();
                                    Indicator.Title = "Uploading Pics " + count + " / " + Global.GetImagePaths().Count;
                                    var imageResult = await Task.Run(() => _apiHelper.FileImageUpload(jpgB));
                                    System.Diagnostics.Debug.WriteLine("url " + result.url);
                                    count += 1;
                                    _fileLists.Add(imageResult);
                                }
                            }
                            else
                            {
                                var jpgB = DependencyService.Get<IFileHelper>().HighQualityNormalUpload(item);
                                if (jpgB != null && jpgB.Length > 0)
                                {
                                    StartBusyIndicator();
                                    Indicator.Title = "Uploading Pics " + count + " / " + Global.GetImagePaths().Count;
                                    var imageResult = await Task.Run(() => _apiHelper.FileImageUpload(jpgB));
                                    System.Diagnostics.Debug.WriteLine("url " + result.url);
                                    count += 1;
                                    _fileLists.Add(imageResult);
                                }
                            }
                        }

                        _phoneContact.File_ID = result.file_id;
                        _fileLists.Add(result);
                        var pdfresult = await Task.Run(() => _apiHelper.PdfUpload(_phoneContact,_fileLists));
                        var list = await App.RecentlyUsedContactDatabase.GetItemsAsync();
                        var prevList = await App.PreviousContactsDatabase.GetItemsAsync();
                        var prevCon = new PreviousContacts();
                         
                        if (list.Any(ex => ex.Name == _phoneContact.Name))
                        {
                           
                            await App.RecentlyUsedContactDatabase.UpdateItemAsync(_phoneContact);
                        }
                        else
                        {
                            await App.RecentlyUsedContactDatabase.SaveItemAsync(_phoneContact);

                        }
                        var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                        var allUploadLinks = "";
                        foreach (var item in _fileLists)
                        {
                            allUploadLinks = allUploadLinks + " " + item.url;
                        }
                        System.Diagnostics.Debug.WriteLine("all links " + allUploadLinks);
                        using (HttpClient client = new HttpClient())
                        {
                            var _op = new UploadsOntraport()
                            {
                                PartnerID = credential.UserID,
                                UploadLinks = allUploadLinks,
                            };
                            var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                            client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                            client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                            var response = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                            if (response.IsSuccessStatusCode)
                            {

                                StopBusyIndicator();
                                pdfText.Opacity = 0;
                                uploadTxt.HorizontalOptions = LayoutOptions.Center;
                                uploadTxt.HorizontalTextAlignment = TextAlignment.Center;
                                uploadTxt.Text = "Upload Complete";
                                pdfText.Text = "We'll take it from here. Rest assured that your file/s will now be Printed & Posted as per your Instructions.";
                                imgCheck.Source = "finalcheck";
                                imgCheck.Opacity = 1;
                                Global.ClearImagePaths();
                            }
                            else
                            {
                                isFail = true;
                                pdfText.Opacity = 0;
                                uploadTxt.HorizontalOptions = LayoutOptions.Center;
                                uploadTxt.HorizontalTextAlignment = TextAlignment.Center;
                                uploadTxt.Text = "Upload Failed";
                                imgCheck.Source = "failed";
                                imgCheck.Opacity = 1;
                                trainingVidBtn.IsVisible = true;
                                uploadTxt.HorizontalOptions = LayoutOptions.CenterAndExpand;
                                trainingVidBtn.Text = "Try Again";
                                StopBusyIndicator();
                            }
                        }
                       
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("pdffgasdasdaff");
                        await DisplayAlert("", "Failed to Upload, Pls try again.", "OK");
                        StopBusyIndicator();
                        isFail = true;
                        pdfText.Opacity = 0;
                        uploadTxt.HorizontalOptions = LayoutOptions.Center;
                        uploadTxt.HorizontalTextAlignment = TextAlignment.Center;
                        uploadTxt.Text = "Upload Failed";
                        imgCheck.Source = "failed";
                        imgCheck.Opacity = 1;
                        trainingVidBtn.IsVisible = true;
                        trainingVidBtn.Text = "Try Again";
                        System.Diagnostics.Debug.WriteLine("4th fail " + Global.GetImagePaths().Count);
                    }
                }
                else
                {
                    await DisplayAlert("", "Failed to Upload, No Internet Connection.", "OK");
                }
            }
            else
            {
                await Navigation.PushModalAsync(new NavigationPage(new HomePage(true)));
            }
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _isFromDetailsPage = false;
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (_isFromDetailsPage)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var result = new FileUpload();
                    StartBusyIndicator();
                    result = await Task.Run(() => _apiHelper.FileUpload());
                    //System.Diagnostics.Debug.WriteLine("msg " + result.message);
                    if (Global.GetIsAbort())
                    {
                        isFail = true;
                        pdfText.Opacity = 0;
                        uploadTxt.HorizontalOptions = LayoutOptions.Center;
                        uploadTxt.HorizontalTextAlignment = TextAlignment.Center;
                        uploadTxt.Text = "Upload Failed";

                        imgCheck.Source = "failed";
                        imgCheck.Opacity = 1;
                        trainingVidBtn.IsVisible = true;
                        trainingVidBtn.Text = "Try Again";
                        StopBusyIndicator();
                        System.Diagnostics.Debug.WriteLine("1st fail " + Global.GetImagePaths().Count);
                    }
                    else
                    {
                        if (result.message == "File is valid, and was successfully uploaded.")
                        {
                            System.Diagnostics.Debug.WriteLine("insss mss");
                            _fileLists.Add(result);
                            _phoneContact.File_ID = result.file_id;
                            var count = 1;
                            foreach (var item in Global.GetImagePaths())
                            {
                                StopBusyIndicator();
                                if (_isLowQual)
                                {
                                    var jpgB = DependencyService.Get<IFileHelper>().LowQualityNormalUpload(item);
                                    if (jpgB != null && jpgB.Length > 0)
                                    {
                                        StartBusyIndicator();
                                        Indicator.Title = "Uploading Pics " + count + " / " + Global.GetImagePaths().Count;
                                        var imageResult = await Task.Run(() => _apiHelper.FileImageUpload(jpgB));
                                        System.Diagnostics.Debug.WriteLine("urlss " + imageResult.url);
                                        count += 1;
                                        _fileLists.Add(imageResult);
                                    }
                                }
                                else
                                {
                                    var jpgB = DependencyService.Get<IFileHelper>().HighQualityNormalUpload(item);
                                    if (jpgB != null && jpgB.Length > 0)
                                    {
                                        StartBusyIndicator();
                                        Indicator.Title = "Uploading Pics " + count + " / " + Global.GetImagePaths().Count;
                                        var imageResult = await Task.Run(() => _apiHelper.FileImageUpload(jpgB));
                                        System.Diagnostics.Debug.WriteLine("url " + imageResult.url);
                                        count += 1;
                                        _fileLists.Add(imageResult);
                                    }
                                }
                            }
                            var pdfresult = await Task.Run(() => _apiHelper.PdfUpload(_phoneContact, _fileLists));

                            if (Global.GetIsAbort())
                            {
                                isFail = true;
                                pdfText.Opacity = 0;
                                uploadTxt.HorizontalOptions = LayoutOptions.Center;
                                uploadTxt.HorizontalTextAlignment = TextAlignment.Center;
                                uploadTxt.Text = "Upload Failed";
                                imgCheck.Source = "failed";
                                imgCheck.Opacity = 1;
                                trainingVidBtn.IsVisible = true;
                                uploadTxt.HorizontalOptions = LayoutOptions.CenterAndExpand;
                                trainingVidBtn.Text = "Try Again";
                                StopBusyIndicator();
                                System.Diagnostics.Debug.WriteLine("2nd fail " + Global.GetImagePaths().Count);
                            }
                            else
                            {
                                var list = await App.RecentlyUsedContactDatabase.GetItemsAsync();
                                var prevList = await App.PreviousContactsDatabase.GetItemsAsync();
                                var prevCon = new PreviousContacts();

                                if (list.Any(e => e.Name == _phoneContact.Name))
                                {
                                    await App.RecentlyUsedContactDatabase.UpdateItemAsync(_phoneContact);
                                }
                                else
                                {
                                    await App.RecentlyUsedContactDatabase.SaveItemAsync(_phoneContact);

                                }
                                if (prevList.Any(e => e.Name == _phoneContact.Name))
                                {
                                    await App.PreviousContactsDatabase.UpdateItemAsync(_phoneContact);
                                }
                                else
                                {
                                    await App.PreviousContactsDatabase.SaveItemAsync(_phoneContact);

                                }
                                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                                var allUploadLinks = "";
                                foreach (var item in _fileLists)
                                {
                                    allUploadLinks = allUploadLinks + " " + item.url;
                                }
                                System.Diagnostics.Debug.WriteLine("all links " + allUploadLinks);
                                using (HttpClient client = new HttpClient())
                                {
                                    var _op = new UploadsOntraport()
                                    {
                                        PartnerID = credential.UserID,
                                        UploadLinks = allUploadLinks,
                                    };
                                    var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                                    client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                                    client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                                    var response = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                                    if (response.IsSuccessStatusCode)
                                    {

                                        StopBusyIndicator();
                                        uploadTxt.HorizontalOptions = LayoutOptions.Center;
                                        uploadTxt.HorizontalTextAlignment = TextAlignment.Center;
                                        uploadTxt.Text = "Upload Complete";
                                        pdfText.Text = "We'll take it from here. Rest assured that your file/s will now be Printed & Posted as per your Instructions.";
                                        imgCheck.Source = "finalcheck";
                                        trainingVidBtn.IsVisible = true;
                                        trainingVidBtn.Text = "HOME";
                                        imgCheck.Opacity = 1;
                                        Global.ClearImagePaths();
                                    }
                                    else
                                    {
                                        isFail = true;
                                        pdfText.Opacity = 0;
                                        uploadTxt.HorizontalOptions = LayoutOptions.Center;
                                        uploadTxt.HorizontalTextAlignment = TextAlignment.Center;
                                        uploadTxt.Text = "Upload Failed";
                                        imgCheck.Source = "failed";
                                        imgCheck.Opacity = 1;
                                        trainingVidBtn.IsVisible = true;
                                        uploadTxt.HorizontalOptions = LayoutOptions.CenterAndExpand;
                                        trainingVidBtn.Text = "Try Again";
                                        StopBusyIndicator();
                                    }
                                }

                            }
                        }
                        else
                        {
                            await DisplayAlert("", "Failed to Upload, Pls try again.", "OK");
                            StopBusyIndicator();
                            isFail = true;
                            pdfText.Opacity = 0;
                            uploadTxt.HorizontalOptions = LayoutOptions.Center;
                            uploadTxt.HorizontalTextAlignment = TextAlignment.Center;
                            uploadTxt.Text = "Upload Failed";
                            imgCheck.Source = "failed";
                            imgCheck.Opacity = 1;
                            trainingVidBtn.IsVisible = true;
                            trainingVidBtn.Text = "Try Again";
                            System.Diagnostics.Debug.WriteLine("3rd fail " + Global.GetImagePaths().Count);
                        }
                    }
                }
            }

        }
    }
}