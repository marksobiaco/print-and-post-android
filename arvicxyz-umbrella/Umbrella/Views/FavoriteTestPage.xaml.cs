﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbrella.Utilities;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class FavoriteTestPage : ContentPage
    {
        public FavoriteTestPage()
        {
            InitializeComponent();
            ShowLikedTab();
        }
        private void ShowLiked(object sender, EventArgs args)
        {
            ShowLikedTab();
        }
        private void ShowDisliked(object sender, EventArgs args)
        {
            ShowDislikedTab();
        }
        private void ShowLikedTab()
        {
            likedBoxView.IsVisible = true;
            dislikedBoxView.IsVisible = false;
            likedStack.Children.Clear();
            var favlist = Global.GetLikedDishes();
            foreach (var item2 in favlist.ToList())
            {
                // System.Diagnostics.Debug.WriteLine("time " + item2.LikedTimed + " " + DateTime.Now);
                // System.Diagnostics.Debug.WriteLine("timessdd " + DateTime.Now.AddMinutes(2) + " " + item2.LikedTimed.Minute);
                if (item2.LikedTimed.AddMinutes(2) <= DateTime.Now)
                {
                    //System.Diagnostics.Debug.WriteLine("yea boi " + item2.LikedTimed.AddMinutes(2) + " " + DateTime.Now.AddMinutes(2));
                    favlist.Remove(item2);
                }
            }
            var count = 0;
            var stackList = new List<StackLayout>();
            var stack = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
            };
            foreach (var item in favlist)
            {
                count += 1;
                System.Diagnostics.Debug.WriteLine("count " + count);
                if (count % 2 != 0)
                {
                    System.Diagnostics.Debug.WriteLine("odd " + count);
                    stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                    };
                    var image = new Image()
                    {
                        Source = item.ImageUri,
                        Aspect = Aspect.AspectFit,
                        HeightRequest = 170,
                        WidthRequest = 190
                    };
                    var tapGestureRecognizer = new TapGestureRecognizer();
                    tapGestureRecognizer.Tapped += async (s, e) => {

                        await Navigation.PushModalAsync(new FavoriteTabTestPage(item));
                    };
                    image.GestureRecognizers.Add(tapGestureRecognizer);
                    stack.Children.Add(image);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("even " + count);
                    var image = new Image()
                    {
                        Source = item.ImageUri,
                        Aspect = Aspect.AspectFit,
                        HeightRequest = 170,
                        WidthRequest = 190
                    };
                    var tapGestureRecognizer = new TapGestureRecognizer();
                    tapGestureRecognizer.Tapped += async (s, e) => {

                        await Navigation.PushModalAsync(new FavoriteTabTestPage(item));
                    };
                    image.GestureRecognizers.Add(tapGestureRecognizer);
                    stack.Children.Add(image);
                }
                likedStack.Children.Add(stack);
            }

        }
        private void ShowDislikedTab()
        {
            likedBoxView.IsVisible = false;
            dislikedBoxView.IsVisible = true;
            likedStack.Children.Clear();
        }
    }
}
