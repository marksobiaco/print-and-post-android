﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Umbrella.Views
{
    public partial class SwipeTestPage : ContentPage
    {
        Xamarin.Forms.Maps.Position mapPosition;
        Xamarin.Essentials.Location userLocation;
        private ObservableCollection<SwipeModel> _swipeItemSource = new ObservableCollection<SwipeModel>()
        {
           new SwipeModel()
            {
                Name = "Food Test #1",
                Description = "Delicious Food #1",
                ImageUri = "egg_salad",
                StoreName = "Sample Store",
                CuisineCategory = "Breakfast",
                DietaryInformation = new string[]
                {
                    "Vegan",
                    "Gluten Free",
                }

            },
            new SwipeModel()
            {
                Name = "Food Test #2",
                Description = "Delicious Food #2",
                ImageUri = "guacamole",
                  StoreName = "Sample Store",
                  CuisineCategory = "Lunch",
                  DietaryInformation = new string[]
                {
                    "Vegan",
                }


            },
            new SwipeModel()
            {
                Name = "Food Test #3",
                Description = "Delicious Food #3",
                ImageUri = "meat2",
                  StoreName = "Sample Store",
                  CuisineCategory = "Dinner",
                    DietaryInformation = new string[]
                {
                    "Gluten Free",
                }

            },
        };
        public SwipeTestPage()
        {
            InitializeComponent();
            cardsView.ItemsSource = _swipeItemSource;
            System.Diagnostics.Debug.WriteLine("index " + cardsView.SelectedIndex + " " + cardsView.OldIndex);
            var selected = (SwipeModel)cardsView.SelectedItem;
            title.Text = AppResources.NotesLabel;
            if (selected.IsLiked)
            {
                likeBtn.IsEnabled = false;
            }
            else
            {
                likeBtn.IsEnabled = true;
            }
            System.Diagnostics.Debug.WriteLine("index " + cardsView.SelectedIndex + " " + cardsView.OldIndex);
            cardsView.ItemSwiped += (view, args) =>
            {
                var item = (SwipeModel)((PanCardView.CardsView)view).SelectedItem;
                var index = ((PanCardView.CardsView)view).SelectedIndex;
                var oldindex = ((PanCardView.CardsView)view).OldIndex;
                var length = ((PanCardView.CardsView)view).ItemsCount;

                System.Diagnostics.Debug.WriteLine("index " + index);
                if (item.IsLiked)
                {
                    likeBtn.IsEnabled = true;
                }
                else
                {
                    likeBtn.IsEnabled = false;
                }
                System.Diagnostics.Debug.WriteLine("old index" + oldindex);
                if (index > oldindex)
                {
                    System.Diagnostics.Debug.WriteLine("left");
                }
                else if (index < oldindex)
                {
                    System.Diagnostics.Debug.WriteLine("right");
                }
                System.Diagnostics.Debug.WriteLine("swiped " + item.Name);

            };
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
                if (status != PermissionStatus.Granted)
                {
                    System.Diagnostics.Debug.WriteLine("not granted");
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        await DisplayAlert("Need location", "Gunna need that location", "OK");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    status = results[Permission.Location];
                }

                if (status == PermissionStatus.Granted)
                {
                    await FindUserLocation();

                    mapPosition = new Xamarin.Forms.Maps.Position(userLocation.Latitude, userLocation.Longitude);

                    System.Diagnostics.Debug.WriteLine("lat " + mapPosition.Latitude + " " + "long " + mapPosition.Longitude);

                    map.MoveToRegion(
                      MapSpan.FromCenterAndRadius(
                          mapPosition, Distance.FromMiles(1)));
                }
                else if (status != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Location Denied", "Can not continue, try again.", "OK");
                }
            }
            catch (Exception ex)
            {
                //Something went wrong
                System.Diagnostics.Debug.WriteLine("swipe error " + ex.InnerException + " " + ex.Message);
            }
         
        }
        private async void LikeTapped(object sender, EventArgs e)
        {
            var currentDish = (SwipeModel)cardsView.SelectedItem;
            Global.AddFavDishes(currentDish);
            currentDish.IsLiked = true;
            currentDish.LikedTimed = DateTime.Now;
            likeBtn.IsEnabled = false;
            await DisplayAlert("", "Added to Today's Fav", "OK");
        }
        private async void DislikeTapped(object sender, EventArgs e)
        {

        }
        async Task FindUserLocation()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Best);
                userLocation = await Geolocation.GetLastKnownLocationAsync();
                System.Diagnostics.Debug.WriteLine(userLocation?.ToString() ?? "no location");
                userLocation = await Geolocation.GetLocationAsync(request);
                System.Diagnostics.Debug.WriteLine(userLocation?.ToString() ?? "no location");
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                System.Diagnostics.Debug.WriteLine("feature not supported " + fnsEx);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                System.Diagnostics.Debug.WriteLine("permission " + pEx);
            }
            catch (Exception ex)
            {
                // Unable to get location
                System.Diagnostics.Debug.WriteLine("exception " + ex);
            }
        }
    }
}
