﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Umbrella.Constants;
using Umbrella.Custom;
using Umbrella.Interfaces;
using Umbrella.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestingAuthenticationPage : ContentPage
    {
        private string _clientSecret;
        private string _currentCurrency;
        public TestingAuthenticationPage(string clientSecret,string currency)
        {
            InitializeComponent();
            _clientSecret = clientSecret;
            _currentCurrency = currency;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                Indicator.TextSize = 35;
            }
            else
            {
                Indicator.TextSize = 25;
            }
            System.Diagnostics.Debug.WriteLine("sec " + _clientSecret);
            var hybridWebView = new HybridWebView(_clientSecret)
            {
                Uri = "clientstripe.html",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            stack.Children.Add(hybridWebView);
            hybridWebView.RegisterAction(async data =>
            {
                try
                {
                    var split = data.Split('!');
                    var amount = split[1];
                    System.Diagnostics.Debug.WriteLine("split " + amount + " " + split[0]);
                    await SendPurchaseOntraport(Int32.Parse(amount));
                  
                    await DisplayAlert("Success", "You will be emailed once the credit has been applied to your account.", "OK");
                    Device.BeginInvokeOnMainThread(async() =>
                    {
                        await Navigation.PopToRootAsync();

                    });
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("ex " + ex.Message);
                }
                
            });

        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private async Task SendPurchaseOntraport(int amount)
        {

            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var contactId = credential.UserID;
            using (HttpClient client = new HttpClient())
            {
                var newAmount = amount / 100;
                string xmlData = "<purchases contact_id='" + contactId + "' product_id ='" + GetProductId(amount,_currentCurrency) + "'><field name='Price'>" + newAmount + "</field></purchases>";

                var requestContent = string.Format("appid={0}&key={1}&return_id={2}&reqType={3}&data={4}",
                        Uri.EscapeDataString(OntraportApi.API_APP_ID),
                        Uri.EscapeDataString(OntraportApi.API_KEY),
                        Uri.EscapeDataString(OntraportApi.RETURN_ID.ToString()),
                        Uri.EscapeDataString(OntraportApi.REQUEST_SALE),
                        Uri.EscapeDataString(xmlData));

                var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));
                var _op = new TopupUploadOntraport()
                {
                    PartnerID = credential.UserID,
                    CurrencyOP = _currentCurrency,
                    TopupAmount = newAmount,
                };
                var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var responseTopup = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                if (responseTopup.IsSuccessStatusCode)
                {
                    System.Diagnostics.Debug.WriteLine("contact id " + contactId);
                    var userprofile = new AddTagOntraport()
                    {
                        objectID = "0",
                        Ids = new string[]
                      {
                        contactId
                      },
                        AddLists = new string[]
                      {
                        "Print & Post Topup SUCCESS"
                      }


                    };
                    var responseContentRequest = JsonConvert.SerializeObject(userprofile, new JsonSerializerSettings());
                    client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                    client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                    System.Diagnostics.Debug.WriteLine("responsess " + content);
                    // var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                    var responseTag = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_TAGS_BYNAME, new StringContent(responseContentRequest, Encoding.UTF8, "application/x-www-form-urlencoded"));

                }

            }
        }
        private string GetProductId(int amount, string currency)
        {
            System.Diagnostics.Debug.WriteLine("get product id  " + amount + " " + currency);
            if (currency == "gbp")
            {
                switch (amount)
                {
                    case 1000:
                        return "36";
                    case 2500:
                        return "26";
                    case 5000:
                        return "27";
                    case 10000:
                        return "28";
                    case 15000:
                        return "37";
                    case 25000:
                        return "34";
                    default:
                        return "";
                }
            }
            else if (currency == "usd")
            {
                switch (amount)
                {
                    case 1000:
                        return "47";
                    case 2500:
                        return "48";
                    case 5000:
                        return "49";
                    case 10000:
                        return "50";
                    default:
                        return "";
                }
            }
            else if (currency == "eur")
            {
                switch (amount)
                {
                    case 1000:
                        return "51";
                    case 2500:
                        return "52";
                    case 5000:
                        return "53";
                    case 10000:
                        return "54";
                    default:
                        return "";
                }
            }
            else
            {
                switch (amount)
                {
                    case 1000:
                        return "36";
                    case 2500:
                        return "26";
                    case 5000:
                        return "27";
                    case 10000:
                        return "28";
                    case 15000:
                        return "37";
                    case 25000:
                        return "34";
                    default:
                        return "";
                }
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            //System.Diagnostics.Debug.WriteLine("clientsec " + clientsec);
        }
    }
}