﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        private ReferralsService _referralService;
        public RegistrationPage()
        {
            InitializeComponent();
            ReferButton.IsEnableButton = CrossConnectivity.Current.IsConnected;
            _referralService = new ReferralsService();
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                ReferButton.IsEnableButton = args.IsConnected ? true : false;
            };
            checkBoxTerms.CheckedChanged += (sender, e) =>
            {
                System.Diagnostics.Debug.WriteLine("checksshgg");
                var check = ((CheckBox)sender).IsChecked;
                System.Diagnostics.Debug.WriteLine("checkss " + check);
                if (termsLayout.BackgroundColor == Color.Red)
                {
                    System.Diagnostics.Debug.WriteLine("checksxfxfs " + check);
                    if (check == true)
                    {
                        termsLayout.BackgroundColor = Color.Transparent;
                    }
                }


            };
            checkBoxPrivacy.CheckedChanged += (sender, e) =>
            {
                System.Diagnostics.Debug.WriteLine("checkss");
                var check = ((CheckBox)sender).IsChecked;
                if (privacyLayout.BackgroundColor == Color.Red)
                {
                    if (check == true)
                    {
                        privacyLayout.BackgroundColor = Color.Transparent;
                    }
                }


            };
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                Indicator.TextSize = 35;
            }
            else
            {
                Indicator.TextSize = 25;
            }
            ReferButton.Clicked += new SingleClick(ReferNowClicked).Click;
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;

        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private void TappedPrivacy(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(" https://www.managedmailservice.co.uk/privacy-policy/ "));
        }
        private void TappedTerms(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.managedmailservice.co.uk/terms-of-service/ "));
        }
        private async void ReferNowClicked(object sender, EventArgs e)
        {
            var page = new ReferAndEarnPage();
            if (EmailEntry.TextColor == Color.Red)
            {
                await DisplayAlert("Invalid Data", "Invalid Email Format", "OK");
            }
            else if (string.IsNullOrEmpty(FirstnameEntry.Text) || string.IsNullOrEmpty(LastnameEntry.Text) || string.IsNullOrEmpty(EmailEntry.Text) ||
                string.IsNullOrEmpty(PhoneNumberEntry.Text) || string.IsNullOrEmpty(MobileNumberEntry.Text))
            {
                await DisplayAlert("Submission Failed", "Please fill up all the entry", "OK");

                if (string.IsNullOrEmpty(FirstnameEntry.Text))
                {
                    FirstnameEntry.IsBorderErrorVisible = true;
                    FirstnameEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (string.IsNullOrEmpty(LastnameEntry.Text))
                {
                    LastnameEntry.IsBorderErrorVisible = true;
                    LastnameEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (string.IsNullOrEmpty(MobileNumberEntry.Text))
                {
                    MobileNumberEntry.IsBorderErrorVisible = true;
                    MobileNumberEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (string.IsNullOrEmpty(PhoneNumberEntry.Text))
                {
                    PhoneNumberEntry.IsBorderErrorVisible = true;
                    PhoneNumberEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }

                if (string.IsNullOrEmpty(EmailEntry.Text))
                {
                    EmailEntry.IsBorderErrorVisible = true;
                    EmailEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
            }
            else if (checkBoxTerms.IsChecked != true || checkBoxPrivacy.IsChecked != true)
            {
                if (checkBoxTerms.IsChecked != true)
                {
                    termsLayout.BackgroundColor = Color.Red;
                }
                if (checkBoxPrivacy.IsChecked != true)
                {
                    privacyLayout.BackgroundColor = Color.Red;
                }
            }
            else
            {
                StartBusyIndicator();
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                var contact_id = await DependencyService.Get<IOntraportContactIdRetriever>().GetOntraportContactId(EmailEntry.Text);
                var isSucess = false;
                if (string.IsNullOrEmpty(contact_id))
                {
                    isSucess = await _referralService.SubmitOntraportContactApi(new OntraportContact()
                    {
                        Firstname = FirstnameEntry.Text,
                        Lastname = LastnameEntry.Text,
                        Company = BusinessNameEntry.Text,
                        Email = EmailEntry.Text,
                        OfficePhone = PhoneNumberEntry.Text,
                        CellPhone = MobileNumberEntry.Text,
                        Message = Editor.Text == "Any Comments or Questions?" ? "" : Editor.Text,
                        PhoneUsed = Device.RuntimePlatform == Device.Android ? 526 : 525,
                        //Relationship = ConvertRelationshipToId(Dropdown.Label),
                        //AccountUsed = contact_id,
                        LastLeadSource = 527
                    });

                    if (isSucess)
                    {
                        var contactid = await DependencyService.Get<IOntraportContactIdRetriever>().GetOntraportContactId(EmailEntry.Text);
                        System.Diagnostics.Debug.WriteLine("contact id " + contact_id);
                        await _referralService.PostTagRegistration(contact_id);
                        StopBusyIndicator();
                        var op = new OntraportContact()
                        {
                            Firstname = FirstnameEntry.Text,
                            Lastname = LastnameEntry.Text,
                            Company = BusinessNameEntry.Text,
                            Email = EmailEntry.Text,
                            OfficePhone = PhoneNumberEntry.Text,
                            CellPhone = MobileNumberEntry.Text,
                            Message = Editor.Text == "Any Comments or Questions?" ? "" : Editor.Text,
                            PhoneUsed = Device.RuntimePlatform == Device.Android ? 526 : 525,
                            LastLeadSource = 527
                            // Relationship = ConvertRelationshipToId(Dropdown.Label),
                            //AccountUsed = contact_id,
                        };
                        await Navigation.PushAsync(new RegistrationAddressPage(contactid, op));
                    }
                    else
                    {
                        await DisplayAlert("Failed", "Register Failed!", "OK");
                    }
                }
                else
                {
                    isSucess = await _referralService.SubmitOntraportContactApi(new OntraportContact()
                    {
                        Firstname = FirstnameEntry.Text,
                        Lastname = LastnameEntry.Text,
                        Company = BusinessNameEntry.Text,
                        Email = EmailEntry.Text,
                        OfficePhone = PhoneNumberEntry.Text,
                        CellPhone = MobileNumberEntry.Text,
                        Message = Editor.Text == "Any Comments or Questions?" ? "" : Editor.Text,
                        PhoneUsed = Device.RuntimePlatform == Device.Android ? 526 : 525,
                        //Relationship = ConvertRelationshipToId(Dropdown.Label),
                        AccountUsed = contact_id,
                        PartnerID = contact_id,
                        LastLeadSource = 527
                    });

                    if (isSucess)
                    {
                        var contactid = await DependencyService.Get<IOntraportContactIdRetriever>().GetOntraportContactId(EmailEntry.Text);
                        System.Diagnostics.Debug.WriteLine("contact id " + contact_id);
                        await _referralService.PostTagRegistration(contact_id);
                        StopBusyIndicator();
                        var op = new OntraportContact()
                        {
                            Firstname = FirstnameEntry.Text,
                            Lastname = LastnameEntry.Text,
                            Company = BusinessNameEntry.Text,
                            Email = EmailEntry.Text,
                            OfficePhone = PhoneNumberEntry.Text,
                            CellPhone = MobileNumberEntry.Text,
                            Message = Editor.Text == "Any Comments or Questions?" ? "" : Editor.Text,
                            PhoneUsed = Device.RuntimePlatform == Device.Android ? 526 : 525,
                            LastLeadSource = 527,
                            AccountUsed = contact_id,
                            PartnerID = contact_id,
                            // Relationship = ConvertRelationshipToId(Dropdown.Label),
                            //AccountUsed = contact_id,
                        };
                        await Navigation.PushAsync(new RegistrationAddressPage(contactid, op));
                    }
                    else
                    {
                        await DisplayAlert("Failed", "Register Failed!", "OK");
                    }
                }


            }
            StopBusyIndicator();
        }
        private void EditorFocused(object sender, EventArgs e)
        {
            Editor.Focus();
            if (Editor.Text == "Any Comments or Questions?")
            {
                Editor.Text = "";
                Editor.TextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
            }
        }
        private void EditorUnfocused(object sender, EventArgs e)
        {
            Editor.Unfocus();
            if (Editor.Text == "")
            {
                Editor.Text = "Any Comments or Questions?";
                Editor.TextColor = (Color)Application.Current.Resources["SecondaryTextColor"];
            }
        }
    }
}