﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Connectivity;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisteredTopupPage : ContentPage
    {
        private string[] SelectionArray = { "£10", "£25", "£50", "£100", "1150", "1250" };
        //private Credential _credential;
        private APIHelper2 _apiHelper;
        private string _email;
        private string _currentCurrency = "gbp";
        public RegisteredTopupPage(string email)
        {
            InitializeComponent();
            Global.SetIsNotif(false);
            Global.SetNotifTopic("");
            _email = email;
            ShowUKTab();
            _apiHelper = new APIHelper2();
            proceedButtonAddCard.Clicked += new SingleClick(ProceedClicked).Click;
            proceedButtonAddCard.IsEnableButton = CrossConnectivity.Current.IsConnected;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                Indicator.TextSize = 35;
            }
            else
            {
                Indicator.TextSize = 25;
            }
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void SkipTapped(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new LoginPage());
        }
        private async void ProceedClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(NameInput.Text) || string.IsNullOrEmpty(ExpiryMonthInput.Text) || string.IsNullOrEmpty(ExpiryYearInput.Text) ||
                string.IsNullOrEmpty(CVCInput.Text) || string.IsNullOrEmpty(Dropdown.SelectedText) || Dropdown.SelectedText == "Select...")
            {
                await DisplayAlert("Prompt", "Please fill up all the entry", "OK");
            }
            else
            {
                var card = new Card()
                {
                    Name = NameInput.Text,
                    Number = NumberInput.Text,
                    ExpiryMonth = int.Parse(ExpiryMonthInput.Text),
                    ExpiryYear = int.Parse(ExpiryYearInput.Text),
                    CVC = CVCInput.Text,
                };
                var last4 = NumberInput.Text.Substring(NumberInput.Text.Length - 4);
                var page = new TopupPage();
                var amount = Dropdown.SelectedText + "00";
                var newamount = 0;
                if (_currentCurrency == "gbp")
                {
                    newamount = Int32.Parse(amount.Replace("£", ""));
                }
                else if (_currentCurrency == "eur")
                {
                    newamount = Int32.Parse(amount.Replace("€", ""));
                }
                else
                {
                    newamount = Int32.Parse(amount.Replace("$", ""));
                }
                StartBusyIndicator();
                var success = await DependencyService.Get<ITopupPaymentService>().ProcessCustomerCard(card, newamount, _email,_currentCurrency);
                if (success.Keys.First())
                {
                    if (success.Values.First() == "Topup Success")
                    {
                        StopBusyIndicator();
                        await DisplayAlert("Success", "You will be emailed once the credit has been applied to your account.", "OK");
                        //Navigation.InsertPageBefore(page, this);
                        await Navigation.PopToRootAsync();
                    }
                    else
                    {
                        StopBusyIndicator();
                        await Navigation.PushAsync(new TestingAuthenticationPage(success.Values.First(),_currentCurrency));
                    }
                }
                else
                {
                    StopBusyIndicator();
                    await DisplayAlert("Top Up Failure", "Please try another card. If the issue persists please contact us", "OK");
                    Navigation.InsertPageBefore(page, this);
                    await Navigation.PopAsync().ConfigureAwait(false);
                }
            }
        }
        private void ShowUKTab()
        {
            CurrencyTabManager.SelectTab(UKTab);
            CurrencyTabManager.DeselectTab(USTab);
            CurrencyTabManager.DeselectTab(EuroTab);
            UKTab.Icon = "topupsign";
            SelectionArray = new string[] { "£10", "£25", "£50", "£100", "1150", "1250" };
            amountIcon.Text = "Amount £";
            Dropdown.SelectedText = "£10";
            _currentCurrency = "gbp";
            topupDescription.Text = "Only cards setup from within the mobile app appear below, not all of those from your web based login.";
        }
        private void UKTapped(object sender, EventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(UKTab);
            CurrencyTabManager.DeselectTab(USTab);
            CurrencyTabManager.DeselectTab(EuroTab);
            UKTab.Icon = "topupsign";
            SelectionArray = new string[] { "£10", "£25", "£50", "£100", "1150", "1250" };
            amountIcon.Text = "Amount £";
            Dropdown.SelectedText = "£10";
            _currentCurrency = "gbp";
            topupDescription.Text = "Only cards setup from within the mobile app appear below, not all of those from your web based login.";
        }
        private void ShowUSTab()
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(USTab);
            CurrencyTabManager.DeselectTab(UKTab);
            CurrencyTabManager.DeselectTab(EuroTab);
            USTab.Icon = "dollar_dark";
            SelectionArray = new string[] { "$10", "$25", "$50", "$100" };
            amountIcon.Text = "Amount $";
            Dropdown.SelectedText = "$10";
            _currentCurrency = "usd";
            topupDescription.Text = "Whilst all Print & Post balances are in £ / GBP you are welcome to pay in US Dollars and we'll do the conversion for you!";
        }
        private void USTapped(object sender, EventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(USTab);
            CurrencyTabManager.DeselectTab(UKTab);
            CurrencyTabManager.DeselectTab(EuroTab);
            USTab.Icon = "dollar_dark";
            SelectionArray = new string[] { "$10", "$25", "$50", "$100" };
            amountIcon.Text = "Amount $";
            Dropdown.SelectedText = "$10";
            _currentCurrency = "usd";
            topupDescription.Text = "Whilst all Print & Post balances are in £ / GBP you are welcome to pay in US Dollars and we'll do the conversion for you!";
        }
        private void ShowEuroTab()
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(EuroTab);
            CurrencyTabManager.DeselectTab(USTab);
            CurrencyTabManager.DeselectTab(UKTab);
            EuroTab.Icon = "euro_dark";
            SelectionArray = new string[] { "€10", "€25", "€50", "€100" };
            amountIcon.Text = "Amount €";
            Dropdown.SelectedText = "€10";
            _currentCurrency = "eur";
            topupDescription.Text = "Whilst all Print & Post balances are in £ / GBP you are welcome to pay in Euros and we'll do the conversion for you!";
        }
        private void EuroTapped(object sender, EventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(EuroTab);
            CurrencyTabManager.DeselectTab(USTab);
            CurrencyTabManager.DeselectTab(UKTab);
            EuroTab.Icon = "euro_dark";
            SelectionArray = new string[] { "€10", "€25", "€50", "€100" };
            amountIcon.Text = "Amount €";
            Dropdown.SelectedText = "€10";
            _currentCurrency = "eur";
            topupDescription.Text = "Whilst all Print & Post balances are in £ / GBP you are welcome to pay in Euros and we'll do the conversion for you!";
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private async void DropdownTapped(object sender, EventArgs e)
        {
            try
            {
                var action = await DisplayActionSheet(Dropdown.SelectedText, "Cancel", null, SelectionArray);
                if (!action.Equals("Cancel"))
                {
                    Dropdown.SelectedText = action;
                    Dropdown.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message + " " + ex.InnerException);
            }
        }
    }
}
