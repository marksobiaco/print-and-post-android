﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Constants;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddressLetterPage : ContentPage
    {
        private string _companyNum = "";
        private string _org = "";
        private string _addr = "";
        private List<Directors> _defaultLists;
        private DetailedAddress _detailedAdd;
        private bool _isFromPostCard;
        public AddressLetterPage(string companyNum, string org, DetailedAddress detailedAddress, bool isFromPostCard)
        {
            InitializeComponent();
            _isFromPostCard = isFromPostCard;
            _defaultLists = new List<Directors>()
            {
                new Directors()
                {
                    Name = "Sir/Madam"
                },
                new Directors()
                {
                    Name = "Someone Else"
                },
            };
            _companyNum = companyNum;
            _detailedAdd = detailedAddress;
            _org = org;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                orText.FontSize = 12;
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                orText.FontSize = 15;
                Indicator.TextSize = 35;
            }
            else
            {
                orText.FontSize = 12;
                Indicator.TextSize = 25;
            }
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var convert = new List<Directors>();
            HttpClient client = new HttpClient();
            var authorization = DependencyService.Get<ICredentialRetriever>().GetCompanyToken();
            if (CrossConnectivity.Current.IsConnected)
            {
                if (!Global.GetBoolApiDone())
                {
                    var uri = "https://api.securesupportcentre.com/api/v1/companies-house/company/" + _companyNum + "/officers";
                    System.Diagnostics.Debug.WriteLine("company " + _companyNum);
                    client.DefaultRequestHeaders.Add("Authorization", authorization);
                    StartBusyIndicator();
                    var content = await client.GetStringAsync(uri);
                    var json = content.Replace("<", "");
                    convert = new List<Directors>();
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    convert = JsonConvert.DeserializeObject<List<Directors>>(diskSpaceArray);
                    try
                    {
                        foreach (var item in convert)
                        {
                            var namesss = item.Name.Split(',');
                            item.FirstName = namesss[1].TrimStart();
                            item.LastName = namesss[0].ToLower();
                            var last = item.LastName.Split(' ');
                            System.Diagnostics.Debug.WriteLine("lenght " + last.Length);
                            if (last.Length > 1)
                            {
                                var concat = "";
                                foreach (var items in last)
                                {
                                    concat = concat + " " + char.ToUpper(items[0]) + items.Substring(1);
                                }
                                System.Diagnostics.Debug.WriteLine("concat " + concat);
                                item.LastName = concat;
                            }
                            else
                            {
                                item.LastName = char.ToUpper(item.LastName[0]) + item.LastName.Substring(1);

                            }

                            System.Diagnostics.Debug.WriteLine("item " + item.Name);
                        }
                        var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

                        if (sizeChecker >= 640 && sizeChecker <= 695)
                        {
                            foreach (var item in convert)
                            {
                                item.DirectorFontSize = 14;
                            }
                            foreach (var item in _defaultLists)
                            {
                                item.DirectorFontSize = 14;
                            }
                        }
                        else if (sizeChecker > 700)
                        {
                            foreach (var item in convert)
                            {
                                item.DirectorFontSize = 17;
                            }
                            foreach (var item in _defaultLists)
                            {
                                item.DirectorFontSize = 17;
                            }
                        }
                        else
                        {
                            foreach (var item in convert)
                            {
                                item.DirectorFontSize = 14;
                            }
                            foreach (var item in _defaultLists)
                            {
                                item.DirectorFontSize = 14;
                            }
                        }
                        directorList.ItemsSource = convert;
                        defaultList.ItemsSource = _defaultLists;
                    }
                    catch (Exception ex)
                    {

                        System.Diagnostics.Debug.WriteLine("Address " + ex.Message);
                    }
                    Global.SetIsDonePull(true);
                }
                   
            }
            StopBusyIndicator();
        }
        private async void BackTapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            defaultList.SelectedItem = null;
            directorList.SelectedItem = null;
        }
        private async void DirectorTapped(object sender, ItemTappedEventArgs e)
        {
            var com = e.Item as Directors;
            if (_isFromPostCard)
            {
                if (com.Name == "Sir/Madam")
                {
                    await Navigation.PushAsync(new PostCardRecipientPage("", "", "", _detailedAdd));
                }
                else if (com.Name == "Someone Else")
                {
                    await Navigation.PushAsync(new PostCardRecipientPage("", "", _org, _detailedAdd));
                }
                else
                {
                    await Navigation.PushAsync(new PostCardRecipientPage(com.FirstName, com.LastName, _org, _detailedAdd));
                }
            }
            else
            {
                if (com.Name == "Sir/Madam")
                {
                    await Navigation.PushAsync(new RecipientAddressPage("", "", "", _detailedAdd));
                }
                else if (com.Name == "Someone Else")
                {
                    await Navigation.PushAsync(new RecipientAddressPage("", "", _org, _detailedAdd));
                }
                else
                {
                    await Navigation.PushAsync(new RecipientAddressPage(com.FirstName, com.LastName, _org, _detailedAdd));
                }
            }
        }
    }
}