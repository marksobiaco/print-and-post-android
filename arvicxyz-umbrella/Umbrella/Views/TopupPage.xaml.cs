﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TopupPage : ContentPage
    {
        private APIHelper2 _apiHelper;
        private string[] SelectionArray = { "£10", "£25", "£50", "£100", "£150", "£250" };
        private string[] CreditCardArray = new string[] { };
        private Credential _credential;
        private DefaultTopupDetails _defaultTopupDetails;
        private string _currentCurrency = "";
        private string _currentAmount = "";
        private List<RetrievedCard> _listCard;
        private List<RetrievedCard> _getlistCard;
        public TopupPage()
        {
            InitializeComponent();
            Global.SetIsNotif(false);
            Global.SetNotifTopic("");
            _apiHelper = new APIHelper2();
            _getlistCard = new List<RetrievedCard>();
            NavigationPage.SetBackButtonTitle(this, "");
            _listCard = new List<RetrievedCard>();
            proceedButtonAddCard.Clicked += new SingleClick(ProceedClicked).Click;
            proceedButtonExistingCard.Clicked += new SingleClick(ProceedExistingClicked).Click;
            _defaultTopupDetails = DependencyService.Get<ICredentialRetriever>().GetCurrencyDefault();
            if (_defaultTopupDetails != null)
            {
                _currentCurrency = string.IsNullOrEmpty(_defaultTopupDetails.DefaultCurrency) ? "gbp" : _defaultTopupDetails.DefaultCurrency;
                _currentAmount = string.IsNullOrEmpty(_defaultTopupDetails.DefaultAmount) ? "10" : _defaultTopupDetails.DefaultAmount;
            }
            _credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                topupDescription.FontSize = 12;
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                topupDescription.FontSize = 15;
                Indicator.TextSize = 35;
            }
            else
            {
                topupDescription.FontSize = 12;
                Indicator.TextSize = 25;
            }
            proceedButtonAddCard.IsEnableButton = CrossConnectivity.Current.IsConnected;
            proceedButtonExistingCard.IsEnableButton = CrossConnectivity.Current.IsConnected;
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                proceedButtonAddCard.IsEnableButton = args.IsConnected ? true : false;
                proceedButtonExistingCard.IsEnableButton = args.IsConnected ? true : false;
            };
            if (_currentCurrency == "gbp")
            {
                ShowUKTab();
            }
            else if (_currentCurrency == "usd")
            {
                ShowUSTab();
            }
            else if (_currentCurrency == "eur")
            {
                ShowEuroTab();
            }
            else
            {
                ShowUKTab();
            }
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                topupDescription.Text = AppResources.TopupDesc;
                balanceLabel.Text = AppResources.TopupBalanceText;
                ExistingCardTab.Label = AppResources.TopupUseExistingTabText;
                AddCardTab.Label = AppResources.TopupAddNewTabText;
                chargeLabel.Text = AppResources.TopupChargeSmallDesc;
                UKTab.Label = AppResources.TopupGBPText;
                cardSmallLabel.Text = AppResources.TopupCardsText;
                Indicator.Title = AppResources.BusyIndicator;
                DropdownAddCard.SelectedText = AppResources.TopupDropdownCardText;
                proceedButtonAddCard.Text = AppResources.TopupButtonText;
                proceedButtonExistingCard.Text = AppResources.TopupButtonText;
                nameCardLabel.Text = AppResources.TopupNameCardText;
                cardNumberLabel.Text = AppResources.TopupCardNumberText;
                expiryLabel.Text = AppResources.TopupExpiryText;
            }
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (CrossConnectivity.Current.IsConnected)
            {
                existingCardTab.IsVisible = false;
                addCardTab.IsVisible = false;
                StartBusyIndicator();
                _getlistCard = await DependencyService.Get<ITopupPaymentService>().GetAllCurrentCards(_credential.User.Email);
                _listCard = _getlistCard.Any() ? _getlistCard : new List<RetrievedCard>();
                StopBusyIndicator();
            }
            else
            {
                CreditCardArray = new string[] { "No Connection" };
            }
            if (_listCard.Count >= 1)
            {
                ShowExistingCardPage();
                //await ShowExistingCardPage();
            }
            else
            {
                ShowAddCardPage();
            }
          
            var bal = new Balance();
            if (CrossConnectivity.Current.IsConnected)
            {
                StartBusyIndicator();
                bal = await Task.Run(() => _apiHelper.GetBalanceApi());
                await App.TopupDatabase.SaveSingleBalanceAsync(bal);
                Global.SetBalance(bal.BalancePoints);
                StopBusyIndicator();
            }
            else
            {
                try
                {
                    var list = await App.TopupDatabase.GetItemsAsync();
                    if (list.Any())
                    {
                        bal = list.FirstOrDefault();
                        Global.SetBalance(bal.BalancePoints);
                    }
                    else
                    {
                        Global.SetBalance(0);
                        await DisplayAlert("", "No Saved Data", "OK");
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Exception " + ex.Message);
                    await DisplayAlert("", "No Saved Data", "OK");
                }
            }
            balanceText.Text = "£" + Global.GetBalance().ToString().Trim();

        }
        private async void DropdownTapped(object sender, EventArgs e)
        {
            try
            {
                var action = await DisplayActionSheet(Dropdown.SelectedText, "Cancel", null, SelectionArray);
                if (!action.Equals("Cancel"))
                {
                    Dropdown.SelectedText = action;
                    Regex pattern = new Regex("[£$€]");
                    _currentAmount = pattern.Replace(action, "");
                    System.Diagnostics.Debug.WriteLine("current amount " + _currentAmount);
                    var gbpDetails = new DefaultTopupDetails()
                    {
                        DefaultCurrency = _currentCurrency,
                        DefaultAmount = _currentAmount
                    };
                    MessagingCenter.Send<DefaultTopupDetails>(gbpDetails, "SaveDefaultCurrency");
                    Dropdown.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
              catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message + " " + ex.InnerException);
            }
        }
        private async void AmountExistingDropdownTapped(object sender, EventArgs e)
        {
            try
            {
                var action = await DisplayActionSheet(DropdownCardAmount.SelectedText, "Cancel", null, SelectionArray);
                if (!action.Equals("Cancel"))
                {
                    DropdownCardAmount.SelectedText = action;
                    Regex pattern = new Regex("[£$€]");
                    _currentAmount = pattern.Replace(action, "");
                    System.Diagnostics.Debug.WriteLine("current amount " + _currentAmount);
                    var gbpDetails = new DefaultTopupDetails()
                    {
                        DefaultCurrency = _currentCurrency,
                        DefaultAmount = _currentAmount
                    };
                    MessagingCenter.Send<DefaultTopupDetails>(gbpDetails, "SaveDefaultCurrency");
                    DropdownCardAmount.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message + " " + ex.InnerException);
            }
        }
        private async void CreditCardDropdownTapped(object sender, EventArgs e)
        {
            try
            {
                var action = await DisplayActionSheet(DropdownAddCard.SelectedText, "Cancel", null, CreditCardArray);
                if (!action.Equals("Cancel"))
                {
                    DropdownAddCard.SelectedText = action;
                    DropdownAddCard.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
             catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message + " " + ex.InnerException);
            }
        }
        private void ShowUKTab()
        {
            CurrencyTabManager.SelectTab(UKTab);
            CurrencyTabManager.DeselectTab(USTab);
            CurrencyTabManager.DeselectTab(EuroTab);
            UKTab.Icon = "topupsign";
            SelectionArray = new string[] { "£10", "£25", "£50", "£100", "£150", "£250" };
            DropdownCardAmount.SelectedText = "£" + _currentAmount;
            Dropdown.SelectedText = "£" + _currentAmount;
            _currentCurrency = "gbp";
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                topupDescription.Text = AppResources.TopupDesc;
                amountIcon.Text = AppResources.TopupAmountGbpText;
                addCardAmountIcon.Text = AppResources.TopupAmountGbpText;
            }
            else
            {
                topupDescription.Text = "Only cards setup from within the mobile app appear below, not all of those from your web based login.";
                amountIcon.Text = "Amount £";
                addCardAmountIcon.Text = "Amount £";
            }
        }
        private void UKTapped(object sender, EventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(UKTab);
            CurrencyTabManager.DeselectTab(USTab);
            CurrencyTabManager.DeselectTab(EuroTab);
            UKTab.Icon = "topupsign";
            SelectionArray = new string[] { "£10", "£25", "£50", "£100", "£150", "£250" };
            DropdownCardAmount.SelectedText = "£" + _currentAmount;
            Dropdown.SelectedText = "£" + _currentAmount;
            _currentCurrency = "gbp";
            var gbpDetails = new DefaultTopupDetails()
            {
                DefaultCurrency = _currentCurrency,
                DefaultAmount = _currentAmount
            };
            MessagingCenter.Send<DefaultTopupDetails>(gbpDetails, "SaveDefaultCurrency");
            topupHeader.ChangeHeader(_currentCurrency);

            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                topupDescription.Text = AppResources.TopupDesc;
                amountIcon.Text = AppResources.TopupAmountGbpText;
                addCardAmountIcon.Text = AppResources.TopupAmountGbpText;
            }
            else
            {
                topupDescription.Text = "Only cards setup from within the mobile app appear below, not all of those from your web based login.";
                amountIcon.Text = "Amount £";
                addCardAmountIcon.Text = "Amount £";
            }
        }
        private void ShowUSTab()
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(USTab);
            CurrencyTabManager.DeselectTab(UKTab);
            CurrencyTabManager.DeselectTab(EuroTab);
            USTab.Icon = "dollar_dark";
            SelectionArray = new string[] { "$10", "$25", "$50", "$100" };
            DropdownCardAmount.SelectedText = "$" + _currentAmount;
            Dropdown.SelectedText = "$" + _currentAmount;
            _currentCurrency = "usd";
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                topupDescription.Text = AppResources.TopupDesc;
                amountIcon.Text = AppResources.TopupAmountUsdText;
                addCardAmountIcon.Text = AppResources.TopupAmountUsdText;
            }
            else
            {
                topupDescription.Text = "Whilst all Print & Post balances are in £ / GBP you are welcome to pay in US Dollars and we'll do the conversion for you!";
                amountIcon.Text = "Amount $";
                addCardAmountIcon.Text = "Amount $";
            }
        }
        private void USTapped(object sender, EventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(USTab);
            CurrencyTabManager.DeselectTab(UKTab);
            CurrencyTabManager.DeselectTab(EuroTab);
            USTab.Icon = "dollar_dark";
            SelectionArray = new string[] { "$10", "$25", "$50", "$100" };
            DropdownCardAmount.SelectedText = "$" + _currentAmount;
            Dropdown.SelectedText = "$" + _currentAmount;
            _currentCurrency = "usd";
            topupHeader.ChangeHeader(_currentCurrency);
            var gbpDetails = new DefaultTopupDetails()
            {
                DefaultCurrency = _currentCurrency,
                DefaultAmount = _currentAmount
            };
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                topupDescription.Text = AppResources.TopupDesc;
                amountIcon.Text = AppResources.TopupAmountUsdText;
                addCardAmountIcon.Text = AppResources.TopupAmountUsdText;
            }
            else
            {
                topupDescription.Text = "Whilst all Print & Post balances are in £ / GBP you are welcome to pay in US Dollars and we'll do the conversion for you!";
                amountIcon.Text = "Amount $";
                addCardAmountIcon.Text = "Amount $";
            }
            MessagingCenter.Send<DefaultTopupDetails>(gbpDetails, "SaveDefaultCurrency");
        }
        private void ShowEuroTab()
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(EuroTab);
            CurrencyTabManager.DeselectTab(USTab);
            CurrencyTabManager.DeselectTab(UKTab);
            EuroTab.Icon = "euro_dark";
            SelectionArray = new string[] { "€10", "€25", "€50", "€100" };
            DropdownCardAmount.SelectedText = "€" + _currentAmount;
            Dropdown.SelectedText = "€" + _currentAmount;
            _currentCurrency = "eur";
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                topupDescription.Text = AppResources.TopupDesc;
                amountIcon.Text = AppResources.TopupAmountEuroText;
                addCardAmountIcon.Text = AppResources.TopupAmountEuroText;
            }
            else
            {
                topupDescription.Text = "Whilst all Print & Post balances are in £ / GBP you are welcome to pay in Euros and we'll do the conversion for you!";

                amountIcon.Text = "Amount €";
                addCardAmountIcon.Text = "Amount €";
            }
        }
        private void EuroTapped(object sender, EventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("yowws");
            CurrencyTabManager.SelectTab(EuroTab);
            CurrencyTabManager.DeselectTab(USTab);
            CurrencyTabManager.DeselectTab(UKTab);
            EuroTab.Icon = "euro_dark";
            SelectionArray = new string[] { "€10", "€25", "€50", "€100" };
            DropdownCardAmount.SelectedText = "€" + _currentAmount;
            Dropdown.SelectedText = "€" + _currentAmount;
            _currentCurrency = "eur";
            topupHeader.ChangeHeader(_currentCurrency);
            var gbpDetails = new DefaultTopupDetails()
            {
                DefaultCurrency = _currentCurrency,
                DefaultAmount = _currentAmount
            };
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                topupDescription.Text = AppResources.TopupDesc;
                amountIcon.Text = AppResources.TopupAmountEuroText;
                addCardAmountIcon.Text = AppResources.TopupAmountEuroText;
            }
            else
            {
                topupDescription.Text = "Whilst all Print & Post balances are in £ / GBP you are welcome to pay in Euros and we'll do the conversion for you!";

                amountIcon.Text = "Amount €";
                addCardAmountIcon.Text = "Amount €";
            }
            MessagingCenter.Send<DefaultTopupDetails>(gbpDetails, "SaveDefaultCurrency");
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }

        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private void AddCardTabTapped(object sender, EventArgs args)
        {
            ShowAddCardPage();
        }
        private void ExistingCardTabTapped(object sender, EventArgs args)
        {
            ShowExistingCardPage();
        }
        private void ShowExistingCardPage()
        {
            TabButtonManager.DeselectTab(AddCardTab);
            TabButtonManager.SelectTab(ExistingCardTab);
            ExistingCardTab.Icon = "existing_card";
            AddCardTab.Icon = "add_new_card_w";
            addCardTab.IsVisible = false;
            existingCardTab.IsVisible = true;
            CreditCardArray = _listCard.Any() ? _getlistCard.Select(x => x.NameWithLastFourDigits).ToArray() : new string[] { };
        }
        private void ShowAddCardPage()
        {
            TabButtonManager.SelectTab(AddCardTab);
            TabButtonManager.DeselectTab(ExistingCardTab);
            ExistingCardTab.Icon = "existing_card_w";
            AddCardTab.Icon = "add_new_card";
            addCardTab.IsVisible = true;
            existingCardTab.IsVisible = false;
        }
        private async void ProceedExistingClicked(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(DropdownAddCard.SelectedText) || DropdownAddCard.SelectedText == "Select Existing Card")
            {
                await DisplayAlert("Attention", "Please select a card?", "OK");
            }
            else if(string.IsNullOrEmpty(DropdownCardAmount.SelectedText) || DropdownCardAmount.SelectedText == "Select...")
            {
                await DisplayAlert("Attention", "Please choose an amount to Top up", "OK");
            }
            else
            {
                var page = new TopupPage();
                var amount = DropdownCardAmount.SelectedText + "00";
                var newamount = 0;
                if (_currentCurrency == "gbp")
                {
                    newamount = Int32.Parse(amount.Replace("£", ""));
                }
                else if (_currentCurrency == "eur")
                {
                    newamount = Int32.Parse(amount.Replace("€", ""));
                }
                else
                {
                    newamount = Int32.Parse(amount.Replace("$", ""));
                }
                var retrievedCard = _listCard.Where(p => p.NameWithLastFourDigits == DropdownAddCard.SelectedText).FirstOrDefault();
                //System.Diagnostics.Debug.WriteLine("retrieved " + retrievedCard.Id + " " + retrievedCard.Last4 + " " + retrievedCard.Name); StartBusyIndicator();
                StartBusyIndicator();
                var success = await DependencyService.Get<ITopupPaymentService>().ProcessIntentPayments(newamount, _credential.User.Email, retrievedCard,_currentCurrency);
                if (success.Keys.First())
                {
                    if (success.Values.First() == "Topup Success")
                    {
                        StopBusyIndicator();
                        await DisplayAlert("Success", "You will be emailed once the credit has been applied to your account.", "OK");
                        //Navigation.InsertPageBefore(page, this);
                        await Navigation.PopToRootAsync();
                    }
                    else
                    {
                        StopBusyIndicator();
                        await Navigation.PushAsync(new TestingAuthenticationPage(success.Values.First(),_currentCurrency));
                    }
                }
                else
                {
                    StopBusyIndicator();
                    await DisplayAlert("Top Up Failure", "Please try another card. If the issue persists please contact us", "OK");
                    Navigation.InsertPageBefore(page, this);
                    await Navigation.PopAsync().ConfigureAwait(false);
                }
             
            }
        }
        private async void ProceedClicked(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(NameInput.Text) || string.IsNullOrEmpty(ExpiryMonthInput.Text) || string.IsNullOrEmpty(ExpiryYearInput.Text) ||
                string.IsNullOrEmpty(CVCInput.Text) || string.IsNullOrEmpty(Dropdown.SelectedText) || Dropdown.SelectedText == "Select...")
            {
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    await DisplayAlert("Attention", "Veuillez remplir tous les champs", "OK");

                }
                else
                {
                    await DisplayAlert("Attention", "Please fill up all the entry", "OK");

                }
            }
            else
            {
                var card = new Card()
                {
                    Name = NameInput.Text,
                    Number = NumberInput.Text,
                    ExpiryMonth = int.Parse(ExpiryMonthInput.Text),
                    ExpiryYear = int.Parse(ExpiryYearInput.Text),
                    CVC = CVCInput.Text,
                };
                var last4 = NumberInput.Text.Substring(NumberInput.Text.Length - 4);
                var page = new TopupPage();
                var amount = Dropdown.SelectedText + "00";
                var isCardAlreadyExist = _listCard.Any(p => p.Last4 == last4 && p.Name == NameInput.Text);
                if (isCardAlreadyExist)
                {
                    await DisplayAlert("Error", "Card Already Exists", "OK");
                }
                else
                {
                    var newamount = 0;
                    if (_currentCurrency == "gbp")
                    {
                        newamount = Int32.Parse(amount.Replace("£", ""));
                    }
                    else if (_currentCurrency == "eur")
                    {
                        newamount = Int32.Parse(amount.Replace("€", ""));
                    }
                    else
                    {
                        newamount = Int32.Parse(amount.Replace("$", ""));
                    }
                    StartBusyIndicator();
                    var success = await DependencyService.Get<ITopupPaymentService>().ProcessCustomerCard(card, newamount, _credential.User.Email,_currentCurrency);
                    if (success.Keys.First())
                    {
                        if (success.Values.First() == "Topup Success")
                        {
                            StopBusyIndicator();
                            await DisplayAlert("Success", "You will be emailed once the credit has been applied to your account.", "OK");
                            //Navigation.InsertPageBefore(page, this);
                            await Navigation.PopToRootAsync();
                        }
                        else
                        {
                            StopBusyIndicator();
                            await Navigation.PushAsync(new TestingAuthenticationPage(success.Values.First(),_currentCurrency));
                        }
                    }
                    else
                    {
                        StopBusyIndicator();
                        await DisplayAlert("Top Up Failure", "Please try another card. If the issue persists please contact us", "OK");
                        Navigation.InsertPageBefore(page, this);
                        await Navigation.PopAsync().ConfigureAwait(false);
                    }
                
                }
            }
        }
    }
}
