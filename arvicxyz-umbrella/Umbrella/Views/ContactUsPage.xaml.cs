﻿using Plugin.Connectivity;
using System;
using System.Net.Http;
using System.Text;
using Umbrella.Constants;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Resx;
using Umbrella.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactUsPage : ContentPage
    {
        private BasicAccountServices _basicAccntService;
        private string[] SelectionArray = { "Business Growth Executive", "Technical Support",
            "Sales Team", "Accounts Department", "Complaints Department", "Other" };

        public ContactUsPage()
        {
            InitializeComponent();
            contactButton.Clicked += new SingleClick(SendMessageClicked).Click;
            contactButton.IsEnableButton = CrossConnectivity.Current.IsConnected;
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                contactDesc.Text = AppResources.ContactUsDesc;
                contactButton.Text = AppResources.ContactUsBtnText;
                Dropdown.SelectedText = AppResources.ContactUsDropdownText;
                Editor.Text = AppResources.ContactUsEditorText;

            }
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                contactButton.IsEnableButton = args.IsConnected ? true : false;
            };
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                contactDesc.FontSize = 12;
            }
            else if (sizeChecker > 700)
            {
                contactDesc.FontSize = 16;
            }
            else
            {
                contactDesc.FontSize = 12;
            }
            _basicAccntService = new BasicAccountServices();
        }
        private void BookTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.bookphonecall.com/organization/24757"));
        }
        private async void DropdownTapped(object sender, EventArgs e)
        {
            try
            {
                var action = await DisplayActionSheet(Dropdown.SelectedText, "Cancel", null, SelectionArray);
                if (!action.Equals("Cancel"))
                {
                    Dropdown.SelectedText = action;
                    Dropdown.SelectedTextColor = Color.Black;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("ex " + ex.InnerException + " " + ex.Message);
            }
        }
        private void EditorFocused(object sender, EventArgs e)
        {
            Editor.Focus();
            if (Editor.Text == "Enter your question here" || Editor.Text == "Ecrivez votre message ici")
            {
                Editor.Text = "";
                Editor.TextColor = (Color) Application.Current.Resources["PrimaryTextColor"];
            }
        }
        private void EditorUnfocused(object sender, EventArgs e)
        {
            Editor.Unfocus();
            if (Editor.Text == "")
            {
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (credential.User.Username == "juju@raptorsms.com")
                {
                    Editor.Text = "Ecrivez votre message ici";
                }
                else
                {
                    Editor.Text = "Enter your question here";
                }
                Editor.TextColor = (Color) Application.Current.Resources["SecondaryTextColor"];
            }
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;

        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private async void SendMessageClicked(object sender, EventArgs e)
        {
            // TODO
            if(string.IsNullOrEmpty(Dropdown.SelectedText) || string.IsNullOrEmpty(Editor.Text) || Dropdown.SelectedText == "Select..." || Editor.Text == "Enter your question here")
            {
                await DisplayAlert("Information", "Please fill up all the entry", "OK");
                if (string.IsNullOrEmpty(Editor.Text))
                {
                    Editor.IsBorderErrorVisible = true;
                    Editor.BackgroundColor = Color.FromHex("#F9E99B");
                }

                if (Editor.Text == "Enter your question here")
                {
                    Editor.IsBorderErrorVisible = true;
                    Editor.BackgroundColor = Color.FromHex("#F9E99B");
                }
            }
            else
            {
                var page = new ContactUsPage();
                StartBusyIndicator();
                var credentials = DependencyService.Get<ICredentialRetriever>().GetCredential();
                var isSuccess = await _basicAccntService.SyncEnquiryTypeOntraport(ConvertEnquiryToId(Dropdown.SelectedText), Editor.Text, credentials.User.Email);
                if (isSuccess)
                {
                    StopBusyIndicator();
                    await DisplayAlert("Message Sent!", "We will be in touch soon.", "OK");
                    Navigation.InsertPageBefore(new HomePage(), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
                    await Navigation.PopAsync();
                }
                else
                {
                    StopBusyIndicator();
                    await DisplayAlert("Message Sending Failed!", "Message Sending Failed", "OK");
                }
            }
            StopBusyIndicator();
        }
        private int ConvertEnquiryToId(string enquiry)
        {
            switch (enquiry)
            {
                case "Other":
                    return 1035;
                case "Complaints Department":
                    return 1036;
                case "Accounts Department":
                    return 1037;
                case "Sales Team":
                    return 1038;
                case "Technical Support":
                    return 1039;
                case "Business Growth Executive":
                    return 1040;
                default:
                    return 0;
            }
        }

    }
}
