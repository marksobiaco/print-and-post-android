﻿using System;
using System.Collections.Generic;
using Umbrella.Constants;
using Umbrella.Controls;
using Umbrella.Custom;
using Umbrella.Interfaces;
using Umbrella.Resx;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class QRCodeReaderPage : ContentPage
    {
        private int _secondsElapsed;
        private int _minutesElapsed;
        public QRCodeReaderPage()
        {
            InitializeComponent();
            _secondsElapsed = 5;
            _minutesElapsed = 60;
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                inkBtn.Text = AppResources.MapInkBtn;
                otherBtn.Text = AppResources.MapPaperBtn;
                errorBtn.Text = AppResources.MapErrorBtn;
                paperBtn.Text = AppResources.MapPaperBtn;
                printingNowText.Text = AppResources.MapPrinterTitle;
                labelText.Text = AppResources.MapPrinterHelp;
                timerLabel.Margin = new Thickness(0, 0, 0, 13);
            }
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (_secondsElapsed >= 1)
                {
                    _secondsElapsed = _secondsElapsed - 1;
                    timerLabel.Text = ConvertIntTimeToString(_secondsElapsed);
                    return true;
                }

                var firstOuterStack = new StackLayout()
                {
                    Padding = new Thickness(2),
                    BackgroundColor = Color.Gray,
                    Orientation = StackOrientation.Horizontal,
                    HeightRequest = 40,
                    WidthRequest = 220,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Center,
                };
                var firstInnerStack = new StackLayout()
                {
                    Padding = new Thickness(3),
                    Orientation = StackOrientation.Horizontal,
                    BackgroundColor = Color.FromHex("#363636"),
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };
                var imageFirst = new Image()
                {
                    Source = "done",
                    Aspect = Aspect.AspectFit,
                    HeightRequest = 25,
                    WidthRequest = 25,
                    HorizontalOptions = LayoutOptions.StartAndExpand
                };
                var labelFirst = new Label()
                {
                    Text = "Printed Fine?",
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalTextAlignment = TextAlignment.Center,
                    VerticalOptions = LayoutOptions.Center,
                    TextColor = Color.White,
                    FontSize = 19,
                };
                var customFontFirst = new FontAwesomeLabel()
                {
                    WidthRequest = 30,
                    HeightRequest = 30,
                    FontFamily = "FontAwesome",
                    VerticalTextAlignment = TextAlignment.Center,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalTextAlignment = TextAlignment.End,
                    HorizontalOptions = LayoutOptions.End,
                    FontSize = 30,
                    TextColor = Color.White,
                    Text = FontIcon.ANGLE_RIGHT,
                };

                firstInnerStack.Children.Add(imageFirst);
                firstInnerStack.Children.Add(labelFirst);
                firstInnerStack.Children.Add(customFontFirst);
                firstOuterStack.Children.Add(firstInnerStack);
                var orLabel = new Label()
                {
                    Text = "OR",
                    TextColor = Color.White,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                    FontAttributes = FontAttributes.Bold,
                };

                var secondOuterStack = new StackLayout()
                {
                    Padding = new Thickness(2),
                    BackgroundColor = Color.Gray,
                    Orientation = StackOrientation.Horizontal,
                    HeightRequest = 40,
                    WidthRequest = 200,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Center,
                };
                var secondInnerStack = new StackLayout()
                {
                    Padding = new Thickness(3),
                    Orientation = StackOrientation.Horizontal,
                    BackgroundColor = Color.FromHex("#363636"),
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };
                var imageSecond = new Image()
                {
                    Source = "cancel",
                    Aspect = Aspect.AspectFit,
                    HeightRequest = 25,
                    WidthRequest = 25,
                    HorizontalOptions = LayoutOptions.StartAndExpand
                };
                var labelSecond = new Label()
                {
                    Text = "Printer Problem?",
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalOptions = LayoutOptions.Center,
                    TextColor = Color.White,
                };
                var customFontSecond = new FontAwesomeLabel()
                {
                    WidthRequest = 30,
                    HeightRequest = 30,
                    FontFamily = "FontAwesome",
                    VerticalTextAlignment = TextAlignment.Center,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalTextAlignment = TextAlignment.End,
                    HorizontalOptions = LayoutOptions.End,
                    FontSize = 30,
                    TextColor = Color.White,
                    Text = FontIcon.ANGLE_RIGHT,
                };

                secondInnerStack.Children.Add(imageSecond);
                secondInnerStack.Children.Add(labelSecond);
                secondInnerStack.Children.Add(customFontSecond);
                secondOuterStack.Children.Add(secondInnerStack);

                var tapGestureFine = new TapGestureRecognizer();
                tapGestureFine.Tapped += async (s, e) =>
                {
                    await Navigation.PushAsync(new ScannerEndPage());

                };

                var tapGestureProblem = new TapGestureRecognizer();
                tapGestureProblem.Tapped += async (s, e) =>
                {
                    errorBtn.IsEnableButton = true;
                    inkBtn.IsEnableButton = true;
                    otherBtn.IsEnableButton = true;
                    paperBtn.IsEnableButton = true;
                    errorBtn.ButtonDirectColor = Color.White;
                    inkBtn.ButtonDirectColor = Color.White;
                    otherBtn.ButtonDirectColor = Color.White;
                    paperBtn.ButtonDirectColor = Color.White;

                };
                firstOuterStack.GestureRecognizers.Add(tapGestureFine);
                secondOuterStack.GestureRecognizers.Add(tapGestureProblem);
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    labelFirst.Text = AppResources.MapPrintedBtn;
                    labelText.Text = AppResources.MapPrintedWhatHappen;
                    labelSecond.Text = AppResources.MapPrinterFailedBtn;
                    labelFirst.FontSize = 15;
                    orLabel.Text = AppResources.MapORText;
                }
                else
                {
                    labelText.Text = "So... What happened???";

                }
                printLayout.Children.Remove(timerLabel);
                printingLayout.IsVisible = false;
                gridTimerLabel.IsVisible = false;
                printLayout.Children.Add(firstOuterStack);
                padPrint.Padding = new Thickness(0, 0, 0, 0);
                printLayout.Children.Add(orLabel);
                printLayout.Children.Add(secondOuterStack);
               
                labelText.FontSize = 21;

                return false;
            });

        }
        private string ConvertIntTimeToString(int seconds)
        {
            var timeString = "";
            System.Diagnostics.Debug.WriteLine("seconds " + seconds);


            if (seconds <= 60)
            {
                timeString = seconds.ToString();
                System.Diagnostics.Debug.WriteLine("string x" + timeString);

            }
            else
            {
                timeString = "1m " + (seconds - 60);
                System.Diagnostics.Debug.WriteLine("string f" + timeString);
            }

            System.Diagnostics.Debug.WriteLine("string g" + timeString);
            return timeString;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            
        }

        private async void NewBtn_Clicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new ScannerEndPage());
        }

        private void SmallNewBtn_Clicked(object sender, EventArgs args)
        {
            errorBtn.IsEnableButton = true;
            inkBtn.IsEnableButton = true;
            otherBtn.IsEnableButton = true;
            paperBtn.IsEnableButton = true;
        }
    }
}
