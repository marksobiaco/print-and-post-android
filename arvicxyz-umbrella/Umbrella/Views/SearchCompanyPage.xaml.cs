﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Umbrella.Constants;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchCompanyPage : ContentPage
    {
        private int _apiCall = 0;
        private int _currentCount = 0;
        private string _currentCompanyNum = "";
        private string _currentOrg = "";
        private string _currentAdd = "";
        private DetailedAddress _detailedAdd;
        private int _letterAdded = 0;
        private bool isFromPostCard = false;
        public SearchCompanyPage()
        {
            InitializeComponent();
            _detailedAdd = new DetailedAddress();
            searchBar.TextChanged += SearchBar_TextChanged;
            customProgbar.Progress = 0.25;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                compdesc.FontSize = 17;
                complabel.FontSize = 20;
                // frame.HeightRequest = 170;
                frame.WidthRequest = 280;
                dbText.FontSize = 12;
                dbFrame.WidthRequest = 300;
                dbFrame.HeightRequest = 80;
                Indicator.TextSize = 30;
                System.Diagnostics.Debug.WriteLine("size");
            }
            else if (sizeChecker > 700)
            {
                compdesc.FontSize = 17;
                complabel.FontSize = 20;
                dbText.FontSize = 15;
                //complabel.FontSize = 18;
                // frame.HeightRequest = 170;
                frame.WidthRequest = 300;
                dbFrame.WidthRequest = 340;
                dbFrame.HeightRequest = 100;
                Indicator.TextSize = 35;
            }
            else
            {
                compdesc.FontSize = 17;
                complabel.FontSize = 20;
                dbText.FontSize = 12;
                Indicator.TextSize = 25;
                //complabel.FontSize = 18;
                dbFrame.WidthRequest = 300;
                dbFrame.HeightRequest = 80;
                // frame.HeightRequest = 170;
                frame.WidthRequest = 280;
            }
        }
        public SearchCompanyPage(bool postcard)
        {
            InitializeComponent();
            _detailedAdd = new DetailedAddress();
            isFromPostCard = postcard;
            searchBar.TextChanged += SearchBar_TextChanged;
            customProgbar.Progress = 0.25;
            compdesc.Text = "Type in the name of the business you would like to send your postcard to. Our database includes over 6 million companies, partnerships and PLCs.";
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                compdesc.FontSize = 17;
                complabel.FontSize = 20;
                //complabel.FontSize = 18;
                // frame.HeightRequest = 170;
                frame.WidthRequest = 280;
                dbText.FontSize = 12;
                Indicator.TextSize = 30;
                dbFrame.WidthRequest = 300;
                dbFrame.HeightRequest = 80;
                System.Diagnostics.Debug.WriteLine("size");
            }
            else if (sizeChecker > 700)
            {
                compdesc.FontSize = 17;
                complabel.FontSize = 20;
                dbText.FontSize = 15;
                Indicator.TextSize = 35;
                //complabel.FontSize = 18;
                // frame.HeightRequest = 170;
                frame.WidthRequest = 300;
                dbFrame.WidthRequest = 340;
                dbFrame.HeightRequest = 100;
            }
            else
            {
                compdesc.FontSize = 17;
                complabel.FontSize = 20;
                dbText.FontSize = 12;
                Indicator.TextSize = 25;
                //complabel.FontSize = 18;
                dbFrame.WidthRequest = 300;
                dbFrame.HeightRequest = 80;
                // frame.HeightRequest = 170;
                frame.WidthRequest = 280;
            }
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (!CrossConnectivity.Current.IsConnected)
            {
                await DisplayAlert("", "Internet Connection Required to use this function", "OK");
            }
            Global.SetIsDonePull(false);
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        async void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            int count = ((Custom.CustomReturnTypeEntry)sender).Text.Trim().Length;
            _letterAdded += 1;
            var authorization = string.IsNullOrEmpty(DependencyService.Get<ICredentialRetriever>().GetCompanyToken()) ? "asdasdasdad" : DependencyService.Get<ICredentialRetriever>().GetCompanyToken();
            System.Diagnostics.Debug.WriteLine("letter " + authorization);

            var convert = new List<Company>();
            HttpClient client = new HttpClient();
            if (CrossConnectivity.Current.IsConnected)
            {
                try
                {
                    System.Diagnostics.Debug.WriteLine("api call " + _apiCall);
                    if (count >= 3 && _apiCall == 0)
                    {
                        _letterAdded = 0;
                        System.Diagnostics.Debug.WriteLine("in");
                        var uri = "https://api.securesupportcentre.com/api/v1/companies-house/search/companies?term=" + ((Custom.CustomReturnTypeEntry)sender).Text;
                        client.DefaultRequestHeaders.Add("Authorization", authorization);
                        confirmBtn.IsVisible = false;
                        StartBusyIndicator();
                        Global.SetIsDonePull(false);
                        databaseLine.IsVisible = false;
                        databaseStack.IsVisible = false;
                        detailsStack.IsVisible = false;
                        companyList.IsVisible = false;
                        var content = await client.GetStringAsync(uri);
                        try
                        {
                            if (content.Contains("Invalid Token"))
                            {
                                await CreateNewToken(count, 1);
                                System.Diagnostics.Debug.WriteLine("create token > 3");
                            }
                            else
                            {
                                var json = content.Replace("<", "");
                                convert = new List<Company>();
                                JObject jo = JObject.Parse(json);
                                var diskSpaceArray = jo.SelectToken("data", false).ToString();
                                convert = JsonConvert.DeserializeObject<List<Company>>(diskSpaceArray);
                                StopBusyIndicator();
                                _apiCall = 1;
                                _currentCount = count;
                                var notlisted = new Company()
                                {
                                    Label = "Company Not Listed?"
                                };
                                foreach (var item in convert)
                                {
                                    item.Label = Regex.Replace(item.Label, @"\s+", " ");
                                }
                                System.Diagnostics.Debug.WriteLine("ccc " + json);
                                convert.Add(notlisted);
                                companyList.ItemsSource = convert;
                                companyList.IsVisible = true;
                                confirmBtn.IsVisible = false;
                            }
                        }
                        catch(Exception exception)
                        {
                            System.Diagnostics.Debug.WriteLine("inner expectionss " + exception.InnerException);
                            await CreateNewToken(count, 1);
                        }

                    }
                    else if (_letterAdded == 3 && _apiCall == 1)
                    {
                        _letterAdded = 0;
                        System.Diagnostics.Debug.WriteLine("out");
                        var uri = "https://api.securesupportcentre.com/api/v1/companies-house/search/companies?term=" + ((Custom.CustomReturnTypeEntry)sender).Text;
                        client.DefaultRequestHeaders.Add("Authorization", authorization);
                        confirmBtn.IsVisible = false;
                        StartBusyIndicator();
                        databaseLine.IsVisible = false;
                        Global.SetIsDonePull(false);
                        databaseStack.IsVisible = false;
                        detailsStack.IsVisible = false;
                        companyList.IsVisible = false;
                        var content = await client.GetStringAsync(uri);
                        try
                        {
                            if (content.Contains("Invalid Token"))
                            {
                                await CreateNewToken(count);
                                System.Diagnostics.Debug.WriteLine("create token > 3");
                            }
                            else
                            {
                                var json = content.Replace("<", "");
                                convert = new List<Company>();
                                JObject jo = JObject.Parse(json);
                                var diskSpaceArray = jo.SelectToken("data", false).ToString();
                                convert = JsonConvert.DeserializeObject<List<Company>>(diskSpaceArray);
                                StopBusyIndicator();
                                _currentCount = count;
                                System.Diagnostics.Debug.WriteLine("ccc " + json);
                                var notlisted = new Company()
                                {
                                    Label = "Company Not Listed?"
                                };
                                foreach (var item in convert)
                                {
                                    item.Label = Regex.Replace(item.Label, @"\s+", " ");
                                }
                                convert.Add(notlisted);
                                companyList.ItemsSource = convert;
                                companyList.IsVisible = true;
                                confirmBtn.IsVisible = false;
                            }
                        }
                        catch(Exception exception)
                        {
                            System.Diagnostics.Debug.WriteLine("inner expection " + exception.InnerException);
                            await CreateNewToken(count);
                        }
                    }
                    else if (count <= 2)
                    {
                        _apiCall = 0;
                    }
                }
                catch(Exception ex)
                {
                    await CreateNewToken(count,1);
                    StopBusyIndicator();
                }
            }


        }
        private async Task CreateNewToken(int count)
        {
            var convert = new List<Company>();
            var client = new HttpClient();
            StartBusyIndicator();

            var uriToken = "https://api.securesupportcentre.com/oauth/token";
            var requestContent = string.Format("grant_type={0}&client_id={1}&client_secret={2}&scope={3}",
               Uri.EscapeDataString("client_credentials"),
               Uri.EscapeDataString("8"),
               Uri.EscapeDataString("Fc8TVxA16r1KtF7BU5CzslGIeFkHOz3xJStagtwJ"),
               Uri.EscapeDataString(""));
            var response = await client.PostAsync(uriToken, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));
            var st = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("repsones " + st);
            var jsons = st.Replace("<", "");
            var newToken = new CompanyToken();
            newToken = JsonConvert.DeserializeObject<CompanyToken>(jsons);
            System.Diagnostics.Debug.WriteLine("new token " + newToken.TokenType + " " + newToken.AccessToken);
            var uri = "https://api.securesupportcentre.com/api/v1/companies-house/search/companies?term=" + searchBar.Text;
            client.DefaultRequestHeaders.Add("Authorization", newToken.TokenType + " " + newToken.AccessToken);

            var token = new CancellationTokenSource();
            token.CancelAfter(7000);
            var result = await client.GetAsync(uri, token.Token);
            var content = await result.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("read async " + content);
            var json = content.Replace("<", "");
            convert = new List<Company>();
            JObject jo = JObject.Parse(json);
            var diskSpaceArray = jo.SelectToken("data", false).ToString();
            convert = JsonConvert.DeserializeObject<List<Company>>(diskSpaceArray);


            _currentCount = count;
            var notlisted = new Company()
            {
                Label = "Company Not Listed?"
            };
            foreach (var item in convert)
            {
                item.Label = Regex.Replace(item.Label, @"\s+", " ");
            }
            convert.Add(notlisted);
            companyList.ItemsSource = convert;
            companyList.IsVisible = true;
            confirmBtn.IsVisible = false;
            MessagingCenter.Send<string>(newToken.TokenType + " " + newToken.AccessToken, "SaveCompanyToken");
            StopBusyIndicator();
        }
        private async Task CreateNewToken(int count, int apicall)
        {
            var convert = new List<Company>();
            var client = new HttpClient();
            StartBusyIndicator();

            var uriToken = "https://api.securesupportcentre.com/oauth/token";
            var requestContent = string.Format("grant_type={0}&client_id={1}&client_secret={2}&scope={3}",
               Uri.EscapeDataString("client_credentials"),
               Uri.EscapeDataString("8"),
               Uri.EscapeDataString("Fc8TVxA16r1KtF7BU5CzslGIeFkHOz3xJStagtwJ"),
               Uri.EscapeDataString(""));
            var response = await client.PostAsync(uriToken, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));
            var st = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("repsones " + st);
            var jsons = st.Replace("<", "");
            var newToken = new CompanyToken();
            newToken = JsonConvert.DeserializeObject<CompanyToken>(jsons);
            System.Diagnostics.Debug.WriteLine("new token " + newToken.TokenType + " " + newToken.AccessToken);
            var uri = "https://api.securesupportcentre.com/api/v1/companies-house/search/companies?term=" + searchBar.Text;
            client.DefaultRequestHeaders.Add("Authorization", newToken.TokenType + " " + newToken.AccessToken);

            var token = new CancellationTokenSource();
            token.CancelAfter(7000);
            var result = await client.GetAsync(uri, token.Token);
            var content = await result.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine("read async " + content);
            var json = content.Replace("<", "");
            convert = new List<Company>();
            JObject jo = JObject.Parse(json);
            var diskSpaceArray = jo.SelectToken("data", false).ToString();
            convert = JsonConvert.DeserializeObject<List<Company>>(diskSpaceArray);

            _apiCall = 1;
            _currentCount = count;
            var notlisted = new Company()
            {
                Label = "Company Not Listed?"
            };
            foreach (var item in convert)
            {
                item.Label = Regex.Replace(item.Label, @"\s+", " ");
            }
            convert.Add(notlisted);
            companyList.ItemsSource = convert;
            companyList.IsVisible = true;
            confirmBtn.IsVisible = false;
            MessagingCenter.Send<string>(newToken.TokenType + " " + newToken.AccessToken, "SaveCompanyToken");
            StopBusyIndicator();
        }
        private async void BackTapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
        private async void ConfirmTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddressLetterPage(_currentCompanyNum, _currentOrg, _detailedAdd, isFromPostCard));
        }
        private async void CompanyTapped(object sender, ItemTappedEventArgs e)
        {
            var com = e.Item as Company;
            searchBar.Unfocus();
            _detailedAdd = new DetailedAddress();
            var convert = new DetailedAddress();
            HttpClient client = new HttpClient();
            var authorization = DependencyService.Get<ICredentialRetriever>().GetCompanyToken();
            if (com.Label == "Company Not Listed?")
            {
                searchBar.Text = "";
                companyList.IsVisible = false;
                detailsStack.IsVisible = false;
                confirmBtn.IsVisible = false;
                await Navigation.PushAsync(new RecipientAddressPage());
            }
            else
            {
                var uri = "https://api.securesupportcentre.com/api/v1/companies-house/company/" + com.CompanyNumber;
                client.DefaultRequestHeaders.Add("Authorization", authorization);
                StartBusyIndicator();
                var content = await client.GetStringAsync(uri);
                var isver = new VerifiedAdd();
                var json = content.Replace("<", "");
                convert = new DetailedAddress();
                JObject jo = JObject.Parse(json);
                var diskSpaceArray = jo.SelectToken("data", false).ToString();
                isver = JsonConvert.DeserializeObject<VerifiedAdd>(diskSpaceArray);
                System.Diagnostics.Debug.WriteLine(diskSpaceArray);
                JObject jo2 = JObject.Parse(diskSpaceArray);
                var array = jo2.SelectToken("registered_office_address", false).ToString();
                System.Diagnostics.Debug.WriteLine(array);
                convert = JsonConvert.DeserializeObject<DetailedAddress>(array);
                System.Diagnostics.Debug.WriteLine("item " + convert.AddLine1);
                if (isver.IsVerified == "false")
                {
                    dbImg.Source = "done";
                    dbText.Text = "A database operated by the UK government confirms that mail being sent to the above company at this address is being accepted";
                }
                else
                {
                    dbImg.Source = "exclamation_or";
                    dbText.Text = "A database operated by the UK government has reported that mail recently sent to the above company at this address has been marked as undeliverable or return to sender.";
                }
                //compAdd.Text = " " + com.AddressSnippet.Replace(",", Environment.NewLine);

                comAd1.IsVisible = string.IsNullOrEmpty(convert.AddLine1) ? false : true;
                comAd2.IsVisible = string.IsNullOrEmpty(convert.AddLine2) ? false : true;
                comLocal.IsVisible = string.IsNullOrEmpty(convert.Locality) ? false : true;
                comReg.IsVisible = string.IsNullOrEmpty(convert.Region) ? false : true;
                comPost.IsVisible = string.IsNullOrEmpty(convert.PostalCode) ? false : true;
                comCoun.IsVisible = string.IsNullOrEmpty(convert.Country) ? false : true;

                comAd1.Text = convert.AddLine1;
                comAd2.Text = convert.AddLine2;
                comLocal.Text = convert.Locality;
                comReg.Text = convert.Region;
                comPost.Text = convert.PostalCode;
                comCoun.Text = convert.Country;

                System.Diagnostics.Debug.WriteLine("com " + com.Label);
                _currentCompanyNum = com.CompanyNumber;

                _currentAdd = com.AddressSnippet;
                _currentOrg = Regex.Replace(com.Label, @"\s+", " ");
                //compNum.Text = "Company Num: " + com.CompanyNumber;
                //compStat.Text = "Status: " + com.CompanyStatus;
                compLabel.Text = com.Label;
                companyList.IsVisible = false;
                detailsStack.IsVisible = true;
                databaseLine.IsVisible = true;
                databaseStack.IsVisible = true;
                confirmBtn.IsVisible = true;
                _detailedAdd = convert;
            }
            StopBusyIndicator();
        }
    }
}