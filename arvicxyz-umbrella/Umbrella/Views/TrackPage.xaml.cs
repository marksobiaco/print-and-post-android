﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Umbrella.Models;
using Umbrella.Interfaces;
using System.Net.Http;
using Umbrella.Constants;
using Newtonsoft.Json;
using Umbrella.Resx;
using Plugin.Connectivity;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrackPage : ContentPage
    {
        public TrackPage()
        {
            InitializeComponent();
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                descLabel.FontSize = 17;
                headerText.FontSize = 21;
            }
            else if (sizeChecker > 700)
            {
                descLabel.FontSize = 17;
                headerText.FontSize = 21;
            }
            else
            {
                descLabel.FontSize = 17;
                headerText.FontSize = 21;
            }
            var lists = new List<Tracking>();
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "rhoward@raptorsms.com" || accnt.User.Username == "juju@raptorsms.com")
            {
                headerText.Text = AppResources.TrackTitle;
                descLabel.Text = AppResources.TrackDesc;
                lists = new List<Tracking>()
               {
                new Tracking()
                {
                    Add1 = "Address Line 1",
                    Add2 = "Address Line 2",
                    Town = "Town",
                    PostCode = "Postcode",
                    City = "City",
                    EnumStatus = "Collected"
                },
                new Tracking()
                {
                    Add1 = "Address Line 1",
                    Add2 = "Address Line 2",
                    Town = "Town",
                    PostCode = "Postcode",
                    City = "City",
                    EnumStatus = "Left with Neighbours"
                },
                new Tracking()
                {
                    Add1 = "Address Line 1",
                    Add2 = "Address Line 2",
                    Town = "Town",
                    PostCode = "PostCode",
                    City = "City",
                    EnumStatus = "Delivered"
                },

            };
            }
        
            sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                foreach (var item in lists)
                {
                    item.MiddleFontSize = 12;
                }
            }
            else if (sizeChecker > 700)
            {
                foreach (var item in lists)
                {
                    item.MiddleFontSize = 13;
                }
            }
            else
            {
                foreach (var item in lists)
                {
                    item.MiddleFontSize = 13;
                }
            }
            list.ItemsSource = lists;
            pdfTutorialFooter.ActiveCallback(true);
        }
        private string GetProductId(int amount)
        {
            switch (amount)
            {
                case 1000:
                    return "36";
                case 2500:
                    return "26";
                case 5000:
                    return "27";
                case 10000:
                    return "28";
                case 15000:
                    return "37";
                case 25000:
                    return "34";
                default:
                    return "";
            }
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            //await SendPurchaseOntraport(2500, "24757", "eur");
            // if(CrossConnectivity)
        }
        private async Task SendPurchaseOntraport(int amount, string customerId, string currency)
        {
            var service = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var email = service.User.Email;
            var contactId = "24757";
            using (HttpClient client = new HttpClient())
            {
                var newAmount = amount / 100;
                string xmlData = "<purchases contact_id='" + contactId + "' product_id ='" + GetProductId(amount) + "'><field name='Price'>" + newAmount + "</field></purchases>";

                var requestContent = string.Format("appid={0}&key={1}&return_id={2}&reqType={3}&data={4}",
                        Uri.EscapeDataString(OntraportApi.API_APP_ID),
                        Uri.EscapeDataString(OntraportApi.API_KEY),
                        Uri.EscapeDataString(OntraportApi.RETURN_ID.ToString()),
                        Uri.EscapeDataString(OntraportApi.REQUEST_SALE),
                        Uri.EscapeDataString(xmlData));

                var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                var _op = new TopupUploadOntraport()
                {
                    PartnerID = credential.UserID,
                    CurrencyOP = currency,
                    TopupAmount = newAmount,
                };
                var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var responseTopup = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                if (responseTopup.IsSuccessStatusCode)
                {
                    System.Diagnostics.Debug.WriteLine("contact id " + contactId);
                    string xmlDataTopup = "<contact id='" + contactId + "'><tag>Print & Post Topup SUCCESS</tag></contact>";

                    var requestContentTag = string.Format("appid={0}&key={1}&return_id={2}&reqType={3}&data={4}",
                            Uri.EscapeDataString(OntraportApi.API_APP_ID),
                            Uri.EscapeDataString(OntraportApi.API_KEY),
                            Uri.EscapeDataString(OntraportApi.RETURN_ID.ToString()),
                            Uri.EscapeDataString("add_tag"),
                            Uri.EscapeDataString(xmlDataTopup));
                    var responseTag = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS2, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));

                }

            }
        }
        private async Task SendTagFailedPaymentOntraport(string currency, int newamount)
        {
            var service = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var email = service.User.Email;
            newamount = newamount / 100;
            var contactId = "24757";
            using (HttpClient client = new HttpClient())
            {
                string xmlData = "<contact id='" + contactId + "'><tag>Print & Post Topup FAILURE</tag></contact>";

                var requestContent = string.Format("appid={0}&key={1}&return_id={2}&reqType={3}&data={4}",
                        Uri.EscapeDataString(OntraportApi.API_APP_ID),
                        Uri.EscapeDataString(OntraportApi.API_KEY),
                        Uri.EscapeDataString(OntraportApi.RETURN_ID.ToString()),
                        Uri.EscapeDataString("add_tag"),
                        Uri.EscapeDataString(xmlData));

                var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS2, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                var _op = new TopupUploadOntraport()
                {
                    PartnerID = credential.UserID,
                    CurrencyOP = currency,
                    TopupAmount = newamount,
                };
                var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var responseTopup = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
            }
        }
        private void TrackTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.royalmail.com/track-your-item#/tracking-results/GQ205120527GB"));
        }
    }
}