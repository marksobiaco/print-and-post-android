﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DefaultSelectionPage : ContentPage
    {
        private string[] SelectionLeaflet = { "Yes Please", "No Thanks" };
        private string[] SelectionInkType = { "Colour", "Black & White" };
        private string[] SelectionPaperType = { "Standard" };
        private string[] SelectionLabelType = { "Printed", "Hand Written" };
        private string[] SelectionDispatchSpeed = { "Standard", "Priority" };
        private string[] SelectionPostageClass = { "Standard", "1st Class", "Signed For" };

        private string[] SelectionInkTypeFR = { "Couleur", "Noir et Blanc" };
        private string[] SelectionPaperTypeFR = { "Standard" };
        private string[] SelectionLabelTypeFR = { "Imprimé", "Ecrit a la main" };
        private string[] SelectionDispatchSpeedFR = { "Standard", "Priorité" };
        private string[] SelectionLeafletFR = { "Oui", "Non" };
        private string[] SelectionPostageClassFR = { "Lettre Prioritaire", "Lettre Verte", "Avec accusé de réception" };
        public DefaultSelectionPage()
        {
            InitializeComponent();
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                inkLabel.Text = AppResources.DefaultPageInkText;
                paperLabel.Text = AppResources.DefaultPagePaperText;
                dispatchLabel.Text = AppResources.DefaultPageDispatchText;
                labelLabel.Text = AppResources.DefaultPageLabelText;
                postageLabel.Text = AppResources.DefaultPagePostageText;
                allowLeaflet.Text = AppResources.DefaultPageLeafText;
                this.Title = AppResources.DefaultPageTitle;
            }
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                var defaults = DependencyService.Get<ICredentialRetriever>().GetDefault();
                Dropdown.SelectedText = string.IsNullOrEmpty(defaults.InkType) ? "Couleur" : defaults.InkType;
                DropdownPaper.SelectedText = string.IsNullOrEmpty(defaults.PaperType) ? "Standard" : defaults.PaperType;
                DropdownDispatch.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard" : defaults.DispatchSpeed;
                DropdownLabelType.SelectedText = string.IsNullOrEmpty(defaults.LabelType) ? "Imprimé" : defaults.LabelType;
                DropDownLeaflet.SelectedText = string.IsNullOrEmpty(defaults.AllowLeaflet) || defaults.AllowLeaflet == "Oui" ? "Oui" : defaults.AllowLeaflet;
                DropdownPostageClass.SelectedText = string.IsNullOrEmpty(defaults.PostageClass) ? "Lettre Prioritaire" : defaults.PostageClass;
            }
            else
            {
                var defaults = DependencyService.Get<ICredentialRetriever>().GetDefault();
                Dropdown.SelectedText = string.IsNullOrEmpty(defaults.InkType) ? "Colour" : defaults.InkType;
                DropdownPaper.SelectedText = string.IsNullOrEmpty(defaults.PaperType) ? "Standard" : defaults.PaperType;
                DropdownDispatch.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard" : defaults.DispatchSpeed;
                DropdownLabelType.SelectedText = string.IsNullOrEmpty(defaults.LabelType) ? "Printed" : defaults.LabelType;
                DropDownLeaflet.SelectedText = string.IsNullOrEmpty(defaults.AllowLeaflet) ? "Yes Please" : defaults.AllowLeaflet;
                DropdownPostageClass.SelectedText = string.IsNullOrEmpty(defaults.PostageClass) ? "Standard" : defaults.PostageClass;
            }
            /*
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Dropdown.WidthRequest = 240;
                DropdownPaper.WidthRequest = 240;
                DropdownDispatch.WidthRequest = 240;
                DropdownLabelType.WidthRequest = 240;
                DropDownLeaflet.WidthRequest = 240;
                DropdownPostageClass.WidthRequest = 240;
                System.Diagnostics.Debug.WriteLine("twice2 - " + sizeChecker);
            }
            else if (sizeChecker > 700)
            {
                Dropdown.WidthRequest = 320;
                DropdownPaper.WidthRequest = 320;
                DropdownDispatch.WidthRequest = 320;
                DropdownLabelType.WidthRequest = 320;
                DropDownLeaflet.WidthRequest = 320;
                DropdownPostageClass.WidthRequest = 320;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("twice1 - " + sizeChecker);
                Dropdown.WidthRequest = 240;
                DropdownPaper.WidthRequest = 240;
                DropdownDispatch.WidthRequest = 240;
                DropdownLabelType.WidthRequest = 240;
                DropDownLeaflet.WidthRequest = 240;
                DropdownPostageClass.WidthRequest = 240;
            }
            */
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            var defaults = DependencyService.Get<ICredentialRetriever>().GetDefault();
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                Dropdown.SelectedText = string.IsNullOrEmpty(defaults.InkType) ? "Couleur" : defaults.InkType;
                DropdownPaper.SelectedText = string.IsNullOrEmpty(defaults.PaperType) ? "Standard" : defaults.PaperType;
                DropdownDispatch.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard" : defaults.DispatchSpeed;
                DropdownLabelType.SelectedText = string.IsNullOrEmpty(defaults.LabelType) ? "Imprimé" : defaults.LabelType;
                DropDownLeaflet.SelectedText = string.IsNullOrEmpty(defaults.AllowLeaflet) || defaults.AllowLeaflet == "Oui" ? "Oui" : defaults.AllowLeaflet;
                DropdownPostageClass.SelectedText = string.IsNullOrEmpty(defaults.PostageClass) ? "Lettre Prioritaire" : defaults.PostageClass;
            }
            else
            {
                Dropdown.SelectedText = string.IsNullOrEmpty(defaults.InkType) ? "Colour" : defaults.InkType;
                DropdownPaper.SelectedText = string.IsNullOrEmpty(defaults.PaperType) ? "Standard" : defaults.PaperType;
                DropdownDispatch.SelectedText = string.IsNullOrEmpty(defaults.DispatchSpeed) ? "Standard" : defaults.DispatchSpeed;
                DropdownLabelType.SelectedText = string.IsNullOrEmpty(defaults.LabelType) ? "Printed" : defaults.LabelType;
                DropDownLeaflet.SelectedText = string.IsNullOrEmpty(defaults.AllowLeaflet) ? "Yes Please" : defaults.AllowLeaflet;
                DropdownPostageClass.SelectedText = string.IsNullOrEmpty(defaults.PostageClass) ? "Standard" : defaults.PostageClass;
            }
         
        }
        private async void SaveClicked(object sender, EventArgs e)
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var defs = new Default()
            {
                InkType = Dropdown.SelectedText,
                PaperType = DropdownPaper.SelectedText,
                DispatchSpeed = DropdownDispatch.SelectedText,
                LabelType = DropdownLabelType.SelectedText,
                PartnerId = credential.UserID,
                AllowLeaflet = DropDownLeaflet.SelectedText,
                PostageClass = DropdownPostageClass.SelectedText
            };
            await DisplayAlert("", "Default Selection Saved", "OK");
            MessagingCenter.Send<Default>(defs, "SaveDefaultSelection");
            await Navigation.PopAsync();
        }
        private async void InkDropDown(object sender, EventArgs e)
        {
            try
            {
                var action = "";
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    action = await DisplayActionSheet(Dropdown.SelectedText, "Cancel", null, SelectionInkTypeFR);
                }
                else
                {
                    action = await DisplayActionSheet(Dropdown.SelectedText, "Cancel", null, SelectionInkType);
                }
                if (!action.Equals("Cancel"))
                {
                    Dropdown.SelectedText = action;
                    Dropdown.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {

            }
        }
        private async void PostageClassDroppedDown(object sender, EventArgs e)
        {
            try
            {
                var action = "";
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    action = await DisplayActionSheet(DropdownPostageClass.SelectedText, "Cancel", null, SelectionPostageClassFR);
                }
                else
                {
                    action = await DisplayActionSheet(DropdownPostageClass.SelectedText, "Cancel", null, SelectionPostageClass);
                }
                if (!action.Equals("Cancel"))
                {
                    DropdownPostageClass.SelectedText = action;
                    DropdownPostageClass.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {

            }
        }
        private async void LeafLetDroppedDown(object sender, EventArgs e)
        {
            try
            {
                var action = "";
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    action = await DisplayActionSheet(DropDownLeaflet.SelectedText, "Cancel", null, SelectionLeafletFR);
                }
                else
                {
                    action = await DisplayActionSheet(DropDownLeaflet.SelectedText, "Cancel", null, SelectionLeaflet);
                }
                if (!action.Equals("Cancel"))
                {
                    DropDownLeaflet.SelectedText = action;
                    DropDownLeaflet.SelectedTextColor = Color.Black;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("ex " + ex.InnerException + " " + ex.Message);
            }

        }
        private async void PaperDroppedDown(object sender, EventArgs e)
        {
            try
            {
                var action = "";
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    action = await DisplayActionSheet(DropdownPaper.SelectedText, "Cancel", null, SelectionPaperTypeFR);
                }
                else
                {
                    action = await DisplayActionSheet(DropdownPaper.SelectedText, "Cancel", null, SelectionPaperType);
                }
                if (!action.Equals("Cancel"))
                {
                    DropdownPaper.SelectedText = action;
                    DropdownPaper.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {

            }
          
        }
        private async void LabelTypeDroppedDown(object sender, EventArgs e)
        {
            try
            {
                var action = "";
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    action = await DisplayActionSheet(DropdownLabelType.SelectedText, "Cancel", null, SelectionLabelTypeFR);
                }
                else
                {
                    action = await DisplayActionSheet(DropdownLabelType.SelectedText, "Cancel", null, SelectionLabelType);
                }
                if (!action.Equals("Cancel"))
                {
                    DropdownLabelType.SelectedText = action;
                    DropdownLabelType.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {

            }
            
        }
        private async void DispatchSpeedDroppedDown(object sender, EventArgs e)
        {
            try
            {
                var action = "";
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    action = await DisplayActionSheet(DropdownDispatch.SelectedText, "Cancel", null, SelectionDispatchSpeedFR);
                }
                else
                {
                    action = await DisplayActionSheet(DropdownDispatch.SelectedText, "Cancel", null, SelectionDispatchSpeed);
                }
                if (!action.Equals("Cancel"))
                {
                    DropdownDispatch.SelectedText = action;
                    DropdownDispatch.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {

            }
           
        }
    }
}