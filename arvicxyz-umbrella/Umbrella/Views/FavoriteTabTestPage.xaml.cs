﻿using System;
using System.Collections.Generic;
using Umbrella.Models;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class FavoriteTabTestPage : ContentPage
    {
        private SwipeModel _swipeModel;
        public FavoriteTabTestPage(SwipeModel image)
        {
            InitializeComponent();
            _swipeModel = image;
            System.Diagnostics.Debug.WriteLine("ssssff");
            // foodImage.Source = image.ImageUri;
            this.BindingContext = image;
            foreach (var item in _swipeModel.DietaryInformation)
            {
                var label = new Label()
                {
                    Text = item,
                    FontSize = 11,
                };
                dietaryLayout.Children.Add(label);
            }
        }
        private async void SkipTapped(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}
