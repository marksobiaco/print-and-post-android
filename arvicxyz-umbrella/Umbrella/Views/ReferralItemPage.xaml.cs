﻿using Xamarin.Forms;
using Umbrella.Models;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReferralItemPage : ContentPage
    {
        public ReferralItemPage(Referral referral)
        {
            InitializeComponent();

            BindingContext = referral;
        }
    }
}
