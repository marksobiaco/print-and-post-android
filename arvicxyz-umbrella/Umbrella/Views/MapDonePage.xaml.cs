﻿using System;
using System.Collections.Generic;
using Umbrella.Interfaces;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class MapDonePage : ContentPage
    {
        private string _address;
        public MapDonePage(string address)
        {
            InitializeComponent();
            _address = address;
            var ver = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            System.Diagnostics.Debug.WriteLine("what ver " + ver);
            if (ver >= 640 && ver <= 695)
            {
                imgCheck.HeightRequest = 200;
                imgCheck.WidthRequest = 200;
            }
            else if (ver > 700)
            {
                imgCheck.HeightRequest = 300;
                imgCheck.WidthRequest = 300;
            }
            else
            {
                imgCheck.HeightRequest = 200;
                imgCheck.WidthRequest = 200;
            }
        }
        async void ColoredDirectedButton_Clicked_1(System.Object sender, System.EventArgs args)
        {
            var enc = Uri.EscapeUriString(_address);
            System.Diagnostics.Debug.WriteLine("encs" + enc);
            await Launcher.OpenAsync("http://maps.apple.com/?daddr=" + enc);
        }

        async void ColoredDirectedButton_Clicked_2(System.Object sender, System.EventArgs args)
        {
            var enc = Uri.EscapeUriString(_address);
            System.Diagnostics.Debug.WriteLine("encs" + enc);
            await Launcher.OpenAsync("http://maps.google.com/maps?daddr=" + enc);
        }
    }
}
