﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreviousAddressesPage : ContentPage
    {
        bool _isFromPostCard;
        public PreviousAddressesPage(bool isFromPostCard)
        {
            InitializeComponent();
            _isFromPostCard = isFromPostCard;
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var list = await App.PreviousContactsDatabase.GetItemsAsync();
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                foreach (var item in list)
                {
                    item.MiddleFontSize = 12;
                }
            }
            else if (sizeChecker > 700)
            {
                foreach (var item in list)
                {
                    item.MiddleFontSize = 15;
                }
            }
            else
            {
                foreach (var item in list)
                {
                    item.MiddleFontSize = 12;
                }
            }
            PhoneListView.ItemsSource = list;
        }
        private async void BackTapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
        private async void PreviousAddressTapped(object sender, ItemTappedEventArgs e)
        {
            var lead = e.Item as PhoneContact;
            if (_isFromPostCard)
            {
                var page = new PostCardRecipientPage(lead);
                await Navigation.PushAsync(page);
            }
            else
            {
                var page = new RecipientAddressPage(lead);
                await Navigation.PushAsync(page);
            }
        }
    }
}