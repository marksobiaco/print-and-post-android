﻿using System;
using System.Collections.Generic;
using Umbrella.Custom;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class MapSelectedPage : ContentPage
    {
        private CustomPin _customPin;
        public MapSelectedPage(CustomPin pin)
        {
            InitializeComponent();
            _customPin = new CustomPin();
            _customPin = pin;
            pinNameLabel.Text = _customPin.Name;
            addressLabel.Text = _customPin.Address;
            var currentday = DateTime.Now.DayOfWeek;
            if (_customPin.Name.Contains("Costa Coffee"))
            {
                iconSource.Source = "costa_icon_large";

            }
            else if (_customPin.Name.Contains("Caffè Nero"))
            {
                iconSource.Source = "cafenero_icon_large";
            }
            else
            {
                iconSource.Source = "print_green_p";
            }
            if (DayOfWeek.Monday == currentday)
            {
                monLabel.FontAttributes = FontAttributes.Bold;
                monLabel.TextColor = Color.FromHex("#013220");
                monLabel.BackgroundColor = Color.FromHex("#EAECCA");
                MonOpenTime.BackgroundColor = Color.FromHex("#EAECCA");
                MonCloseTime.BackgroundColor = Color.FromHex("#EAECCA");
            }
            else if (DayOfWeek.Tuesday == currentday)
            {
                tuesLabel.FontAttributes = FontAttributes.Bold;
                tuesLabel.TextColor = Color.FromHex("#013220");
                tuesLabel.BackgroundColor = Color.FromHex("#EAECCA");
                TuesOpenTime.BackgroundColor = Color.FromHex("#EAECCA");
                TuesCloseTime.BackgroundColor = Color.FromHex("#EAECCA");
            }
            else if (DayOfWeek.Wednesday == currentday)
            {
                wedLabel.FontAttributes = FontAttributes.Bold;
                wedLabel.TextColor = Color.FromHex("#013220");
                wedLabel.BackgroundColor = Color.FromHex("#EAECCA");
                WedCloseTime.BackgroundColor = Color.FromHex("#EAECCA");
                WedOpenTime.BackgroundColor = Color.FromHex("#EAECCA");
            }
            else if (DayOfWeek.Thursday == currentday)
            {
                thurLabel.FontAttributes = FontAttributes.Bold;
                thurLabel.TextColor = Color.FromHex("#013220");
                thurLabel.BackgroundColor = Color.FromHex("#EAECCA");
                ThurCloseTime.BackgroundColor = Color.FromHex("#EAECCA");
                ThurOpenTime.BackgroundColor = Color.FromHex("#EAECCA");
            }
            else if (DayOfWeek.Friday == currentday)
            {
                friLabel.FontAttributes = FontAttributes.Bold;
                friLabel.TextColor = Color.FromHex("#013220");
                friLabel.BackgroundColor = Color.FromHex("#EAECCA");
                FriCloseTime.BackgroundColor = Color.FromHex("#EAECCA");
                FriOpenTime.BackgroundColor = Color.FromHex("#EAECCA");
            }
            else if (DayOfWeek.Saturday == currentday)
            {
                satLabel.FontAttributes = FontAttributes.Bold;
                satLabel.TextColor = Color.FromHex("#013220");
                satLabel.BackgroundColor = Color.FromHex("#EAECCA");
                SatCloseTime.BackgroundColor = Color.FromHex("#EAECCA");
                SatOpenTime.BackgroundColor = Color.FromHex("#EAECCA");
            }
            else if (DayOfWeek.Sunday == currentday)
            {
                sunLabel.FontAttributes = FontAttributes.Bold;
                sunLabel.TextColor = Color.FromHex("#013220");
                SunCloseTime.BackgroundColor = Color.FromHex("#EAECCA");
                sunLabel.BackgroundColor = Color.FromHex("#EAECCA");
                SunOpenTime.BackgroundColor = Color.FromHex("#EAECCA");
            }
            if (_customPin.Periods != null)
            {
                foreach (var item in _customPin.Periods)
                {
                    if (item.open.day == 1)
                    {
                        MonOpenTime.Text = item.open.time.Insert(2, ":");
                        MonCloseTime.Text = item.close.time.Insert(2, ":");
                    }
                    if (item.open.day == 2)
                    {
                        TuesOpenTime.Text = item.open.time.Insert(2, ":"); ;
                        TuesCloseTime.Text = item.close.time.Insert(2, ":"); ;
                    }
                    if (item.open.day == 3)
                    {
                        WedOpenTime.Text = item.open.time.Insert(2, ":"); ;
                        WedCloseTime.Text = item.close.time.Insert(2, ":"); ;
                    }
                    if (item.open.day == 4)
                    {
                        ThurOpenTime.Text = item.open.time.Insert(2, ":"); ;
                        ThurCloseTime.Text = item.close.time.Insert(2, ":"); ;
                    }
                    if (item.open.day == 5)
                    {
                        FriOpenTime.Text = item.open.time.Insert(2, ":"); ;
                        FriCloseTime.Text = item.close.time.Insert(2, ":"); ;
                    }
                    if (item.open.day == 6)
                    {
                        SatOpenTime.Text = item.open.time.Insert(2, ":"); ;
                        SatCloseTime.Text = item.close.time.Insert(2, ":"); ;
                    }
                    if (item.open.day == 0)
                    {
                        SunOpenTime.Text = item.open.time.Insert(2, ":"); ;
                        SunCloseTime.Text = item.close.time.Insert(2, ":"); ;
                    }
                }
            }
        }
        async void ColoredDirectedButton_Clicked(System.Object sender, System.EventArgs args)
        {
            await Navigation.PushAsync(new MapSelectLocationPage(_customPin.Address));
        }

        async void ColoredDirectedButton_Clicked_1(System.Object sender, System.EventArgs args)
        {
            var enc = Uri.EscapeUriString(_customPin.Address);
            System.Diagnostics.Debug.WriteLine("encs" + enc);
            await Launcher.OpenAsync("http://maps.apple.com/?daddr=" + enc);
        }
        async void ColoredDirectedButton_Clicked_3(System.Object sender, System.EventArgs args)
        {
            await Launcher.OpenAsync(_customPin.Url);
        }

        async void ColoredDirectedButton_Clicked_2(System.Object sender, System.EventArgs args)
        {
            var enc = Uri.EscapeUriString(_customPin.Address);
            System.Diagnostics.Debug.WriteLine("encs" + enc);
            await Launcher.OpenAsync("http://maps.google.com/maps?daddr=" + enc);
        }
    }
}
