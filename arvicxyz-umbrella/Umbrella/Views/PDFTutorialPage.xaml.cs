﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Interfaces;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PDFTutorialPage : ContentPage
    {

        public PDFTutorialPage()
        {
            InitializeComponent();
            Global.SetUploadPageType("");

        }
        private void TrainingClicked(object sender, EventArgs args)
        {
            Device.OpenUri(new Uri("https://www.ManagedMailService.co.uk/print-and-post-app-android-training/"));
        }
        private void PostcardClicked(object sender, EventArgs args)
        {
            var permission = DependencyService.Get<ILaunchCalendar>().CheckIfPermissionAllowed();
            if (permission)
            {
                Global.SetUploadPageType("PDFTutorialPage");
                DependencyService.Get<ILaunchCalendar>().LaunchSingleImagePicker();
            }
            else
            {
                Global.SetUploadPageType("PDFTutorialPage");
                DependencyService.Get<ILaunchCalendar>().AskPermission();
            }
        }
        private async void ImportClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new ImportPage());
        }
        private async void TrackClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new TrackPage());
        }
      
        protected override void OnAppearing()
        {
            base.OnAppearing();
            DependencyService.Get<ILaunchCalendar>().AskPermission();
        }

    }
}