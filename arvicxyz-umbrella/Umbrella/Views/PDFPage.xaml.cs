﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Interfaces;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PDFPage : ContentPage
    {
        public PDFPage()
        {
            InitializeComponent();
            Global.SetIsDonePick(false);
            Global.SetIsFromLocalFiles(false);
            Global.SetBoolIsHomePage(false);
            try
            {
                var name = Global.GetPdfName();
                var pdfsplit = name.Split('.');
                var pathwithout = Path.GetFileNameWithoutExtension(pdfsplit[0]);
                System.Diagnostics.Debug.WriteLine(pathwithout + " asds " + pdfsplit[0] + " " + pdfsplit[1]);
                var count = pathwithout.Length;
                if (count > 20)
                {
                    var limitstring = pathwithout.Substring(0, 20) + ".pdf";
                    pdfText.Text = limitstring;
                }
                else
                {
                    pdfText.Text = name;
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("PDF exception " + ex.InnerException + " " + ex.Message);
            }
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (credential.UserID == "24757" || credential.UserID == "80385")
            {
                printCollectLayout.IsVisible = true;
            }
            if (credential.User.Username == "juju@raptorsms.com")
            {
                newContactLabel.Text = AppResources.PostcardNewContact;
                postCompLabel.Text = AppResources.PostcardCompany;
                postMeLabel.Text = AppResources.PostcardMe;
            }

        }
        private async void PrintCollectTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PrintCollectPage());
        }
        async void PreviousTapped(object sender, EventArgs args)
        {
            await Navigation.PopAsync();
        }
        private async void NewContactTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RecipientAddressPage());
        }
        private async void ExistingContactTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PreviousAddressesPage(false));
        }
        private async void SendToMeTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RecipientAddressPage("send"));
        }
        private async void SendCompanyTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SearchCompanyPage());
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}