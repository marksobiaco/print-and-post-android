﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainTimeSchedule : CarouselPage
    {
        private List<string> timeStrings;
        private List<string> laterTimeStrings;
        private List<string> earlierTimeStrings;
        private string _classIdLastTap = "";
        public MainTimeSchedule()
        {
            InitializeComponent();
            this.CurrentPage = mainPage;
            NavigationPage.SetBackButtonTitle(this, "");
            earlierTimeStrings = new List<string>()
            {
                "7:00 AM",
                "7:20 AM",
                "7:40 AM",
                "8:00 AM",
                "8:20 AM",
                "8:40 AM",
            };
            timeStrings = new List<string>()
            {
                "9:00 AM",
                "9:20 AM",
                "9:40 AM",
                "10:00 AM",
                "10:20 AM",
                "10:40 AM",
            };
            laterTimeStrings = new List<string>()
            {
                "11:00 AM",
                "11:20 AM",
                "11:40 AM",
                "12:00 PM",
                "12:20 PM",
                "12:40 PM",
            };
            dateNow.Text = DateTime.Now.ToString("yyyy-MM-dd");
            var secondCount = 0;
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (s, e) => {
                var fr = (Frame)s;
                var stacklayout = (StackLayout)fr.Content;
                var label = (Label)stacklayout.Children[1];
                var image = (Image)stacklayout.Children[0];
                if (label.Text != _classIdLastTap)
                {
                    foreach (Frame item in timeGrid.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    foreach (Frame item in timeGridEarlier.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    foreach (Frame item in timeGridLater.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    image.IsVisible = true;
                    fr.BackgroundColor = Color.Gray;
                    _classIdLastTap = label.Text;

                }
                else
                {
                    fr.BackgroundColor = Color.FromHex("#363636");
                    _classIdLastTap = "";
                    image.IsVisible = false;
                }

            };
            for (int i = 0; i < timeStrings.Count; i++)
            {
                string time = timeStrings[i];
                if (i <= 2)
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);

                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizer);
                    timeGrid.Children.Add(frame, 0, i);
                }
                else
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);
                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizer);
                    timeGrid.Children.Add(frame, 1, secondCount);
                    secondCount += 1;
                }

            }
            dateNowLater.Text = DateTime.Now.ToString("yyyy-MM-dd");
            var secondCountLater = 0;
            var tapGestureRecognizerLater = new TapGestureRecognizer();
            tapGestureRecognizerLater.Tapped += (s, e) => {
                var fr = (Frame)s;
                var stacklayout = (StackLayout)fr.Content;
                var label = (Label)stacklayout.Children[1];
                var image = (Image)stacklayout.Children[0];
                if (label.Text != _classIdLastTap)
                {
                    foreach (Frame item in timeGrid.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    foreach (Frame item in timeGridEarlier.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    foreach (Frame item in timeGridLater.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    image.IsVisible = true;
                    fr.BackgroundColor = Color.Gray;
                    _classIdLastTap = label.Text;

                }
                else
                {
                    fr.BackgroundColor = Color.FromHex("#363636");
                    _classIdLastTap = "";
                    image.IsVisible = false;
                }

            };
            for (int i = 0; i < laterTimeStrings.Count; i++)
            {
                string time = laterTimeStrings[i];
                if (i <= 2)
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);

                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizerLater);
                    timeGridLater.Children.Add(frame, 0, i);
                }
                else
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);
                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizerLater);
                    timeGridLater.Children.Add(frame, 1, secondCountLater);
                    secondCountLater += 1;
                }

            }

            dateNowEarlier.Text = DateTime.Now.ToString("yyyy-MM-dd");
            var secondCountEarlier = 0;
            var tapGestureRecognizerEarlier = new TapGestureRecognizer();
            tapGestureRecognizerEarlier.Tapped += (s, e) => {
                var fr = (Frame)s;
                var stacklayout = (StackLayout)fr.Content;
                var label = (Label)stacklayout.Children[1];
                var image = (Image)stacklayout.Children[0];
                if (label.Text != _classIdLastTap)
                {
                    foreach (Frame item in timeGrid.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    foreach (Frame item in timeGridEarlier.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    foreach (Frame item in timeGridLater.Children)
                    {
                        item.BackgroundColor = Color.FromHex("#363636");
                        var stack = (StackLayout)item.Content;
                        var newImage = (Image)stack.Children[0];
                        newImage.IsVisible = false;
                    }
                    image.IsVisible = true;
                    fr.BackgroundColor = Color.Gray;
                    _classIdLastTap = label.Text;

                }
                else
                {
                    fr.BackgroundColor = Color.FromHex("#363636");
                    _classIdLastTap = "";
                    image.IsVisible = false;
                }

            };
            for (int i = 0; i < earlierTimeStrings.Count; i++)
            {
                string time = earlierTimeStrings[i];
                if (i <= 2)
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);

                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizerEarlier);
                    timeGridEarlier.Children.Add(frame, 0, i);
                }
                else
                {
                    var stack = new StackLayout()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = 0
                    };
                    var label = new Label()
                    {
                        Text = time,
                        HorizontalTextAlignment = TextAlignment.End,
                        FontSize = 15,
                        TextColor = Color.White,
                        FontAttributes = FontAttributes.Bold,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                    };
                    var image = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        Source = "check_mark",
                        HeightRequest = 15,
                        WidthRequest = 15,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Start,
                        IsVisible = false,
                    };
                    stack.Children.Add(image);
                    stack.Children.Add(label);
                    var frame = new Frame()
                    {
                        Content = stack,
                        HasShadow = false,
                        CornerRadius = 17,
                        Padding = new Thickness(10),
                        BackgroundColor = Color.FromHex("#363636"),
                    };
                    frame.GestureRecognizers.Add(tapGestureRecognizerEarlier);
                    timeGridEarlier.Children.Add(frame, 1, secondCountEarlier);
                    secondCountEarlier += 1;
                }

            }
        }
        async void BackToSchedulePage(object sender, EventArgs args)
        {
            Global.SetChosenTime(_classIdLastTap);
            await Navigation.PopAsync();
        }
        void EarlierMoveLaterPage(object sender, EventArgs args)
        {
            this.CurrentPage = this.Children[1];
        }
        void LaterPageMoveEarlierPage(object sender, EventArgs args)
        {
            this.CurrentPage = this.Children[1];
        }
        void MoveEarlierPage(object sender, EventArgs args)
        {
            this.CurrentPage = this.Children[0];
        }
        void MoveLaterPage(object sender, EventArgs args)
        {
            this.CurrentPage = this.Children[2];
        }
    }
}
