﻿using Com.OneSignal;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        public SettingsPage()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
            verNum.Text = "Version: " + DependencyService.Get<ILaunchCalendar>().GetVersionNumber();
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                var defNotification = DependencyService.Get<ICredentialRetriever>().GetDefaultNotification();
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (defNotification.GeneralNotif == "Yes")
                {
                    OneSignal.Current.SendTag("general", credential.UserID);
                }
                else
                {
                    OneSignal.Current.DeleteTag("general");
                }
                if (defNotification.NewLeadNotif == "Yes")
                {
                    OneSignal.Current.SendTag("signed_alerts", credential.UserID);
                }
                else
                {
                    OneSignal.Current.DeleteTag("signed_alerts");
                }
                if (defNotification.NewMessageNotif == "Yes")
                {
                    OneSignal.Current.SendTag("special_delivery", credential.UserID);
                }
                else
                {
                    OneSignal.Current.DeleteTag("special_delivery");
                }
                if (defNotification.LeadConvertedNotif == "Yes")
                {
                    OneSignal.Current.SendTag("refer_earn_updates", credential.UserID);
                }
                else
                {
                    OneSignal.Current.DeleteTag("refer_earn_updates");
                }
            };
        }

        private async void SettingsItemTapped(object sender, ItemTappedEventArgs e)
        {
            var menu = e.Item as SettingsItem;

            if (menu.Target != null)
            {
              
                if (menu.Target.Name == "PrivacyPolicyPage")
                {
                    //Device.OpenUri(new Uri("https://www.umbrellasupport.co.uk/privacy-policy/"));
                    Device.OpenUri(new Uri("https://www.managedmailservice.co.uk/privacy-policy/"));
                }
                else if (menu.Target.Name == "TermsAndConditionsPage")
                {
                    Device.OpenUri(new Uri("https://www.umbrellasupport.co.uk/terms-of-service/ "));
                }
                else if (menu.Target.Name == "HomePage")
                {
                    Device.OpenUri(new Uri("https://www.managedmailservice.co.uk/pricing "));
                }
                else if(menu.Target.Name == "DonePage")
                {
                    try
                    {
                        var scanner = DependencyService.Get<ILaunchCalendar>();
                        var result = await scanner.LaunchScanner();
                        if (result != null)
                        {
                            if (result != "Cancelled")
                            {
                                await DisplayAlert("", "The data is " + result, "OK");
                                System.Diagnostics.Debug.WriteLine(result);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine("ex " + ex.InnerException + " " + ex.Message);

                    }
                }
                else if (menu.Target.Name == "QRCodeReaderPage")
                {
                    await Navigation.PushAsync(new QRCodeReaderPage());
                    /*
                    
                    */
                }
                else
                {
                    var page = (ContentPage)Activator.CreateInstance(menu.Target);
                    await Navigation.PushAsync(page);
                }

            }
            else
            {
                string action = "Logout";
                if (menu.Title.Equals(action))
                {
                    OneSignal.Current.SetSubscription(false);
                    MessagingCenter.Send<string>(action, "RemoveDefaultNotification");
                    OneSignal.Current.DeleteTags(new List<string>()
                    {
                        "general",
                        "signed_alerts",
                        "special_delivery",
                        "refer_earn_updates",
                    });
                    MessagingCenter.Send<string>(action, "RemoveUserData");
                    MessagingCenter.Send<string>(action, "RemoveStoreStatus");
                    MessagingCenter.Send<string>(action, "RemoveDefaultCurrency");
                    MessagingCenter.Send<string>(action, "RemoveDefaultSelection");
                    Global.SetByte(new byte[] { });
                    Global.SetPdfName("");
                    //await Navigation.PopModalAsync();
                    App.Current.MainPage = new NavigationPage(new LoginPage());
                    await DisplayAlert("Logout Success!", "You have successfully signed out your account.", "OK");
                }
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (SettingsListView.SelectedItem == null)
            {
                return;
            }
            SettingsListView.SelectedItem = null;
        }
    }
}
