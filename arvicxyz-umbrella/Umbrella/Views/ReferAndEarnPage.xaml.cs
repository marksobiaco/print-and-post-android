﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Services;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReferAndEarnPage : ContentPage
    {
        private string[] SelectionArray = { "Friend", "Colleague", "Family Member", "Someone Just Met", "Other" };
        private ReferralsService _referralService;
       
        public ReferAndEarnPage()
        {
            InitializeComponent();
            Global.SetIsNotif(false);
            Global.SetNotifTopic("");
            NavigationPage.SetBackButtonTitle(this, "");

            _referralService = new ReferralsService();
            ReferButton.IsEnableButton = CrossConnectivity.Current.IsConnected;

            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                ReferButton.IsEnableButton = args.IsConnected ? true : false;
            };
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            System.Diagnostics.Debug.WriteLine("height  " + sizeChecker);
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Editor.HeightRequest = 120;
                PreviousReferralsTab.LabelSize = 12;
                ReferAndEarnTab.LabelSize = 12;
                ReferralsListView.HeightRequest = 600;
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                Editor.HeightRequest = 190;
                ReferralsListView.HeightRequest = 750;
                PreviousReferralsTab.LabelSize = 15;
                ReferAndEarnTab.LabelSize = 15;
                Indicator.TextSize = 35;
            }
            else
            {
                Editor.HeightRequest = 120;
                PreviousReferralsTab.LabelSize = 12;
                ReferralsListView.HeightRequest = 550;
                ReferAndEarnTab.LabelSize = 12;
                Indicator.TextSize = 25;
            }
            ReferButton.Clicked += new SingleClick(ReferNowClicked).Click;
            ShowReferAndEarn();
        }

        public ReferAndEarnPage(bool prev)
        {
            InitializeComponent();
            Global.SetIsNotif(false);
            Global.SetNotifTopic("");
            NavigationPage.SetBackButtonTitle(this, "");
            _referralService = new ReferralsService();
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            System.Diagnostics.Debug.WriteLine("height  " + sizeChecker);
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Editor.HeightRequest = 120;
                PreviousReferralsTab.LabelSize = 12;
                ReferAndEarnTab.LabelSize = 12;
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                Editor.HeightRequest = 190;
                ReferralsListView.HeightRequest = 750;
                PreviousReferralsTab.LabelSize = 15;
                ReferAndEarnTab.LabelSize = 15;
                Indicator.TextSize = 35;
            }
            else
            {
                Editor.HeightRequest = 120;
                Indicator.TextSize = 25;
                PreviousReferralsTab.LabelSize = 12;
                ReferAndEarnTab.LabelSize = 12;
            }
            ReferButton.IsEnableButton = CrossConnectivity.Current.IsConnected;
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                ReferButton.IsEnableButton = args.IsConnected ? true : false;
            };
          
            ReferButton.Clicked += new SingleClick(ReferNowClicked).Click;
            ShowPreviousReferrals();
        }
        private void ReferAndEarnButtonTapped(object sender, EventArgs args)
        {
            ShowReferAndEarn();
        }
        private async void ReferAFriendTapped(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new ReferFriendPage());
        }
        private void PreviousReferralsButtonTapped(object sender, EventArgs args)
        {
            ShowPreviousReferrals();
        }
      
        private async void DropdownTapped(object sender, EventArgs e)
        {
            try
            {
                var action = await DisplayActionSheet(Dropdown.SelectedText, "Cancel", null, SelectionArray);
                if (!action.Equals("Cancel"))
                {
                    Dropdown.SelectedText = action;
                    Dropdown.SelectedTextColor = (Color)Application.Current.Resources["PrimaryTextColor"];
                }
            }
            catch (Exception ex)
            {

            }
           
        }
        private void EditorFocused(object sender, EventArgs e)
        {
            Editor.Focus();
            if (Editor.Text == "What you said so far")
            {
                Editor.Text = "";
                Editor.TextColor = (Color) Application.Current.Resources["PrimaryTextColor"];
            }
        }
        private void EditorUnfocused(object sender, EventArgs e)
        {
            Editor.Unfocus();
            if (Editor.Text == "")
            {
                Editor.Text = "What you said so far";
                Editor.TextColor = (Color) Application.Current.Resources["SecondaryTextColor"];
            }
        }
        private async void ReferralsItemTapped(object sender, ItemTappedEventArgs e)
        {
            var referral = e.Item as Referral;
            var page = new ReferralItemPage(referral);
            await Navigation.PushAsync(page);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ReferFooter.ActiveReferAndEarn(true);
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            ReferFooter.ActiveReferAndEarn(false);
            ReferAndEarnTab.BackgroundColor = Color.Default;
            if (ReferralsListView.SelectedItem == null)
            {
                return;
            }
            ReferralsListView.SelectedItem = null;
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;

        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }

        private async void ReferNowClicked(object sender, EventArgs e)
        {
            var page = new ReferAndEarnPage();
            if (EmailEntry.TextColor == Color.Red)
            {
                await DisplayAlert("Invalid Data", "Invalid Email Format", "OK");
            }
            else if(string.IsNullOrEmpty(FirstnameEntry.Text) || string.IsNullOrEmpty(LastnameEntry.Text) || string.IsNullOrEmpty(EmailEntry.Text) ||
                string.IsNullOrEmpty(PhoneNumberEntry.Text) || string.IsNullOrEmpty(MobileNumberEntry.Text) || string.IsNullOrEmpty(Editor.Text) || string.IsNullOrEmpty(Dropdown.SelectedText)
                || Editor.Text == "What you said so far" || Dropdown.SelectedText == "Select...")
            {
                await DisplayAlert("Submission Failed", "Please fill up all the entry", "OK");

                if (string.IsNullOrEmpty(FirstnameEntry.Text))
                {
                    FirstnameEntry.IsBorderErrorVisible = true;
                    FirstnameEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (string.IsNullOrEmpty(LastnameEntry.Text))
                {
                    LastnameEntry.IsBorderErrorVisible = true;
                    LastnameEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (string.IsNullOrEmpty(MobileNumberEntry.Text))
                {
                    MobileNumberEntry.IsBorderErrorVisible = true;
                    MobileNumberEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (string.IsNullOrEmpty(PhoneNumberEntry.Text))
                {
                    PhoneNumberEntry.IsBorderErrorVisible = true;
                    PhoneNumberEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (string.IsNullOrEmpty(EmailEntry.Text))
                {
                    EmailEntry.IsBorderErrorVisible = true;
                    EmailEntry.BackgroundColor = Color.FromHex("#F9E99B");
                }
                if (Editor.Text == "What you said so far")
                {
                    Editor.IsBorderErrorVisible = true;
                    Editor.BackgroundColor = Color.FromHex("#F9E99B");
                }
            }
            else
            {
                StartBusyIndicator();
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                var contact_id = await DependencyService.Get<IOntraportContactIdRetriever>().GetOntraportContactId(credential.User.Email);
                var isSucess = await _referralService.SubmitOntraportContactApi(new OntraportContact()
                              {
                                Firstname = FirstnameEntry.Text,
                                Lastname = LastnameEntry.Text,
                                Company = BusinessNameEntry.Text,
                                Email = EmailEntry.Text,
                                OfficePhone = PhoneNumberEntry.Text,
                                CellPhone = MobileNumberEntry.Text,
                                Message = Editor.Text,
                                PhoneUsed = Device.RuntimePlatform == Device.Android ? 519 : 518,
                                Relationship = ConvertRelationshipToId(Dropdown.SelectedText),
                                AccountUsed = contact_id,
                              });
                if (isSucess) {
                    StopBusyIndicator();
                    await DisplayAlert("Success", "Submission Complete!", "OK");
                    Navigation.InsertPageBefore(new HomePage(), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Failed", "Submission Failed!", "OK");
                }
            }
            StopBusyIndicator();
        }
        private void ShowReferAndEarn()
        {
            TabButtonManager.SelectTab(ReferAndEarnTab);
            TabButtonManager.DeselectTab(PreviousReferralsTab);
            ReferAndEarnTab.Icon = "refer_and_earn_red";
            PreviousReferralsTab.Icon = "previous_referrals";

            ReferAndEarnLayout.IsVisible = true;
            ReferralsListView.IsVisible = false;
        }
        private async void ShowPreviousReferrals()
        {
            TabButtonManager.DeselectTab(ReferAndEarnTab);
            TabButtonManager.SelectTab(PreviousReferralsTab);
            ReferAndEarnTab.Icon = "refer_and_earn";
            PreviousReferralsTab.Icon = "previous_referrals_red";

            ReferAndEarnLayout.IsVisible = false;
            ReferralsListView.IsVisible = true;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            var list = new List<Referral>();
            if (CrossConnectivity.Current.IsConnected)
            {
                StartBusyIndicator();
                list = await Task.Run(() => _referralService.GetEarningsData());
                if (sizeChecker >= 640 && sizeChecker <= 695)
                {
                    foreach (var item in list)
                    {
                        item.MiddleFontSize = 12;
                    }
                }
                else if (sizeChecker > 700)
                {
                    foreach (var item in list)
                    {
                        item.MiddleFontSize = 15;
                    }
                }
                else
                {
                    foreach (var item in list)
                    {
                        item.MiddleFontSize = 12;
                    }
                }
                await App.ReferEarnDatabase.SaveReferralsAsync(list);
            }
            else
            {
                try
                {
                    list = await App.ReferEarnDatabase.GetItemsAsync();
                    if (sizeChecker >= 640 && sizeChecker <= 695)
                    {
                        foreach (var item in list)
                        {
                            item.MiddleFontSize = 12;
                        }
                    }
                    else if (sizeChecker > 700)
                    {
                        foreach (var item in list)
                        {
                            item.MiddleFontSize = 15;
                        }
                    }
                    else
                    {
                        foreach (var item in list)
                        {
                            item.MiddleFontSize = 12;
                        }
                    }
                    if (!list.Any())
                    {
                        await DisplayAlert("", "No Saved Data", "OK");
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Exception " + ex.Message);
                    await DisplayAlert("", "No Saved Data", "OK");
                }
            }
            StopBusyIndicator();
            ReferralsListView.ItemsSource = list;
        }
        private int ConvertRelationshipToId(string relationship)
        {
            switch (relationship)
            {
                case "Other":
                    return 1023;
                case "Family Member":
                    return 1024;
                case "Friend":
                    return 1025;
                case "Someone Just Met":
                    return 1026;
                case "Colleague":
                    return 1027;
                default:
                    return 0;
            }
        }
    }
}
