﻿using System;
using System.Collections.Generic;
using Umbrella.Interfaces;
using Xamarin.Forms;

namespace Umbrella.Views
{
    public partial class ScannerEndPage : ContentPage
    {
        public ScannerEndPage()
        {
            InitializeComponent();
            var ver = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            System.Diagnostics.Debug.WriteLine("what ver " + ver);
            if (ver >= 640 && ver <= 695)
            {
                imgCheck.HeightRequest = 145;
                imgCheck.WidthRequest = 145;
                miniDesc.Margin = 10;
                pdfText.FontSize = 14;
                System.Diagnostics.Debug.WriteLine("what verssss " + ver);
                miniDesc.LineHeight = 1.5;
                uploadTxt.Margin = new Thickness(0, 3, 0, 3);
            }
            else if (ver > 700)
            {
                imgCheck.HeightRequest = 300;
                imgCheck.WidthRequest = 300;
            }
            else
            {
                imgCheck.HeightRequest = 145;
                imgCheck.WidthRequest = 145;
                miniDesc.Margin = 10;
                pdfText.FontSize = 14;
                System.Diagnostics.Debug.WriteLine("what verssss " + ver);
                miniDesc.LineHeight = 1.5;
                System.Diagnostics.Debug.WriteLine("what verxxx " + ver);
            }
        }
        async void ColoredDirectedButton_Clicked_1(System.Object sender, System.EventArgs args)
        {
            await Navigation.PushModalAsync(new NavigationPage(new HomePage()));
        }

        async void ColoredDirectedButton_Clicked_2(System.Object sender, System.EventArgs args)
        {
            await Navigation.PushAsync(new ReferFriendPage());
        }
    }
}
