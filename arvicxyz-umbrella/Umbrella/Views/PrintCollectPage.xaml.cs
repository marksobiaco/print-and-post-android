﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Umbrella.Constants;
using Umbrella.Custom;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Utilities;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrintCollectPage : ContentPage
    {
        Position mapPosition;
        Xamarin.Essentials.Location userLocation;
        private int appeared = 0;
        Position br = new Position(51.381196, 0.077206);
        Position sw = new Position(51.409095, -0.188041);
        Position se = new Position(51.492012, -0.108623);
        Position w = new Position(51.519513, -0.153156);
        Position ssw = new Position(51.506977, -0.131610);
        string br_placeId = "ChIJi6T0nnGr2EcRnjIYNlUStb4";
        string sw_placeId = "ChIJhSyO5Z8IdkgRr4fzdm9p144";
        string se_placeId = "ChIJrVbUUpYEdkgRjJsjdWs3fOg";
        string w_placeId = "ChIJt6r28M0adkgRPJmshU_nszg";
        string ssw_placeId = "ChIJEQ3orNEEdkgRD3YeTvvl59s";


        //original pins
        string coffee_placeId = "ChIJOZfSraYIdkgR6KSi1hzt5nY";
        Position coffee = new Position(51.415714, -0.193012);
        string cafenero_cut_placeId = "ChIJNUaL8boEdkgRphp8Qh_oZCQ";
        Position cafenero_cut = new Position(51.50275329999999, -0.1079413);
        string cafenero_tooting_placeId = "ChIJtc50sgsGdkgRt8QWSYgkSsQ";
        Position cafenero_tooting = new Position(51.4280269, -0.1676869);
        string costacoffee_petts_placeId = "ChIJQS5rEp6r2EcRQxKAgBIsdKU";
        Position costacoffee_petts = new Position(51.39001680000001, 0.07515669999999999);
        List<Position> printerPlaceList;
        List<string> printerPlaceIdList;
        public PrintCollectPage()
        {

            InitializeComponent();
            Global.ClearPinList();
            appeared = 1;
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                Indicator.TextSize = 30;
            }
            else if (sizeChecker > 700)
            {
                Indicator.TextSize = 35;
            }
            else
            {
                Indicator.TextSize = 25;
            }
            System.Diagnostics.Debug.WriteLine("map collect page" + appeared);
            printerPlaceList = new List<Position>();
           
            printerPlaceList.Add(coffee);
            printerPlaceList.Add(cafenero_cut);
            printerPlaceList.Add(cafenero_tooting);
            printerPlaceList.Add(costacoffee_petts);

            printerPlaceIdList = new List<string>();
            
            printerPlaceIdList.Add(costacoffee_petts_placeId);
            printerPlaceIdList.Add(coffee_placeId);
            printerPlaceIdList.Add(cafenero_cut_placeId);
            printerPlaceIdList.Add(cafenero_tooting_placeId);

        }
        private async void LikeTapped(object sender, EventArgs e)
        {
            await FindUserLocation();

            mapPosition = new Position(userLocation.Latitude, userLocation.Longitude);

            System.Diagnostics.Debug.WriteLine("lat " + mapPosition.Latitude + " " + "long " + mapPosition.Longitude);

            map.MoveToRegion(
              MapSpan.FromCenterAndRadius(
                  mapPosition, Distance.FromMiles(1)));
        }
        async Task FindUserLocation()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Best);
                userLocation = await Geolocation.GetLastKnownLocationAsync();
                System.Diagnostics.Debug.WriteLine(userLocation?.ToString() ?? "no location");
                userLocation = await Geolocation.GetLocationAsync(request);
                System.Diagnostics.Debug.WriteLine(userLocation?.ToString() ?? "no location");
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                System.Diagnostics.Debug.WriteLine("feature not supported " + fnsEx);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                System.Diagnostics.Debug.WriteLine("permission " + pEx);
            }
            catch (Exception ex)
            {
                // Unable to get location
                System.Diagnostics.Debug.WriteLine("exception " + ex);
            }
        }
        public async void PinSelected(string test)
        {
            System.Diagnostics.Debug.WriteLine("testinggggs " + test);
            var printlist = Global.GetListPins();
            foreach (var item in printlist)
            {
                if (item.Url == test)
                {
                    await Navigation.PushAsync(new MapSelectedPage(item));
                }
            }

        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<string>(this, "PinSelected");
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "PinSelected", PinSelected, null);
            if (appeared == 1)
            {
                try
                {
                    Geocoder geoCoder = new Geocoder();
                    // Global.ClearPinList();
                    await FindUserLocation();

                    mapPosition = new Position(userLocation.Latitude, userLocation.Longitude);

                    //System.Diagnostics.Debug.WriteLine("lat " + mapPosition.Latitude + " " + "long " + mapPosition.Longitude);

                    map.MoveToRegion(
                      MapSpan.FromCenterAndRadius(
                          mapPosition, Distance.FromMiles(1)));

                    map.CustomPins = new List<CustomPin>();
                    var rootObject = new RootStartingObject();
                    using (HttpClient client = new HttpClient())
                    {
                        foreach (var item in printerPlaceIdList)
                        {
                            var response = await client.GetStringAsync("https://maps.googleapis.com/maps/api/place/details/json?place_id=" + item +
                   "&fields=name,rating,formatted_address,opening_hours,geometry,icon,url,formatted_phone_number&key=" + UmbrellaApi.GOOGLEPLACE_API_ANDROID);

                            System.Diagnostics.Debug.WriteLine("response " + response);
                            rootObject = JsonConvert.DeserializeObject<RootStartingObject>(response);
                            CustomPin pin = new CustomPin()
                            {
                                Type = PinType.Place,
                                Position = new Position(rootObject.result.geometry.location.lat, rootObject.result.geometry.location.lng),
                                Address = rootObject.result.formatted_address,
                                Label = rootObject.result.name,
                                Name = rootObject.result.name,
                                Url = rootObject.result.url,
                                Icon = rootObject.result.icon,
                            };
                            System.Diagnostics.Debug.WriteLine("pin data out of " + rootObject.result.name + " " + rootObject.result.formatted_address);
                            if (rootObject.result.opening_hours != null)
                            {
                                pin.Periods = rootObject.result.opening_hours.periods;
                            }

                            map.CustomPins.Add(pin);
                            Global.AddPinList(pin);
                            System.Diagnostics.Debug.WriteLine("map " + map.CustomPins.Count);
                            foreach (var mapcustom in map.CustomPins)
                            {
                                System.Diagnostics.Debug.WriteLine("map custom " + mapcustom.Name + " " + mapcustom.Address);
                            }
                            map.Pins.Add(pin);

                        }

                    }
                    appeared = 2;
                }
                catch (FeatureNotSupportedException fnsEx)
                {
                    System.Diagnostics.Debug.WriteLine("error");
                    // Feature not supported on device
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("errors" + ex.InnerException + " " + ex.Message);
                    // Handle exception that may have occurred in geocoding
                }
            }
        }
        async void Address_Clicked(object sender, System.EventArgs e)
        {
            var encoded = Uri.EscapeUriString(entryAddress.Text);
            //Global.ClearPinList();
            var data = new RootObject();
            System.Diagnostics.Debug.WriteLine("encoded " + encoded);
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var response = await client.GetStringAsync("https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + encoded +
                "&inputtype=textquery&fields=formatted_address,place_id,name,geometry&key=" + UmbrellaApi.GOOGLEPLACE_API_ANDROID);

                    System.Diagnostics.Debug.WriteLine("response " + response);
                    data = JsonConvert.DeserializeObject<RootObject>(response);
                    if (data.status == "ZERO_RESULTS")
                    {
                        await DisplayAlert("", "No Results, Pls enter more details", "OK");
                    }
                    else
                    {
                        if (data.candidates != null)
                        {
                            System.Diagnostics.Debug.WriteLine(data.candidates.FirstOrDefault().geometry.location.lat);
                            mapPosition = new Position(data.candidates.FirstOrDefault().geometry.location.lat, data.candidates.FirstOrDefault().geometry.location.lng);
                            map.MoveToRegion(
                             MapSpan.FromCenterAndRadius(
                             mapPosition, Distance.FromMiles(1)));
                        }
                    }


                }
            }
            catch (Exception ex)
            {

            }


        }
        async void entryAddress_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            var txt = (Entry)sender;
            var encoded = Uri.EscapeUriString(txt.Text);
            var autocompleteList = new RootObjectAutoComplete();
            //Global.ClearPinList();

            System.Diagnostics.Debug.WriteLine("encoded " + encoded);
            try
            {
                if (!string.IsNullOrEmpty(encoded))
                {
                    using (HttpClient client = new HttpClient())
                    {
                        var response = await client.GetStringAsync("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + encoded +
                    "&key=" + UmbrellaApi.GOOGLEPLACE_API_ANDROID);

                        System.Diagnostics.Debug.WriteLine("response " + response);
                        autocompleteList = JsonConvert.DeserializeObject<RootObjectAutoComplete>(response);
                        if (autocompleteList.status == "ZERO_RESULTS")
                        {
                            await DisplayAlert("", "No Results found. Please check your spelling or use different location wording.", "OK");
                            entryAddress.Text = "";
                            encoded = "";
                        }
                        else
                        {
                            if (autocompleteList.predictions != null)
                            {
                                listViewSearch.HeightRequest = 45 * autocompleteList.predictions.Count;
                                listViewSearch.ItemsSource = autocompleteList.predictions;

                                listViewSearch.IsVisible = true;
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        async void listViewSearch_ItemTapped(System.Object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var data = (Prediction)e.Item;
            var dataResult = new RootObjectPlaceDetails();
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    StartBusyIndicator();
                    var response = await client.GetStringAsync("https://maps.googleapis.com/maps/api/place/details/json?place_id=" + data.place_id +
                "&fields=name,url,icon,formatted_address,geometry&key=" + UmbrellaApi.GOOGLEPLACE_API_ANDROID);

                    System.Diagnostics.Debug.WriteLine("response " + response);
                    dataResult = JsonConvert.DeserializeObject<RootObjectPlaceDetails>(response);
                    if (dataResult.status == "ZERO_RESULTS")
                    {
                        StopBusyIndicator();
                        await DisplayAlert("", "No Results found. Please check your spelling or use different location wording.", "OK");

                    }
                    else
                    {
                        if (dataResult.result != null)
                        {
                            System.Diagnostics.Debug.WriteLine("lat lang " + dataResult.result.geometry.location.lat + " " + dataResult.result.geometry.location.lat);
                            mapPosition = new Position(dataResult.result.geometry.location.lat, dataResult.result.geometry.location.lng);
                            entryAddress.Text = "";
                            listViewSearch.IsVisible = false;
                            StopBusyIndicator();
                            CustomPin pin = new CustomPin()
                            {
                                Type = PinType.Place,
                                Position = new Position(dataResult.result.geometry.location.lat, dataResult.result.geometry.location.lng),
                                Name = dataResult.result.name,
                                Address = dataResult.result.formatted_address,
                                Label = dataResult.result.name,
                                Url = "pin_locator",
                                Icon = dataResult.result.icon,
                            };
                            var currentPins = Global.GetListPins();
                            var ifDuplicate = true;
                            foreach (var item in currentPins)
                            {
                                System.Diagnostics.Debug.WriteLine("compare position " + item.Position.Latitude + " " + pin.Position.Latitude);
                                System.Diagnostics.Debug.WriteLine("compare name " + item.Name + " " + pin.Name);
                                System.Diagnostics.Debug.WriteLine("compare address " + item.Address + " " + pin.Address);
                                if (item.Position != pin.Position || item.Address != pin.Address || item.Name != pin.Name)
                                {
                                    ifDuplicate = false;
                                }
                                else
                                {
                                    ifDuplicate = true;
                                }
                            }
                            System.Diagnostics.Debug.WriteLine("is duplicate " + ifDuplicate);
                            if (!ifDuplicate)
                            {
                                map.CustomPins.Add(pin);
                                Global.AddPinList(pin);
                                map.Pins.Add(pin);
                            }
                            map.MoveToRegion(
                               MapSpan.FromCenterAndRadius(
                               mapPosition, Distance.FromMiles(1)));
                            DependencyService.Get<ILaunchCalendar>().HideKeyboard();
                        }
                    }


                }
            }
            catch (Exception ex)
            {

            }
            System.Diagnostics.Debug.WriteLine("sender" + data.place_id);
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        async void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            var encoded = "";
            if (!string.IsNullOrEmpty(entryAddress.Text))
            {
                encoded = Uri.EscapeUriString(entryAddress.Text);
            }
            else
            {
                await DisplayAlert("", "Please fill an address", "OK");
            }

            //Global.ClearPinList();
            var data = new RootObject();
            //System.Diagnostics.Debug.WriteLine("encoded " + encoded);
            try
            {
                using (HttpClient client = new HttpClient())
                {

                    if (!string.IsNullOrEmpty(encoded))
                    {
                        var response = await client.GetStringAsync("https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + encoded +
               "&inputtype=textquery&fields=formatted_address,place_id,name,geometry&key=" + UmbrellaApi.GOOGLEPLACE_API_ANDROID);

                        System.Diagnostics.Debug.WriteLine("response " + response);
                        data = JsonConvert.DeserializeObject<RootObject>(response);
                        if (data.status == "ZERO_RESULTS")
                        {
                            await DisplayAlert("", "No Results found. Please check your spelling or use different location wording.", "OK");
                        }
                        else
                        {
                            if (data.candidates != null)
                            {
                                System.Diagnostics.Debug.WriteLine(data.candidates.FirstOrDefault().geometry.location.lat);
                                mapPosition = new Position(data.candidates.FirstOrDefault().geometry.location.lat, data.candidates.FirstOrDefault().geometry.location.lng);
                                map.MoveToRegion(
                                 MapSpan.FromCenterAndRadius(
                                 mapPosition, Distance.FromMiles(1)));
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }

        }

        void map_MapClicked(System.Object sender, Xamarin.Forms.Maps.MapClickedEventArgs e)
        {
            listViewSearch.IsVisible = false;
        }

    }
}