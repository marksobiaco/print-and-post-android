﻿using Com.OneSignal;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : CarouselPage
    {
        public HomePage()
        {
            try
            {
                InitializeComponent();
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    headerText.Text = AppResources.HomePageHeaderText;
                    descLabel.Text = AppResources.HomePageDescText;
                    trainingVidBtn.Text = AppResources.HomePageFreeTrainingBtnText;
                    postBtn.Text = AppResources.HomePagePostcardBtnText;
                    trainingVidBtn.WidthRequest = 180;
                    importBtn.WidthRequest = 180;
                    postBtn.WidthRequest = 180;
                }
                var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
                if (sizeChecker >= 640 && sizeChecker <= 695)
                {
                    descLabel.FontSize = 17;
                    headerText.FontSize = 21;
                }
                else if (sizeChecker > 700)
                {
                    descLabel.FontSize = 17;
                    headerText.FontSize = 21;
                }
                else
                {
                    descLabel.FontSize = 17;
                    headerText.FontSize = 21;
                }
                NavigationPage.SetBackButtonTitle(this, "");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            var _defaultTopupDetails = DependencyService.Get<ICredentialRetriever>().GetCurrencyDefault();
            var currency = "";
            if (_defaultTopupDetails != null)
            {
                currency = string.IsNullOrEmpty(_defaultTopupDetails.DefaultCurrency) ? "gbp" : _defaultTopupDetails.DefaultCurrency;
            }
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    await App.Current.MainPage.DisplayAlert("Important Notice!", "No internet connection detected.", "OK");
                }
                else
                {
                    var defNotification = DependencyService.Get<ICredentialRetriever>().GetDefaultNotification();
                    var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                    if (defNotification.GeneralNotif == "Yes")
                    {
                        OneSignal.Current.SendTag("general", credential.UserID);
                    }
                    else
                    {
                        OneSignal.Current.DeleteTag("general");
                    }
                    if (defNotification.NewLeadNotif == "Yes")
                    {
                        OneSignal.Current.SendTag("signed_alerts", credential.UserID);
                    }
                    else
                    {
                        OneSignal.Current.DeleteTag("signed_alerts");
                    }
                    if (defNotification.NewMessageNotif == "Yes")
                    {
                        OneSignal.Current.SendTag("special_delivery", credential.UserID);
                    }
                    else
                    {
                        OneSignal.Current.DeleteTag("special_delivery");
                    }
                    if (defNotification.LeadConvertedNotif == "Yes")
                    {
                        OneSignal.Current.SendTag("refer_earn_updates", credential.UserID);
                    }
                    else
                    {
                        OneSignal.Current.DeleteTag("refer_earn_updates");
                    }
                }
            };
            this.CurrentPage = homepageBase;
            CheckCredential();
            SetupNotification();
            pdfTutorialFooter.ActiveRewardLevel(true);
        }
        public HomePage(bool isFromLoginPage)
        {
            try
            {
                InitializeComponent();
                var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (accnt.User.Username == "juju@raptorsms.com")
                {
                    headerText.Text = AppResources.HomePageHeaderText;
                    descLabel.Text = AppResources.HomePageDescText;
                    trainingVidBtn.Text = AppResources.HomePageFreeTrainingBtnText;
                    postBtn.Text = AppResources.HomePagePostcardBtnText;
                    trainingVidBtn.WidthRequest = 180;
                    importBtn.WidthRequest = 180;
                    postBtn.WidthRequest = 180;
                }
                var _defaultTopupDetails = DependencyService.Get<ICredentialRetriever>().GetCurrencyDefault();
                var currency = "";
                if (_defaultTopupDetails != null)
                {
                    currency = string.IsNullOrEmpty(_defaultTopupDetails.DefaultCurrency) ? "gbp" : _defaultTopupDetails.DefaultCurrency;
                }
                pdfTutorialHeader.ChangeHeader(currency);
                var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();
                if (sizeChecker >= 640 && sizeChecker <= 695)
                {
                    descLabel.FontSize = 17;
                    headerText.FontSize = 21;
                }
                else if (sizeChecker > 700)
                {
                    descLabel.FontSize = 17;
                    headerText.FontSize = 21;
                }
                else
                {
                    descLabel.FontSize = 17;
                    headerText.FontSize = 21;
                }
                NavigationPage.SetBackButtonTitle(this, "");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    await App.Current.MainPage.DisplayAlert("Important Notice!", "No internet connection detected.", "OK");
                }
                else
                {
                    var defNotification = DependencyService.Get<ICredentialRetriever>().GetDefaultNotification();
                    var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                    if (defNotification.GeneralNotif == "Yes")
                    {
                        OneSignal.Current.SendTag("general", credential.UserID);
                    }
                    else
                    {
                        OneSignal.Current.DeleteTag("general");
                    }
                    if (defNotification.NewLeadNotif == "Yes")
                    {
                        OneSignal.Current.SendTag("signed_alerts", credential.UserID);
                    }
                    else
                    {
                        OneSignal.Current.DeleteTag("signed_alerts");
                    }
                    if (defNotification.NewMessageNotif == "Yes")
                    {
                        OneSignal.Current.SendTag("special_delivery", credential.UserID);
                    }
                    else
                    {
                        OneSignal.Current.DeleteTag("special_delivery");
                    }
                    if (defNotification.LeadConvertedNotif == "Yes")
                    {
                        OneSignal.Current.SendTag("refer_earn_updates", credential.UserID);
                    }
                    else
                    {
                        OneSignal.Current.DeleteTag("refer_earn_updates");
                    }
                }
            };
            System.Diagnostics.Debug.WriteLine("Homepage" + Global.GetCountPush());
            this.CurrentPage = homepageBase;
            SetupNotification();
            pdfTutorialFooter.ActiveRewardLevel(true);
        }
        private void CheckCredential()
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("check");
              
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (credential != null)
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        if (!Global.GetIsNotif())
                        {
                            System.Diagnostics.Debug.WriteLine("LoadAsync");
                            LoadLeadsAsync();
                        }

                    }
                  
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error" + e.Message);
            }
        }
        private void SetupNotification()
        {
            OneSignal.Current.DeleteTags(new List<string>()
            {
                "redios",
                "printpostss"

            });
            var defNotification = DependencyService.Get<ICredentialRetriever>().GetDefaultNotification();
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            defNotification.GeneralNotif = string.IsNullOrEmpty(defNotification.GeneralNotif) ? "Yes" : defNotification.GeneralNotif;
            defNotification.NewLeadNotif = string.IsNullOrEmpty(defNotification.NewLeadNotif) ? "Yes" : defNotification.NewLeadNotif;
            defNotification.NewMessageNotif = string.IsNullOrEmpty(defNotification.NewMessageNotif) ? "Yes" : defNotification.NewMessageNotif;
            defNotification.LeadConvertedNotif = string.IsNullOrEmpty(defNotification.LeadConvertedNotif) ? "Yes" : defNotification.LeadConvertedNotif;
            System.Diagnostics.Debug.WriteLine("defffss g" + defNotification.GeneralNotif);
            if (defNotification.GeneralNotif == "Yes")
            {
                OneSignal.Current.SendTag("general", credential.UserID);
            }
            else
            {
                OneSignal.Current.DeleteTag("general");
            }
            if (defNotification.NewLeadNotif == "Yes")
            {
                OneSignal.Current.SendTag("signed_alerts", credential.UserID);
            }
            else
            {
                OneSignal.Current.DeleteTag("signed_alerts");
            }
            if (defNotification.NewMessageNotif == "Yes")
            {
                OneSignal.Current.SendTag("special_delivery", credential.UserID);
            }
            else
            {
                OneSignal.Current.DeleteTag("special_delivery");
            }
            if (defNotification.LeadConvertedNotif == "Yes")
            {
                OneSignal.Current.SendTag("refer_earn_updates", credential.UserID);
            }
            else
            {
                OneSignal.Current.DeleteTag("refer_earn_updates");
            }

            MessagingCenter.Send<DefaultNotification>(defNotification, "SaveDefaultNotification");
            System.Diagnostics.Debug.WriteLine("defffss " + defNotification.GeneralNotif);
        }
        private void LoadLeadsAsync()
        {
            //App.LeadsViewModel.SetData();
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            OneSignal.Current.SetSubscription(true);
            System.Diagnostics.Debug.WriteLine("One Signal Initialized");
        }
        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            int index = Children.IndexOf(CurrentPage);
            if (index == 1)
            {
                Global.SetBoolIsHomePage(true);
            }
            else
            {
                Global.SetBoolIsHomePage(false);
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            var _defaultTopupDetails = DependencyService.Get<ICredentialRetriever>().GetCurrencyDefault();
            var currency = "";
            if (_defaultTopupDetails != null)
            {
                currency = string.IsNullOrEmpty(_defaultTopupDetails.DefaultCurrency) ? "gbp" : _defaultTopupDetails.DefaultCurrency;
            }
            pdfTutorialHeader.ChangeHeader(currency);
            //Global.SetBoolIsHomePage(true);

            if (Global.GetBoolIsHomePage())
            {
                this.CurrentPage = homepageBase;
            }

        }
        private void TrainingClicked(object sender, EventArgs args)
        {
            Device.OpenUri(new Uri("https://www.ManagedMailService.co.uk/print-and-post-app-android-training/"));
        }
        private void PostcardClicked(object sender, EventArgs args)
        {
            var permission = DependencyService.Get<ILaunchCalendar>().CheckIfPermissionAllowed();
            if (permission)
            {
                Global.SetUploadPageType("PDFTutorialPage");
                DependencyService.Get<ILaunchCalendar>().LaunchSingleImagePicker();
            }
            else
            {
                Global.SetUploadPageType("PDFTutorialPage");
                DependencyService.Get<ILaunchCalendar>().AskPermission();
            }
        }
        private async void ImportClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new ImportPage());
        }
        private async void LateDispatchClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new LateDispatchPage(new Models.PhoneContact(),"homepage",false));
        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}
