﻿using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Constants;
using Umbrella.Helpers;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Umbrella.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostCardDonePage : ContentPage
    {
        private APIHelper2 _apiHelper;
        private PhoneContact _phoneContact;
        private bool isFail = false;
        private List<FileUpload> _fileLists;
        private bool _isFromDetailsPage = false;
        public PostCardDonePage(PhoneContact phoneContact, bool isFromDetailsPage)
        {
            InitializeComponent();
            _phoneContact = new PhoneContact();
            _isFromDetailsPage = isFromDetailsPage;
            _phoneContact = phoneContact;
            _apiHelper = new APIHelper2();
            _fileLists = new List<FileUpload>();
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (credential.User.Username == "juju@raptorsms.com")
            {
                pdfText.Text = AppResources.PostcardDoneDesc;
                uploadTxt.Text = AppResources.PostcardDoneTitle;
                Indicator.Title = AppResources.PostcardDoneLoading;
            }
            else
            {
                pdfText.Text = "Give us a moment...";
            }
            trainingVidBtn.IsEnableButton = CrossConnectivity.Current.IsConnected;

            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                trainingVidBtn.IsEnableButton = args.IsConnected ? true : false;
            };
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                imgCheck.HeightRequest = 200;
                imgCheck.WidthRequest = 200;
                Indicator.TextSize = 25;
            }
            else if (sizeChecker > 700)
            {
                imgCheck.HeightRequest = 300;
                imgCheck.WidthRequest = 300;
                Indicator.TextSize = 35;
            }
            else
            {
                imgCheck.HeightRequest = 200;
                imgCheck.WidthRequest = 200;
                Indicator.TextSize = 25;
            }
        }
        private void StartBusyIndicator()
        {
            BusyIndicator.IsVisible = true;
            Indicator.IsBusy = true;
        }
        private void StopBusyIndicator()
        {
            BusyIndicator.IsVisible = false;
            Indicator.IsBusy = false;
        }
        private async void DoneTapped(object sender, EventArgs e)
        {
            if (isFail)
            {
                StartBusyIndicator();
                pdfText.Opacity = 1;
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (credential.User.Username == "juju@raptorsms.com")
                    {
                        pdfText.Text = AppResources.PostcardDoneDesc;
                        uploadTxt.Text = AppResources.PostcardDoneTitle;
                        Indicator.Title = AppResources.PostcardDoneLoading;
                    }
                    else
                    {
                        uploadTxt.Text = "Uploading...";
                        pdfText.Text = "Give us a moment...";
                    }
                    imgCheck.Opacity = 0;
                    var result = new FileUpload();
                    foreach (var item in Global.GetImagePaths())
                    {
                        var jpgB = DependencyService.Get<IFileHelper>().HighQualityUpload(item);
                        if (jpgB != null && jpgB.Length > 0)
                        {
                            result = await Task.Run(() => _apiHelper.FileImageUpload(jpgB));
                        }
                    }
                    System.Diagnostics.Debug.WriteLine("msg " + result.message);
                    if (result.message == "File is valid, and was successfully uploaded.")
                    {
                        _phoneContact.File_ID = result.file_id;
                        _fileLists.Add(result);
                        var pdfresult = await Task.Run(() => _apiHelper.PostCardUpload(_phoneContact));
                        var list = await App.RecentlyUsedContactDatabase.GetItemsAsync();
                        if (list.Any(ex => ex.Name == _phoneContact.Name))
                        {
                            await App.RecentlyUsedContactDatabase.UpdateItemAsync(_phoneContact);
                        }
                        else
                        {
                            await App.RecentlyUsedContactDatabase.SaveItemAsync(_phoneContact);

                        }
                        var allUploadLinks = "";
                        foreach (var item in _fileLists)
                        {
                            allUploadLinks = allUploadLinks + " " + item.url;
                        }
                        System.Diagnostics.Debug.WriteLine("all links " + allUploadLinks);
                        using (HttpClient client = new HttpClient())
                        {
                            var _op = new UploadsOntraport()
                            {
                                PartnerID = credential.UserID,
                                UploadLinks = allUploadLinks,
                            };
                            var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                            client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                            client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                            var response = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                            if (response.IsSuccessStatusCode)
                            {
                                StopBusyIndicator();

                                if (credential.User.Username == "juju@raptorsms.com")
                                {
                                    pdfText.Text = AppResources.PostcardDoneNewDesc;
                                    uploadTxt.Text = AppResources.PostcardDoneNewTitle;
                                    trainingVidBtn.Text = AppResources.PostcardDoneBtn;
                                }
                                else
                                {
                                    uploadTxt.Text = "Upload Success!";
                                    pdfText.Text = "Wow cool picture and message! We'll now convert it into a postcard and Print & Post it as per your instructions.";
                                    trainingVidBtn.Text = "HOME";
                                }
                                imgCheck.Source = "finalcheck";
                                imgCheck.Opacity = 1;
                                trainingVidBtn.IsVisible = true;
                                Global.ClearImagePaths();
                                Global.SetIsDonePick(false);
                            }
                            else
                            {
                                isFail = true;
                                pdfText.Opacity = 0;
                                uploadTxt.Text = "Upload Failed";
                                imgCheck.Source = "failed";
                                imgCheck.Opacity = 1;
                                trainingVidBtn.IsVisible = true;
                                trainingVidBtn.Text = "Try Again";
                                StopBusyIndicator();
                            }
                        }

                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("pdffgasdasdaff");
                        await DisplayAlert("", "Failed to Upload, Pls try again.", "OK");
                        StopBusyIndicator();
                        isFail = true;
                        pdfText.Opacity = 0;
                        uploadTxt.Text = "Upload Failed";
                        imgCheck.Source = "failed";
                        trainingVidBtn.IsVisible = true;
                        imgCheck.Opacity = 1;
                        trainingVidBtn.Text = "Try Again";
                    }
                }
            }
            else
            {
                await Navigation.PushModalAsync(new NavigationPage(new HomePage(true)));
            }
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _isFromDetailsPage = false;
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
         

            if (_isFromDetailsPage)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    StartBusyIndicator();
                    var result = new FileUpload();
                    foreach (var item in Global.GetImagePaths())
                    {
                        var jpgB = DependencyService.Get<IFileHelper>().HighQualityUpload(item);
                        if (jpgB != null && jpgB.Length > 0)
                        {
                            result = await Task.Run(() => _apiHelper.FileImageUpload(jpgB));
                        }
                    }
                    //System.Diagnostics.Debug.WriteLine("msg " + result.message);
                    if (Global.GetIsAbort())
                    {
                        isFail = true;
                        pdfText.Opacity = 0;
                        uploadTxt.Text = "Upload Failed";
                        imgCheck.Source = "failed";
                        imgCheck.Opacity = 1;
                        trainingVidBtn.IsVisible = true;
                        trainingVidBtn.Text = "Try Again";
                        StopBusyIndicator();
                    }
                    else
                    {
                        if (result.message == "File is valid, and was successfully uploaded.")
                        {
                            System.Diagnostics.Debug.WriteLine("insss mss");
                            _phoneContact.File_ID = result.file_id;
                            _fileLists.Add(result);
                            var pdfresult = await Task.Run(() => _apiHelper.PostCardUpload(_phoneContact));

                            if (Global.GetIsAbort())
                            {
                                isFail = true;
                                pdfText.Opacity = 0;
                                uploadTxt.Text = "Upload Failed";
                                imgCheck.Source = "failed";
                                imgCheck.Opacity = 1;
                                trainingVidBtn.IsVisible = true;
                                trainingVidBtn.Text = "Try Again";
                                StopBusyIndicator();
                            }
                            else
                            {
                                var list = await App.RecentlyUsedContactDatabase.GetItemsAsync();
                                var prevList = await App.PreviousContactsDatabase.GetItemsAsync();
                                var prevCon = new PreviousContacts();

                                if (list.Any(e => e.Name == _phoneContact.Name))
                                {
                                    await App.RecentlyUsedContactDatabase.UpdateItemAsync(_phoneContact);
                                }
                                else
                                {
                                    await App.RecentlyUsedContactDatabase.SaveItemAsync(_phoneContact);

                                }
                                if (prevList.Any(e => e.Name == _phoneContact.Name))
                                {
                                    await App.PreviousContactsDatabase.UpdateItemAsync(_phoneContact);
                                }
                                else
                                {
                                    await App.PreviousContactsDatabase.SaveItemAsync(_phoneContact);

                                }
                                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                                var allUploadLinks = "";
                                foreach (var item in _fileLists)
                                {
                                    allUploadLinks = allUploadLinks + " " + item.url;
                                }
                                System.Diagnostics.Debug.WriteLine("all links " + allUploadLinks);
                                using (HttpClient client = new HttpClient())
                                {
                                    var _op = new UploadsOntraport()
                                    {
                                        PartnerID = credential.UserID,
                                        UploadLinks = allUploadLinks,
                                    };
                                    var content = JsonConvert.SerializeObject(_op, new JsonSerializerSettings());
                                    client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                                    client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                                    var response = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                                    if (response.IsSuccessStatusCode)
                                    {
                                        StopBusyIndicator();

                                        if (credential.User.Username == "juju@raptorsms.com")
                                        {
                                            pdfText.Text = AppResources.PostcardDoneNewDesc;
                                            uploadTxt.Text = AppResources.PostcardDoneNewTitle;
                                            trainingVidBtn.Text = AppResources.PostcardDoneBtn;
                                        }
                                        else
                                        {
                                            uploadTxt.Text = "Upload Success!";
                                            pdfText.Text = "Wow cool picture and message! We'll now convert it into a postcard and Print & Post it as per your instructions.";
                                            trainingVidBtn.Text = "HOME";
                                        }
                                        imgCheck.Source = "finalcheck";
                                        imgCheck.Opacity = 1;
                                        trainingVidBtn.IsVisible = true;
                                        Global.ClearImagePaths();
                                        Global.SetIsDonePick(false);
                                    }
                                    else
                                    {
                                        isFail = true;
                                        pdfText.Opacity = 0;
                                        uploadTxt.Text = "Upload Failed";
                                        imgCheck.Source = "failed";
                                        imgCheck.Opacity = 1;
                                        trainingVidBtn.IsVisible = true;
                                        trainingVidBtn.Text = "Try Again";
                                        StopBusyIndicator();
                                    }
                                }

                            }
                        }
                        else
                        {
                            await DisplayAlert("", "Failed to Upload, Pls try again.", "OK");
                            StopBusyIndicator();
                            isFail = true;
                            pdfText.Opacity = 0;
                            uploadTxt.Text = "Upload Failed";
                            imgCheck.Source = "failed";
                            imgCheck.Opacity = 1;
                            trainingVidBtn.IsVisible = true;
                            trainingVidBtn.Text = "Try Again";
                        }
                    }
                }
            }

        }
    }
}