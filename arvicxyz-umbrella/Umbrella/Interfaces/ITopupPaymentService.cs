﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Interfaces
{
    public interface ITopupPaymentService
    {
       Task<Dictionary<bool,string>> ProcessCustomerCard(Models.Card card, int amount, string email, string currency);
       Task<Dictionary<bool, string>> ProcessExistingCard(int amount, string email, RetrievedCard card, string currency);
       Task<List<RetrievedCard>> GetAllCurrentCards(string email);
       Task<Dictionary<bool, string>> ProcessIntentPayments(int amount, string email, RetrievedCard card, string currency);
    }
}
