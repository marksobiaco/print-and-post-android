﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbrella.Interfaces
{
    public interface IScreenSizeRetriever
    {
        int GetHeight();
    }
}
