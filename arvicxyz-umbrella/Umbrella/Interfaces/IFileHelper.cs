﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbrella.Interfaces
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
        void ForPrintOnly(string message);
        List<string> GetSpecialFolders();
        byte[] ResizeImage(byte[] imageData);
        byte[] HighQualityUpload(byte[] imageData);
        byte[] LowQualityUpload(byte[] imageData);
        byte[] HighQualityNormalUpload(byte[] imageData);
        byte[] LowQualityNormalUpload(byte[] imageData);
        bool CheckTime();
    }
}
