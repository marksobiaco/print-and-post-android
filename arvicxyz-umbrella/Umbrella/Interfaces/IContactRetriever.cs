﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Interfaces
{
    public interface IContactRetriever
    {
        IEnumerable<PhoneContact> GetAllContacts();
    }
}
