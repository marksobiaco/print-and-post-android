﻿using Umbrella.Models;

namespace Umbrella.Interfaces
{
    public interface ICredentialRetriever
    {
        Credential GetCredential();
        string GetStatus();
        Default GetDefault();
        string GetMessageCount();
        string GetCompanyToken();
        DefaultNotification GetDefaultNotification();
        DefaultTopupDetails GetCurrencyDefault();
    }
}
