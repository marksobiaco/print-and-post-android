﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbrella.Interfaces
{
    public interface ILaunchCalendar
    {
        string GetVersionNumber();
        Task<string> LaunchScanner();
        void OpenGmail();
        void HideKeyboard();
        bool CheckIfPermissionAllowed();
        void AskPermission();
        void LaunchCalendar();
        void OpenDropbox();
        void LaunchGmail();
        void LaunchFileManager();
        void LaunchImagePicker();
        void LaunchSingleImagePicker();
        void LaunchChooser();
    }
}
