﻿using System;
using System.Globalization;
using Umbrella.Interfaces;
using Xamarin.Forms;

namespace Umbrella.Converters
{
    public class BooleanToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isToggled = (bool) value;
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (credential.User.Username == "juju@raptorsms.com")
            {
                if (isToggled)
                {
                    return "Oui";
                }
                else
                {
                    return "No";
                }
            }
            else
            {
                if (isToggled)
                {
                    return "Yes";
                }
                else
                {
                    return "No";
                }
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
