﻿using System;
using System.Globalization;
using Umbrella.Enums;
using Xamarin.Forms;

namespace Umbrella.Converters
{
    public class ReferralStatusToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (string)value;
            switch (status)
            {
                case "payment-dispute":
                    return "wrong_mark";
                case "sold":
                    return "check_mark";
                case "cancelled":
                    return "wrong_mark";
                case "not-compatible":
                    return "wrong_mark";
                case "lead":
                    return "in_progress_mark";
                default:
                    return "in_progress_mark";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
