﻿using System;
using System.Globalization;
using Umbrella.Enums;
using Xamarin.Forms;

namespace Umbrella.Converters
{
    public class ReferralStatusToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = (string)value;
            switch (status)
            {
                case "sold":
                    return "Became Client";
                case "not-compatible":
                    return "Not Suitable";
                case "payment-dispute":
                    return "Payment Dispute";
                case "cancelled":
                    return "Client Cancelled";
                case "lead":
                    return "Follow\nUp Stage";
                default:
                    return "Follow\nUp Stage";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
