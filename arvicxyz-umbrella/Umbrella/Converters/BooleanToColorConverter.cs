﻿using System;
using System.Globalization;
using Xamarin.Forms;
namespace Umbrella.Converters
{
    public class BooleanToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stat = (string)value;
            if (stat == "Delivered" || stat == "Left with Neighbours")
            {
                return Color.Green;
            }
            else
            {
                return Color.Red;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
