﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using Umbrella.Models;
using Umbrella.Constants;
using Umbrella.Enums;
using Xamarin.Forms;
using Umbrella.Interfaces;
using Umbrella.Models.Leads;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Umbrella.Utilities;

namespace Umbrella.Helpers
{ 
    public static class APIHelper
    { 
        public async static Task<LoyaltyData> GetLoyaltyAsync()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
           
            var ls = new LoyaltyData();
            // var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/loyaltycard/");
            var client = new RestSharp.RestClient("http://mastereman.com/firebase/qr.php");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);
            System.Diagnostics.Debug.WriteLine("GetLoyaltyAsync : " + credential.UserID.ToString());
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<LoyaltyData>(request, response => {
                System.Diagnostics.Debug.WriteLine(response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    ls = JsonConvert.DeserializeObject<LoyaltyData>(json);
                }
                catch (InvalidCastException e)
                {
                    System.Diagnostics.Debug.WriteLine("ERRor : " + e.ToString());
                }

                Wait.Set();
            });
            Wait.WaitOne();

            if (ls.points == "0")
            {
                System.Diagnostics.Debug.WriteLine("NULL if Stat" + ls.points);

                 
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("else Stat" + ls.points);


                //u.Registered = "";
                System.Diagnostics.Debug.WriteLine("u.Id " + ls.barcodedata);



            }

            return ls;
        }
        class AuthenticationResult
        {
            public AuthenticationStatus Status { get; set; }
            public User User { get; set; }
        }
    }
    public class APIHelper2{
        public List<CallBackList> GetCallBackLists(string id)
        {

            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient("https://api.securesupportcentre.com/api/v1/callback/lists/" + id);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", UmbrellaApi.AUTHORIZATION);

            List<CallBackList> convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<List<CallBackList>>(request, response => {
                System.Diagnostics.Debug.WriteLine("CallBackList Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    convert = JsonConvert.DeserializeObject<List<CallBackList>>(diskSpaceArray);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("CallBackList Error : " + e.ToString());
                    convert = new List<CallBackList>();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public CallBackProfile GetCallBackProfile(string id)
        {

            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient("https://api.securesupportcentre.com/api/v1/callback/" + id);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", UmbrellaApi.AUTHORIZATION);

            CallBackProfile convert = null;
            List<CallBackEnquiry> enq = new List<CallBackEnquiry>();
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<CallBackProfile>(request, response => {
                System.Diagnostics.Debug.WriteLine("CallBackProfile Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    var enquiryArray = jo.SelectToken("data", false).SelectToken("enquiries").ToString();
                    convert = JsonConvert.DeserializeObject<CallBackProfile>(diskSpaceArray);
                    enq = JsonConvert.DeserializeObject<List<CallBackEnquiry>>(enquiryArray);
                    var notesConcat = "";
                    foreach (var item in enq)
                    {
                        notesConcat = notesConcat + " " + item.Notes;
                    }
                    convert.Notes = notesConcat;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("CallBackProfile Error : " + e.ToString());
                    convert = new CallBackProfile();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public MonthlyEarnings GetMonthlyEarnings()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var ls = new MonthlyEarnings();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/earnings/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID);
            request.AddParameter("populate_website", "ssc");
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);

            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<MonthlyEarnings>(request, response => {
                System.Diagnostics.Debug.WriteLine("Monthly Earnings Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    ls = JsonConvert.DeserializeObject<MonthlyEarnings>(json);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Monthly Earnings Error : " + e.ToString());
                    ls = new MonthlyEarnings();
                }

                Wait.Set();
            });
            Wait.WaitOne();

            return ls;
        }
        public List<OntraportJsonAddress> GetAddresDetails()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{OntraportApi.ONTRAPORT_API_LINK_CONTACTS}?id=" + credential.UserID);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Api-Appid", OntraportApi.API_APP_ID);
            request.AddHeader("Api-Key", OntraportApi.API_KEY);

            List<OntraportJsonAddress> convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<List<OntraportJsonAddress>>(request, response => {
                System.Diagnostics.Debug.WriteLine("Address Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    convert = JsonConvert.DeserializeObject<List<OntraportJsonAddress>>(diskSpaceArray);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Store Error Error : " + e.ToString());
                    convert = new List<OntraportJsonAddress>();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public async Task<List<OntraportJsonModel>> GetStore()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            List<OntraportJsonModel> convert = null;
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                    client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                    var response = await client.GetStringAsync($"{OntraportApi.ONTRAPORT_API_LINK_CONTACTS}?id=" + credential.UserID);
                    JObject jo = JObject.Parse(response);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    convert = JsonConvert.DeserializeObject<List<OntraportJsonModel>>(diskSpaceArray);
                    // string customerId = string.Empty;
                    System.Diagnostics.Debug.WriteLine("testing cards " + response);

                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Status error " + ex.InnerException + " " + ex.Message);
            }

            return convert;
        }
        public List<OntraportJsonModel> GetStoreStatusAPi()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{OntraportApi.ONTRAPORT_API_LINK_CONTACTS}?id="+credential.UserID);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Api-Appid", OntraportApi.API_APP_ID);
            request.AddHeader("Api-Key", OntraportApi.API_KEY);

            List<OntraportJsonModel> convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<List<OntraportJsonModel>>(request, response => {
                System.Diagnostics.Debug.WriteLine("Store Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    convert = JsonConvert.DeserializeObject<List<OntraportJsonModel>>(diskSpaceArray);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Store Error Error : " + e.ToString());
                    convert = new List<OntraportJsonModel>()
                    {
                        new OntraportJsonModel()
                        {
                            f1670 = "Open"
                        }
                    };
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public FileUpload FileUpload()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/fileupload/");
            var ls = new FileUpload();
            var request = new RestRequest(Method.POST);
            //var formData = new StreamContent(new FileStream);
            //request.AddParameter("file_contents", formData);
            if(Global.GetByte() == null)
            {
                System.Diagnostics.Debug.WriteLine("null byte");
            }
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddParameter("service_type", "print-and-post");
            request.AddFile("file_contents", Global.GetByte(), Global.GetPdfName(), "multipart/form-data");
            //request.AddHeader("Content-Type", "multipart/form-data");
            request.AddParameter("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddParameter("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddParameter("umbrella-partner-id", UmbrellaApi.PARTNER_ID);

            FileUpload convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<FileUpload>(request, response => {
                System.Diagnostics.Debug.WriteLine("File Upload Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    convert = JsonConvert.DeserializeObject<FileUpload>(json);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Upload Error : " + e.ToString());
                    convert = new FileUpload();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public FileUpload FileImageUpload(byte[] filebyte)
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/fileupload/");
            var ls = new FileUpload();
            var request = new RestRequest(Method.POST);
            //var formData = new StreamContent(new FileStream);
            //request.AddParameter("file_contents", formData);
            if (Global.GetByte() == null)
            {
                System.Diagnostics.Debug.WriteLine("null byte");
            }
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddParameter("service_type", "print-and-post");
            request.AddFile("file_contents", filebyte, "test.jpeg", "multipart/form-data");
            //request.AddHeader("Content-Type", "multipart/form-data");
            request.AddParameter("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddParameter("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddParameter("umbrella-partner-id", UmbrellaApi.PARTNER_ID);

            FileUpload convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<FileUpload>(request, response => {
                System.Diagnostics.Debug.WriteLine("File Upload Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    convert = JsonConvert.DeserializeObject<FileUpload>(json);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Upload Error : " + e.ToString());
                    convert = new FileUpload();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public PdfFileUpload PdfUpload(PhoneContact phone, List<FileUpload> fileList)
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/pdf/");
            var ls = phone;

            var request = new RestRequest(Method.POST);
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);
            var fileIdString = "";

            if (fileList.Any())
            {
                foreach (var item in fileList)
                {
                    fileIdString = fileIdString + "" + item.file_id + ",";
                }
                fileIdString = fileIdString.Remove(fileIdString.Length - 1);
                request.AddParameter("accompanying_photo", fileIdString);
                System.Diagnostics.Debug.WriteLine("file strings " + fileIdString);
            }

            //var formData = new StreamContent(new FileStream);
            //request.AddParameter("file_contents", formData);
            request.AddParameter("envelope", "yes");
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddParameter("paper_type", "standard");
            request.AddParameter("haslabel", "yes");
            request.AddParameter("ink", ls.Ink);
            request.AddParameter("source", "ppt-mobile");
            request.AddParameter("post_type", "letter");
            request.AddParameter("cost_type", "credit");
            request.AddParameter("postage_class", ls.PostageClass);
            request.AddParameter("handwritten", ls.Handwritten);
            request.AddParameter("leaflet", ls.AllowLeaflet);
            if (ls.IsFromLateDispatchPage)
            {
                request.AddParameter("late_night_dispatch", "yes");
                request.AddParameter("priority_dispatch", "no");
            }
            else
            {
                request.AddParameter("late_night_dispatch", "no");
                request.AddParameter("priority_dispatch", ls.Priority);
            }
            request.AddParameter("file_id", ls.File_ID);
            request.AddParameter("city", ls.City);
            request.AddParameter("county", ls.Country);
            request.AddParameter("postcode", ls.PostalCode);
            request.AddParameter("town", ls.State);
            request.AddParameter("first_name", ls.FirstName);
            if (!string.IsNullOrEmpty(ls.PostDate))
            {
                request.AddParameter("schedule", ls.PostDate);
            }
            else
            {

            }
            request.AddParameter("last_name", ls.LastName);
            //label param
            if (string.IsNullOrEmpty(ls.CompanyName))
            {
                request.AddParameter("address_label_1", ls.Name);
                request.AddParameter("address_label_2", ls.Address);
                request.AddParameter("address_label_3", ls.Address2);
                request.AddParameter("address_label_4", ls.State);
                request.AddParameter("address_label_5", ls.City);
                request.AddParameter("address_label_6", ls.Country);
                request.AddParameter("address_label_7", ls.PostalCode);
                request.AddParameter("deliver_to", ls.Name + " " + ls.PostalCode);
            }
            else
            {
                request.AddParameter("organisation_name", ls.CompanyName);
                request.AddParameter("address_label_1", ls.CompanyName);
                request.AddParameter("address_label_2", ls.Name);
                request.AddParameter("address_label_3", ls.Address);
                request.AddParameter("address_label_4", ls.Address2);
                request.AddParameter("address_label_5", ls.State + ", " + ls.City);
                request.AddParameter("address_label_6", ls.Country);
                request.AddParameter("address_label_7", ls.PostalCode);
                request.AddParameter("deliver_to", ls.CompanyName + " " + ls.PostalCode);
            }

            //request.AddHeader("Content-Type", "multipart/form-data");


            PdfFileUpload convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<PdfFileUpload>(request, response => {
                System.Diagnostics.Debug.WriteLine("PDF Upload Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    convert = JsonConvert.DeserializeObject<PdfFileUpload>(json);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("PDF Upload Error : " + e.ToString());
                    convert = new PdfFileUpload();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public PdfFileUpload PostCardUpload(PhoneContact phone)
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/pdf/");
            var ls = phone;

            var request = new RestRequest(Method.POST);
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);

            //var formData = new StreamContent(new FileStream);
            //request.AddParameter("file_contents", formData);
            request.AddParameter("envelope", "yes");
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddParameter("paper_type", "standard");
            request.AddParameter("haslabel", "yes");
            request.AddParameter("ink", ls.Ink);
            request.AddParameter("source", "ppt-mobile");
            request.AddParameter("post_type", "postcard");
            request.AddParameter("cost_type", "credit");
            request.AddParameter("postage_class", ls.PostageClass);
            request.AddParameter("handwritten", ls.Handwritten);
            request.AddParameter("leaflet", "no");
            if (ls.IsFromLateDispatchPage)
            {
                request.AddParameter("late_night_dispatch", "yes");
                System.Diagnostics.Debug.WriteLine("latenight dispatch");
                request.AddParameter("priority_dispatch", "no");
            }
            else
            {
                request.AddParameter("late_night_dispatch", "no");
                System.Diagnostics.Debug.WriteLine("latenight dispatch");
                request.AddParameter("priority_dispatch", ls.Priority);
            }
            request.AddParameter("file_id", ls.File_ID);
            request.AddParameter("city", ls.City);
            request.AddParameter("county", ls.Country);
            request.AddParameter("postcode", ls.PostalCode);
            request.AddParameter("town", ls.State);
            request.AddParameter("first_name", ls.FirstName);
            request.AddParameter("last_name", ls.LastName);
            if (!string.IsNullOrEmpty(ls.PostDate))
            {
                request.AddParameter("schedule", ls.PostDate);
            }
            else
            {

            }
            request.AddParameter("postcard_message", ls.PostCardMessage);
            //label param
            if (string.IsNullOrEmpty(ls.CompanyName))
            {
                request.AddParameter("address_label_1", ls.Name);
                request.AddParameter("address_label_2", ls.Address);
                request.AddParameter("address_label_3", ls.Address2);
                request.AddParameter("address_label_4", ls.State);
                request.AddParameter("address_label_5", ls.City);
                request.AddParameter("address_label_6", ls.Country);
                request.AddParameter("address_label_7", ls.PostalCode);
                request.AddParameter("deliver_to", ls.Name + " " + ls.PostalCode);
            }
            else
            {
                request.AddParameter("organisation_name", ls.CompanyName);
                request.AddParameter("address_label_1", ls.CompanyName);
                request.AddParameter("address_label_2", ls.Name);
                request.AddParameter("address_label_3", ls.Address);
                request.AddParameter("address_label_4", ls.Address2);
                request.AddParameter("address_label_5", ls.State + ", " + ls.City);
                request.AddParameter("address_label_6", ls.Country);
                request.AddParameter("address_label_7", ls.PostalCode);
                request.AddParameter("deliver_to", ls.CompanyName + " " + ls.PostalCode);
            }

            //request.AddHeader("Content-Type", "multipart/form-data");


            PdfFileUpload convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<PdfFileUpload>(request, response => {
                System.Diagnostics.Debug.WriteLine("PDF Upload Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    convert = JsonConvert.DeserializeObject<PdfFileUpload>(json);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("PDF Upload Error : " + e.ToString());
                    convert = new PdfFileUpload();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public List<CalendarEvent> GetCalendarApiWeek()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/calendar/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID);
            request.AddParameter("action", "get");
            request.AddParameter("flag_status", "1");
            request.AddParameter("only_from_current_timestamp", "1");
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);

            List<CalendarEvent> convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<List<CalendarEvent>>(request, response => {
                System.Diagnostics.Debug.WriteLine("Calendar Data Week " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    if (diskSpaceArray == "[]")
                    {
                        convert = new List<CalendarEvent>();
                    }
                    else
                    {
                        convert = JsonConvert.DeserializeObject<List<CalendarEvent>>(diskSpaceArray);
                        foreach (var item in convert)
                        {
                            item.Subject = item.Subject.Replace("\\", "");
                        }
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Calendar Week Error : " + e.ToString());
                    convert = new List<CalendarEvent>();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public List<Lead> GetLead()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/enquiryusers/");
            var ls = new Lead();
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);

            List<Lead> convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<List<Lead>>(request, response => {
                System.Diagnostics.Debug.WriteLine("Lead Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    convert = JsonConvert.DeserializeObject<List<Lead>>(diskSpaceArray);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Lead Error : " + e.ToString());
                    convert = new List<Lead>();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
        public Loyalty GetLoyalty()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var ls = new Loyalty();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/loyaltycard/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID);
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);

            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<Loyalty>(request, response => {
                System.Diagnostics.Debug.WriteLine("Loyalty : " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    ls = JsonConvert.DeserializeObject<Loyalty>(json);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Loyalty Error : " + e.ToString());
                    ls = new Loyalty();
                }

                Wait.Set();
            });
            Wait.WaitOne();

            return ls;
        }
   
        public List<CallBack> GetCallBack()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();

            var ls = new CallBack();
            // var dd = new Data();
            // var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/loyaltycard/");
            var client = new RestSharp.RestClient("http://mastereman.com/firebase/leads.php");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);
            System.Diagnostics.Debug.WriteLine("GetCallBack: " + credential.UserID.ToString());

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            System.Diagnostics.Debug.WriteLine("ERRor : " + content.ToString());
            JObject jo = JObject.Parse(content);
            var diskSpaceArray = jo.SelectToken("data", false).ToString();

            List<CallBack> convert = JsonConvert.DeserializeObject<List<CallBack>>(diskSpaceArray);

            // DiskSpaceInfo[] diskSpaceArray = jo.SelectToken("d", false).ToObject<DiskSpaceInfo[]>();
            // System.Diagnostics.Debug.WriteLine("Count : " + convert);

            return convert;
        }
        public RewardLevel GetReward()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/reward_level/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID);
            request.AddParameter("email", credential.User.Email);
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);
            RewardLevel convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<RewardLevel>(request, response => {
                System.Diagnostics.Debug.WriteLine("Reward Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    convert = JsonConvert.DeserializeObject<RewardLevel>(json);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Reward Error : " + e.ToString());
                    convert = new RewardLevel();
                }

                Wait.Set();
            });
            Wait.WaitOne();

            return convert;
        }
        public Balance GetBalanceApi()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/accountbalance/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID);
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);
            Balance convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<Balance>(request, response => {
                System.Diagnostics.Debug.WriteLine("Balance Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    convert = JsonConvert.DeserializeObject<Balance>(json);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Balance Error : " + e.ToString());
                    convert = new Balance();
                }

                Wait.Set();
            });
            Wait.WaitOne();

            return convert;
        }
        public async Task<RewardLevel> SendReward(Notes note)
        {

            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();

            //  var ls = new Lead();

            //System.Diagnostics.Debug.WriteLine($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/enquiryusers/");

            var client = new RestSharp.RestClient("http://mastereman.com/restful/pages/samplereward.php");

            var ls = new Lead();
            //   var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/enquiryusers/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddParameter("status", note.status);
            request.AddParameter("category", note.category);
            request.AddParameter("contact_enquiry", note.contact_enquiry);
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);
            //   var content = response.Content;
            RewardLevel convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<Lead>(request, response => {
                System.Diagnostics.Debug.WriteLine(response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    // var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    convert = JsonConvert.DeserializeObject<RewardLevel>(json);
                    // convert = JsonConvert.DeserializeObject<RewardLevel>(diskSpaceArray);

                    System.Diagnostics.Debug.WriteLine("jo : " + convert.ToString());
                    System.Diagnostics.Debug.WriteLine("-end-");

                }
                catch (InvalidCastException e)
                {
                    System.Diagnostics.Debug.WriteLine("ERRor : " + e.ToString());
                }

                Wait.Set();
            });
            Wait.WaitOne();

            return convert;
        }
        public async Task<List<Notes>> GetNotes()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();

            //  var ls = new Lead();

          //  System.Diagnostics.Debug.WriteLine($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/enquiryusers/");

            var client = new RestSharp.RestClient("http://mastereman.com/restful/pages/notes.php");

            var ls = new Notes();
            
           // var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/notes/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);
            //   var content = response.Content;
            List<Notes> convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<Notes>(request, response => {
                System.Diagnostics.Debug.WriteLine("notes "+response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    // ls = JsonConvert.DeserializeObject<Lead>(json);
                    convert = JsonConvert.DeserializeObject<List<Notes>>(diskSpaceArray);

                    System.Diagnostics.Debug.WriteLine("convert : " + convert.Count());

                }
                 
                catch (InvalidCastException e)
                {
                    System.Diagnostics.Debug.WriteLine("ERRor : " + e.ToString());
                }

                Wait.Set();
            });
            Wait.WaitOne();

            return convert;
        }
        public async Task<String>  ActionsButton(LeadsActionType LeadsActionType ,string leads_id)
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();

            //  var ls = new Lead();

            //  System.Diagnostics.Debug.WriteLine($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/enquiryusers/");

            var client = new RestSharp.RestClient("http://mastereman.com/restful/pages/markasconverted.php");

            var ls = new Notes();

            // var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/notes/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("partner_id", credential.UserID.ToString());
            request.AddParameter("leads_id", leads_id.ToString());
            request.AddHeader("umbrella-api-username", UmbrellaApi.USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.PARTNER_ID);
            //   var content = response.Content;
            List<Notes> convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<Notes>(request, response => {
                System.Diagnostics.Debug.WriteLine("notes " + response.Content);
                try
                {
                   /* var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    // ls = JsonConvert.DeserializeObject<Lead>(json);
                    convert = JsonConvert.DeserializeObject<List<Notes>>(diskSpaceArray);

                    System.Diagnostics.Debug.WriteLine("convert : " + convert.Count());
*/
                }

                catch (InvalidCastException e)
                {
                    System.Diagnostics.Debug.WriteLine("ERRor : " + e.ToString());
                }

                Wait.Set();
            });
            Wait.WaitOne();
            var returnString="";
            if(LeadsActionType == LeadsActionType.MarkAsConverted){
                returnString = "Succesfully Mark as Converted";
            }
            else if (LeadsActionType == LeadsActionType.SellEnquiry)
            {
                returnString = "Succesfully Mark as SellEnquiry";
            }
            else if (LeadsActionType == LeadsActionType.Unconvertible)
            {
                returnString = "Succesfully Mark as Unconvertible";
            }
            else if (LeadsActionType == LeadsActionType.BookCallBack)
            {
                returnString = "BookCallBack";
            }
            else if (LeadsActionType == LeadsActionType.ChangeCategory)
            {
                returnString = "Category Changed";
            }
            return returnString;
        }
    }
}
