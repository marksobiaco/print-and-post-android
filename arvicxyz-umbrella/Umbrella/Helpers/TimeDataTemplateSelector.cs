﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Controls;
using Xamarin.Forms;

namespace Umbrella.Helpers
{
    public class TimeDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate EarlierTimeTemplate { get; set; }
        public DataTemplate MainTimeTemplate { get; set; }
        public DataTemplate LaterTimeTemplate { get; set; }
        public TimeDataTemplateSelector()
        {
            try
            {

                EarlierTimeTemplate = new DataTemplate(typeof(EarlierTimeView));
                MainTimeTemplate = new DataTemplate(typeof(MainTimeView));
                LaterTimeTemplate = new DataTemplate(typeof(LaterTimeView));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("system " + ex.Message + " " + ex.InnerException);
            }
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            switch ((int)item)
            {
                case 1: return EarlierTimeTemplate;
                case 3: return LaterTimeTemplate;
                default: return MainTimeTemplate;
            };
        }
    }
}
