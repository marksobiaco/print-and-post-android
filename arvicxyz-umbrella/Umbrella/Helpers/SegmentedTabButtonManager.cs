﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Controls;
using Xamarin.Forms;

namespace Umbrella.Helpers
{
    public class SegmentedTabButtonManager
    {
        private const string NormalColorString = "MainBlackColor";
        private const string SelectedColorString = "MainWhiteColor";

        public static void SelectTab(SegmentedTabButton tabButton)
        {
            Color normalColor = (Color)Application.Current.Resources[NormalColorString];
            Color selectedColor = (Color)Application.Current.Resources[SelectedColorString];

            tabButton.Color = Color.White;
            tabButton.BorderColor = Color.White;
            tabButton.LabelColor = normalColor;
        }

        public static void SelectTab(Label tabLabel)
        {
            Color normalColor = (Color)Application.Current.Resources[NormalColorString];
            Color selectedColor = (Color)Application.Current.Resources[SelectedColorString];

            tabLabel.BackgroundColor = selectedColor;
            tabLabel.TextColor = normalColor;
        }

        public static void DeselectTab(SegmentedTabButton tabButton)
        {
            Color normalColor = (Color)Application.Current.Resources[NormalColorString];
            Color selectedColor = (Color)Application.Current.Resources[SelectedColorString];

            tabButton.Color = normalColor;
            tabButton.BorderColor = Color.White;
            tabButton.LabelColor = selectedColor;
        }

        public static void DeselectTab(Label tabLabel)
        {
            Color normalColor = (Color)Application.Current.Resources[NormalColorString];
            Color selectedColor = (Color)Application.Current.Resources[SelectedColorString];

            tabLabel.BackgroundColor = normalColor;
            tabLabel.TextColor = selectedColor;
        }
    }
}
