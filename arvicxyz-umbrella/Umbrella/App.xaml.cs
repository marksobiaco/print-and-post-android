﻿using Umbrella.Data;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.ViewModels;
using Umbrella.Views;
using Xamarin.Forms;
using Umbrella.Utilities;
using Umbrella.Enums;
using System.Threading.Tasks;
using Plugin.Connectivity;
using BranchXamarinSDK;
using System.Collections.Generic;
using Com.OneSignal;
using Com.OneSignal.Abstractions;

namespace Umbrella
{

    public partial class App : Application, IBranchBUOSessionInterface
    {
        public static byte[] CroppedImage;
        public static bool IsNotified { get; set; }
        public static string AppName { get; set; }

      
        static PreviousContactsDatabase previousContactsDatabase;
        public static PreviousContactsDatabase PreviousContactsDatabase
        {
            get
            {
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (previousContactsDatabase == null)
                {
                    previousContactsDatabase = new PreviousContactsDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("PreviousContacts" + credential.UserID + ".db3"));
                }
                else
                {
                    previousContactsDatabase = new PreviousContactsDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("PreviousContacts" + credential.UserID + ".db3"));

                }
                return previousContactsDatabase;
            }
        }
        static OntraportAddressDatabase ontraportAddressDatabase;
        public static OntraportAddressDatabase OntraportAddressDatabase
        {
            get
            {
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (ontraportAddressDatabase == null)
                {
                    ontraportAddressDatabase = new OntraportAddressDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("OntraportAddress" + credential.UserID + ".db3"));
                }
                else
                {
                    ontraportAddressDatabase = new OntraportAddressDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("OntraportAddress" + credential.UserID + ".db3"));

                }
                return ontraportAddressDatabase;
            }
        }
        static RecentlyUsedContactDatabase recentlyUsedContactDatabase;
        public static RecentlyUsedContactDatabase RecentlyUsedContactDatabase
        {
            get
            {
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (recentlyUsedContactDatabase == null)
                {
                    recentlyUsedContactDatabase = new RecentlyUsedContactDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("RecentContactDbRed" + credential.UserID + ".db3"));
                }
                else
                {
                    recentlyUsedContactDatabase = new RecentlyUsedContactDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("RecentContactDbRed" + credential.UserID + ".db3"));
                }
                return recentlyUsedContactDatabase;
            }
        }
        static TopupDatabase topupDatabase;
        public static TopupDatabase TopupDatabase
        {
            get
            {
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (topupDatabase == null)
                {
                    topupDatabase = new TopupDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("TopUp" + credential.UserID + ".db3"));
                }
                else
                {
                    topupDatabase = new TopupDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("TopUp" + credential.UserID + ".db3"));
                }
                return topupDatabase;
            }
        }
        static ReferEarnDatabase referEarnDatabase;
        public static ReferEarnDatabase ReferEarnDatabase
        {
            get
            {
                var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
                if (referEarnDatabase == null)
                {
                    referEarnDatabase = new ReferEarnDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("ReferEarn" + credential.UserID + ".db3"));
                }
                else
                {
                    referEarnDatabase = new ReferEarnDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("ReferEarn" + credential.UserID + ".db3"));
                }
                return referEarnDatabase;
            }
        }
      
        public App()
        {
            InitializeComponent();
            AppName = (string)Current.Resources["AppNameString"];
            OneSignal.Current.StartInit("7968f180-c07b-4dd8-bdca-6c4525a4ef6d")
                  .Settings(new Dictionary<string, bool>() {
                { IOSSettings.kOSSettingsKeyAutoPrompt, false },
                { IOSSettings.kOSSettingsKeyInAppLaunchURL, false } }).EndInit();
            OneSignal.Current.RegisterForPushNotifications();
            if (!Global.GetIsSleep())
            {
                MessagingCenter.Subscribe<string>(this, "MoveRewardPage", MoveRewardPage, null);
            }
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (credential != null)
            {
                MainPage = new NavigationPage(new HomePage());
               
            }
            else
            {
                MainPage = new NavigationPage(new LoginPage());
            }
        }
        protected async override void OnStart()
        {
            base.OnStart();
            if (!CrossConnectivity.Current.IsConnected)
            {
                await App.Current.MainPage.DisplayAlert("Important Notice!", "No internet connection detected.", "OK");
            }
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            System.Diagnostics.Debug.WriteLine("onstart" + Global.GetCountPush());
            if (credential != null)
            {
              
                if (Global.GetNotifTopic() == "pdf")
                {
                    System.Diagnostics.Debug.WriteLine("pdf in");
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new PDFPage()));
                }
            }
          
        }
        #region IBranchBUOSessionInterface implementation

        public void InitSessionComplete(BranchUniversalObject buo, BranchLinkProperties blp)
        {
            System.Diagnostics.Debug.WriteLine("InitSession BUO" + buo.ToJsonString() + " " + blp.ToJsonString());
            var paramss = Branch.GetInstance().GetLastReferringParams();
            var branch = new ReferralBranchModel();
            foreach (var item in paramss)
            {
                System.Diagnostics.Debug.WriteLine("data" + item.Value.ToString() + " " + item.Key.ToString());
                if (item.Key.ToString() == "referlink")
                {
                    //await App.Current.MainPage.DisplayAlert("test ", item.Value.ToString(), "OK");
                    branch.ReferCodes = item.Value.ToString();
                    System.Diagnostics.Debug.WriteLine("codes " + branch.ReferCodes);
                }
                ///await App.Current.MainPage.DisplayAlert("test ", item.Value.ToString(), "OK");
            }
            if (branch != null)
            {
                System.Diagnostics.Debug.WriteLine("not null");
                if (!string.IsNullOrEmpty(branch.ReferCodes))
                {
                    System.Diagnostics.Debug.WriteLine("not empty");
                    MessagingCenter.Send<string>(branch.ReferCodes, "ChangeReferCodes");
                    Global.SetBranchModel(branch);
                }
            }

        }

        public void SessionRequestError(BranchError error)
        {
            System.Diagnostics.Debug.WriteLine("Branch Errr " + error.ErrorMessage);
        }
        #endregion
        #region IBranchSessionInterface implementation

        public void InitSessionComplete(Dictionary<string, object> data)
        {
            System.Diagnostics.Debug.WriteLine("InitSession Interface");

        }

        public void CloseSessionComplete()
        {

        }

        #endregion
        private async void MoveRewardPage(string page)
        {
            System.Diagnostics.Debug.WriteLine("move page " + Global.GetIsNotif() + " " + Global.GetNotifTopic());
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if(credential != null)
            {
                Global.SetIsNotif(true);
                if (Global.GetNotifTopic() == "import")
                {
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new ImportPage()));
                }
                else if (Global.GetNotifTopic() == "track")
                {
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new TrackPage()));
                }
                else if (Global.GetNotifTopic() == "refer")
                {
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new ReferAndEarnPage()));
                }
                else if (Global.GetNotifTopic() == "previousreferrals")
                {
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new ReferAndEarnPage(true)));
                }
                else if (Global.GetNotifTopic() == "topup")
                {
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new TopupPage()));
                }
                else if (Global.GetNotifTopic() == "pdf")
                {
                    System.Diagnostics.Debug.WriteLine("pdf in");
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new PDFPage()));
                }
                else if (string.IsNullOrEmpty(Global.GetNotifTopic()))
                {
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new HomePage()));
                }
            }
        }
        protected async override void OnResume()
        {
            base.OnResume();
            Global.SetIsSleep(false);
            if (Global.GetIsDonePick() && Global.GetUploadPageType() == "PDFDetailsPage")
            {
                Global.SetUploadPageType("");
                await MainPage.Navigation.PushModalAsync(new NavigationPage(new PDFDetailsPage(Global.GetPhoneContact())));
            }
            else if (Global.GetIsDonePick() && Global.GetUploadPageType() == "PDFTutorialPage")
            {
                Global.SetUploadPageType("");
                await MainPage.Navigation.PushModalAsync(new NavigationPage(new PostCardContactPage()));
            }
            else if(Global.GetIsDonePick() && Global.GetUploadPageType() == "ImageCropPage")
            {
                Global.SetUploadPageType("");
                await MainPage.Navigation.PushModalAsync(new NavigationPage(new ImageCropPage()));
            }
            else if (Global.GetIsDonePick() && Global.GetIsFromLocalFiles())
            {
                if (Global.GetUploadPageType() == "image")
                {
                    Global.SetUploadPageType("");
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new PostCardContactPage()));
                }
                else
                {
                    Global.SetUploadPageType("");
                    await MainPage.Navigation.PushModalAsync(new NavigationPage(new PDFPage()));
                }

            }
            System.Diagnostics.Debug.WriteLine("on resume");
        }
        protected override void OnSleep()
        {
            Global.SetIsSleep(true);
            System.Diagnostics.Debug.WriteLine("on sleep");
        }
        
    }
}