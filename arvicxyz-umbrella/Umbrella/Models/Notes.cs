﻿using System;
namespace Umbrella.Models
{
    public class Notes
    {
        public string id;
        public String status;
        public String category;
        public String contact_enquiry;
        public String partner_notes;
        public String user_id;
        public String user_email;
        public String bpc_user_id;
    }

    public class TemporaryNotes
    {
        public string Author { get; set; }
        public DateTime SavedDate { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
    }
}
