﻿using System.ComponentModel;
using Com.OneSignal;
using Umbrella.Interfaces;
using Xamarin.Forms;

namespace Umbrella.Models
{
    public class NotificationsItem : INotifyPropertyChanged
    {
        public string Title { get; set; }
        public string ToogleFor { get; set; }
        public string Description { get; set; }
        public double DescriptionFontSize { get; set; }
        public double TitleFontSize { get; set; }
        public double WidthBox { get; set; }

        private bool isToggled;
        public bool IsToggled
        {
            get { return isToggled; }
            set
            {
                if (isToggled != value)
                {
                    isToggled = value;
                    NotifyPropertyChanged("IsToggled", ToogleFor);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName, string ToogleFor)
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var defNotification = DependencyService.Get<ICredentialRetriever>().GetDefaultNotification();
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                if (credential.User.Username == "juju@raptorsms.com")
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                    if (isToggled)
                    {
                        if (ToogleFor == "general")
                        {
                            defNotification.GeneralNotif = "Oui";
                        }
                        if (ToogleFor == "refer_earn_updates")
                        {
                            defNotification.LeadConvertedNotif = "Oui";
                        }
                        if (ToogleFor == "special_delivery")
                        {
                            defNotification.NewMessageNotif = "Oui";
                        }
                        if (ToogleFor == "signed_alerts")
                        {
                            defNotification.NewLeadNotif = "Oui";
                        }
                        //CrossFirebasePushNotification.Current.Subscribe(ToogleFor);
                        OneSignal.Current.SendTag(ToogleFor, credential.UserID);
                        System.Diagnostics.Debug.WriteLine(" Subscribed " + ToogleFor);
                    }
                    else
                    {
                        if (ToogleFor == "general")
                        {
                            defNotification.GeneralNotif = "No";
                        }
                        if (ToogleFor == "refer_earn_updates")
                        {
                            defNotification.LeadConvertedNotif = "No";
                        }
                        if (ToogleFor == "special_delivery")
                        {
                            defNotification.NewMessageNotif = "No";
                        }
                        if (ToogleFor == "signed_alerts")
                        {
                            defNotification.NewLeadNotif = "No";
                        }
                        OneSignal.Current.DeleteTag(ToogleFor);
                        //CrossFirebasePushNotification.Current.Unsubscribe(ToogleFor);
                        System.Diagnostics.Debug.WriteLine(" Unsubscribe " + ToogleFor);
                    }
                    MessagingCenter.Send<DefaultNotification>(defNotification, "SaveDefaultNotification");
                }
                else
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                    if (isToggled)
                    {
                        if (ToogleFor == "general")
                        {
                            defNotification.GeneralNotif = "Yes";
                        }
                        if (ToogleFor == "refer_earn_updates")
                        {
                            defNotification.LeadConvertedNotif = "Yes";
                        }
                        if (ToogleFor == "special_delivery")
                        {
                            defNotification.NewMessageNotif = "Yes";
                        }
                        if (ToogleFor == "signed_alerts")
                        {
                            defNotification.NewLeadNotif = "Yes";
                        }
                        //CrossFirebasePushNotification.Current.Subscribe(ToogleFor);
                        OneSignal.Current.SendTag(ToogleFor, credential.UserID);
                        System.Diagnostics.Debug.WriteLine(" Subscribed " + ToogleFor);
                    }
                    else
                    {
                        if (ToogleFor == "general")
                        {
                            defNotification.GeneralNotif = "No";
                        }
                        if (ToogleFor == "refer_earn_updates")
                        {
                            defNotification.LeadConvertedNotif = "No";
                        }
                        if (ToogleFor == "special_delivery")
                        {
                            defNotification.NewMessageNotif = "No";
                        }
                        if (ToogleFor == "signed_alerts")
                        {
                            defNotification.NewLeadNotif = "No";
                        }
                        OneSignal.Current.DeleteTag(ToogleFor);
                        //CrossFirebasePushNotification.Current.Unsubscribe(ToogleFor);
                        System.Diagnostics.Debug.WriteLine(" Unsubscribe " + ToogleFor);
                    }
                    MessagingCenter.Send<DefaultNotification>(defNotification, "SaveDefaultNotification");

                }
            }
        }
    }
}
