﻿using System;
namespace Umbrella.Models
{
    public class ReferralBranchModel
    {
        public string ReferCodes { get; set; }

        public string ReferLink { get; set; }
    }
}
