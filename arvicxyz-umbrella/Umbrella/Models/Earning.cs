﻿using Newtonsoft.Json;
using System;

namespace Umbrella.Models
{
    public class Earning
    {
        public string Name { get; set; }

        public string Business { get; set; }

        public decimal Value { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public DateTime Created { get; set; }
    }
    public class MonthlyEarnings
    {
        [JsonProperty("this_month")]
        public string ThisMonth { get; set; }
        [JsonProperty("this_month_amount")]
        public string ThisMonthAmount { get; set; }
        public string ThisEuroAmount
        {
            get
            {
                return "£" + ThisMonthAmount;
            }
        }
        [JsonProperty("month_1")]
        public string Month1 { get; set; }
        [JsonProperty("month_1_amount")]
        public string ThisMonthAmount1 { get; set; }
        public string ThisEuroAmount1
        {
            get
            {
                return "£" + ThisMonthAmount1;
            }
        }
        [JsonProperty("month_2")]
        public string Month2 { get; set; }
        [JsonProperty("month_2_amount")]
        public string ThisMonthAmount2 { get; set; }
        public string ThisEuroAmount2
        {
            get
            {
                return "£" + ThisMonthAmount2;
            }
        }
        [JsonProperty("month_3")]
        public string Month3 { get; set; }
        [JsonProperty("month_3_amount")]
        public string ThisMonthAmount3 { get; set; }
        public string ThisEuroAmount3
        {
            get
            {
                return "£" + ThisMonthAmount3;
            }
        }
    }
}
