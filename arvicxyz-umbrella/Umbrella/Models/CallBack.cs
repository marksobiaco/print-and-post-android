﻿using System;
using Newtonsoft.Json;
using Umbrella.Helpers;
using Umbrella.Converters;

namespace Umbrella.Models
{
    public class CallBack : ObservableBase
    {
        public Lead Lead { get; set; }

        public string ContactType { get; set; }

        public DateTime ScheduledDateTime { get; set; }

        private string _EnquiryId;

        [JsonProperty("id")]
        public string EnquiryId { get { return this._EnquiryId; } set { this.SetProperty(ref this._EnquiryId, value); } }


        private string _ContactId;
        [JsonProperty("partner_id")]
        public string ContactId { get { return this._ContactId; } set { this.SetProperty(ref this._ContactId, value); } }

        private string _Name;
        [JsonProperty("last_name")]
        public string Name { get { return this._Name; } set { this.SetProperty(ref this._Name, value); } }

        private string _Business;
        [JsonProperty("company")]
        public string Business { get { return this._Business; } set { this.SetProperty(ref this._Business, value); } }

        private string _Email;
        [JsonProperty("email_address")]
        public string Email { get { return this._Email; } set { this.SetProperty(ref this._Email, value); } }

    }
    public class CallBackEnquiry
    {
        [JsonProperty("notes")]
        public string Notes { get; set; }
    }
    public class CallBackProfile
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("company")]
        public string Company { get; set; }
        [JsonProperty("id")]
        public string ContactId { get; set; }
        [JsonProperty("partner_id")]
        public string partner_id { get; set; }
        [JsonProperty("contact_source")]
        public string ContactSource { get; set; }
        [JsonProperty("address_line1")]
        public string AddressLine1 { get; set; }
        [JsonProperty("home_phone")]
        public string MobilePhoneNum { get; set; }
        [JsonProperty("mobile_phone")]
        public string HomePhoneNum { get; set; }
        [JsonProperty("office_phone")]
        public string OfficePhoneNum { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        public string Name
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Notes { get; set; }

    }
    public class CallBackList
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("company")]
        public string Company { get; set; }
        [JsonProperty("id")]
        public string ContactId { get; set; }
        [JsonProperty("partner_id")]
        public string partner_id { get; set; }
        [JsonProperty("call_back_date")]
        public string CallBackDate { get; set; }
        [JsonProperty("call_back_time")]
        public string CallBackTime { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        public double MiddleBoxFont { get; set; }
        public double FirstBoxFont { get; set; }
        public double FirstBoxWidth { get; set; }
        public string Name
        {
            get
            {
                return Title + " " + FirstName + " " + LastName;
            }
        }
    }
}
