﻿using System;
using System.Collections.Generic;

namespace Umbrella.Models
{
    public class GoogleApiModels
    {

    }
    // Place Details
    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Close
    {
        public int day { get; set; }
        public string time { get; set; }
    }

    public class Open
    {
        public int day { get; set; }
        public string time { get; set; }
    }

    public class Period
    {
        public Close close { get; set; }
        public Open open { get; set; }
    }

    public class OpeningHours
    {
        public bool open_now { get; set; }
        public List<Period> periods { get; set; }
        public List<string> weekday_text { get; set; }
    }

    public class Result
    {
        public string formatted_phone_number { get; set; }
        public Geometry geometry { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public string formatted_address { get; set; }
        public OpeningHours opening_hours { get; set; }
        public string url { get; set; }
        public double rating { get; set; }
    }

    public class RootStartingObject
    {
        public List<object> html_attributions { get; set; }
        public Result result { get; set; }
        public string status { get; set; }
    }
    //Google Places Search
    public class LocationSearch
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class NortheastSearch
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class SouthwestSearch
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class ViewportSearch
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class GeometrySearch
    {
        public Location location { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Candidate
    {
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string name { get; set; }
        public string place_id { get; set; }
    }

    public class RootObject
    {
        public List<Candidate> candidates { get; set; }
        public string status { get; set; }
    }
    public class MatchedSubstring
    {
        public int length { get; set; }
        public int offset { get; set; }
    }

    public class MainTextMatchedSubstring
    {
        public int length { get; set; }
        public int offset { get; set; }
    }

    public class StructuredFormatting
    {
        public string main_text { get; set; }
        public List<MainTextMatchedSubstring> main_text_matched_substrings { get; set; }
        public string secondary_text { get; set; }
    }

    public class Term
    {
        public int offset { get; set; }
        public string value { get; set; }
    }

    public class Prediction
    {
        public string description { get; set; }
        public string id { get; set; }
        public List<MatchedSubstring> matched_substrings { get; set; }
        public string place_id { get; set; }
        public string reference { get; set; }
        public StructuredFormatting structured_formatting { get; set; }
        public List<Term> terms { get; set; }
        public List<string> types { get; set; }
    }

    public class RootObjectAutoComplete
    {
        public List<Prediction> predictions { get; set; }
        public string status { get; set; }
    }
    public class GeometryPlaceDetails
    {
        public Location location { get; set; }
    }

    public class ResultDetails
    {
        public Geometry geometry { get; set; }
        public string name { get; set; }
    }

    public class RootObjectPlaceDetails
    {
        public List<object> html_attributions { get; set; }
        public Result result { get; set; }
        public string status { get; set; }
    }
}
