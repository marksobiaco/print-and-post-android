﻿using Newtonsoft.Json;
using Umbrella.Enums;

namespace Umbrella.Models
{
    public class Charity
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("country")]
        public string Location { get; set; }
        [JsonProperty("short_bio")]
        public string Message { get; set; }
        [JsonProperty("image_url")]
        public string Image { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("location_btn_url")]
        public string Location_URL { get; set; }
        public double CharityListFontSize { get; set; }
    }
    public class Tracking
    {
        public string Add1 { get; set; }
        public double MiddleFontSize { get; set; }
        public string Add2 { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string EnumCode { get; set; }
        public string EnumStatus { get; set; }
        public string WholeAddress
        {
            get
            {
                return Add1 + ", " + Add2 + ", " + Town + ", " + City + ", " + PostCode;
            }

        }
    }
}
