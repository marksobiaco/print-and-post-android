﻿using System;

using Newtonsoft.Json;

namespace Umbrella.Models
{
    public class Loyalty
    {
        [JsonProperty("partner_id")]
        public string partner_id { get; set; }

        [JsonProperty("points")]
        public string points { get; set; }

        [JsonProperty("package_level")]
        public string package_level { get; set; }

        [JsonProperty("barcode")]
        public string barcode { get; set; }

        [JsonProperty("barcode_url")]
        public string barcode_url { get; set; }

        [JsonProperty("business_name")]
        public string business_name { get; set; }
        [JsonProperty("firstname")]
        public string firstname { get; set; }
        [JsonProperty("lastname")]
        public string lastname { get; set; }

        public string Fullname
        {
            get
            {
                return firstname + " " + lastname; 
            }
        }


    }
}
