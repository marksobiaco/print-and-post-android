﻿using Newtonsoft.Json;

namespace Umbrella.Models
{
    public class Card
    {
        public string Name { get; set; }

        public string Number { get; set; }

        public int ExpiryMonth { get; set; }

        public int ExpiryYear { get; set; }

        public string CVC { get; set; }
    }
    public class RetrievedCard
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Last4 { get; set; }
        public string Brand { get; set; }
        public string NameWithLastFourDigits
        {
            get
            {
                var text = string.Empty;
                if (Brand == "American Express")
                {
                    text = Name + " (Amex " + Last4 + ")";
                }
                else if (Brand == "MasterCard")
                {
                    text = Name + " (MC " + Last4 + ")";
                }
                else if (Brand == "Visa")
                {
                    text = Name + " (Visa " + Last4 + ")";
                }
                return text;
            }
        }
    }
    public class Balance
    {
        [JsonProperty("partner_id")]
        public string Id { get; set; }
        [JsonProperty("op_account_balance")]
        public double BalancePoints { get; set; }
    }
}
