﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbrella.Models
{
    public class OntraportContact
    {
        [JsonProperty("id")]
        public string PartnerID { get; set; }
        [JsonProperty("firstname")]
        public string Firstname { get; set; }
        [JsonProperty("lastname")]
        public string Lastname { get; set; }
        [JsonProperty("company")]
        public string Company { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("office_phone")]
        public string OfficePhone { get; set; }
        [JsonProperty("cell_phone")]
        public string CellPhone { get; set; }
        [JsonProperty("f1561")]
        public string Message { get; set; }
        [JsonProperty("n_lead_source")]
        public int PhoneUsed {  get; set; }
        [JsonProperty("freferrer")]
        public string AccountUsed { get; set; }
        [JsonProperty("f1652")]
        public int Relationship { get; set; }
        [JsonProperty("l_lead_source")]
        public int LastLeadSource { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("f1892")]
        public string MMSCountry { get; set; }
        [JsonProperty("f1893")]
        public string MMSComments { get; set; }
    }
    public class AddTagOntraport
    {
        [JsonProperty("objectID")]
        public string objectID { get; set; }
        [JsonProperty("add_names")]
        public string[] AddLists { get; set; }
        [JsonProperty("ids")]
        public string[] Ids { get; set; }
    }
    public class UploadsOntraport
    {
        [JsonProperty("id")]
        public string PartnerID { get; set; }
        [JsonProperty("f1958")]
        public string UploadLinks { get; set; }

    }
    public class TopupUploadOntraport
    {
        [JsonProperty("id")]
        public string PartnerID { get; set; }
        [JsonProperty("f1959")]
        public string CurrencyOP { get; set; }
        [JsonProperty("f1616")]
        public int TopupAmount { get; set; }
        [JsonProperty("n_lead_source")]
        public int PhoneUsed { get; set; }

    }
    public class OntraportJsonAddress
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string company { get; set; }
        public string County_456 { get; set; }
        public string Town_340 { get; set; }
    }
    public class FileUpload
    {
        public string url { get; set; }
        public string partner_id { get; set; }
        public string file_id { get; set; }
        public string filename { get; set; }
        public string message { get; set; }

    }
    public class PdfFileUpload
    {
        public string success { get; set; }
        public string process_status { get; set; }
        public string Queue_id { get; set; }
        public string message { get; set; }
        public string process_message { get; set; }

    }
    public class Countries
    {
        [JsonProperty("name")]
        public string CountryName { get; set; }
    }
    public class OntraportJsonModel
    {
        public string id { get; set; }
        public string f1670 { get; set; }
    }
    public class RootListJsonModel
    {
        public List<OntraportJsonModel> data { get; set; }
    }
    public class OntraportEnquiryContact
    {
        [JsonProperty("f1561")]
        public string Message { get; set; }
        [JsonProperty("f1653")]
        public int EnquiryType { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
