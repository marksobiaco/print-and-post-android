﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Umbrella.Models
{
    public class SwipeModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public string Name { get; set; }
        public string ImageUri { get; set; }
        public string Description { get; set; }
        public string StoreName { get; set; }
        public string CuisineCategory { get; set; }
        public string[] DietaryInformation { get; set; }
        private bool _isLiked;
        public bool IsLiked
        {
            get
            {
                return _isLiked;
            }
            set
            {
                if (_isLiked != value)
                {
                    _isLiked = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private DateTime _likedTimed;
        public DateTime LikedTimed
        {
            get
            {
                return _likedTimed;
            }
            set
            {
                if (_likedTimed != value)
                {
                    _likedTimed = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }
}
