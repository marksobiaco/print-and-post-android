﻿using Newtonsoft.Json;
using System;
using Umbrella.Enums;

namespace Umbrella.Models
{
    public class Referral
    {
        [JsonProperty("first_name")]
        public string Firstname { get; set; }
        [JsonProperty("last_name")]
        public string Lastname { get; set; }
        public double MiddleFontSize { get; set; }
        public string Name
        {
            get
            {
                return Firstname + " " + Lastname;
            }
        }
        [JsonProperty("business_name")]
        public string Business { get; set; }

        public string Relationship { get; set; }
        public string EmailAddress { get; set; }
        [JsonProperty("home_phone")]
        public string PhoneNumber { get; set; }
        [JsonProperty("mobile_phone")]
        public string MobileNumber { get; set; }
        [JsonProperty("description")]
        public string Message { get; set; }
        [JsonProperty("status")]
        public string StringStatus { get; set; }
        public ReferralStatus EnumStatus { get; set; }
        [JsonProperty("date_of_birth")]
        public string Created { get; set; }
    }
}
