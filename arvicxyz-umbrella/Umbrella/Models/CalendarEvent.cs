﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace Umbrella.Models
{
    public class CalendarEvent
    {
        public double TwelveFontSize { get; set; }
        public double WidthBox { get; set; }
        [JsonProperty("event_name")]
        public string Subject { get; set; }
        public string Message { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("start_date")]
        public string StartEventDateTime { get; set; }
        [JsonProperty("end_date")]
        public string EndEventDateTime { get; set; }
        [JsonProperty("start_time")]
        public string StartTime { get; set; }
        [JsonProperty("end_time")]
        public string EndTime { get; set; }
        public string StringLocation
        {
            get
            {
                if (string.IsNullOrEmpty(Location))
                {
                    return "No Location";
                }
                return "Location: " + Location;
            }
        }
        public DateTime StartEventDT
        {
            get
            {
                var timeFormat = "dd-MM-yyyy HH:mm:ss";
                var timeFormat2 = "yyyy-MM-dd HH:mm:ss";
                var dateoutput = new DateTime();
                if (DateTime.TryParseExact(StartEventDateTime + " " + StartTime, timeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateoutput))
                {
                    return dateoutput;
                }
                else
                {
                    dateoutput = DateTime.ParseExact(StartEventDateTime + " " + StartTime, timeFormat2, CultureInfo.InvariantCulture);
                }
                return dateoutput;
            }
        }
        public DateTime EndEventDT
        {
            get
            {
                var timeFormat = "dd-MM-yyyy HH:mm:ss";
                var timeFormat2 = "yyyy-MM-dd HH:mm:ss";
                var dateoutput = new DateTime();
                if (DateTime.TryParseExact(EndEventDateTime + " " + EndTime, timeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateoutput))
                {
                    return dateoutput;
                }
                else
                {
                    dateoutput = DateTime.ParseExact(EndEventDateTime + " " + EndTime, timeFormat2, CultureInfo.InvariantCulture);
                }
                return dateoutput;
            }
        }
    }
}
