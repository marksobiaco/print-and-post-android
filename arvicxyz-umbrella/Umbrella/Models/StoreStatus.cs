﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbrella.Models
{
    public class StoreStatus
    {
        public string StatusImage { get; set; }
        public string StatusName { get; set; }
    }
}
