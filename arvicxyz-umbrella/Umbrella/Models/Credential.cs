﻿using Newtonsoft.Json;

namespace Umbrella.Models
{
    public class Credential
    {
        public User User { get; set; }

        public string Password { get; set; }
        public string UserID { get; set; }
    }
    public class DefaultTopupDetails
    {
        public string DefaultCurrency { get; set; }
        public string DefaultAmount { get; set; }
    }
    public class Default
    {
        public string InkType { get; set; }
        public string PaperType { get; set; }
        public string LabelType { get; set; }
        public string DispatchSpeed { get; set; }
        public string PartnerId { get; set; }
        public string AllowLeaflet { get; set; }
        public string PostageClass { get; set; }
    }
    public class DefaultNotification
    {
        public string GeneralNotif { get; set; }
        public string NewLeadNotif { get; set; }
        public string NewMessageNotif { get; set; }
        public string LeadConvertedNotif { get; set; }
    }
    public class VerifiedAdd
    {
        [JsonProperty("undeliverable_registered_office_address")]
        public string IsVerified { get; set; }
    }
    public class DetailedAddress
    {
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("address_line_2")]
        public string AddLine2 { get; set; }
        [JsonProperty("locality")]
        public string Locality { get; set; }
        [JsonProperty("region")]
        public string Region { get; set; }
        [JsonProperty("address_line_1")]
        public string AddLine1 { get; set; }
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }
    }
    public class CompanyToken
    {
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
    public class Company
    {
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("company_status")]
        public string CompanyStatus { get; set; }
        [JsonProperty("company_number")]
        public string CompanyNumber { get; set; }
        [JsonProperty("address_snippet")]
        public string AddressSnippet { get; set; }
    }
    public class Directors
    {
        [JsonProperty("namesss")]
        public string Name { get; set; }
        [JsonProperty("officer_role")]
        public string Role { get; set; }
        [JsonProperty("occupation")]
        public string Occupation { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        public double DirectorFontSize { get; set; }
        public string Fullname
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}

