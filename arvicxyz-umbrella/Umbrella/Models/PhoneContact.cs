﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbrella.Models
{
    public class PhoneContact
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string CompanyName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string HomeNumber { get; set; }
        public string MainNumber { get; set; }
        public string OfficeNumber { get; set; }
        public string EmailAdd { get; set; }
        public string PartnerId { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public double MiddleFontSize { get; set; }
        public string PostDate { get; set; }
        public string AllowLeaflet { get; set; }
        public string PostCardMessage { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Ink { get; set; }
        public string Priority { get; set; }
        public string PostageClass { get; set; }
        public string Handwritten { get; set; }
        public string File_ID { get; set; }
        public bool IsFromLateDispatchPage { get; set; }
        public string Name { get => $"{FirstName} {LastName}"; }
    }
    public class PreviousContacts
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string CompanyName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string HomeNumber { get; set; }
        public string MainNumber { get; set; }
        public string OfficeNumber { get; set; }
        public string EmailAdd { get; set; }
        public string PartnerId { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string PostDate { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public string Name { get => $"{FirstName} {LastName}"; }
    }
}
