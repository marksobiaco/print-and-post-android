﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class RewardLevelDatabase
    {
        readonly SQLiteAsyncConnection database;
        public RewardLevelDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<RewardLevelApi>().Wait();
        }
        public Task ResetRewardLevelApis()
        {
            return database.DropTableAsync<RewardLevelApi>();
        }
        public Task<List<RewardLevelApi>> GetItemsAsync()
        {
            return database.Table<RewardLevelApi>().ToListAsync();
        }
        public Task<int> SaveRewardLevelApisAsync(List<RewardLevelApi> RewardLevelApis)
        {
            return database.InsertAllAsync(RewardLevelApis);
        }
        public Task<int> SaveSingleRewardLevelApiAsync(RewardLevelApi RewardLevelApis)
        {
            database.DropTableAsync<RewardLevelApi>().Wait();
            database.CreateTableAsync<RewardLevelApi>().Wait();
            return database.InsertAsync(RewardLevelApis);
        }
        public Task<List<RewardLevelApi>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<RewardLevelApi>($"SELECT * FROM [RewardLevelApi]");
        }
    }
}
