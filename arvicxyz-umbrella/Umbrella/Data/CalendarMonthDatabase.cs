﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class CalendarMonthDatabase
    {
        readonly SQLiteAsyncConnection database;
        public CalendarMonthDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<CalendarEvent>().Wait();
        }
        public Task ResetCalendarEvents()
        {
            return database.DropTableAsync<CalendarEvent>();
        }
        public Task<List<CalendarEvent>> GetItemsAsync()
        {
            return database.Table<CalendarEvent>().ToListAsync();
        }
        public Task<int> SaveCalendarEventsAsync(List<CalendarEvent> CalendarEvents)
        {
            database.DropTableAsync<CalendarEvent>().Wait();
            database.CreateTableAsync<CalendarEvent>().Wait();
            return database.InsertAllAsync(CalendarEvents);
        }
        public Task<int> SaveItemAsync(CalendarEvent item)
        {
            return database.InsertAsync(item);
        }
        public Task<List<CalendarEvent>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<CalendarEvent>($"SELECT * FROM [CalendarEvent]");
        }
    }
}
