﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class MyPADatabase
    {
        readonly SQLiteAsyncConnection database;
        public MyPADatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<MyPAMessages>().Wait();
        }
        public Task ResetMyPAMessagess()
        {
            return database.DropTableAsync<MyPAMessages>();
        }
        public Task<List<MyPAMessages>> GetItemsAsync()
        {
            return database.Table<MyPAMessages>().ToListAsync();
        }
        public Task<int> SaveMyPAMessagessAsync(List<MyPAMessages> MyPAMessagess)
        {
            database.DropTableAsync<MyPAMessages>().Wait();
            database.CreateTableAsync<MyPAMessages>().Wait();
            return database.InsertAllAsync(MyPAMessagess);
        }
        public Task<int> SaveItemAsync(MyPAMessages item)
        {
            return database.UpdateAsync(item);
        }
        public Task<List<MyPAMessages>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<MyPAMessages>($"SELECT * FROM [MyPAMessages]");
        }
    }
}
