﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class TopupDatabase
    {
        readonly SQLiteAsyncConnection database;
        public TopupDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Balance>().Wait();
        }
        public Task ResetBalances()
        {
            return database.DropTableAsync<Balance>();
        }
        public Task<List<Balance>> GetItemsAsync()
        {
            return database.Table<Balance>().ToListAsync();
        }
        public Task<int> SaveBalancesAsync(List<Balance> Balances)
        {
            return database.InsertAllAsync(Balances);
        }
        public Task<int> SaveSingleBalanceAsync(Balance Balances)
        {
            database.DropTableAsync<Balance>().Wait();
            database.CreateTableAsync<Balance>().Wait();
            return database.InsertAsync(Balances);
        }
        public Task<List<Balance>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<Balance>($"SELECT * FROM [Balance]");
        }
    }
}
