﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class OntraportAddressDatabase
    {
        readonly SQLiteAsyncConnection database;
        public OntraportAddressDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<OntraportJsonAddress>().Wait();
        }
        public Task ResetOntraportJsonAddress()
        {
            return database.DropTableAsync<OntraportJsonAddress>();
        }
        public Task<List<OntraportJsonAddress>> GetItemsAsync()
        {
            return database.Table<OntraportJsonAddress>().ToListAsync();
        }
        public Task<int> SaveOntraportJsonAddressAsync(List<OntraportJsonAddress> OntraportJsonAddress)
        {
            return database.InsertAllAsync(OntraportJsonAddress);
        }
        public Task<int> SaveSingleOntraportJsonAddressAsync(OntraportJsonAddress OntraportJsonAddress)
        {
            database.DropTableAsync<OntraportJsonAddress>().Wait();
            database.CreateTableAsync<OntraportJsonAddress>().Wait();
            return database.InsertAsync(OntraportJsonAddress);
        }
        public Task<List<OntraportJsonAddress>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<OntraportJsonAddress>($"SELECT * FROM [OntraportJsonAddress]");
        }
    }
}
