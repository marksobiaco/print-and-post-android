﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class ReferEarnDatabase
    {
        readonly SQLiteAsyncConnection database;
        public ReferEarnDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Referral>().Wait();
        }
        public Task ResetReferrals()
        {
            return database.DropTableAsync<Referral>();
        }
        public Task<List<Referral>> GetItemsAsync()
        {
            return database.Table<Referral>().ToListAsync();
        }
        public Task<int> SaveReferralsAsync(List<Referral> Referrals)
        {
            database.DropTableAsync<Referral>().Wait();
            database.CreateTableAsync<Referral>().Wait();
            return database.InsertAllAsync(Referrals);
        }
        public Task<int> SaveItemAsync(Referral item)
        {
            return database.InsertAsync(item);
        }
        public Task<List<Referral>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<Referral>($"SELECT * FROM [Referral]");
        }
    }
}
