﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class KivaDatabase
    {
        readonly SQLiteAsyncConnection database;
        public KivaDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Charity>().Wait();
        }
        public Task ResetCharitys()
        {
            return database.DropTableAsync<Charity>();
        }
        public Task<List<Charity>> GetItemsAsync()
        {
            return database.Table<Charity>().ToListAsync();
        }
        public Task<int> SaveCharitysAsync(List<Charity> Charitys)
        {
            database.DropTableAsync<Charity>().Wait();
            database.CreateTableAsync<Charity>().Wait();
            return database.InsertAllAsync(Charitys);
        }
        public Task<int> SaveItemAsync(Charity item)
        {
            return database.UpdateAsync(item);
        }
        public Task<List<Charity>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<Charity>($"SELECT * FROM [Charity]");
        }
    }
}
