﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class TemporaryNotesDatabase
    {
        readonly SQLiteAsyncConnection database;
        public TemporaryNotesDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<TemporaryNotes>().Wait();
        }
        public Task ResetTemporaryNotess()
        {
            return database.DropTableAsync<TemporaryNotes>();
        }
        public Task<List<TemporaryNotes>> GetItemsAsync()
        {
            return database.Table<TemporaryNotes>().ToListAsync();
        }
        public Task<int> SaveTemporaryNotessAsync(List<TemporaryNotes> TemporaryNotess)
        {
            return database.InsertAllAsync(TemporaryNotess);
        }
        public Task<int> SaveSingleTemporaryNotesAsync(TemporaryNotes TemporaryNotess)
        {
            return database.InsertAsync(TemporaryNotess);
        }

        public Task<List<TemporaryNotes>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<TemporaryNotes>($"SELECT * FROM [TemporaryNotes]");


        }
    }
}
