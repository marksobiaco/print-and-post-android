﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class LoyaltyCardDatabase
    {
        readonly SQLiteAsyncConnection database;
        public LoyaltyCardDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Loyalty>().Wait();
        }
        public Task ResetLoyaltys()
        {
            return database.DropTableAsync<Loyalty>();
        }
        public Task<List<Loyalty>> GetItemsAsync()
        {
            return database.Table<Loyalty>().ToListAsync();
        }
        public Task<int> SaveLoyaltysAsync(List<Loyalty> Loyaltys)
        {
            return database.InsertAllAsync(Loyaltys);
        }
        public Task<int> SaveSingleLoyaltyAsync(Loyalty Loyaltys)
        {
            database.DropTableAsync<Loyalty>().Wait();
            database.CreateTableAsync<Loyalty>().Wait();
            return database.InsertAsync(Loyaltys);
        }
        public Task<List<Loyalty>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<Loyalty>($"SELECT * FROM [Loyalty]");


        }
    }
}
