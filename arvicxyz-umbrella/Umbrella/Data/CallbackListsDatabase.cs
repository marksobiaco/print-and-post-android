﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class CallbackListsDatabase
    {
        readonly SQLiteAsyncConnection database;
        public CallbackListsDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<CallBackList>().Wait();
        }
        public Task ResetCallBackLists()
        {
            return database.DropTableAsync<CallBackList>();
        }
        public Task<List<CallBackList>> GetItemsAsync()
        {
            return database.Table<CallBackList>().ToListAsync();
        }
        public Task<int> SaveCallBackListsAsync(List<CallBackList> CallBackLists)
        {
            database.DropTableAsync<CallBackList>().Wait();
            database.CreateTableAsync<CallBackList>().Wait();
            return database.InsertAllAsync(CallBackLists);
        }
        public Task<int> SaveItemAsync(CallBackList item)
        {
            return database.InsertAsync(item);
        }
        public Task<List<CallBackList>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<CallBackList>($"SELECT * FROM [CallBackList]");
        }
    }

}
