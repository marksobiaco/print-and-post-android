﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class AppointmentDatabase
    {
        readonly SQLiteAsyncConnection database;
        public AppointmentDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<AppointmentStoreUpdate>().Wait();
        }
        public Task ResetAppointmentStoreUpdates()
        {
            return database.DropTableAsync<AppointmentStoreUpdate>();
        }
        public Task<List<AppointmentStoreUpdate>> GetItemsAsync()
        {
            return database.Table<AppointmentStoreUpdate>().ToListAsync();
        }
        public Task<int> SaveAppointmentStoreUpdatesAsync(List<AppointmentStoreUpdate> AppointmentStoreUpdates)
        {

            return database.InsertAllAsync(AppointmentStoreUpdates);
        }
        public Task<int> SaveItemAsync(AppointmentStoreUpdate item)
        {
            database.DropTableAsync<AppointmentStoreUpdate>().Wait();
            database.CreateTableAsync<AppointmentStoreUpdate>().Wait();
            return database.InsertAsync(item);
        }
        public Task<List<AppointmentStoreUpdate>> GetItemsByCategoryAsync()
        {
            return database.QueryAsync<AppointmentStoreUpdate>($"SELECT * FROM [AppointmentStoreUpdate]");
        }
    }
}
