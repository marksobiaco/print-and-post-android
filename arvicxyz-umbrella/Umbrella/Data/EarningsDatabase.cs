﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class EarningsDatabase
    {
        readonly SQLiteAsyncConnection database;
        public EarningsDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<MonthlyEarnings>().Wait();
        }   
        public Task ResetMonthlyEarnings()
        {
            return database.DropTableAsync<MonthlyEarnings>();
        }
        public Task<List<MonthlyEarnings>> GetItemsAsync()
        {
            return database.Table<MonthlyEarnings>().ToListAsync();
        }
        public Task<int> SaveMonthlyEarningsAsync(List<MonthlyEarnings> MonthlyEarnings)
        {
            return database.InsertAllAsync(MonthlyEarnings);
        }
        public Task<int> SaveSingleMonthlyEarningsAsync(MonthlyEarnings MonthlyEarnings)
        {
            database.DropTableAsync<MonthlyEarnings>().Wait();
            database.CreateTableAsync<MonthlyEarnings>().Wait();
            return database.InsertAsync(MonthlyEarnings);
        }
        public Task<List<MonthlyEarnings>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<MonthlyEarnings>($"SELECT * FROM [MonthlyEarnings]");
        }
    }
}
