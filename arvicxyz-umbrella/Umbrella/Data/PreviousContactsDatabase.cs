﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Models;

namespace Umbrella.Data
{
    public class PreviousContactsDatabase
    {
        readonly SQLiteAsyncConnection database;
        public PreviousContactsDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<PhoneContact>().Wait();
        }
        public Task ResetPhoneContacts()
        {
            return database.DropTableAsync<PhoneContact>();
        }
        public Task<List<PhoneContact>> GetItemsAsync()
        {
            return database.Table<PhoneContact>().ToListAsync();
        }
        public Task<int> SavePhoneContactsAsync(List<PhoneContact> PhoneContacts)
        {

            return database.InsertAllAsync(PhoneContacts);
        }
        public Task<int> SaveItemAsync(PhoneContact item)
        {
            return database.InsertAsync(item);
        }
        public Task<int> UpdateItemAsync(PhoneContact item)
        {
            return database.UpdateAsync(item);
        }
        public Task<List<PhoneContact>> GetItemsByCategoryAsync()
        {

            return database.QueryAsync<PhoneContact>($"SELECT * FROM [PrevPhoneContact]");
        }
    }
}
