﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Umbrella.Constants;
using Umbrella.Enums;
using Umbrella.Interfaces;
using Umbrella.Models;
using Xamarin.Forms;

namespace Umbrella.Services
{
    public class ReferralsService
    {
        public async Task<bool> PostTagRegistration(string contactId)
        {
            using (HttpClient client = new HttpClient())
            {
                //string xmlData = "<contact id='" + contactId + "'><tag>MMS Print & Post Micro Account</tag></contact>";

                var requestContent = string.Format("objectID={0}&add_list={1}&ids={2}",
                        Uri.EscapeDataString("0"),
                        Uri.EscapeDataString("615"),
                        Uri.EscapeDataString(contactId));

                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var response = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_TAGS, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));
                System.Diagnostics.Debug.WriteLine("response " + response.Content.ToString());
                return response.IsSuccessStatusCode;
            }
        }
        public async Task<bool> SubmitOntraportContactApi(OntraportContact contact)
        {
            using (HttpClient client = new HttpClient())
            {
                var userProfile = contact;
                var content = JsonConvert.SerializeObject(userProfile, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                return response.IsSuccessStatusCode;
            }
        }
        public List<Referral> GetEarningsData()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var client = new RestSharp.RestClient($"{UmbrellaApi.SCHEME}://{UmbrellaApi.API_HOST_URL}/referred-and-earned/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("owner_partner_id", credential.UserID);
            request.AddParameter("action", "get");
            request.AddParameter("populate_website", "ssc");
            request.AddParameter("type", "affiliate");
            request.AddHeader("umbrella-api-username", UmbrellaApi.ARMY_USERNAME);
            request.AddHeader("umbrella-api-key", UmbrellaApi.ARMY_PASSKEY);
            request.AddHeader("umbrella-partner-id", UmbrellaApi.ARMY_PARTNER_ID);

            List<Referral> convert = null;
            EventWaitHandle Wait = new AutoResetEvent(false);

            var asyncHandle = client.ExecuteAsync<List<Referral>>(request, response => {
                System.Diagnostics.Debug.WriteLine("Earnings Data " + response.Content);
                try
                {
                    var json = response.Content.Replace("<", "");
                    JObject jo = JObject.Parse(json);
                    var diskSpaceArray = jo.SelectToken("data", false).ToString();
                    convert = JsonConvert.DeserializeObject<List<Referral>>(diskSpaceArray);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Earnings Error : " + e.ToString());
                    convert = new List<Referral>();
                }

                Wait.Set();
            });
            Wait.WaitOne();
            return convert;
        }
    }
}
