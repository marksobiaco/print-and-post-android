﻿using System.Collections.Generic;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Umbrella.Views;
using Xamarin.Forms;

namespace Umbrella.Services
{
    public class SettingsService
    {
        public List<SettingsItem> GetSettingsItems()
        {
            var list = new List<SettingsItem>();
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com" || accnt.User.Username == "rhoward@raptorsms.com")
            {
                list.Add(new SettingsItem
                {
                    Icon = "notifications_icon",
                    Title = AppResources.SettingsNotificationHeader,
                    Description = AppResources.SettingsNotificationDesc,
                    Target = typeof(NotificationsPage)
                });

                list.Add(new SettingsItem
                {
                    Icon = "terms_and_conditions_icon",
                    Title = AppResources.SettingsTermsHeader,
                    Description = AppResources.SettingsTermsDesc,
                    Target = typeof(PDFPage)
                });

                list.Add(new SettingsItem
                {
                    Icon = "privacy_policy_icon",
                    Title = AppResources.SettingsPrivacyHeader,
                    Description = AppResources.SettingsPrivacyDesc,
                    Target = typeof(PDFTutorialPage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "defaultset",
                    Title = AppResources.SettingsDefaultsHeader,
                    Description = AppResources.SettingsDefaultsDesc,
                    Target = typeof(DefaultSelectionPage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "signred",
                    Title = AppResources.SettingsRatesHeader,
                    Description = AppResources.SettingsRatesDesc,
                    Target = typeof(HomePage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "contact_us_icon",
                    Title = AppResources.SettingsContactHeader,
                    Description = AppResources.SettingsContactDesc,
                    Target = typeof(ContactUsPage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "redqr",
                    Title = "QR Code Reader for Printer",
                    Description = "Scan the QR code on the front of the Printer.",
                    Target = typeof(QRCodeReaderPage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "redqr",
                    Title = "QR Code Reader for Printer Scan",
                    Description = "Scan the QR code on the front of the Printer.",
                    Target = typeof(DonePage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "logout_icon",
                    Title = AppResources.SettingsLogoutHeader,
                    Description = AppResources.SettingsLogoutDesc,
                    Target = null
                });
            }
            else
            {
                list.Add(new SettingsItem
                {
                    Icon = "notifications_icon",
                    Title = "Notifications",
                    Description = "Set Umbrella app notification settings",
                    Target = typeof(NotificationsPage)
                });

                list.Add(new SettingsItem
                {
                    Icon = "terms_and_conditions_icon",
                    Title = "Terms & Conditions",
                    Description = "Read this apps terms and conditions",
                    Target = typeof(TermsAndConditionsPage)
                });

                list.Add(new SettingsItem
                {
                    Icon = "privacy_policy_icon",
                    Title = "Privacy Policy",
                    Description = "Read this apps privacy policy",
                    Target = typeof(PrivacyPolicyPage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "defaultset",
                    Title = "Print & Post Defaults",
                    Description = "Set Default Selection in Print & Post",
                    Target = typeof(DefaultSelectionPage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "signred",
                    Title = "Print & Post Rates",
                    Description = "Full list of Print & Post Charges",
                    Target = typeof(HomePage)
                });
                list.Add(new SettingsItem
                {
                    Icon = "contact_us_icon",
                    Title = "Contact Us",
                    Description = "Contact Umbrella Business Support",
                    Target = typeof(ContactUsPage)
                });
                /*
                list.Add(new SettingsItem
                {
                    Icon = "redqr",
                    Title = "QR Code Reader for Printer",
                    Description = "Scan the QR code on the front of the Printer.",
                    Target = typeof(QRCodeReaderPage)
                });
                */
                list.Add(new SettingsItem
                {
                    Icon = "logout_icon",
                    Title = "Logout",
                    Description = "Logout current user and clear cache",
                    Target = null
                });
            }
           
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                foreach (var item in list)
                {
                    item.DescriptionFontSize = 12;
                    item.TitleFontSize = 16;
                }
            }
            else if (sizeChecker > 700)
            {
                foreach (var item in list)
                {
                    item.DescriptionFontSize = 15;
                    item.TitleFontSize = 18;
                }
            }
            else
            {
                foreach (var item in list)
                {
                    item.DescriptionFontSize = 12;
                    item.TitleFontSize = 16;
                }
            }
            return list;
        }
    }
}
