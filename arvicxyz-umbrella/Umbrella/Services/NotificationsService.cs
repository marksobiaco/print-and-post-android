﻿using System.Collections.Generic;
using Umbrella.Interfaces;
using Umbrella.Models;
using Umbrella.Resx;
using Xamarin.Forms;

namespace Umbrella.Services
{
    public class NotificationsService
    {
        public List<NotificationsItem> GetNotificationsItems()
        {
            var list = new List<NotificationsItem>();
            var defNotification = DependencyService.Get<ICredentialRetriever>().GetDefaultNotification();
            var accnt = DependencyService.Get<ICredentialRetriever>().GetCredential();
            if (accnt.User.Username == "juju@raptorsms.com")
            {
                list.Add(new NotificationsItem
                {
                    Title = AppResources.NotificationGeneral,
                    Description = AppResources.NotificationGeneralDesc,
                    IsToggled = defNotification.GeneralNotif == "Oui" || defNotification.GeneralNotif == "Yes" ? true : false,
                    ToogleFor = "general"
                });

                list.Add(new NotificationsItem
                {
                    Title = AppResources.NotificationAlerts,
                    Description = AppResources.NotificationAlertsDesc,
                    IsToggled = defNotification.NewLeadNotif == "Oui" || defNotification.NewLeadNotif == "Yes" ? true : false,
                    ToogleFor = "signed_alerts"

                });

                list.Add(new NotificationsItem
                {
                    Title = AppResources.NotificationDelivery,
                    Description = AppResources.NotificationDeliveryDesc,
                    IsToggled = defNotification.NewMessageNotif == "Oui" || defNotification.NewMessageNotif == "Yes" ? true : false,
                    ToogleFor = "special_delivery"
                });

                list.Add(new NotificationsItem
                {
                    Title = AppResources.NotificationRefer,
                    Description = AppResources.NotificationReferDesc,
                    IsToggled = defNotification.LeadConvertedNotif == "Oui" || defNotification.LeadConvertedNotif == "Yes" ? true : false,
                    ToogleFor = "refer_earn_updates"
                });

            }
            else
            {
                list.Add(new NotificationsItem
                {
                    Title = "General",
                    Description = "General Notifications, Managed Mail Service News, Balance Alerts etc",
                    IsToggled = defNotification.GeneralNotif == "Yes" || defNotification.GeneralNotif == "Oui" ? true : false,
                    ToogleFor = "general"
                });

                list.Add(new NotificationsItem
                {
                    Title = "Signed For Alerts",
                    Description = "Receive ongoing notifications for items sent via Signed For Delivery..",
                    IsToggled = defNotification.NewLeadNotif == "Yes" || defNotification.NewLeadNotif == "Oui" ? true : false,
                    ToogleFor = "signed_alerts"

                });

                list.Add(new NotificationsItem
                {
                    Title = "Special Delivery Alerts",
                    Description = "Receive ongoing notifications for items sent via Special Delivery.",
                    IsToggled = defNotification.NewMessageNotif == "Yes" || defNotification.NewMessageNotif == "Oui" ? true : false,
                    ToogleFor = "special_delivery"
                });

                list.Add(new NotificationsItem
                {
                    Title = "Refer & Earn Updates",
                    Description = "Receive real time alerts when you have earned FREE credits from those you have referred.",
                    IsToggled = defNotification.LeadConvertedNotif == "Yes" || defNotification.LeadConvertedNotif == "Oui" ? true : false,
                    ToogleFor = "refer_earn_updates"
                });

            }
            var sizeChecker = DependencyService.Get<IScreenSizeRetriever>().GetHeight();

            if (sizeChecker >= 640 && sizeChecker <= 695)
            {
                foreach (var item in list)
                {
                    item.DescriptionFontSize = 12;
                    item.TitleFontSize = 16;
                }
            }
            else if (sizeChecker > 700)
            {
                foreach (var item in list)
                {
                    item.DescriptionFontSize = 15;
                    item.TitleFontSize = 18;
                }
            }
            else
            {
                foreach (var item in list)
                {
                    item.DescriptionFontSize = 12;
                    item.TitleFontSize = 16;
                }
            }
            return list;
        }
    }
}
