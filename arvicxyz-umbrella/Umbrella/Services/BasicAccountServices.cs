﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Umbrella.Constants;
using Umbrella.Interfaces;
using Umbrella.Models;
using Xamarin.Forms;

namespace Umbrella.Services
{
    public class BasicAccountServices
    {
        public async Task<bool> SyncMyPAMessage(string message, string contactId)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var requestContent = string.Format("id={0}&f1823={1}",
                                                   Uri.EscapeDataString(contactId),
                                                   Uri.EscapeDataString(message));

                var res = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));

                string xmlData2 = "<contact id='" + contactId + "'><tag>Umbrella - My PA</tag></contact>";

                var requestContent2 = string.Format("appid={0}&key={1}&return_id={2}&reqType={3}&data={4}",
                        Uri.EscapeDataString(OntraportApi.API_APP_ID),
                        Uri.EscapeDataString(OntraportApi.API_KEY),
                        Uri.EscapeDataString(OntraportApi.RETURN_ID.ToString()),
                        Uri.EscapeDataString("add_tag"),
                        Uri.EscapeDataString(xmlData2));

                var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS2, new StringContent(requestContent2, Encoding.UTF8, "application/x-www-form-urlencoded"));

                return res.IsSuccessStatusCode;
            }

        }
        public async Task<bool> ForgetPasswordService(string contactId)
        {
            using (HttpClient client = new HttpClient())
            {
                var userprofile = new AddTagOntraport()
                {
                    objectID = "0",
                    Ids = new string[]
                    {
                        contactId
                    },
                    AddLists = new string[]
                    {
                        "Print & Post App - Forgotten Password"
                    }


                };
                var content = JsonConvert.SerializeObject(userprofile, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                System.Diagnostics.Debug.WriteLine("responsess " + content);
                // var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                var response = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_TAGS_BYNAME, new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded"));
                var st = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("response " + st + " ");
                return response.IsSuccessStatusCode;


            }
        }
        public async Task<bool> SyncEnquiryTypeOntraport(int enquiryType, string contactEnquiry, string email)
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            using (HttpClient client = new HttpClient())
            {
                var requestContent = string.Format("id={0}&email={1}&f1653={2}&f1572={3}",
                         Uri.EscapeDataString(credential.UserID),
                         Uri.EscapeDataString(email),
                         Uri.EscapeDataString(enquiryType.ToString()),
                         Uri.EscapeDataString(contactEnquiry));
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                var res = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));
                if (res.IsSuccessStatusCode)
                {
                    SendEnquiryTag();
                    return res.IsSuccessStatusCode;
                }
                return res.IsSuccessStatusCode;
            }

        }
        public async Task<bool> CreateUserOntraport(string email)
        {
            using (HttpClient client = new HttpClient())
            {
                string xmlData = "<contact><Group_Tag name='Contact Information'><field name='Email'>" + email + "</field></Group_Tag></contact>";
                var requestContent = string.Format("appid={0}&key={1}&return_id={2}&reqType={3}&data={4}",
                       Uri.EscapeDataString(OntraportApi.API_APP_ID),
                       Uri.EscapeDataString(OntraportApi.API_KEY),
                       Uri.EscapeDataString(OntraportApi.RETURN_ID.ToString()),
                       Uri.EscapeDataString("add"),
                       Uri.EscapeDataString(xmlData));
                var res = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS2, new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded"));

                return res.IsSuccessStatusCode;
            }

        }
        private async void SendEnquiryTag()
        {
            var credential = DependencyService.Get<ICredentialRetriever>().GetCredential();
            var contact_id = await DependencyService.Get<IOntraportContactIdRetriever>().GetOntraportContactId(credential.User.Email);
            using (HttpClient client = new HttpClient())
            {
                var userprofile = new AddTagOntraport()
                {
                    objectID = "0",
                    Ids = new string[]
                   {
                        contact_id
                   },
                    AddLists = new string[]
                   {
                        "Umbrella App - Contact Form"
                   }


                };
                var content = JsonConvert.SerializeObject(userprofile, new JsonSerializerSettings());
                client.DefaultRequestHeaders.Add("Api-Appid", OntraportApi.API_APP_ID);
                client.DefaultRequestHeaders.Add("Api-Key", OntraportApi.API_KEY);
                System.Diagnostics.Debug.WriteLine("responsess " + content);
                // var response = await client.PostAsync(OntraportApi.ONTRAPORT_API_LINK_CONTACTS, new StringContent(content, Encoding.UTF8, "application/json"));
                var response = await client.PutAsync(OntraportApi.ONTRAPORT_API_LINK_TAGS_BYNAME, new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded"));
                var st = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine("response " + st + " ");
            }
        }
    }
}
