﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbrella.ViewModels
{
    public class ScheduleTimeViewModel : INotifyPropertyChanged
    {
        private int _position;
        public int Position
        {
            get
            {
                return _position;
            }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    NotifyPropertyChanged("Position");
                }
            }
        }
        private string _summaryText;
        public string SummaryText
        {
            get
            {
                return _summaryText;
            }
            set
            {
                if (_summaryText != value)
                {
                    _summaryText = value;
                    NotifyPropertyChanged("SummaryText");
                }
            }
        }
        public ObservableCollection<int> TimeSchedList { get; set; }
        public ScheduleTimeViewModel()
        {
            TimeSchedList = new ObservableCollection<int>()
            {
                1,2,3
            };
            _position = 1;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
